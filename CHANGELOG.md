# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.9.28 - 2024-12-04(16:42:42 +0000)

### Other

- Ping function doesn't work when the ProtocolVersion is  “IPv4”  or “IPv6”

## Release v0.9.27 - 2024-11-26(14:21:14 +0000)

### Other

- - [Security]Command injection: remove system() call in tr181-ipdiagnostics

## Release v0.9.26 - 2024-10-01(15:43:25 +0000)

### Other

- Parameters under IP.Diagnostics.IPPing. are not updated after successful ping test

## Release v0.9.25 - 2024-09-10(07:14:04 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v0.9.24 - 2024-08-30(11:44:43 +0000)

### Other

- - [TRACEROUTE] RouteHops entries are not added when DataBlocksize value is less than 46 for IPv4 and 72 for IPv6

## Release v0.9.23 - 2024-08-30(10:42:46 +0000)

### Other

- Parameters under IP.Diagnostics.IPPing. are not updated after successful ping test

## Release v0.9.22 - 2024-08-22(08:15:29 +0000)

### Other

- - Command injection in tr181-ipdiagnostics

## Release v0.9.21 - 2024-08-02(12:41:34 +0000)

### Other

- [IP Diagnostics] Speedtest fails to launch

## Release v0.9.20 - 2024-07-30(14:13:56 +0000)

### Other

- - [IP diagnostics] Add ability to disable download/upload diagnostics support

## Release v0.9.19 - 2024-07-29(11:09:56 +0000)

### Other

- - [TRACEROUTE] RouteHops entries are not added when DataBlocksize value is less than 46 for IPv4 and 72 for IPv6
- - [CDROUTER][TR069] Device.IP.Diagnostics.TraceRoute seems using UDP by default iso ICMP

## Release v0.9.18 - 2024-07-23(07:54:10 +0000)

### Fixes

- Better shutdown script

## Release v0.9.17 - 2024-06-07(08:12:19 +0000)

### Other

- - [CHR2fA][Diagnostics] Device.IP.Diagnostics.DownloadDiagnostics.DiagnosticsState and Device.IP.Diagnostics.UploadDiagnostics.DiagnosticsState could not be launched when setting all parameters together

## Release v0.9.16 - 2024-05-28(08:09:44 +0000)

### Other

- - [prpl][tr181-ipdiagnostics] Add unit test for mod-ipd-ftx

## Release v0.9.15 - 2024-05-22(08:01:25 +0000)

### Fixes

- - Device.IP.Diagnostics.DownloadDiagnostics.DiagnosticsState and Device.IP.Diagnostics.UploadDiagnostics.DiagnosticsState could not be launched when setting all parameters together

## Release v0.9.14 - 2024-05-11(07:10:53 +0000)

### Fixes

- [tr181-ipdiagnostics] [IPPING] Wrong parameter "Status" should have been "DiagnosticState" reopened

## Release v0.9.13 - 2024-05-07(09:39:58 +0000)

### Fixes

- [tr181-ipdiagnostics] [IPPING] Wrong parameter "Status" should have been "DiagnosticState"

## Release v0.9.12 - 2024-04-29(09:28:56 +0000)

### Other

- - [internal][CI][tr181-ipdiagnostics] Plugin doesn't compile

## Release v0.9.11 - 2024-04-26(11:16:28 +0000)

### Other

- ci: disable debian package compilation

## Release v0.9.10 - 2024-04-24(15:36:35 +0000)

### Changes

- - [tr181-ipdiagnostics][speedtest]ACS does not allow setting Download Diagnostics parameters in a single SetParameterValues RPC after DUT reset_hard

## Release v0.9.9 - 2024-04-22(09:38:02 +0000)

### Fixes

- [tr181-ipdiagnostics][Ping] Changed 'DiagnosticState' to 'Status'

## Release v0.9.8 - 2024-04-18(15:05:54 +0000)

### Changes

- - [prpl][libfiletransfer] Handle concurrent transfer

## Release v0.9.7 - 2024-04-15(13:31:12 +0000)

### Fixes

- [CHR2fA][HDM][TraceRoute] Device.IP.Diagnostics.TraceRoute.DataBlockSize could not be set to 38

## Release v0.9.6 - 2024-04-10(07:09:05 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.9.5 - 2024-04-05(09:56:34 +0000)

### Fixes

- Issue HOP-5945: [IP-PING] [USP] "Cancelled" in DiagnosticsState instead of "Complete" when using USP

## Release v0.9.4 - 2024-03-29(14:49:19 +0000)

### Fixes

- Issue HOP-6216: [CHR2fA][IPPing] 'Error_Other' instead of 'Error_CannotResolveHostName' in case of Invalid Hostname

## Release v0.9.3 - 2024-03-21(10:39:47 +0000)

### Other

- [tr181-ipdiagnostics] [TraceRoute] Occasional, 'Not Complete' in DiagnosticsStateTraceroute Instead of 'Complete'

## Release v0.9.2 - 2024-03-19(14:25:57 +0000)

### Other

- Rework TR-181 interface stacks indexing number

## Release v0.9.1 - 2024-03-19(08:26:32 +0000)

### Fixes

- [IPDiagnostics] Traceroute Test Not started after Request

## Release v0.9.0 - 2024-03-15(16:02:12 +0000)

### New

- - [TR181-ip diagnostics] [TR143] Support for HTTP and FTP based download/upload diagnostics

## Release v0.8.3 - 2024-03-04(12:03:02 +0000)

### Fixes

- [IP Ping] FailureCount does not match the number of pings requested with no Response

## Release v0.8.2 - 2024-02-26(09:47:00 +0000)

### Fixes

- [SpeedTest][Iperf3] Issue with generated iperf script when using the RPC

## Release v0.8.1 - 2024-02-06(14:44:07 +0000)

### Fixes

- [tr181-ipdiagnostics] [TraceRoute] TraceRoute Connat handle values under 1000 milliseconds without an error.

## Release v0.8.0 - 2024-02-05(16:21:03 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.7.1 - 2024-01-24(16:11:22 +0000)

### Other

- [dmproxy] Some parameter paths are not well shown in the dm proxy

## Release v0.7.0 - 2024-01-17(09:32:51 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.6.5 - 2024-01-16(13:44:31 +0000)

### Fixes

- [IP-PING][IPv6] "Error_Other" in DiagnosticsState when wrong interface is used.

## Release v0.6.4 - 2024-01-11(09:55:18 +0000)

### Fixes

- [IP-PING] [WNC-CHR2] "Error_Other" in DiagnosticsState when an unknown Host is used.

## Release v0.6.3 - 2023-12-22(08:54:39 +0000)

### Fixes

- [tr181-IPDIagnostics] The default timeout of the traceroute tool is in seconds instead of miliseconds.

## Release v0.6.2 - 2023-12-13(16:29:02 +0000)

### Fixes

- [Speedtest][tr181-ipdiagnostics] The files created by the plugin must be located in the /tmp folder

## Release v0.6.1 - 2023-11-27(10:44:49 +0000)

### Fixes

- IPDiagnostics.DownloadDiagnostics() API not working

## Release v0.6.0 - 2023-10-16(10:27:08 +0000)

### New

- [amxrt][no-root-user][capability drop] All amx plugins must be adapted to run as non-root and lmited capabilities

## Release v0.5.1 - 2023-09-28(07:52:57 +0000)

### Other

- wrong placement of the ping and the traceroute tool in tr181-ipdiagnostics

## Release v0.5.0 - 2023-08-21(12:36:19 +0000)

### New

- Add support for tracroute feature (USP + CWMP)

## Release v0.4.2 - 2023-08-11(08:42:43 +0000)

### Fixes

- IP Diagnostics some data type parameters need to be changed to unsigned int 32 bit.

## Release v0.4.1 - 2023-07-13(07:28:26 +0000)

### Fixes

- [USP] GSDM should return whether commands are (a)sync

## Release v0.4.0 - 2023-06-29(09:28:23 +0000)

### New

- Add support for ipping feature (USP + CWMP)

## Release v0.3.2 - 2023-06-20(07:26:52 +0000)

### Other

- Opensource component

## Release v0.3.1 - 2023-03-30(06:58:17 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v0.3.0 - 2023-01-05(16:36:24 +0000)

### New

- [import-dbg] Disable import-dbg by default for all amxrt plugin

## Release v0.2.2 - 2022-09-06(06:45:02 +0000)

### Other

- [IPDiagnostics]Document the ip.diagnostics (iperf) speedtest implementation

## Release v0.2.1 - 2022-08-19(11:54:28 +0000)

## Release v0.2.0 - 2022-08-18(06:47:07 +0000)

### New

- [Speedtest][IPDiagnostics] iperf must be supported as speedtest implementation

## Release v0.1.0 - 2022-06-15(11:21:24 +0000)

### New

- [Speedtest][IPDiagnostics] Development of a Speedtest/IP.Diagnostics amx plugin

