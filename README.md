# tr181-ipdiagnostics

The tr181-ipdiagnostics works based on the separation of concerns, providing diagnostics tests using the
following tools:
- Iperf

Those are the tests and the modules that implement them:
- Download speedtest (iperf)
- Upload speedtest (iperf)

## compile and install
```
make
sudo make install
```

## run unit tests
```
make test
```

The test uses test data from the process(iperf) output and tests the parsing, and not the module actually
running on background. If tests start to fail because of a change, then `gdb` is a good tool. The tests are
organized per topic (DownloadDiagnostics, UploadDiagnostics, common functions...) and a breakpoint can
be added at the topic's event handler to start debugging.
```
cd test/path/to/test
gdb ./run_test
```

## run integration tests

To test using a modules' process in background and test the real behavior of the program, do the following:
```
cd test
make int
```

In order to run the integration tests, the container must have Iperf3 (version 3.7) installed.

## run plugin inside a Ambiorix Docker container

Refer to the Ambiorix tutorials for the steps to launch the Ambiorix Docker container.

### compile/install dependencies: (make + sudo make install)

tr181-ipdiagnostics:
  libnetmodel
  libsahtrace

netmodel:
  mod_dmext

iperf3:
  libiperf0
  libsctp1

### run dependencies
```
# start bus
sudo ubusd&

# start dependencies
sudo ip-manager -D
sudo netmodel -D
```

### run tr181-ipdiagnostics
```
sudo tr181-ipdiagnostics -D
```

### tips & tricks

You can run a local iperf server in order to perform the Upload and Download tests.
```
iperf3 -s -D
```
