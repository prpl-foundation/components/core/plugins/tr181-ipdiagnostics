/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__IP_DIAGNOSTICS_DOWNLOAD_H__)
#define __IP_DIAGNOSTICS_DOWNLOAD_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "upload_download.h"

//functions
#define IPD_DOWNLOAD_TEST_START "start-download"
#define IPD_DOWNLOAD_TEST_CHECK "check-download"
#define IPD_DOWNLOAD_TEST_STOP  "stop-download"
#define IPD_DOWNLOAD_TEST_ABORT "abort-download"

/*Config keys*/
#define DOWN_CFG_KEY_DOWNLOAD_URL           "DownloadURL"
//Extra variables for the return variant
#define DOWN_CFG_KEY_SIZE_OF_TEST           "SizeOfTest"

/*Download return keys*/
#define DOWN_RET_KEY_TEST_RCVD          "TestBytesReceived"
#define DOWN_RET_KEY_TEST_RCVD_FULL     "TestBytesReceivedUnderFullLoading"
//PerConnectionResult object
#define DOWN_RET_KEY_PER_CONN_TEST_RCVD     "TestBytesReceived"
//IncrementalResult object
#define DOWN_RET_KEY_INCR_RES_TEST_RCVD  "TestBytesReceived"

#define DOWN_STATUS_ERR_TRANSFER    "Error_TransferFailed"
#define DOWN_STATUS_ERR_WRONG_SIZE  "Error_IncorrectSize"

#define DOWNLOAD_TEST_STATUS(status) \
    str_down_test_status[status]

typedef enum _download_test_status_ {
    down_test_complete,
    down_test_error_resolve_host_name,
    down_test_error_no_route_to_host,
    down_test_error_init_conn_failed,
    down_test_error_no_response,
    down_test_error_transfer_failed,
    down_test_error_pass_req_failed,
    down_test_error_login_failed,
    down_test_error_no_transfer_mode,
    down_test_error_no_pasv,
    down_test_error_incorrect_size,
    down_test_error_timeout,
    down_test_error_internal,
    down_test_error_other,
    down_test_not_complete,
    down_test_not_executed,
    download_test_num_of_status
} download_test_status_t;

extern const cstring_t str_down_test_status[download_test_num_of_status];

#ifdef __cplusplus
}
#endif

#endif // __IP_DIAGNOSTICS_DOWNLOAD_H__
