/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__TR_IPDIAGNOSTICS_PRIV_H__)
#define __TR_IPDIAGNOSTICS_PRIV_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_types.h>
#include <amxb/amxb.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>

typedef enum ip_diagnostics_status {
    ipdiag_error = -1,
    ipdiag_ok = 0
} ip_diagnostics_status_t;

int _ip_diagnostics_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser);

amxd_status_t _Reset(amxd_object_t* object,
                     amxd_function_t* func,
                     amxc_var_t* args,
                     amxc_var_t* ret);

amxd_status_t _interface_destroy(amxd_object_t* intf,
                                 amxd_param_t* param,
                                 amxd_action_t reason,
                                 const amxc_var_t* const args,
                                 amxc_var_t* const retval,
                                 void* priv);

amxd_status_t _lastchange_read(amxd_object_t* intf,
                               amxd_param_t* param, amxd_action_t reason,
                               const amxc_var_t* const args,
                               amxc_var_t* const retval, void* priv);

int ipd_init(amxd_dm_t* dm, amxo_parser_t* parser);
void ipd_shutdown(void);
amxd_dm_t* ipd_get_dm(void);
amxo_parser_t* ipd_get_parser(void);
amxb_bus_ctx_t* ipd_get_ctx(void);
amxb_bus_ctx_t* ip_manager_get_ctx(void);
amxd_object_t* ipd_get_root_object(void);
amxd_object_t* ipd_get_ipping_object(void);
amxd_object_t* ipd_get_trcr_object(void);
cstring_t ipd_get_prefix(void);

amxd_trans_t* ipd_transaction_create(const amxd_object_t* const object);
amxd_status_t ipd_transaction_apply(amxd_trans_t* trans);

bool ipd_search_llist(cstring_t string, amxc_llist_t* llist);
void ipd_obj_data_to_var(amxd_object_t* object, amxc_llist_t* filter_list, amxc_var_t* var);
void ipd_var_to_dm(amxd_trans_t* transaction, const char* sub_object,
                   amxc_var_t* values, amxc_llist_t* filter_list);
amxc_var_t* ipd_fetch_interface_ip(amxc_var_t* interface, amxc_var_t* protocol_version);
int ipd_validate_cfg_params(amxc_var_t* input_args, amxd_object_t* cfg_obj, amxc_var_t* params, const cstring_t tool);
int ipd_get_current_time(amxc_var_t* var);
amxd_status_t ipd_resolve_ip_interface(amxc_var_t* conf_args);
amxd_status_t ipd_set_non_mandatory_def_value(amxc_var_t* params, const cstring_t key);
int32_t ipd_tool_get_dm_fd(const cstring_t tool);
void ipd_filter_ret(amxc_var_t* mod_ret, bool return_var, const cstring_t tool);

/* Debugging Event Printer */
void _print_event(const char* const sig_name,
                  const amxc_var_t* const data,
                  void* const priv);

/* Debugging Event Logger */
void _syslog_event(const char* const sig_name,
                   const amxc_var_t* const data,
                   void* const priv);

char* component_get_path_instance(amxb_bus_ctx_t* bus,
                                  const char* query);

#ifdef __cplusplus
}
#endif

#endif // __TR_IPDIAGNOSTICS_PRIV_H__
