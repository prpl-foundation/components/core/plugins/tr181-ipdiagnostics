/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__IP_DIAGNOSTICS_IPPING_H__)
#define __IP_DIAGNOSTICS_IPPING_H__

#ifdef __cplusplus
extern "C"
{
#endif

// IPUtils specific
#define IPPING_IPUTILS       "iputils"

//functions
#define IPD_IPPING_TEST_START "start-ping"
#define IPD_IPPING_TEST_CHECK "check-ping"
#define IPD_IPPING_TEST_STOP  "stop-ping"
#define IPD_IPPING_TEST_ABORT "abort-ping"

//Process object
#define IPPING_RET_KEY_PROC                "Process"
#define IPPING_RET_KEY_PROC_PID            "ID"
#define IPPING_RET_KEY_PROC_FD_STDOUT      "FDStdOut"
#define IPPING_RET_KEY_PROC_FD_STDERR      "FDStdErr"

//Config object
#define IPPING_RET_KEY_CFG                  "Config"
#define IPPING_RET_KEY_PRTCLV               "ProtocolVersion"
#define IPPING_RET_KEY_NAME                 "Name"
#define IPPING_RET_KEY_NMBRP                "NumberOfRepetitions"
#define IPPING_RET_KEY_TMT                  "Timeout"
#define IPPING_RET_KEY_HST                  "Host"
#define IPPING_RET_KEY_DTBS                 "DataBlockSize"
#define IPPING_RET_KEY_DSCP                 "DSCP"
#define IPPING_RET_KEY_DST                  "DiagnosticsState"
#define IPPING_RET_KEY_PST                  "PingState"
#define IPPING_RET_KEY_MIN                  "MinimumResponseTime"
#define IPPING_RET_KEY_MIND                 "MinimumResponseTimeDetailed"
#define IPPING_RET_KEY_AVG                  "AverageResponseTime"
#define IPPING_RET_KEY_AVGD                 "AverageResponseTimeDetailed"
#define IPPING_RET_KEY_MAX                  "MaximumResponseTime"
#define IPPING_RET_KEY_MAXD                 "MaximumResponseTimeDetailed"
#define IPPING_RET_KEY_SUCC                 "SuccessCount"
#define IPPING_RET_KEY_FC                   "FailureCount"
#define IPPING_RET_KEY_CMD                  "Command"
#define IPPING_RET_KEY_INTF                 "Interface"
#define IPPING_RET_KEY_CTRL                 "Controller"
#define IPPING_RET_KEY_IPUSD                "IPAddressUsed"

#define IPPING_STATUS_NONE             "None"
#define IPPING_STATUS_COMPLETE         "Complete"
#define IPPING_STATUS_REQUESTED        "Requested"
#define IPPING_STATUS_CANCELED         "Canceled"
#define IPPING_STATUS_ERROR            "Error"
#define IPPING_STATUS_ERR_HOST_NAME    "Error_CannotResolveHostName"
#define IPPING_STATUS_ERR_ROUTE        "Error_NoRouteToHost"
#define IPPING_STATUS_ERR_INTERNAL     "Error_Internal"
#define IPPING_STATUS_ERR_OTHER        "Error_Other"
#define IPPING_STATUS_NOT_COMPLETE     "Not_Complete"

typedef enum _ipping_test_status_ {
    ipping_test_none,
    ipping_test_error_complete,
    ipping_test_error_requested,
    ipping_test_error_canceled,
    ipping_test_error_error,
    ipping_test_error_err_host_name,
    ipping_test_error_err_route,
    ipping_test_error_err_internal,
    ipping_test_error_err_other,
    ipping_test_not_complete,
    ipping_test_num_of_status
} ipping_test_status_t;

#ifdef __cplusplus
}
#endif

#endif // __IP_DIAGNOSTICS_IPPING_H__