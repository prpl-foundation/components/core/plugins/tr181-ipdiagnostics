/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__TR_IPDIAGNOSTICS_PRIV_IPPING_H__)
#define __TR_IPDIAGNOSTICS_PRIV_IPPING_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_function.h>

#define IPD_IPPING_CONFIG "IPPing"

typedef struct _ipd_ipping_scheduler {
    int32_t fd_stdout;
    int32_t fd_stderr;
    int32_t pid;
    uint64_t call_id;
    bool prev_test;
} ipd_ipping_scheduler_t;

amxd_status_t _IPPing(amxd_object_t* object, amxd_function_t* func, amxc_var_t* args, amxc_var_t* ret);
void _ipd_ipping_check_dst(const char* const event_name,
                           const amxc_var_t* const event_data,
                           void* const priv);
amxd_status_t ipd_ipping_start(void (* check_function)(int, void*),
                               ipd_ipping_scheduler_t* scheduler,
                               amxc_var_t* args, amxc_var_t* ret);
amxd_status_t ipd_ipping_stop(ipd_ipping_scheduler_t* sch);
void ipd_ipping_check(int fd, void* priv);
void ipd_ipping_sched_check(int fd, void* priv, ipd_ipping_scheduler_t* scheduler);
amxd_status_t ipd_ipping_save_result_to_dm(amxc_var_t* ret, amxd_object_t* ipd_ipping);
amxd_status_t ipd_ipping_save_status_to_dm(amxc_var_t* ret, amxd_object_t* ipd_ipping);
bool ipd_ipping_is_iputils(void);

#ifdef __cplusplus
}
#endif

#endif // __TR_IPDIAGNOSTICS_PRIV_IPPING_H__