/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__TR_IPDIAGNOSTICS_PRIV_UP_DOWN_H__)
#define __TR_IPDIAGNOSTICS_PRIV_UP_DOWN_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "upload_download.h"

#define IPD_MAX_UP_DOWN_RESULTS 5

/*Variant specific key*/
#define IPD_UP_DOWN_RESULT_URI       "ResultURI"

/*Internal definitions*/
#define IPD_UP_DOWN_DOWNLOAD_PREFIX "Download"
#define IPD_UP_DOWN_UPLOAD_PREFIX   "Upload"
//tests
#define IPD_UP_DOWN_DOWNLOAD "download"
#define IPD_UP_DOWN_UPLOAD   "upload"
//functions
#define IPD_UP_DOWN_TEST_START "start"
#define IPD_UP_DOWN_TEST_CHECK "check"
#define IPD_UP_DOWN_TEST_STOP  "stop"
#define IPD_UP_DOWN_TEST_ABORT "abort"

/*IPDiagnostics parameters*/
#define IPD_PREFIX                  "Prefix"
#define IPD_UP_DOWN_MAX_CONNECTIONS "DiagnosticsMaxConnections"
#define IPD_UP_DOWN_MAX_INCR_RESULT "DiagnosticsMaxIncrementalResult"
#define IPD_NUM_UP_DOWN_RESULT_1    "NumberOf"
#define IPD_NUM_UP_DOWN_RESULT_2    "Result"
#define IPD_UP_DOWN_CONFIG          "Config"
#define IPD_UP_DOWN_RESULT          "Result"

typedef struct _ipd_up_down_scheduler {
    uint32_t instance_index;
    int32_t fd;
} ipd_up_down_scheduler_t;

const cstring_t ipd_up_down_max_conn(ipd_up_down_test_t test);
const cstring_t ipd_up_down_max_incr_res(ipd_up_down_test_t test);
const cstring_t ipd_up_down_num_result(ipd_up_down_test_t test);
const cstring_t ipd_up_down_config(ipd_up_down_test_t test);
const cstring_t ipd_up_down_result(ipd_up_down_test_t test);
const cstring_t ipd_up_down_function(ipd_up_down_test_t test, const cstring_t function);
int ipd_up_down_save_result_to_dm(amxc_var_t* ret, uint32_t result_instance_index,
                                  amxd_object_t* ipd_results, ipd_up_down_test_t test);

void ipd_up_down_check(int fd, void* priv, ipd_up_down_test_t test, ipd_up_down_scheduler_t* sch);
amxd_status_t ipd_up_down_start(ipd_up_down_test_t test, ipd_up_down_scheduler_t* sch,
                                void (* check_function)(int, void*),
                                amxc_var_t* args, amxc_var_t* ret);

#ifdef __cplusplus
}
#endif

#endif // __TR_IPDIAGNOSTICS_PRIV_UP_DOWN_H__
