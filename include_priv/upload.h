/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__IP_DIAGNOSTICS_UPLOAD_H__)
#define __IP_DIAGNOSTICS_UPLOAD_H__

#ifdef __cplusplus
extern "C"
{
#endif

//functions
#define IPD_UPLOAD_TEST_START "start-upload"
#define IPD_UPLOAD_TEST_CHECK "check-upload"
#define IPD_UPLOAD_TEST_STOP  "stop-upload"
#define IPD_UPLOAD_TEST_ABORT "abort-upload"

/*Config keys*/
#define UP_CFG_KEY_UPLOAD_URL             "UploadURL"
#define UP_CFG_KEY_FILE_LENGTH            "TestFileLength"

/*Upload return keys*/
#define UP_RET_KEY_TEST_SENT          "TestBytesSent"
#define UP_RET_KEY_TEST_SENT_FULL     "TestBytesSentUnderFullLoading"
//PerConnectionResult object
#define UP_RET_KEY_PER_CONN_TEST_SENT     "TestBytesSent"
//IncrementalResult object
#define UP_RET_KEY_INCR_RES_TEST_SENT  "TestBytesSent"

#define UP_STATUS_ERR_NO_CWD      "Error_NoCWD"
#define UP_STATUS_ERR_NO_STOR     "Error_NoSTOR"
#define UP_STATUS_ERR_TRAN_COMP   "Error_NoTransferComplete"

#define UPLOAD_TEST_STATUS(status) \
    str_up_test_status[status]

typedef enum _upload_test_status_ {
    up_test_complete,
    up_test_error_resolve_host_name,
    up_test_error_no_route_to_host,
    up_test_error_init_conn_failed,
    up_test_error_no_response,
    up_test_error_pass_req_failed,
    up_test_error_login_failed,
    up_test_error_no_transfer_mode,
    up_test_error_no_pasv,
    up_test_error_no_cwd,
    up_test_error_no_stor,
    up_test_error_no_transfer_complete,
    up_test_error_timeout,
    up_test_error_internal,
    up_test_error_other,
    up_test_not_complete,
    up_test_not_executed,
    upload_test_num_of_status
} upload_test_status_t;

extern const cstring_t str_up_test_status[upload_test_num_of_status];

#ifdef __cplusplus
}
#endif

#endif // __IP_DIAGNOSTICS_UPLOAD_H__
