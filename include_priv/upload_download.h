/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__IP_DIAGNOSTICS_UP_DOWN_H__)
#define __IP_DIAGNOSTICS_UP_DOWN_H__

#ifdef __cplusplus
extern "C"
{
#endif

/*Internal definitions*/
typedef enum _ipd_up_down_test_ {
    ipd_up_down_download,
    ipd_up_down_upload,
    ipd_up_down_numr_of_tests
} ipd_up_down_test_t;

/*Config keys*/
#define UP_DOWN_CFG_KEY_INTERFACE              "Interface"
#define UP_DOWN_CFG_KEY_UPLOAD_URL             "UploadURL"
#define UP_DOWN_CFG_KEY_DSCP                   "DSCP"
#define UP_DOWN_CFG_KEY_ETH_PRIORITY           "EthernetPriority"
#define UP_DOWN_CFG_KEY_TIME_TEST_DURATION     "TimeBasedTestDuration"
#define UP_DOWN_CFG_KEY_TIME_TEST_MEAS_INT     "TimeBasedTestMeasurementInterval"
#define UP_DOWN_CFG_KEY_TIME_TEST_MEAS_OFFSET  "TimeBasedTestMeasurementOffset"
#define UP_DOWN_CFG_KEY_PROTOCOL_VERSION       "ProtocolVersion"
#define UP_DOWN_CFG_KEY_NUM_OF_CONNECTIONS     "NumberOfConnections"
#define UP_DOWN_CFG_KEY_EN_CONN_RESULTS        "EnablePerConnectionResults"
//Extra variables for the return variant
#define UP_DOWN_CFG_KEY_INTERFACE_IP           "InterfaceIP"
#define UP_DOWN_CFG_KEY_RESULT_NUMBER          "ResultNumber"

/*Upload return keys*/
#define UP_DOWN_RET_KEY_STATUS             "Status"
#define UP_DOWN_RET_KEY_IP_USED            "IPAddressUsed"
#define UP_DOWN_RET_KEY_ROM_TIME           "ROMTime"
#define UP_DOWN_RET_KEY_BOM_TIME           "BOMTime"
#define UP_DOWN_RET_KEY_EOM_TIME           "EOMTime"
#define UP_DOWN_RET_KEY_TOTAL_RCVD         "TotalBytesReceived"
#define UP_DOWN_RET_KEY_TOTAL_SENT         "TotalBytesSent"
#define UP_DOWN_RET_KEY_TOTAL_RCVD_FULL    "TotalBytesReceivedUnderFullLoading"
#define UP_DOWN_RET_KEY_TOTAL_SENT_FULL    "TotalBytesSentUnderFullLoading"
#define UP_DOWN_RET_KEY_PERIOD_FULL        "PeriodOfFullLoading"
#define UP_DOWN_RET_KEY_TCP_REQ_TIME       "TCPOpenRequestTime"
#define UP_DOWN_RET_KEY_TCP_RESP_TIME      "TCPOpenResponseTime"
//PerConnectionResult object
#define UP_DOWN_RET_KEY_PER_CONN_RES           "PerConnectionResult"
#define UP_DOWN_RET_KEY_PER_CONN_ROM_TIME      "ROMTime"
#define UP_DOWN_RET_KEY_PER_CONN_BOM_TIME      "BOMTime"
#define UP_DOWN_RET_KEY_PER_CONN_EOM_TIME      "EOMTime"
#define UP_DOWN_RET_KEY_PER_CONN_TOTAL_RCVD    "TotalBytesReceived"
#define UP_DOWN_RET_KEY_PER_CONN_TOTAL_SENT    "TotalBytesSent"
#define UP_DOWN_RET_KEY_PER_CONN_TCP_REQ_TIME  "TCPOpenRequestTime"
#define UP_DOWN_RET_KEY_PER_CONN_TCP_RESP_TIME "TCPOpenResponseTime"
//IncrementalResult object
#define UP_DOWN_RET_KEY_INCR_RES            "IncrementalResult"
#define UP_DOWN_RET_KEY_INCR_RES_TOTAL_RCVD "TotalBytesReceived"
#define UP_DOWN_RET_KEY_INCR_RES_TOTAL_SENT "TotalBytesSent"
#define UP_DOWN_RET_KEY_INCR_RES_START_TIME "StartTime"
#define UP_DOWN_RET_KEY_INCR_RES_END_TIME   "EndTime"
//Process object
#define UP_DOWN_RET_KEY_PROC                "Process"
#define UP_DOWN_RET_KEY_PROC_PID            "ID"
#define UP_DOWN_RET_KEY_PROC_FD_STDOUT      "FDStdOut"
#define UP_DOWN_RET_KEY_PROC_FD_STDERR      "FDStdErr"
//Config object
#define UP_DOWN_RET_KEY_CFG                 "Config"

#define UP_DOWN_STATUS_COMPLETE        "Complete"
#define UP_DOWN_STATUS_ERR_HOST_NAME   "Error_CannotResolveHostName"
#define UP_DOWN_STATUS_ERR_ROUTE       "Error_NoRouteToHost"
#define UP_DOWN_STATUS_ERR_INIT_CONN   "Error_InitConnectionFailed"
#define UP_DOWN_STATUS_ERR_NO_RESPONSE "Error_NoResponse"
#define UP_DOWN_STATUS_ERR_PASSWORD    "Error_PasswordRequestFailed"
#define UP_DOWN_STATUS_ERR_LOGIN       "Error_LoginFailed"
#define UP_DOWN_STATUS_ERR_NO_TRANSFER "Error_NoTransferMode"
#define UP_DOWN_STATUS_ERR_NO_PASV     "Error_NoPASV"
#define UP_DOWN_STATUS_ERR_TIMEOUT     "Error_Timeout"
#define UP_DOWN_STATUS_ERR_INTERNAL    "Error_Internal"
#define UP_DOWN_STATUS_ERR_OTHER       "Error_Other"
#define UP_DOWN_STATUS_NOT_COMPLETE    "Not_Complete"
#define UP_DOWN_STATUS_NOT_EXECUTED    "Not_Executed"

#ifdef __cplusplus
}
#endif

#endif // __IP_DIAGNOSTICS_UP_DOWN_H__
