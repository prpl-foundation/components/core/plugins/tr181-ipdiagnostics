include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all
	$(MAKE) -C modules/mod-ipd-iperf/src all
	$(MAKE) -C modules/mod-ipd-ftx/src all
	$(MAKE) -C modules/mod-ipd-ping/src all
	$(MAKE) -C modules/mod-ipd-traceroute/src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C modules/mod-ipd-iperf/src clean
	$(MAKE) -C modules/mod-ipd-ftx/src clean
	$(MAKE) -C modules/mod-ipd-ping/src clean
	$(MAKE) -C modules/mod-ipd-traceroute/src clean
	$(MAKE) -C test clean

install: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-ipd-iperf.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-ipd-iperf.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-ipd-ftx.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-ipd-ftx.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-ipd-ping.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-ipd-ping.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-ipd-traceroute.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-ipd-traceroute.so
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
ifeq ($(CONFIG_PRPL_IP_DIAGNOSTIC_UPLOAD_DOWNLOAD),y)
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_downdiag.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_downdiag.odl
endif
ifeq ($(CONFIG_PRPL_IP_DIAGNOSTIC_UPLOAD_DOWNLOAD),y)
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_download.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_download.odl
endif
ifeq ($(CONFIG_PRPL_IP_DIAGNOSTIC_UPLOAD_DOWNLOAD),y)
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_updiag.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_updiag.odl
endif
ifeq ($(CONFIG_PRPL_IP_DIAGNOSTIC_UPLOAD_DOWNLOAD),y)
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_upload.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_upload.odl
endif
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_ipping.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_ipping.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_traceroute.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_traceroute.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_udpecho.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_udpecho.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_iplayercapacity.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_iplayercapacity.odl
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/$(COMPONENT)_defaults
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_defaults/* $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)_defaults/
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-ip-diagnostics_mapping.odl
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(DEST)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(DEST)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/kill_process.sh $(DEST)/usr/lib/amx/scripts/kill_process.sh

package: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-ipd-iperf.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-ipd-iperf.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-ipd-ftx.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-ipd-ftx.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-ipd-ping.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-ipd-ping.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-ipd-traceroute.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-ipd-traceroute.so
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT).odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT).odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_definition.odl
ifeq ($(CONFIG_PRPL_IP_DIAGNOSTIC_UPLOAD_DOWNLOAD),y)
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_downdiag.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_downdiag.odl
endif
ifeq ($(CONFIG_PRPL_IP_DIAGNOSTIC_UPLOAD_DOWNLOAD),y)
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_download.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_download.odl
endif
ifeq ($(CONFIG_PRPL_IP_DIAGNOSTIC_UPLOAD_DOWNLOAD),y)
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_updiag.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_updiag.odl
endif
ifeq ($(CONFIG_PRPL_IP_DIAGNOSTIC_UPLOAD_DOWNLOAD),y)
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_upload.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_upload.odl
endif
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_ipping.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_ipping.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_traceroute.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_traceroute.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_udpecho.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_udpecho.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_iplayercapacity.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_iplayercapacity.odl
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/$(COMPONENT)_defaults
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_defaults/* $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)_defaults/
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-ip-diagnostics_mapping.odl
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0755 scripts/kill_process.sh $(PKGDIR)/usr/lib/amx/scripts/kill_process.sh
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(eval ODLFILES += odl/$(COMPONENT).odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C modules/mod-ipd-ping/test run
	$(MAKE) -C modules/mod-ipd-ping/test coverage
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test