/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <regex.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_ipd_common.h"

// module trace zone
#define ME "mod-ipd-comn"

/**
   @brief
   Write string to a file.

   @param[in] fname file name.
   @param[in] string string containing the information to be written.
 */
void mod_ipd_write_to_file(const char* fname, amxc_string_t* string) {
    SAH_TRACEZ_IN(ME);
    FILE* p_file = NULL;

    when_str_empty_trace(fname, exit, ERROR, "Filename is empty.");
    when_str_empty_trace(amxc_string_get(string, 0), exit, ERROR, "String to be appended is empty.");

    p_file = fopen(fname, "w");
    when_null_trace(p_file, exit, ERROR, "Could not open file descriptor for %s error %s", fname, strerror(errno));
    fputs(amxc_string_get(string, 0), p_file);
    fclose(p_file);

exit:
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Extract a substring with a regular expression match.

   @param[in] str Source string containing the information to be searched.
   @param[in] regex Regular expression.
   @param[out] extracted_str Substring matched.

   @return
   0 if there is a match with the regex. Not 0 if there isn't a match.
 */
int mod_ipd_extract_regex_from_string(amxc_string_t* str,
                                      const cstring_t regex,
                                      amxc_string_t* extracted_str) {
    SAH_TRACEZ_IN(ME);
    regex_t finder;
    size_t match_size = 0;
    cstring_t substr = NULL;
    int ret = -1;
    int cflags = REG_EXTENDED;
    regmatch_t pmatch[2] = {{-1, -1}, {-1, -1}};

    when_null_trace(extracted_str, exit, ERROR, "Extracted string is not initialised.");
    when_true_trace(amxc_string_is_empty(str), exit, ERROR, "String to be searched is NULL.");
    when_null_trace(regex, exit, ERROR, "Regular expression is NULL");

    ret = regcomp(&finder, regex, cflags);
    when_failed(ret, clean);

    ret = regexec(&finder, amxc_string_get(str, 0), 1, pmatch, 0);
    when_failed(ret, clean);

    match_size = pmatch[0].rm_eo - pmatch[0].rm_so;
    substr = amxc_string_dup(str, pmatch[0].rm_so, match_size);
    amxc_string_push_buffer(extracted_str, substr, match_size + 1);

clean:
    regfree(&finder);
exit:
    SAH_TRACEZ_OUT(ME);
    return ret;
}

/**
   @brief
   Read the information from the file and save to a string.

   @param[in] fname file name.
   @param[out] string string containing the information of the file appended.
 */
void mod_ipd_read_from_file(const char* fname, amxc_string_t* string) {
    SAH_TRACEZ_IN(ME);
    FILE* p_file = NULL;
    cstring_t buffer = NULL;
    long length = 0;
    size_t bytes_read = 0;

    when_str_empty_trace(fname, exit, ERROR, "Filename is empty.");

    p_file = fopen(fname, "rb");
    when_null_trace(p_file, exit, ERROR, "Could not open file descriptor for %s error %s", fname, strerror(errno));

    fseek(p_file, 0, SEEK_END);
    length = ftell(p_file);
    fseek(p_file, 0, SEEK_SET);
    buffer = calloc(1, length + 1);
    when_null_trace(buffer, exit, ERROR, "Could not allocate memory to the buffer");
    bytes_read = fread(buffer, 1, length, p_file);

    if(bytes_read <= 0) {
        SAH_TRACEZ_ERROR(ME, "Could not read from file %s, read bytes problem", fname);
        free(buffer);
        goto exit;
    }
    amxc_string_push_buffer(string, buffer, length + 1);

exit:
    if(p_file != NULL) {
        fclose(p_file);
    }
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Delete a file with a file name.
 */
void mod_ipd_delete_filef(const char* fmt, ...) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t* str = NULL;
    va_list arglist;

    amxc_string_new(&str, 0);
    va_start(arglist, fmt);

    when_null_trace(fmt, exit, ERROR, "fmt is NULL");

    amxc_string_vsetf(str, fmt, arglist);
    when_str_empty(amxc_string_get(str, 0), exit);

    if(access(amxc_string_get(str, 0), F_OK) == 0) {
        remove(amxc_string_get(str, 0));
    }

exit:
    amxc_string_delete(&str);
    va_end(arglist);
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Read the information from the file descriptor and save to a string.

   @param[in] fd file descriptor.
   @param[out] string string containing the information of the fd appended.
 */
void mod_ipd_read_from_fd(const int fd, amxc_string_t* string) {
    SAH_TRACEZ_IN(ME);
    char buffer[STDOUT_BUFFER_SIZE];
    ssize_t bytes = 0;

    when_null_trace(string, exit, ERROR, "Output string is not initialised.");
    when_false_trace(fd > 0, exit, ERROR, "File descriptor is not bigger than 0.");

    bytes = read(fd, buffer, STDOUT_BUFFER_SIZE);
    when_true_trace(((bytes <= 0)), exit, ERROR, "Problem with the buffer size, no bytes were read");
    when_true_trace(((bytes >= STDOUT_BUFFER_SIZE)), exit, ERROR, "Problem with the buffer size, not big enough");

    if(buffer[0] != '\0') {
        buffer[bytes] = 0;
        amxc_string_append(string, buffer, bytes);
    }

exit:
    SAH_TRACEZ_OUT(ME);
}

/**
 * @brief
 *  Shifts the dscp command two bits to the left
 *  Gives the ToS for the ping command. Example: DSCP = 00001000 -> ToS = 00100000
 *
 * @param dscp  DSCP desired for the ping command
 * @return Translated ToS value
 */
unsigned char mod_ipd_dscp_to_tos(unsigned char dscp) {
    return dscp << 2;
}

/**
 * @brief
 * Function that returns the position of the string contained in the list when the key is valid
 *
 * @param stdout_lines String list
 * @param key Key to search in the list of strings
 * @return int The number of the string in the list, -1 if no line is found
 */
int mod_ipd_find_line(amxc_llist_t* stdout_lines, const char* key) {
    SAH_TRACEZ_IN(ME);
    int line_num = -1;
    int num_lines = amxc_llist_size(stdout_lines);
    amxc_string_t* line = NULL;

    when_null_trace(stdout_lines, exit, ERROR, "String list is NULL");
    when_str_empty_trace(key, exit, ERROR, "Key is empty");

    while(line_num < num_lines - 1) {
        line_num++;
        line = amxc_string_get_from_llist(stdout_lines, line_num);
        if(amxc_string_search(line, key, 0) != -1) {
            goto exit;
        }
    }
    line_num = -1;

exit:
    SAH_TRACEZ_OUT(ME);
    return line_num;
}

/**
 * @brief
 * Find the interface ip address by making a tracing of "ip_address"
 * Uses the IP Route tool
 *
 * @param ip_address
 * @return char*
 */
char* mod_ipd_find_interface_ip_address(const char* ip_address) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t aux_string;
    amxc_string_t extracted_string;
    amxc_string_t extracted_ip;
    FILE* fp = NULL;
    char buff[256];
    char* intf_ip_address = NULL;
    int rv = -1;

    amxc_string_init(&aux_string, 0);
    amxc_string_init(&extracted_string, 0);
    amxc_string_init(&extracted_ip, 0);
    when_str_empty_trace(ip_address, skip, ERROR, "Interface IP address for tracing source address is empty");

    //open command for reading the output
    amxc_string_setf(&aux_string, "ip route get %s", ip_address);
    fp = popen(amxc_string_get(&aux_string, 0), "r");
    if(fp == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not open file descriptor for system command '%s'", amxc_string_get(&aux_string, 0));
        goto skip;
    }
    if(fgets(buff, sizeof(buff), fp) == NULL) {
        SAH_TRACEZ_ERROR(ME, "Could not fetch the string output of system command '%s'", amxc_string_get(&aux_string, 0));
        goto skip;
    }

    amxc_string_setf(&aux_string, "%s", buff);
    amxc_string_trim(&aux_string, NULL);

    //IPv4
    rv = mod_ipd_extract_regex_from_string(&aux_string, MOD_IPD_REGEX_SRC_IPV4, &extracted_string);
    if(rv == 0) {
        rv = mod_ipd_extract_regex_from_string(&extracted_string, MOD_IPD_REGEX_IPV4, &extracted_ip);
        intf_ip_address = strdup(rv == 0 ? amxc_string_get(&extracted_ip, 0) : "");
        goto exit;
    }
    //IPv6
    rv = mod_ipd_extract_regex_from_string(&aux_string, MOD_IPD_REGEX_SRC_IPV6, &extracted_string);
    if(rv == 0) {
        rv = mod_ipd_extract_regex_from_string(&extracted_string, MOD_IPD_REGEX_IPV6, &extracted_ip);
        intf_ip_address = strdup(rv == 0 ? amxc_string_get(&extracted_ip, 0) : "");
        goto exit;
    }
skip:
    intf_ip_address = strdup("");
exit:
    if(fp != NULL) {
        pclose(fp);
    }
    amxc_string_clean(&extracted_string);
    amxc_string_clean(&extracted_ip);
    amxc_string_clean(&aux_string);
    SAH_TRACEZ_OUT(ME);
    return intf_ip_address;
}