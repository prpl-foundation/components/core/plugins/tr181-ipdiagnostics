/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_ipd_common.h"
#include "mod_ipd_ftx_common.h"
#include "mod_ipd_ftx_download.h"
#include "mod_ipd_ftx_upload.h"
#include "mod_ipd_ftx.h"
#include "ip_diagnostics.h"
#include "upload_download.h"
#include "download.h"
#include "upload.h"

// module trace zone
#define ME "ipd-ftx"

static amxp_signal_t* monitor_fd_signal = NULL;
static amxp_signal_t* fd_event_signal = NULL;
static amxp_signal_t* download_ended_signal = NULL;
static amxp_signal_t* all_download_ended_signal = NULL;
static amxp_signal_t* upload_ended_signal = NULL;
static amxp_signal_t* get_interface_stats = NULL;
static amxp_signal_t* all_upload_ended_signal = NULL;
static amxp_signal_t* signal_ready = NULL;

static void ftx_fd_event(UNUSED const char* signame, const amxc_var_t* const data, UNUSED void* const priv) {
    char notif;
    int rv = 0;
    uint32_t nb_down = 0;
    uint32_t nb_up = 0;
    ipd_ftx_request_state_t* on_going_download = get_on_going_download(&nb_down);
    ipd_ftx_request_state_t* on_going_upload = get_on_going_upload(&nb_up);

    for(uint32_t i = 0; i < nb_down; ++i) {
        if(on_going_download[i].pipe_fd[0] == GET_INT32(data, NULL)) {
            rv = read(on_going_download[i].pipe_fd[0], &notif, 1);
            if(rv > 0) {
                on_going_download[i].cb(&on_going_download[i]);
            }
            return;
        }
    }
    for(uint32_t i = 0; i < nb_up; ++i) {
        if(on_going_upload[i].pipe_fd[0] == GET_INT32(data, NULL)) {
            rv = read(on_going_upload[i].pipe_fd[0], &notif, 1);
            if(rv > 0) {
                on_going_upload[i].cb(&on_going_upload[i]);
            }
            return;
        }
    }
}

int connection_cb(int fd, bool add) {
    int retval = 0;
    amxc_var_t data;

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(int32_t, &data, "fd", fd);
    amxc_var_add_key(bool, &data, "add", add);
    amxp_signal_trigger(monitor_fd_signal, &data);
    amxc_var_clean(&data);
    return retval;
}

static void signal_ready_cb(UNUSED const char* signame, UNUSED const amxc_var_t* const data, UNUSED void* const priv) {
}

static AMXM_CONSTRUCTOR ipd_ftx_start(void) {
    int rv = -1;
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;
    function_t function[] = {
        {IPD_DOWNLOAD_TEST_START, mod_download_test_start},
        {IPD_DOWNLOAD_TEST_CHECK, mod_download_test_check},
        {IPD_DOWNLOAD_TEST_STOP, mod_download_test_stop},
        {IPD_DOWNLOAD_TEST_ABORT, mod_download_test_abort},
        {IPD_UPLOAD_TEST_START, mod_upload_test_start},
        {IPD_UPLOAD_TEST_CHECK, mod_upload_test_check},
        {IPD_UPLOAD_TEST_STOP, mod_upload_test_stop},
        {IPD_UPLOAD_TEST_ABORT, mod_upload_test_abort},
        {NULL, 0}
    };

    rv = amxm_module_register(&mod, so, MOD_IPD_143_CTRL);
    when_failed_trace(rv, error, ERROR, "Failed to register the module %s", MOD_IPD_143_CTRL);
    for(size_t i = 0; function[i].name != NULL; i++) {
        rv = amxm_module_add_function(mod, function[i].name, function[i].callback);
        if(rv != 0) {
            SAH_TRACEZ_WARNING(ME, "Failed to register function %s from %s", function[i].name, MOD_IPD_143_CTRL);
        }
    }
    amxp_signal_new(NULL, &monitor_fd_signal, "mod-ipd-ftx:fd_to_monitor");
    amxp_signal_new(NULL, &fd_event_signal, "mod-ipd-ftx:fd_event");
    amxp_signal_new(NULL, &signal_ready, "mod-ipd-ftx:signal_ready");
    amxp_signal_new(NULL, &download_ended_signal, "mod-ipd-ftx:download_ended");
    amxp_signal_new(NULL, &all_download_ended_signal, "mod-ipd-ftx:all_download_ended");
    amxp_signal_new(NULL, &upload_ended_signal, "mod-ipd-ftx:upload_ended");
    amxp_signal_new(NULL, &get_interface_stats, "mod-ipd-ftx:get_interface_stats");
    amxp_signal_new(NULL, &all_upload_ended_signal, "mod-ipd-ftx:all_upload_ended");
    amxp_slot_connect(NULL, "mod-ipd-ftx:fd_event", NULL, ftx_fd_event, NULL);
    amxp_slot_connect(NULL, "mod-ipd-ftx:signal_ready", NULL, signal_ready_cb, NULL);
error:
    return rv;
}

static AMXM_DESTRUCTOR ipd_ftx_stop(void) {
    amxp_signal_delete(&monitor_fd_signal);
    amxp_signal_delete(&fd_event_signal);
    amxp_signal_delete(&signal_ready);
    amxp_signal_delete(&download_ended_signal);
    amxp_signal_delete(&all_download_ended_signal);
    amxp_signal_delete(&upload_ended_signal);
    amxp_signal_delete(&get_interface_stats);
    amxp_signal_delete(&all_upload_ended_signal);
    return 0;
}
