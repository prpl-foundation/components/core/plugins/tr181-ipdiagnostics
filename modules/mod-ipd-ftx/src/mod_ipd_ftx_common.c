/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <curl/curl.h>
#include <string.h>
#include <netinet/ether.h>
#include <netinet/ip.h>
#include <sys/socket.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "mod_ipd_ftx.h"
#include "mod_ipd_ftx_common.h"
#include "download.h"
#include "upload.h"
#include "tx_diag.h"

#define ME "ipd-ftx"

void set_times_with_offset(amxc_ts_t* dest, const amxc_ts_t* start, bool exist, double offset) {
    int32_t nsec = (offset - (int64_t) offset) * 1000000000;

    when_false(exist, exit);
    memcpy(dest, start, sizeof(*start));
    dest->sec += (int64_t) (offset);
    if(1000000000 - dest->nsec <= nsec) {
        ++dest->sec;
        dest->nsec = nsec - (1000000000 - dest->nsec);
    } else {
        dest->nsec += nsec;
    }
exit:
    return;
}

int curl_sockopt_cb(void* clientp, curl_socket_t curlfd, UNUSED curlsocktype purpose) {
    ipd_ftx_request_state_t* state = (ipd_ftx_request_state_t*) clientp;
    amxc_ts_t ts;
    int rv = -1;

    if(GET_ARG(state->params, "EthernetPriority")) {
        uint32_t priority = GET_UINT32(state->params, "EthernetPriority");

        when_failed_trace(setsockopt(curlfd, SOL_SOCKET, SO_PRIORITY, &priority, sizeof(priority)), exit, ERROR, "Cannot set ethernet priority %hhu", priority);
    }
    if(GET_ARG(state->params, "DSCP")) {
        uint32_t dscp = GET_UINT32(state->params, "DSCP");

        when_failed_trace(setsockopt(curlfd, IPPROTO_IP, IP_TOS, &dscp, sizeof(dscp)), exit, ERROR, "Cannot set DSCP %hhu", dscp);
    }
    amxc_ts_now(&ts);
    amxc_var_add_key(amxc_ts_t, state->params, "result_tcpopenrequest", &ts);
    rv = 0;
exit:
    return (rv != 0) ? CURL_SOCKOPT_ERROR : CURL_SOCKOPT_OK;
}

void log_interface_stats(const char* type, const char* time, ipd_ftx_request_state_t* state) {
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "type", type);
    amxc_var_add_key(cstring_t, &args, "time", time);
    amxc_var_add_key(uint32_t, &args, "index", state->index);
    amxp_sigmngr_emit_signal(NULL, "mod-ipd-ftx:get_interface_stats", &args);
    amxc_var_clean(&args);
}

uint32_t convert_error_code_from_curl(CURLcode curl_code, uint32_t response_code, txdiag_type_t type) {
    uint32_t error_code = (type == tx_download) ? down_test_error_other : up_test_error_other;

    switch((uint32_t) curl_code) {
    case 0:     // CURLE_OK,                       0
        error_code = (type == tx_download) ? down_test_complete : up_test_complete;
        break;
    case 1:     // CURLE_UNSUPPORTED_PROTOCOL,     1
        error_code = (type == tx_download) ? down_test_error_other : up_test_error_other;
        break;
    case 2:     // CURLE_FAILED_INIT,              2
        error_code = (type == tx_download) ? down_test_error_internal : up_test_error_internal;
        break;
    case 3:     // CURLE_URL_MALFORMAT,            3
        error_code = (type == tx_download) ? down_test_error_internal : up_test_error_internal;
        break;
    case 4:     // CURLE_NOT_BUILT_IN,             4 - [was obsoleted in August 2007 for 7.17.0, reused in April 2011 for 7.21.5]
        error_code = (type == tx_download) ? down_test_error_internal : up_test_error_internal;
        break;
    case 5:     // CURLE_COULDNT_RESOLVE_PROXY,    5
    case 6:     // CURLE_COULDNT_RESOLVE_HOST,     6
        error_code = (type == tx_download) ? down_test_error_resolve_host_name : up_test_error_resolve_host_name;
        break;
    case 7:     // CURLE_COULDNT_CONNECT,          7
        error_code = (type == tx_download) ? down_test_error_init_conn_failed : up_test_error_init_conn_failed;
        break;
    case 8:     // CURLE_WEIRD_SERVER_REPLY,       8
        error_code = (type == tx_download) ? down_test_error_transfer_failed : up_test_error_no_transfer_complete;
        break;
    case 9:     // CURLE_REMOTE_ACCESS_DENIED,     9 a service was denied by the server due to lack of access - when login fails this is not returned.
        error_code = (type == tx_download) ? down_test_error_login_failed : up_test_error_login_failed;
        break;
    case 10:    // CURLE_FTP_ACCEPT_FAILED,        10 - [was obsoleted in April 2006 for 7.15.4, reused in Dec 2011 for 7.24.0]
        error_code = (type == tx_download) ? down_test_error_init_conn_failed : up_test_error_init_conn_failed;
        break;
    case 11:    // CURLE_FTP_WEIRD_PASS_REPLY,     11
    case 12:    // CURLE_FTP_ACCEPT_TIMEOUT,       12 - timeout occurred accepting server [was obsoleted in August 2007 for 7.17.0, reused in Dec 2011 for 7.24.0]
        break;
    case 13:    // CURLE_FTP_WEIRD_PASV_REPLY,     13
        error_code = (type == tx_download) ? down_test_error_no_pasv : up_test_error_no_pasv;
        break;
    case 14:    // CURLE_FTP_WEIRD_227_FORMAT,     14
    case 15:    // CURLE_FTP_CANT_GET_HOST,        15
    case 16:    // CURLE_HTTP2,                    16 - A problem in the http2 framing layer. [was obsoleted in August 2007 for 7.17.0, reused in July 2014 for 7.38.0]
    case 17:    // CURLE_FTP_COULDNT_SET_TYPE,     17
        break;
    case 18:    // CURLE_PARTIAL_FILE,             18
        error_code = (type == tx_download) ? down_test_error_transfer_failed : up_test_error_no_transfer_complete;
        break;
    case 19:    // CURLE_FTP_COULDNT_RETR_FILE,    19
        error_code = (type == tx_download) ? down_test_error_transfer_failed : up_test_error_no_transfer_complete;
        break;
    case 20:    // CURLE_OBSOLETE20,               20 - NOT USED
    case 21:    // CURLE_QUOTE_ERROR,              21 - quote command failure
        break;
    case 22:    // CURLE_HTTP_RETURNED_ERROR,      22
        switch(response_code) {
        case 401:
            error_code = (type == tx_download) ? down_test_error_login_failed : up_test_error_login_failed;
            break;
        case 204:
        case 403:
        case 404:
            error_code = (type == tx_download) ? down_test_error_transfer_failed : up_test_error_no_transfer_complete;
            break;
        case 400:
        default:
            break;
        }
        break;
    case 23:    // CURLE_WRITE_ERROR,              23
        error_code = (type == tx_download) ? down_test_error_internal : up_test_error_internal;
        break;
    case 24:    // CURLE_OBSOLETE24,               24 - NOT USED
    case 25:    // CURLE_UPLOAD_FAILED,            25 - failed upload "command"
    case 26:    // CURLE_READ_ERROR,               26 - couldn't open/read from file
        break;
    case 27:    // CURLE_OUT_OF_MEMORY,            27
        error_code = (type == tx_download) ? down_test_error_internal : up_test_error_internal;
        break;
    case 28:    // CURLE_OPERATION_TIMEDOUT,       28 - the timeout time was reached
        error_code = (type == tx_download) ? down_test_error_timeout : up_test_error_timeout;
        break;
    case 29:    // CURLE_OBSOLETE29,               29 - NOT USED
    case 30:    // CURLE_FTP_PORT_FAILED,          30 - FTP PORT operation failed
    case 31:    // CURLE_FTP_COULDNT_USE_REST,     31 - the REST command failed
    case 32:    // CURLE_OBSOLETE32,               32 - NOT USED
    case 33:    // CURLE_RANGE_ERROR,              33 - RANGE "command" didn't work
        break;
    case 34:    // CURLE_HTTP_POST_ERROR,          34
    case 35:    // CURLE_SSL_CONNECT_ERROR,        35 - wrong when connecting with SSL
        error_code = (type == tx_download) ? down_test_error_internal : up_test_error_internal;
        break;
    case 36:    // CURLE_BAD_DOWNLOAD_RESUME,      36 - couldn't resume download
        break;
    case 37:    // CURLE_FILE_COULDNT_READ_FILE,   37
        error_code = (type == tx_download) ? down_test_error_internal : up_test_error_internal;
        break;
    case 38:    // CURLE_LDAP_CANNOT_BIND,         38
    case 39:    // CURLE_LDAP_SEARCH_FAILED,       39
    case 40:    // CURLE_OBSOLETE40,               40 - NOT USED
    case 41:    // CURLE_FUNCTION_NOT_FOUND,       41 - NOT USED starting with 7.53.0
    case 42:    // CURLE_ABORTED_BY_CALLBACK,      42
    case 43:    // CURLE_BAD_FUNCTION_ARGUMENT,    43
    case 44:    // CURLE_OBSOLETE44,               44 - NOT USED
    case 45:    // CURLE_INTERFACE_FAILED,         45 - CURLOPT_INTERFACE failed
    case 46:    // CURLE_OBSOLETE46,               46 - NOT USED
        error_code = (type == tx_download) ? down_test_error_internal : up_test_error_internal;
        break;
    case 47:    // CURLE_TOO_MANY_REDIRECTS,       47 - catch endless re-direct loops
        break;
    case 48:    // CURLE_UNKNOWN_OPTION,           48 - User specified an unknown option
    case 49:    // CURLE_SETOPT_OPTION_SYNTAX,     49 - Malformed setopt option
        error_code = (type == tx_download) ? down_test_error_internal : up_test_error_internal;
        break;
    case 50:    // CURLE_OBSOLETE50,               50 - NOT USED
    case 51:    // CURLE_OBSOLETE51,               51 - NOT USED
    case 52:    // CURLE_GOT_NOTHING,              52 - when this is a specific error
        break;
    case 53:    // CURLE_SSL_ENGINE_NOTFOUND,      53 - SSL crypto engine not found
    case 54:    // CURLE_SSL_ENGINE_SETFAILED,     54 - can not set SSL crypto engine as default
        error_code = (type == tx_download) ? down_test_error_internal : up_test_error_internal;
        break;
    case 55:    // CURLE_SEND_ERROR,               55 - failed sending network data
    case 56:    // CURLE_RECV_ERROR,               56 - failure in receiving network data
    case 57:    // CURLE_OBSOLETE57,               57 - NOT IN USE
        break;
    case 58:    // CURLE_SSL_CERTPROBLEM,          58 - problem with the local certificate
    case 59:    // CURLE_SSL_CIPHER,               59 - couldn't use specified cipher
        error_code = (type == tx_download) ? down_test_error_internal : up_test_error_internal;
        break;
    case 60:    // CURLE_PEER_FAILED_VERIFICATION,  60 - peer's certificate or fingerprint wasn't verified fine
        error_code = (type == tx_download) ? down_test_error_login_failed : up_test_error_login_failed;
        break;
    case 61:    // CURLE_BAD_CONTENT_ENCODING,     61 - Unrecognized/bad encoding
    case 62:    // CURLE_OBSOLETE62,               62 - NOT IN USE since 7.82.0
        break;
    case 63:    // CURLE_FILESIZE_EXCEEDED,        63 - Maximum file size exceeded
        error_code = (type == tx_download) ? down_test_error_incorrect_size : up_test_error_other;
        break;
    case 64:    // CURLE_USE_SSL_FAILED,           64 - Requested FTP SSL level failed
    case 65:    // CURLE_SEND_FAIL_REWIND,         65 - Sending the data requires a rewind that failed
    case 66:    // CURLE_SSL_ENGINE_INITFAILED,    66 - failed to initialise ENGINE
        error_code = (type == tx_download) ? down_test_error_internal : up_test_error_internal;
        break;
    case 67:    // CURLE_LOGIN_DENIED,             67 - user, password or similar was not accepted and we failed to login
        error_code = (type == tx_download) ? down_test_error_login_failed : up_test_error_login_failed;
        break;
    case 68:    // CURLE_TFTP_NOTFOUND,            68 - file not found on server
    case 69:    // CURLE_TFTP_PERM,                69 - permission problem on server
    case 70:    // CURLE_REMOTE_DISK_FULL,         70 - out of disk space on server
    case 71:    // CURLE_TFTP_ILLEGAL,             71 - Illegal TFTP operation
    case 72:    // CURLE_TFTP_UNKNOWNID,           72 - Unknown transfer ID
    case 73:    // CURLE_REMOTE_FILE_EXISTS,       73 - File already exists
    case 74:    // CURLE_TFTP_NOSUCHUSER,          74 - No such user
    case 75:    // CURLE_OBSOLETE75,               75 - NOT IN USE since 7.82.0
    case 76:    // CURLE_OBSOLETE76,               76 - NOT IN USE since 7.82.0
    case 77:    // CURLE_SSL_CACERT_BADFILE,       77 - could not load CACERT file, missing or wrong format
        break;
    case 78:    // CURLE_REMOTE_FILE_NOT_FOUND,    78 - remote file not found
        error_code = (type == tx_download) ? down_test_error_internal : up_test_error_internal;
        break;
    case 79:    // CURLE_SSH,                      79 - error from the SSH layer, somewhat generic so the error message will be of interest when this has happened
    case 80:    // CURLE_SSL_SHUTDOWN_FAILED,      80 - Failed to shut down the SSL connection
    case 81:    // CURLE_AGAIN,                    81 - socket is not ready for send/recv, wait till it's ready and try again (Added in 7.18.2)
    case 82:    // CURLE_SSL_CRL_BADFILE,          82 - could not load CRL file, missing or wrong format (Added in 7.19.0)
    case 83:    // CURLE_SSL_ISSUER_ERROR,         83 - Issuer check failed.  (Added in 7.19.0)
        error_code = (type == tx_download) ? down_test_error_internal : up_test_error_internal;
        break;
    case 84:    // CURLE_FTP_PRET_FAILED,          84 - a PRET command failed
    case 85:    // CURLE_RTSP_CSEQ_ERROR,          85 - mismatch of RTSP CSeq numbers
    case 86:    // CURLE_RTSP_SESSION_ERROR,       86 - mismatch of RTSP Session Ids
        break;
    case 87:    // CURLE_FTP_BAD_FILE_LIST,        87 - unable to parse FTP file list
    case 88:    // CURLE_CHUNK_FAILED,             88 - chunk callback reported error
    case 89:    // CURLE_NO_CONNECTION_AVAILABLE,  89 - No connection available, the session will be queued
    case 90:    // CURLE_SSL_PINNEDPUBKEYNOTMATCH,  90 - specified pinned public key did not match
    case 91:    // CURLE_SSL_INVALIDCERTSTATUS,    91 - invalid certificate status
    case 92:    // CURLE_HTTP2_STREAM,             92 - stream error in HTTP/2 framing layer
    case 93:    // CURLE_RECURSIVE_API_CALL,       93 - an api function was called from inside a callback
    case 94:    // CURLE_AUTH_ERROR,               94 - an authentication function returned an error
    case 95:    // CURLE_HTTP3,                    95 - An HTTP/3 layer problem
    case 96:    // CURLE_QUIC_CONNECT_ERROR,       96 - QUIC connection error
    case 97:    // CURLE_PROXY,                    97 - proxy handshake error
    case 98:    // CURLE_SSL_CLIENTCERT,           98 - client-side certificate required
    case 99:    // CURLE_UNRECOVERABLE_POLL,       99 - poll/select returned fatal error
        error_code = (type == tx_download) ? down_test_error_internal : up_test_error_internal;
        break;
    default:
        break;
    }

    return error_code;
}