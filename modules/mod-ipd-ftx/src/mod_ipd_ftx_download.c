/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <curl/curl.h>

#include "mod_ipd_common.h"
#include "mod_ipd_ftx.h"
#include "mod_ipd_ftx_common.h"
#include "mod_ipd_ftx_download.h"
#include "upload_download.h"
#include "download.h"
#include "ip_diagnostics_priv.h"

// module trace zone
#define ME "ftx-d"
#define ON_GOING_REQ_INDEX(x) ((x == 0) ? (0) : (x - 1))

static ipd_ftx_request_state_t* _on_going_req = NULL;
static uint32_t _on_going_nb_req = 0;
static uint32_t started_download = 0;
static pthread_mutex_t started_download_mutex = PTHREAD_MUTEX_INITIALIZER;

const cstring_t str_down_test_status[download_test_num_of_status] = {
    UP_DOWN_STATUS_COMPLETE,
    UP_DOWN_STATUS_ERR_HOST_NAME,
    UP_DOWN_STATUS_ERR_ROUTE,
    UP_DOWN_STATUS_ERR_INIT_CONN,
    UP_DOWN_STATUS_ERR_NO_RESPONSE,
    DOWN_STATUS_ERR_TRANSFER,
    UP_DOWN_STATUS_ERR_PASSWORD,
    UP_DOWN_STATUS_ERR_LOGIN,
    UP_DOWN_STATUS_ERR_NO_TRANSFER,
    UP_DOWN_STATUS_ERR_NO_PASV,
    DOWN_STATUS_ERR_WRONG_SIZE,
    UP_DOWN_STATUS_ERR_TIMEOUT,
    UP_DOWN_STATUS_ERR_INTERNAL,
    UP_DOWN_STATUS_ERR_OTHER,
    UP_DOWN_STATUS_NOT_COMPLETE,
    UP_DOWN_STATUS_NOT_EXECUTED
};

ipd_ftx_request_state_t* get_on_going_download(uint32_t* nb_req) {
    *nb_req = _on_going_nb_req;
    return _on_going_req;
}

static bool are_all_requests_done(void) {
    bool rv = true;

    when_null(_on_going_req, exit);
    for(uint32_t i = 0; i < _on_going_nb_req; ++i) {
        if(_on_going_req[i].finished == false) {
            rv = false;
            break;
        }
    }
exit:
    return rv;
}

static inline void clean_on_going_request(void) {
    for(uint32_t i = 0; i < _on_going_nb_req; ++i) {
        amxc_var_delete(&_on_going_req[i].params);
        if(_on_going_req[i].target != NULL) {
            fclose(_on_going_req[i].target);
            _on_going_req[i].target = NULL;
        }
        connection_cb(_on_going_req[i].pipe_fd[0], false);
        close(_on_going_req[i].pipe_fd[0]);
        close(_on_going_req[i].pipe_fd[1]);
    }
    pthread_mutex_destroy(&started_download_mutex);
    free(_on_going_req);
    _on_going_nb_req = 0;
    _on_going_req = NULL;
}

static bool ftx_reply_handler_cb(void* priv) {
    amxc_var_t data;
    ipd_ftx_request_state_t* state = (ipd_ftx_request_state_t*) priv;

    pthread_join(state->thread, NULL);
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    when_null(state, exit);
    state->finished = true;
    amxc_var_move(amxc_var_add_new_key(&data, "error_code"), amxc_var_take_key(state->params, "result_code"));
    amxc_var_add_key(uint32_t, &data, "index", state->index);
    amxc_var_add_key(cstring_t, &data, "status", DOWNLOAD_TEST_STATUS(GET_UINT32(&data, "error_code")));
    if(GET_UINT32(&data, "error_code") == down_test_complete) {
        amxc_var_move(amxc_var_add_new_key(&data, "ROMTime"), amxc_var_take_key(state->params, "result_rom"));
        amxc_var_move(amxc_var_add_new_key(&data, "BOMTime"), amxc_var_take_key(state->params, "result_bom"));
        amxc_var_move(amxc_var_add_new_key(&data, "EOMTime"), amxc_var_take_key(state->params, "result_eom"));
        amxc_var_move(amxc_var_add_new_key(&data, "TCPOpenRequestTime"), amxc_var_take_key(state->params, "result_tcpopenrequest"));
        amxc_var_move(amxc_var_add_new_key(&data, "TCPOpenResponseTime"), amxc_var_take_key(state->params, "result_tcpopenresponse"));
        amxc_var_add_key(uint32_t, &data, "TestBytesReceived", state->transfered_bytes);
        amxc_var_move(amxc_var_add_new_key(&data, "IPAddressUsed"), amxc_var_take_key(state->params, "result_ipaddress"));
        amxc_var_add_key(uint32_t, &data, "BytesUnderFullLoading", GET_UINT32(state->params, "result_fullloading_stop") - GET_UINT32(state->params, "result_fullloading_start"));
    }
    amxp_signal_emit(amxp_sigmngr_find_signal(NULL, "mod-ipd-ftx:download_ended"), &data);
    if((state->index != 0) && are_all_requests_done()) {
        amxp_signal_emit(amxp_sigmngr_find_signal(NULL, "mod-ipd-ftx:all_download_ended"), &data);
        clean_on_going_request();
    } else if(state->index == 0) {
        clean_on_going_request();
    }
exit:
    amxc_var_clean(&data);
    return true;
}

static size_t write_cb(void* data, size_t size, size_t nmemb, void* userp) {
    ipd_ftx_request_state_t* state = (ipd_ftx_request_state_t*) userp;

    if(state->canceled) {
        return CURL_WRITEFUNC_ERROR;
    }
    pthread_mutex_lock(&started_download_mutex);
    if(GET_ARG(state->params, "result_bom") == NULL) {
        amxc_ts_t ts;

        amxc_ts_now(&ts);
        amxc_var_add_key(amxc_ts_t, state->params, "result_bom", &ts);
        ++started_download;
        log_interface_stats("Download", "BOM", state);
    }
    if((GET_ARG(state->params, "result_fullloading_start") == NULL) && (started_download == _on_going_nb_req)) {
        amxc_var_add_key(uint32_t, state->params, "result_fullloading_start", state->transfered_bytes);
    }
    state->transfered_bytes += nmemb;
    if((GET_ARG(state->params, "result_fullloading_stop") == NULL) && (GET_ARG(state->params, "result_fullloading_start") != NULL) && (started_download != _on_going_nb_req)) {
        amxc_var_add_key(uint32_t, state->params, "result_fullloading_stop", state->transfered_bytes);
    }
    pthread_mutex_unlock(&started_download_mutex);
    return fwrite(data, size, nmemb, state->target);
}

static void* ftx_download_routine(void* priv) {
    CURL* handle = NULL;
    CURLcode code = CURLE_FAILED_INIT;
    long response_code = 0;
    ipd_ftx_request_state_t* state = (ipd_ftx_request_state_t*) priv;
    int rv = 0;
    amxc_ts_t ts = {0};
    double curl_ts = 0.0;
    cstring_t local_ip = NULL;
    long header_size = 0;

    when_null(state, exit);
    state->target = fopen("/dev/null", "w");
    code = curl_global_init(CURL_GLOBAL_ALL);
    SAH_TRACEZ_NOTICE(ME, "%s curl_global_init: %d index request %d URL: %s", curl_version(), rv, state->index, GET_CHAR(state->params, "DownloadURL"));
    when_failed(code, exit);
    when_null_status(state->target, exit, code = CURLE_FAILED_INIT);
    handle = curl_easy_init();
    when_null_status(handle, exit, code = CURLE_FAILED_INIT);
    curl_easy_setopt(handle, CURLOPT_URL, GET_CHAR(state->params, "DownloadURL"));
    curl_easy_setopt(handle, CURLOPT_WRITEDATA, state);
    curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, write_cb);
    curl_easy_setopt(handle, CURLOPT_SOCKOPTDATA, state);
    curl_easy_setopt(handle, CURLOPT_SOCKOPTFUNCTION, curl_sockopt_cb);
    curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1);
    curl_easy_setopt(handle, CURLOPT_FAILONERROR, 1);
    curl_easy_setopt(handle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    code = curl_easy_perform(handle);
    when_true(state->canceled, exit);
    curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &response_code);
    if(code == CURLE_OK) {
        pthread_mutex_lock(&started_download_mutex);
        --started_download;
        pthread_mutex_unlock(&started_download_mutex);
        if((GET_ARG(state->params, "result_fullloading_stop") == NULL) && (GET_ARG(state->params, "result_fullloading_start") != NULL)) {
            amxc_var_add_key(uint32_t, state->params, "result_fullloading_stop", state->transfered_bytes);
        }
        curl_easy_getinfo(handle, CURLINFO_HEADER_SIZE, &header_size);
        state->transfered_bytes += (uint32_t) header_size;
        log_interface_stats("Download", "EOM", state);
        amxc_ts_now(&ts);
        amxc_var_add_key(amxc_ts_t, state->params, "result_eom", &ts);
        curl_easy_getinfo(handle, CURLINFO_CONNECT_TIME, &curl_ts);
        set_times_with_offset(&ts, amxc_var_constcast(amxc_ts_t, GET_ARG(state->params, "result_tcpopenrequest")), GET_ARG(state->params, "result_tcpopenrequest") != NULL, curl_ts);
        amxc_var_add_key(amxc_ts_t, state->params, "result_tcpopenresponse", &ts);
        curl_easy_getinfo(handle, CURLINFO_PRETRANSFER_TIME, &curl_ts);
        set_times_with_offset(&ts, amxc_var_constcast(amxc_ts_t, GET_ARG(state->params, "result_tcpopenrequest")), GET_ARG(state->params, "result_tcpopenrequest") != NULL, curl_ts);
        amxc_var_add_key(amxc_ts_t, state->params, "result_rom", &ts);
        curl_easy_getinfo(handle, CURLINFO_LOCAL_IP, &local_ip);
        amxc_var_add_key(cstring_t, state->params, "result_ipaddress", local_ip);
    }
    SAH_TRACEZ_NOTICE(ME, "index request: %d code: %d response_code: %ld", state->index, code, response_code);
exit:
    if(state != NULL) {
        amxc_var_add_key(uint32_t, state->params, "result_code", convert_error_code_from_curl(code, response_code, tx_download));
        if(!state->canceled) {
            rv = write(state->pipe_fd[1], "e", 1);
            if(rv != 1) {
                SAH_TRACEZ_ERROR(ME, "write(%d) finished notification failed rv: %d", state->pipe_fd[1], rv);
            }
        }
        if(state->target != NULL) {
            fclose(state->target);
            state->target = NULL;
        }
    }
    curl_easy_cleanup(handle);
    curl_global_cleanup();
    return NULL;
}

static int request_download_test(uint32_t index, UNUSED amxc_var_t* params) {
    int rv = 0;
    ipd_ftx_request_state_t* state = &_on_going_req[ON_GOING_REQ_INDEX(index)];

    state->index = index;
    pthread_create(&state->thread, NULL, ftx_download_routine, state);
    return rv;
}

static void request_download_abort(void) {
    when_null(_on_going_req, exit);
    for(uint32_t i = 0; i < _on_going_nb_req; ++i) {
        if(_on_going_req[i].finished == false) {
            _on_going_req[i].canceled = true;
            SAH_TRACEZ_NOTICE(ME, "Trying to cancel download thread (Request n%d)", i);
            pthread_join(_on_going_req[i].thread, NULL);
        }
    }
    clean_on_going_request();
exit:
    return;
}

static int init_on_going_req(uint32_t nb_req, amxc_var_t* params) {
    int rv = -1;

    _on_going_req = calloc(nb_req, sizeof(*_on_going_req));
    when_null_trace(_on_going_req, exit, ERROR, "Allocation failed");
    for(uint32_t i = 0; i < nb_req; ++i) {
        when_failed_trace(pipe(_on_going_req[i].pipe_fd), exit, ERROR, "Couldn't create pipe");
        connection_cb(_on_going_req[i].pipe_fd[0], true);
        when_failed_trace(amxc_var_new(&_on_going_req[i].params), exit, ERROR, "Allocation failed");
        when_failed_trace(amxc_var_copy(_on_going_req[i].params, params), exit, ERROR, "Allocation while copying failed");
        _on_going_req[i].cb = ftx_reply_handler_cb;
    }
    pthread_mutex_init(&started_download_mutex, NULL);
    _on_going_nb_req = nb_req;
    rv = 0;
exit:
    return rv;
}

int mod_download_test_start(UNUSED const char* function_name,
                            amxc_var_t* params,
                            UNUSED amxc_var_t* ret) {
    int rv = -1;
    uint32_t nb_conn = GET_UINT32(params, "NumberOfConnections");

    when_not_null(_on_going_req, exit);
    started_download = 0;
    when_failed(init_on_going_req((nb_conn == 0) ? 1 : nb_conn, params), exit);
    for(uint32_t i = 1; i <= ((nb_conn == 0) ? 1 : nb_conn); ++i) {
        rv = request_download_test((nb_conn == 0) ? 0 : i, params);
        when_failed_trace(rv, exit, ERROR, "Failed to start download test %d", rv);
    }
exit:
    if(rv != 0) {
        request_download_abort();
    }
    return rv;
}

int mod_download_test_check(UNUSED const char* function_name,
                            UNUSED amxc_var_t* params,
                            UNUSED amxc_var_t* ret) {
    return 0;
}

int mod_download_test_stop(UNUSED const char* function_name,
                           UNUSED amxc_var_t* params,
                           UNUSED amxc_var_t* ret) {
    return 0;
}

int mod_download_test_abort(UNUSED const char* function_name,
                            UNUSED amxc_var_t* params,
                            UNUSED amxc_var_t* ret) {
    request_download_abort();
    return 0;
}
