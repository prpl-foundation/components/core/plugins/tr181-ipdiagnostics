MACHINE = $(shell $(CC) -dumpmachine)

COMMON_SRCDIR = $(realpath ../../../common/src)
SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/mod_coverage)
INCDIR = $(realpath ../../include_priv ../../../common/include_priv ../../../../include_priv)
MOCK_SRCDIR = $(realpath ../common/)

HEADERS = $(wildcard $(INCDIR)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c) \
		  $(wildcard $(MOCK_SRCDIR)/*.c) \
		  $(wildcard $(COMMON_SRCDIR)/*.c)

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
		  --std=gnu99 -fPIC -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
		  -fkeep-inline-functions -fkeep-static-functions \
		   -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread -DUNIT_TEST \
		   -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) \
		   -lamxb -lamxc -lamxd -lamxj -lamxm -lamxo -lamxp -lamxut -ldl -lsahtrace -lpthread -lm

CFLAGS += $(shell pkg-config --cflags libcurl openssl)
LDFLAGS += $(shell pkg-config --libs libcurl openssl)
