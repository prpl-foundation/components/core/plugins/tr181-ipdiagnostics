/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MOD_IPD_IPERF_COMMON_H__)
#define __MOD_IPD_IPERF_COMMON_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdlib.h>
#include "upload_download.h"

#define IPERF_DEFAULT_BLOCK_SIZE 100000000

#define DEFAULT_SET_PATH_FLAGS AMXC_VAR_FLAG_AUTO_ADD | AMXC_VAR_FLAG_COPY

#define SCRIPT_KILL_PROCESS "/usr/lib/amx/scripts/kill_process.sh"

#define IPERF_PATH_FILES "/tmp/"
#define IPERF_DOWNLOAD_PREFIX "iperf-download-"
#define IPERF_UPLOAD_PREFIX "iperf-upload-"

#define IPERF_DOWN_SCRIPT_FILE_PATH IPERF_PATH_FILES IPERF_DOWNLOAD_PREFIX
#define IPERF_DOWN_RESULT_FILE_PATH IPERF_PATH_FILES IPERF_DOWNLOAD_PREFIX "result-"

#define IPERF_UP_SCRIPT_FILE_PATH IPERF_PATH_FILES IPERF_UPLOAD_PREFIX
#define IPERF_UP_RESULT_FILE_PATH IPERF_PATH_FILES IPERF_UPLOAD_PREFIX "result-"

void iperf_replace_htable_uint64(amxc_var_t* hash_table,
                                 const char* const path,
                                 uint64_t value);

void iperf_split_string_lines(amxc_string_t* string, amxc_llist_t* lines);
int iperf_parse_date_string(amxc_var_t* date_var, struct tm* ptm);
int iperf_parse_local_ip_from_line(amxc_string_t* str, const cstring_t protocol_version, amxc_string_t* str_out);
int iperf_calculate_new_ptm(int old_us, int end_int, int end_int_us, struct tm* ptm);
void iperf_convert_ptm_to_var(struct tm* ptm, int microseconds, amxc_var_t* var);

int iperf_is_valid_interval_line(amxc_string_t* line);
uint64_t iperf_parse_test_bytes(amxc_string_t* line);
int iperf_parse_start_interval(amxc_string_t* line);
int iperf_parse_end_interval(amxc_string_t* line);
int iperf_parse_end_interval_microseconds(amxc_string_t* line);

int iperf_get_current_time(amxc_var_t* var);

int iperf_kill_process(const cstring_t process_name);
const cstring_t iperf_get_file_path(ipd_up_down_test_t test, bool is_result_file);

#ifdef __cplusplus
}
#endif

#endif // __MOD_IPD_IPERF_COMMON_H__
