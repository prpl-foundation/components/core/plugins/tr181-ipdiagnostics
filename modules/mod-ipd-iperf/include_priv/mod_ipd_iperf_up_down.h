/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MOD_IPD_IPERF_UP_DOWN_H__)
#define __MOD_IPD_IPERF_UP_DOWN_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdlib.h>
#include <upload_download.h>

#define IPERF_DEFAULT_BLOCK_SIZE 100000000

typedef struct _iperf_intermediate {
    struct tm start_time;
    struct tm end_time;
    uint64_t test_bytes;
    uint64_t total_bytes;
    uint64_t* conn_test_bytes;
} iperf_up_down_intermediate_t;

int iperf_status_error_other(ipd_up_down_test_t test);
int iperf_status_error_internal(ipd_up_down_test_t test);
int iperf_status_not_complete(ipd_up_down_test_t test);
cstring_t iperf_get_url(amxc_var_t* params, ipd_up_down_test_t test);
void iperf_delete_test_script(amxc_var_t* params, ipd_up_down_test_t test);
void iperf_delete_result_file(amxc_var_t* params, ipd_up_down_test_t test);
void iperf_replace_status(amxc_var_t* ret, ipd_up_down_test_t test, int status);
int iperf_kill_iperf_test(amxc_var_t* params, ipd_up_down_test_t test);
int iperf_kill_script_test(amxc_var_t* params, ipd_up_down_test_t test);

int iperf_up_down_test_start(amxc_var_t* params, amxc_var_t* ret, ipd_up_down_test_t test);
int iperf_up_down_test_check(amxc_var_t* params, amxc_var_t* ret, ipd_up_down_test_t test);
int iperf_up_down_test_stop(amxc_var_t* params, amxc_var_t* ret, ipd_up_down_test_t test);
int iperf_up_down_test_abort(amxc_var_t* ret, ipd_up_down_test_t test);



#ifdef __cplusplus
}
#endif

#endif // __MOD_IPD_IPERF_UP_DOWN_H__
