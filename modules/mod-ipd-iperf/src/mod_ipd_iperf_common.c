/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <regex.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>

#include "upload_download.h"
#include "mod_ipd_common.h"
#include "mod_ipd_iperf_common.h"

// module trace zone
#define ME "iperf-c"

#define REGEX_IPV4 "(\\b25[0-5]|\\b2[0-4][0-9]|\\b[01]?[0-9][0-9]?)" \
    "(\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}"

#define REGEX_IPV6 "(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a" \
    "-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-" \
    "9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA" \
    "-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-f" \
    "A-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-" \
    "fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a" \
    "-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F" \
    "]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80" \
    ":(:[0-9a-fA-F]{0,4}){0,4}\\%[0-9a-zA-Z]{1,}|::(fff" \
    "f(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0" \
    "-9]){0,1}[0-9])\\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0" \
    "-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-" \
    "5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}(25[0-" \
    "5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))"

/**
   @brief
   Replace the value of a key in the hash table. uint64 values.

   @param[in] hash_table root hash table.
   @param[in] path Path where the key is present.
   @param[in] value Value to be added, uint64 type.
 */
void iperf_replace_htable_uint64(amxc_var_t* hash_table,
                                 const char* const path,
                                 uint64_t value) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* var;

    when_null_trace(hash_table, exit, ERROR, "Hashtable is NULL.");
    when_null_trace(path, exit, ERROR, "Path string is NULL.");

    amxc_var_new(&var);
    amxc_var_set(uint64_t, var, value);
    amxc_var_set_pathf(hash_table, var, DEFAULT_SET_PATH_FLAGS, "%s", path);
    amxc_var_delete(&var);

exit:
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Split a string in a list with a line for each element.

   @param[in] string Source string containing the information to be splitted.
   @param[out] lines List containing a line for each element.
 */
void iperf_split_string_lines(amxc_string_t* string, amxc_llist_t* lines) {
    SAH_TRACEZ_IN(ME);
    cstring_t cur_line;
    cstring_t next_line;

    when_null_trace(lines, exit, ERROR, "Output list is not initialised.");
    when_null_trace(string, exit, ERROR, "String to be splitted is NULL.");
    when_false_trace(string->length > 0, exit, ERROR, "String to be splitted is empty.");

    cur_line = string->buffer;

    while(cur_line) {
        next_line = strchr(cur_line, '\n');
        if(next_line) {
            *next_line = '\0';
        }

        amxc_llist_add_string(lines, cur_line);

        if(next_line) {
            *next_line = '\n';
        }
        cur_line = next_line ? (next_line + 1) : NULL;
    }

exit:
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Parse the string date and create the tm (Time struct) from this.

   @param[in] date String with the date. For example: 2008-04-09T15:01:05.123456Z
   @param[out] ptm Time structure.

   @return
   -1 if the parsing fails; Otherwise return the microseconds.
 */
int iperf_parse_date_string(amxc_var_t* date_var, struct tm* ptm) {
    int ret_value = -1;
    char year[5];
    char month[3];
    char day[3];
    char hour[3];
    char minute[3];
    char second[3];
    char micro[7];
    cstring_t date = amxc_var_dyncast(cstring_t, date_var);

    when_null_trace(ptm, exit, ERROR, "Output time struct is not initialised.");
    when_false_trace(strlen(date) == 27, exit, ERROR, "Failed to parse the string in the date struct.");

    year[0] = date[0];
    year[1] = date[1];
    year[2] = date[2];
    year[3] = date[3];
    year[4] = '\0';
    month[0] = date[5];
    month[1] = date[6];
    month[2] = '\0';
    day[0] = date[8];
    day[1] = date[9];
    day[2] = '\0';
    hour[0] = date[11];
    hour[1] = date[12];
    hour[2] = '\0';
    minute[0] = date[14];
    minute[1] = date[15];
    minute[2] = '\0';
    second[0] = date[17];
    second[1] = date[18];
    second[2] = '\0';
    micro[0] = date[20];
    micro[1] = date[21];
    micro[2] = date[22];
    micro[3] = date[23];
    micro[4] = date[24];
    micro[5] = date[25];
    micro[6] = '\0';

    ptm->tm_year = atoi(year) - 1900;
    ptm->tm_mon = atoi(month) - 1;
    ptm->tm_mday = atoi(day);
    ptm->tm_hour = atoi(hour);
    ptm->tm_min = atoi(minute);
    ptm->tm_sec = atoi(second);

    ptm->tm_isdst = 0;

    ret_value = atoi(micro);

exit:
    free(date);
    return ret_value;
}

/**
   @brief
   Add seconds and microseconds to an existing ptm.

   @param[in] old_us Old microseconds.
   @param[in] end_int End time in seconds.
   @param[in] end_int_us End time in microseconds.
   @param[out] ptm Time structure.

   @return
   Return the microseconds.
 */
int iperf_calculate_new_ptm(int old_us, int end_int, int end_int_us, struct tm* ptm) {
    int microseconds = old_us;
    int end_interval = end_int;

    microseconds += end_int_us;
    if(microseconds >= 1000000) {
        end_interval++;
        microseconds = microseconds - 1000000;
    }
    ptm->tm_sec += end_interval;
    mktime(ptm);
    return microseconds;
}

/**
   @brief
   Convert ptm to var.

   @param[in] ptm Time struct.
   @param[in] microseconds Microseconds.
   @param[out] var Variant to be returned.
 */
void iperf_convert_ptm_to_var(struct tm* ptm, int microseconds, amxc_var_t* var) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t* aux_string = NULL;

    when_null_trace(var, exit, ERROR, "Output variant is not initialised.");
    when_null_trace(ptm, exit, ERROR, "Time struct is NULL");
    when_false_trace(microseconds >= 0, exit, ERROR, "Microseconds can't be negative.");

    amxc_string_new(&aux_string, 0);

    if(microseconds > 999999) {
        microseconds = 999999;
    }
    amxc_string_setf(aux_string, "%04d-%02d-%02dT%02d:%02d:%02d.%06dZ", 1900 + ptm->tm_year,
                     1 + ptm->tm_mon,
                     ptm->tm_mday,
                     ptm->tm_hour,
                     ptm->tm_min,
                     ptm->tm_sec,
                     microseconds);
    amxc_var_set(cstring_t, var, amxc_string_get(aux_string, 0));

    amxc_string_delete(&aux_string);

exit:
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Parse the local IP from a string.

   @param[in] str amxc_string_t object with the string to be parsed.
   @param[in] protocol_version Protocol version. (Any, IPv4 or IPv6).
   @param[out] str_out amxc_string_t object with the parsed IP

   @return
   0 if the function passes.
 */
int iperf_parse_local_ip_from_line(amxc_string_t* str, const cstring_t protocol_version, amxc_string_t* str_out) {
    amxc_string_t extracted_string;
    amxc_string_t* regex_ip = NULL;
    amxc_string_t* regex = NULL;
    int ret_value = -1;

    amxc_string_init(&extracted_string, 0);
    amxc_string_new(&regex, 0);
    amxc_string_new(&regex_ip, 0);

    when_null_trace(str_out, exit, ERROR, "Output string is not initialised.");
    when_null_trace(str, exit, ERROR, "String to be parsed is NULL.");
    when_false_trace(str->length > 0, exit, ERROR, "String to be parsed is empty.");
    when_str_empty_trace(protocol_version, exit, ERROR, "Protocol version is empty.");

    if(strcmp(protocol_version, "IPv6") == 0) {
        amxc_string_setf(regex_ip, "%s", REGEX_IPV6);
    } else {
        amxc_string_setf(regex_ip, "%s", REGEX_IPV4);
    }
    amxc_string_setf(regex, "local %s", amxc_string_get(regex_ip, 0));

    ret_value = mod_ipd_extract_regex_from_string(str,
                                                  amxc_string_get(regex, 0),
                                                  &extracted_string);
    when_failed_trace(ret_value, exit, ERROR, "Failed to find the pattern in the string.");

    ret_value = mod_ipd_extract_regex_from_string(&extracted_string,
                                                  amxc_string_get(regex_ip, 0),
                                                  &extracted_string);
    when_failed_trace(ret_value, exit, ERROR, "Failed to find the pattern in the string.");

    amxc_string_set(str_out, amxc_string_get(&extracted_string, 0));

exit:
    amxc_string_clean(&extracted_string);
    amxc_string_delete(&regex);
    amxc_string_delete(&regex_ip);
    return ret_value;
}

/**
   @brief
   Check if it's a valid interval.

   @param[in] line Line with the information to be parsed.

   @return
   0 if it's a valid interval line.
 */
int iperf_is_valid_interval_line(amxc_string_t* line) {
    amxc_string_t extracted_string;
    int rv = -1;

    when_null_trace(line, exit, ERROR, "Line is NULL.");
    when_false_trace(line->length > 0, exit, ERROR, "Line is empty.");

    amxc_string_init(&extracted_string, 0);

    rv = mod_ipd_extract_regex_from_string(line,
                                           "\\[.*\\].*[0-9]+\\.[0-9][0-9]-[0-9]+\\.[0-9][0-9]",
                                           &extracted_string);

    amxc_string_clean(&extracted_string);

exit:
    return rv;
}

/**
   @brief
   Parse the test bytes.

   @param[in] line Line with the information to be parsed.

   @return
   Test bytes.
 */
uint64_t iperf_parse_test_bytes(amxc_string_t* line) {
    float float_bytes = 0.0;
    amxc_string_t extracted_string;
    amxc_string_t partial_string;
    uint64_t test_bytes = 0;
    int rv = -1;

    amxc_string_init(&extracted_string, 0);
    amxc_string_init(&partial_string, 0);

    when_null_trace(line, exit, ERROR, "Line is NULL.");
    when_false_trace(line->length > 0, exit, ERROR, "Line is empty.");

    rv = mod_ipd_extract_regex_from_string(line,
                                           "[0-9]+\\.*[0-9]+ [P|T|G|M|K]*Bytes",
                                           &partial_string);
    when_failed(rv, exit);
    rv = mod_ipd_extract_regex_from_string(&partial_string,
                                           "[0-9]+\\.*[0-9]+",
                                           &extracted_string);
    when_failed(rv, exit);
    float_bytes = strtof(amxc_string_get(&extracted_string, 0), NULL);

    mod_ipd_extract_regex_from_string(&partial_string,
                                      "[P|T|G|M|K]*Bytes",
                                      &extracted_string);

    switch(extracted_string.buffer[0]) {
    case 'P':
        test_bytes = (uint64_t) (float_bytes * 1000);
        test_bytes = test_bytes * 1000000000000;
        break;
    case 'T':
        test_bytes = (uint64_t) (float_bytes * 1000);
        test_bytes = test_bytes * 1000000000;
        break;
    case 'G':
        test_bytes = (uint64_t) (float_bytes * 1000);
        test_bytes = test_bytes * 1000000;
        break;
    case 'M':
        test_bytes = (uint64_t) (float_bytes * 1000);
        test_bytes = test_bytes * 1000;
        break;
    case 'K':
        test_bytes = (uint64_t) (float_bytes * 1000);
        break;
    case 'B':
    default:
        test_bytes = (uint64_t) float_bytes;
        break;
    }

exit:
    amxc_string_clean(&extracted_string);
    amxc_string_clean(&partial_string);
    return test_bytes;
}

/**
   @brief
   Parse the start interval.

   @param[in] line Line with the information to be parsed.

   @return
   Start interval.
 */
int iperf_parse_start_interval(amxc_string_t* line) {
    amxc_string_t extracted_string;
    int start_interval = 0;
    int rv = -1;

    amxc_string_init(&extracted_string, 0);

    when_null_trace(line, exit, ERROR, "Line is NULL.");
    when_false_trace(line->length > 0, exit, ERROR, "Line is empty.");

    rv = mod_ipd_extract_regex_from_string(line,
                                           "[0-9]+\\.[0-9][0-9]-",
                                           &extracted_string);
    when_failed(rv, exit);
    rv = mod_ipd_extract_regex_from_string(&extracted_string,
                                           "[0-9]+",
                                           &extracted_string);
    when_failed(rv, exit);
    start_interval = atoi(amxc_string_get(&extracted_string, 0));
exit:
    amxc_string_clean(&extracted_string);
    return start_interval;
}

/**
   @brief
   Parse the end interval.

   @param[in] line Line with the information to be parsed.

   @return
   End interval.
 */
int iperf_parse_end_interval(amxc_string_t* line) {
    amxc_string_t extracted_string;
    int end_interval = 0;
    int rv = -1;

    amxc_string_init(&extracted_string, 0);

    when_null_trace(line, exit, ERROR, "Line is NULL.");
    when_false_trace(line->length > 0, exit, ERROR, "Line is empty.");

    rv = mod_ipd_extract_regex_from_string(line,
                                           "-[0-9]+\\.[0-9][0-9]",
                                           &extracted_string);
    when_failed(rv, exit);
    rv = mod_ipd_extract_regex_from_string(&extracted_string,
                                           "[0-9]+",
                                           &extracted_string);
    when_failed(rv, exit);
    end_interval = atoi(amxc_string_get(&extracted_string, 0));
exit:
    amxc_string_clean(&extracted_string);
    return end_interval;
}

/**
   @brief
   Parse the end interval microseconds.

   @param[in] line Line with the information to be parsed.

   @return
   End interval microseconds.
 */
int iperf_parse_end_interval_microseconds(amxc_string_t* line) {
    amxc_string_t extracted_string;
    amxc_string_t partial_string;
    int end_interval_microseconds = 0;
    int rv = -1;

    amxc_string_init(&extracted_string, 0);
    amxc_string_init(&partial_string, 0);

    when_null_trace(line, exit, ERROR, "Line is NULL.");
    when_false_trace(line->length > 0, exit, ERROR, "Line is empty.");

    rv = mod_ipd_extract_regex_from_string(line,
                                           "-[0-9]+\\.[0-9][0-9]",
                                           &partial_string);
    when_failed(rv, exit);
    rv = mod_ipd_extract_regex_from_string(&partial_string,
                                           "\\.[0-9][0-9]",
                                           &extracted_string);
    when_failed(rv, exit);
    rv = mod_ipd_extract_regex_from_string(&extracted_string,
                                           "[0-9]+",
                                           &extracted_string);
    when_failed(rv, exit);
    end_interval_microseconds = atoi(amxc_string_get(&extracted_string, 0));
    end_interval_microseconds = end_interval_microseconds * 10000;
exit:
    amxc_string_clean(&extracted_string);
    amxc_string_clean(&partial_string);
    return end_interval_microseconds;
}

/**
   @brief
   Get the current time.

   @param[out] var Variant with the time.

   @return
   Not 0 if the function fails.
 */
int iperf_get_current_time(amxc_var_t* var) {
    amxc_string_t* str = NULL;
    struct tm* ptm = NULL;
    time_t rawtime;
    struct timespec ts;
    int microseconds = 0;
    int rv = -1;

    amxc_string_new(&str, 0);

    when_null_trace(var, exit, ERROR, "Output variant is not initialised.");

    rawtime = time(NULL);
    when_false_trace(rawtime != -1, exit, ERROR, "The time() function failed.");
    ptm = gmtime(&rawtime);
    when_null_trace(ptm, exit, ERROR, "The localtime() function failed.");
    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    microseconds = (int) (ts.tv_nsec / 1000);
    amxc_string_setf(str, "%04d-%02d-%02dT%02d:%02d:%02d.%06dZ", 1900 + ptm->tm_year,
                     1 + ptm->tm_mon,
                     ptm->tm_mday,
                     ptm->tm_hour,
                     ptm->tm_min,
                     ptm->tm_sec,
                     microseconds);
    amxc_var_set(cstring_t, var, amxc_string_get(str, 0));

    rv = 0;
exit:
    amxc_string_delete(&str);
    return rv;
}

/**
   @brief
   Kill a specific process.

   @param[in] process_name Input parameters. NULL will kill all the scripts.

   @return
   Return of the kill command.
 */
int iperf_kill_process(const cstring_t process_name) {
    int rv = -1;
    amxp_subproc_t* proc = NULL;

    rv = amxp_subproc_new(&proc);
    when_failed_trace(rv, exit, ERROR, "Failed to initialize the subprocess.");

    when_str_empty_trace(process_name, exit, ERROR, "Process has an empty string name.");
    when_failed_trace(access(SCRIPT_KILL_PROCESS, F_OK), exit, ERROR, "Script '%s' doesn't exist.", SCRIPT_KILL_PROCESS);

    rv = amxp_subproc_start(proc, (char*) "sh", SCRIPT_KILL_PROCESS, process_name, NULL);
    when_failed_trace(rv, exit, ERROR, "Failed to run the shell script.");

    amxp_subproc_wait(proc, 1000);
    amxp_subproc_kill(proc, SIGKILL);

exit:
    amxp_subproc_delete(&proc);
    return rv;
}

const cstring_t iperf_get_file_path(ipd_up_down_test_t test, bool is_result_file) {
    if(test == ipd_up_down_download) {
        if(is_result_file) {
            return IPERF_DOWN_RESULT_FILE_PATH;
        } else {
            return IPERF_DOWN_SCRIPT_FILE_PATH;
        }
    } else {
        if(is_result_file) {
            return IPERF_UP_RESULT_FILE_PATH;
        } else {
            return IPERF_UP_SCRIPT_FILE_PATH;
        }
    }
}