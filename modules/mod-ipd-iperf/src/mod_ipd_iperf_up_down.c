/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_ipd_common.h"
#include "mod_ipd_iperf_common.h"
#include "mod_ipd_iperf_up_down.h"
#include "upload.h"
#include "download.h"

// module trace zone
#define ME "iperf-cup"

/**
   @brief
   Return the status Error_Other according to the test type.

   @param[in] test download or upload.

   @return
   Return the status.
 */
int iperf_status_error_other(ipd_up_down_test_t test) {
    int status = 1;
    if(test == ipd_up_down_download) {
        status = down_test_error_other;
    } else if(test == ipd_up_down_upload) {
        status = up_test_error_other;
    }
    return status;
}

/**
   @brief
   Return the status Error_Internal according to the test type.

   @param[in] test download or upload.

   @return
   Return the status.
 */
int iperf_status_error_internal(ipd_up_down_test_t test) {
    int status = 1;
    if(test == ipd_up_down_download) {
        status = down_test_error_internal;
    } else if(test == ipd_up_down_upload) {
        status = up_test_error_internal;
    }
    return status;
}

/**
   @brief
   Return the status Not_Complete according to the test type.

   @param[in] test download or upload.

   @return
   Return the status.
 */
int iperf_status_not_complete(ipd_up_down_test_t test) {
    int status = 1;
    if(test == ipd_up_down_download) {
        status = down_test_not_complete;
    } else if(test == ipd_up_down_upload) {
        status = up_test_not_complete;
    }
    return status;
}

/**
   @brief
   Return the DownloadURL or UploadURL according to the test type.

   @param[in] params Input parameters.
   @param[in] test download or upload.

   @return
   Return of the URL string.
 */
cstring_t iperf_get_url(amxc_var_t* params, ipd_up_down_test_t test) {
    cstring_t url = NULL;

    when_null_trace(params, exit, ERROR, "Input parameter is NULL.");

    if(test == ipd_up_down_download) {
        url = (cstring_t) GETP_CHAR(params, UP_DOWN_RET_KEY_CFG "." DOWN_CFG_KEY_DOWNLOAD_URL);
    } else if(test == ipd_up_down_upload) {
        url = (cstring_t) GETP_CHAR(params, UP_DOWN_RET_KEY_CFG "." UP_CFG_KEY_UPLOAD_URL);
    }
exit:
    return url;
}

/**
   @brief
   Delete the test script according to the test type.

   @param[in] params Input parameters. If NULL all the files will be deleted.
   @param[in] test download or upload.
 */
void iperf_delete_test_script(amxc_var_t* params, ipd_up_down_test_t test) {
    SAH_TRACEZ_IN(ME);
    int rv = 0;
    const uint32_t result_number = GETP_UINT32(params, UP_DOWN_RET_KEY_CFG "." UP_DOWN_CFG_KEY_RESULT_NUMBER);
    amxc_string_t* command = NULL;
    amxp_subproc_t* rm_proc = NULL;

    amxc_string_new(&command, 0);
    amxp_subproc_new(&rm_proc);

    if(params == NULL) {
        amxc_string_setf(command, "%s*", iperf_get_file_path(test, false));
        rv = amxp_subproc_start(rm_proc, (char*) "rm", "-rf", amxc_string_get(command, 0), NULL);
        when_failed_trace(rv, exit, ERROR, "Failed to launch the subprocess");
        when_failed_trace(amxp_subproc_get_exitstatus(rm_proc), exit, ERROR, "Unable to delete files");
    } else {
        mod_ipd_delete_filef("%s%u.sh", iperf_get_file_path(test, false), result_number);
    }

exit:
    amxp_subproc_delete(&rm_proc);
    amxc_string_delete(&command);
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Delete the result file according to the test type.

   @param[in] params Input parameters.
   @param[in] test download or upload.
 */
void iperf_delete_result_file(amxc_var_t* params, ipd_up_down_test_t test) {
    SAH_TRACEZ_IN(ME);
    int rv = 0;
    const uint32_t result_number = GETP_UINT32(params, UP_DOWN_RET_KEY_CFG "." UP_DOWN_CFG_KEY_RESULT_NUMBER);
    amxc_string_t* command = NULL;
    amxp_subproc_t* rm_proc = NULL;

    amxc_string_new(&command, 0);
    amxp_subproc_new(&rm_proc);

    if(params == NULL) {
        amxc_string_setf(command, "%s*", iperf_get_file_path(test, true));
        rv = amxp_subproc_start(rm_proc, (char*) "rm", "-rf", amxc_string_get(command, 0), NULL);
        when_failed_trace(rv, exit, ERROR, "Failed to launch the subprocess");
        when_failed_trace(amxp_subproc_get_exitstatus(rm_proc), exit, ERROR, "Unable to delete files");
    } else {
        mod_ipd_delete_filef("%s%u", iperf_get_file_path(test, true), result_number);
    }

exit:
    amxp_subproc_delete(&rm_proc);
    amxc_string_delete(&command);
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Replace the Status on the variant.

   @param[in] ret Return variant.
   @param[in] test download or upload.
   @param[in] status Status number to be converted to string and added to the variant.
 */
void iperf_replace_status(amxc_var_t* ret, ipd_up_down_test_t test, int status) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* var;

    amxc_var_new(&var);

    when_null_trace(ret, exit, ERROR, "Return variant is NULL.");

    if(test == ipd_up_down_download) {
        amxc_var_set(cstring_t, var, (cstring_t) DOWNLOAD_TEST_STATUS(status));
    } else if(test == ipd_up_down_upload) {
        amxc_var_set(cstring_t, var, (cstring_t) UPLOAD_TEST_STATUS(status));
    }
    amxc_var_set_pathf(ret, var, DEFAULT_SET_PATH_FLAGS, UP_DOWN_RET_KEY_STATUS);


exit:
    amxc_var_delete(&var);
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Kill a specific iperf process.

   @param[in] params Input parameters. NULL will kill all the tests.
   @param[in] test download or upload.

   @return
   Return of the kill command.
 */
int iperf_kill_iperf_test(amxc_var_t* params, ipd_up_down_test_t test) {
    int rv = 0;
    cstring_t url;
    const cstring_t e_str = "";
    amxc_string_t* aux_string = NULL;

    amxc_string_new(&aux_string, 0);

    url = iperf_get_url(params, test);
    if(url == NULL) {
        url = (cstring_t) e_str;
    }
    if(test == ipd_up_down_download) {
        amxc_string_setf(aux_string, "iperf3\\ -c\\ %s.*-R", url);
    } else if(test == ipd_up_down_upload) {
        amxc_string_setf(aux_string, "iperf3\\ -c\\ %s.*--forceflush\\ --connect-timeout", url);
    }
    amxc_string_replace(aux_string, ":", " -p ", UINT32_MAX);
    rv = iperf_kill_process(amxc_string_get(aux_string, 0));

    amxc_string_delete(&aux_string);
    return rv;
}

/**
   @brief
   Kill a specific script process.

   @param[in] params Input parameters. NULL will kill all the scripts.
   @param[in] test download or upload.

   @return
   Return of the kill command.
 */
int iperf_kill_script_test(amxc_var_t* params, ipd_up_down_test_t test) {
    int rv = -1;
    amxc_string_t* aux_string = NULL;
    const uint32_t result_number = GETP_UINT32(params, UP_DOWN_RET_KEY_CFG "." UP_DOWN_CFG_KEY_RESULT_NUMBER);

    amxc_string_new(&aux_string, 0);

    amxc_string_setf(aux_string, "%s%u.sh", iperf_get_file_path(test, false), result_number);
    rv = iperf_kill_process(amxc_string_get(aux_string, 0));
    when_failed_trace(rv, exit, ERROR, "Failed to kill the process with name: '%s'", amxc_string_get(aux_string, 0));

exit:
    amxc_string_delete(&aux_string);
    return rv;
}