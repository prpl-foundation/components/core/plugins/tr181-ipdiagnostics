/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>

#include "mod_ipd_common.h"
#include "mod_ipd_iperf_common.h"
#include "mod_ipd_iperf_up_down.h"
#include "upload.h"
#include "download.h"

// module trace zone
#define ME "iperf-cup-check"

/**
   @brief
   Read the information from the result file and save to a string.

   @param[in] params Input parameters.
   @param[in] test download or upload.
   @param[out] string string containing the information of the file appended.
 */
static void iperf_read_result_file(amxc_var_t* params, ipd_up_down_test_t test,
                                   amxc_string_t* string) {
    amxc_string_t* result_file = NULL;
    const uint32_t result_number = GETP_UINT32(params, UP_DOWN_RET_KEY_CFG "." UP_DOWN_CFG_KEY_RESULT_NUMBER);

    amxc_string_new(&result_file, 0);

    amxc_string_setf(result_file, "%s%u", iperf_get_file_path(test, true), result_number);
    mod_ipd_read_from_file(amxc_string_get(result_file, 0), string);

    amxc_string_delete(&result_file);
}

/**
   @brief
   Write the information from a string and save to the result file.

   @param[in] params Input parameters.
   @param[in] test download or upload.
   @param[out] string string containing the information to be written.
 */
static void iperf_write_result_file(amxc_var_t* params, ipd_up_down_test_t test,
                                    amxc_string_t* string) {
    amxc_string_t* result_file = NULL;
    const uint32_t result_number = GETP_UINT32(params, UP_DOWN_RET_KEY_CFG "." UP_DOWN_CFG_KEY_RESULT_NUMBER);

    amxc_string_new(&result_file, 0);

    amxc_string_setf(result_file, "%s%u", iperf_get_file_path(test, true), result_number);
    mod_ipd_write_to_file(amxc_string_get(result_file, 0), string);

    amxc_string_delete(&result_file);
}

/**
   @brief
   Parse the status from the string.
   Based on known messages from the iperf3 process, infer the download status.

   @param[in] stdout_lines Linked list of lines.
   @param[in] test download or upload.

   @return
   Test status based on the enum download_test_status_t and upload_test_status_t
 */
static int iperf_parse_status(amxc_llist_t* stdout_lines, ipd_up_down_test_t test) {
    int status = 0;
    int num_of_lines = amxc_llist_size(stdout_lines);
    amxc_string_t* line = NULL;

    //Set the status to Not_Complete
    status = iperf_status_not_complete(test);

    when_false(num_of_lines, exit);

    line = amxc_string_get_from_llist(stdout_lines, num_of_lines - 1);

    if(amxc_string_search(line, "iperf3:", 0) != -1) {
        //Set the status to Error_Internal
        status = iperf_status_error_internal(test);
        if(amxc_string_search(line, "unable to send control message", 0) != -1) {
            if(test == ipd_up_down_download) {
                status = down_test_error_no_response;
            } else if(test == ipd_up_down_upload) {
                status = up_test_error_no_response;
            }
        } else if(amxc_string_search(line, "iperf3: interrupt", 0) != -1) {
            if(test == ipd_up_down_download) {
                status = down_test_error_transfer_failed;
            } else if(test == ipd_up_down_upload) {
                status = up_test_error_no_transfer_complete;
            }
        }
    } else {
        if(amxc_string_search(line, "iperf Done", 0) != -1) {
            status = 0;
        }
    }

exit:
    return status;
}

/**
   @brief
   Parse local IP from the standard output string.

   @param[in] ret Return variant.
   @param[in] stdout_lines Linked list of lines.
   @param[in] line_start Start of the list to be parsed.

   @return
   Parse status. 0 if it passes;
 */
static int iperf_parse_local_ip(amxc_var_t* ret, amxc_llist_t* stdout_lines, int line_start) {
    int rv = -1;
    int ret_value = -1;
    int line_num = line_start;
    int num_of_lines;
    amxc_string_t* aux_string = NULL;
    amxc_string_t* line = NULL;
    amxc_var_t aux_var;
    const cstring_t protocol_version = GETP_CHAR(ret, UP_DOWN_RET_KEY_CFG "."UP_DOWN_CFG_KEY_PROTOCOL_VERSION);

    amxc_string_new(&aux_string, 0);
    amxc_var_init(&aux_var);

    num_of_lines = amxc_llist_size(stdout_lines);
    when_false(num_of_lines, exit);

    for(; line_num < num_of_lines; line_num++) {
        line = amxc_string_get_from_llist(stdout_lines, line_num);
        ret_value = amxc_string_search(line, "local", 0);
        if(ret_value != -1) {
            break;
        }
    }
    when_false((ret_value != -1) && (line_num < num_of_lines), exit);
    line = amxc_string_get_from_llist(stdout_lines, line_num);
    ret_value = iperf_parse_local_ip_from_line(line, protocol_version, aux_string);
    when_failed(ret_value, exit);

    amxc_var_set(cstring_t, &aux_var, (cstring_t) amxc_string_get(aux_string, 0));
    amxc_var_set_pathf(ret, &aux_var, DEFAULT_SET_PATH_FLAGS, UP_DOWN_RET_KEY_IP_USED);

    rv = 0;

exit:
    amxc_string_delete(&aux_string);
    amxc_var_clean(&aux_var);
    return rv;
}

/**
   @brief
   Parse incremental result from the standard output string.

   @param[in] ret Return variant.
   @param[in] line Line to be parsed.
   @param[in] test download or upload.
   @param[out] intervals Intervals.
   @param[out] num_of_intervals Number of intervals.
   @param[out] microseconds Current microseconds measure.
   @param[out] period_full_loading Full period of the test.
   @param[out] ptm Time structure.

   @return
   Parse status. 0 if it passes;
 */
static int iperf_parse_interval(amxc_var_t* ret, amxc_string_t* line,
                                ipd_up_down_test_t test, iperf_up_down_intermediate_t** intervals,
                                uint16_t* num_of_intervals, int* microseconds, uint64_t* period_full_loading,
                                struct tm* ptm) {
    int rv = -1;
    int ret_value = -1;
    iperf_up_down_intermediate_t* new_intervals = NULL;
    amxc_var_t* incremental_result = NULL;
    amxc_var_t* incremental_result_index = NULL;
    amxc_string_t* aux_string = NULL;
    amxc_var_t* end_time;
    int start_interval = 0;
    int end_interval_microseconds = 0;
    int end_interval = 0;
    uint64_t prev_test_bytes = 0;
    uint64_t test_bytes = 0;
    uint64_t total_bytes_recv = 0;
    uint64_t total_bytes_sent = 0;
    amxc_var_t aux_var;

    const uint32_t number_of_connections = GETP_UINT32(ret, UP_DOWN_RET_KEY_CFG "."UP_DOWN_CFG_KEY_NUM_OF_CONNECTIONS);
    amxc_var_t* bom_time = GET_ARG(ret, UP_DOWN_RET_KEY_BOM_TIME);

    amxc_string_new(&aux_string, 0);
    amxc_var_init(&aux_var);

    ret_value = iperf_is_valid_interval_line(line);
    when_failed(ret_value, exit);

    (*num_of_intervals)++;
    new_intervals = (iperf_up_down_intermediate_t*) realloc(*intervals, (*num_of_intervals) * sizeof(iperf_up_down_intermediate_t));
    if(new_intervals == NULL) {
        //  free(*intervals);
        goto exit;
    }
    *intervals = new_intervals;
    (*intervals)[*num_of_intervals - 1].start_time = *ptm;

    // Check if the interval is already registered
    incremental_result = amxc_var_get_key(ret, UP_DOWN_RET_KEY_INCR_RES, AMXC_VAR_FLAG_DEFAULT);
    incremental_result_index = amxc_var_get_pathf(incremental_result, AMXC_VAR_FLAG_DEFAULT, "%u", *num_of_intervals);
    if(incremental_result_index == NULL) {
        amxc_string_setf(aux_string, "%d", *num_of_intervals);
        incremental_result_index = amxc_var_add_key(amxc_htable_t, incremental_result, amxc_string_get(aux_string, 0), NULL);

        start_interval = iperf_parse_start_interval(line);
        end_interval = iperf_parse_end_interval(line);
        end_interval_microseconds = iperf_parse_end_interval_microseconds(line);

        test_bytes = iperf_parse_test_bytes(line);
        if(*num_of_intervals > 1) {
            prev_test_bytes = (*intervals)[*num_of_intervals - 2].total_bytes;
        }
        if(test == ipd_up_down_download) {
            total_bytes_recv = prev_test_bytes + test_bytes;
        } else if(test == ipd_up_down_upload) {
            total_bytes_sent = prev_test_bytes + test_bytes;
        }

        iperf_convert_ptm_to_var(ptm, *microseconds, &aux_var);

        amxc_var_set_pathf(incremental_result_index, &aux_var, DEFAULT_SET_PATH_FLAGS, UP_DOWN_RET_KEY_INCR_RES_START_TIME);
        if(test == ipd_up_down_download) {
            amxc_var_add_key(uint64_t, incremental_result_index, DOWN_RET_KEY_INCR_RES_TEST_RCVD, test_bytes);
            iperf_replace_htable_uint64(ret, DOWN_RET_KEY_TEST_RCVD, total_bytes_recv);
            iperf_replace_htable_uint64(ret, DOWN_RET_KEY_TEST_RCVD_FULL, total_bytes_recv);
        } else if(test == ipd_up_down_upload) {
            amxc_var_add_key(uint64_t, incremental_result_index, UP_RET_KEY_INCR_RES_TEST_SENT, test_bytes);
            iperf_replace_htable_uint64(ret, UP_RET_KEY_TEST_SENT, total_bytes_sent);
            iperf_replace_htable_uint64(ret, UP_RET_KEY_TEST_SENT_FULL, total_bytes_sent);
        }
        amxc_var_add_key(uint64_t, incremental_result_index, UP_DOWN_RET_KEY_INCR_RES_TOTAL_RCVD, total_bytes_recv);
        amxc_var_add_key(uint64_t, incremental_result_index, UP_DOWN_RET_KEY_INCR_RES_TOTAL_SENT, total_bytes_sent);
        iperf_replace_htable_uint64(ret, UP_DOWN_RET_KEY_TOTAL_SENT, total_bytes_sent);
        iperf_replace_htable_uint64(ret, UP_DOWN_RET_KEY_TOTAL_SENT_FULL, total_bytes_sent);
        iperf_replace_htable_uint64(ret, UP_DOWN_RET_KEY_TOTAL_RCVD, total_bytes_recv);
        iperf_replace_htable_uint64(ret, UP_DOWN_RET_KEY_TOTAL_RCVD_FULL, total_bytes_recv);

        //Calculate the precise end time
        iperf_parse_date_string(bom_time, ptm);
        *microseconds = iperf_calculate_new_ptm(*microseconds, end_interval, end_interval_microseconds, ptm);
        iperf_convert_ptm_to_var(ptm, *microseconds, &aux_var);

        amxc_var_set_pathf(incremental_result_index, &aux_var, DEFAULT_SET_PATH_FLAGS, UP_DOWN_RET_KEY_INCR_RES_END_TIME);
        amxc_var_set_pathf(ret, &aux_var, DEFAULT_SET_PATH_FLAGS, UP_DOWN_RET_KEY_EOM_TIME);

        (*period_full_loading) += (end_interval - start_interval) * 1000000;
        iperf_replace_htable_uint64(ret, UP_DOWN_RET_KEY_PERIOD_FULL, *period_full_loading);
    } else {
        // Else just take the data from the variant (from the previous parsing)
        if(test == ipd_up_down_download) {
            test_bytes = amxc_var_dyncast(uint64_t, GETP_ARG(incremental_result_index, DOWN_RET_KEY_INCR_RES_TEST_RCVD));
            total_bytes_recv = amxc_var_dyncast(uint64_t, GETP_ARG(incremental_result_index, UP_DOWN_RET_KEY_INCR_RES_TOTAL_RCVD));
        } else if(test == ipd_up_down_upload) {
            test_bytes = amxc_var_dyncast(uint64_t, GETP_ARG(incremental_result_index, UP_RET_KEY_INCR_RES_TEST_SENT));
            total_bytes_sent = amxc_var_dyncast(uint64_t, GETP_ARG(incremental_result_index, UP_DOWN_RET_KEY_INCR_RES_TOTAL_SENT));
        }
        end_time = GET_ARG(incremental_result_index, UP_DOWN_RET_KEY_INCR_RES_END_TIME);
        *microseconds = iperf_parse_date_string(end_time, ptm);
    }

    (*intervals)[*num_of_intervals - 1].test_bytes = test_bytes;
    (*intervals)[*num_of_intervals - 1].conn_test_bytes = calloc(number_of_connections, sizeof(uint64_t));
    (*intervals)[*num_of_intervals - 1].end_time = *ptm;
    if(test == ipd_up_down_download) {
        (*intervals)[*num_of_intervals - 1].total_bytes = total_bytes_recv;
    } else if(test == ipd_up_down_upload) {
        (*intervals)[*num_of_intervals - 1].total_bytes = total_bytes_sent;
    }

    rv = 0;

exit:
    amxc_string_delete(&aux_string);
    amxc_var_clean(&aux_var);
    return rv;
}

/**
   @brief
   Parse incremental result from the standard output string.

   @param[in] ret Return variant.
   @param[in] stdout_lines Linked list of lines.
   @param[in] test download or upload.
   @param[in] line_start Start of the list to be parsed.
   @param[out] intervals Intervals.
   @param[out] num_of_intervals Number of intervals.

   @return
   Parse status. 0 if it passes;
 */
static int iperf_parse_incremental_result(amxc_var_t* ret, amxc_llist_t* stdout_lines, int line_start,
                                          ipd_up_down_test_t test, iperf_up_down_intermediate_t** intervals,
                                          uint16_t* num_of_intervals) {
    int rv = -1;
    int ret_value = -1;
    amxc_var_t* incremental_result = NULL;
    amxc_string_t aux_string;
    amxc_string_t* line = NULL;
    int line_num = line_start;
    int num_of_lines;
    struct tm ptm;
    int microseconds;
    uint64_t test_bytes = 0;
    amxc_var_t aux_var;

    const uint32_t number_of_connections = GETP_UINT32(ret, UP_DOWN_RET_KEY_CFG "."UP_DOWN_CFG_KEY_NUM_OF_CONNECTIONS);
    amxc_var_t* bom_time = GET_ARG(ret, UP_DOWN_RET_KEY_BOM_TIME);
    uint64_t period_full_loading = (uint64_t) amxc_var_dyncast(uint64_t, GETP_ARG(ret, UP_DOWN_RET_KEY_PERIOD_FULL));

    microseconds = iperf_parse_date_string(bom_time, &ptm);

    amxc_string_init(&aux_string, 0);
    amxc_var_init(&aux_var);

    when_false_trace(microseconds >= 0, exit, ERROR, "Failed to parse the BOM time.");

    num_of_lines = amxc_llist_size(stdout_lines);
    when_false(num_of_lines, exit);

    /*Find beginning of the intervals*/
    for(; line_num < num_of_lines; line_num++) {
        line = amxc_string_get_from_llist(stdout_lines, line_num);
        ret_value = mod_ipd_extract_regex_from_string(line,
                                                      "\\[.*ID\\].*Interval",
                                                      &aux_string);
        if(ret_value == 0) {
            line_num++;
            break;
        }
    }
    when_false((ret_value == 0) && (line_num < num_of_lines), exit);

    // Create IncrementalResult in case it's not created
    incremental_result = amxc_var_get_key(ret, UP_DOWN_RET_KEY_INCR_RES, AMXC_VAR_FLAG_DEFAULT);
    if(incremental_result == NULL) {
        amxc_var_add_key(amxc_htable_t, ret, UP_DOWN_RET_KEY_INCR_RES, NULL);
    }

    for(; line_num < num_of_lines; line_num++) {
        line = amxc_string_get_from_llist(stdout_lines, line_num);
        if(amxc_string_search(line, "Test Complete", 0) != -1) {
            break;
        }

        if(number_of_connections > 1) {
            if((line_num + (int) number_of_connections) < num_of_lines) {
                line_num = line_num + number_of_connections;
            } else {
                break;
            }
        }

        line = amxc_string_get_from_llist(stdout_lines, line_num);
        //Parse interval
        ret_value = iperf_parse_interval(ret, line, test, intervals, num_of_intervals,
                                         &microseconds, &period_full_loading, &ptm);
        if(ret_value != 0) {
            break;
        }

        if(number_of_connections > 1) {
            for(int conn = 0; conn < (int) number_of_connections; conn++) {
                line = amxc_string_get_from_llist(stdout_lines, line_num - number_of_connections + conn);
                ret_value = iperf_is_valid_interval_line(line);
                if(ret_value != 0) {
                    break;
                }

                test_bytes = iperf_parse_test_bytes(line);
                (*intervals)[*num_of_intervals - 1].conn_test_bytes[conn] = test_bytes;
            }
            line_num++;
        } else {
            (*intervals)[*num_of_intervals - 1].conn_test_bytes[0] = (*intervals)[*num_of_intervals - 1].test_bytes;
        }
    }
    rv = 0;

exit:
    amxc_string_clean(&aux_string);
    amxc_var_clean(&aux_var);
    return rv;
}

/**
   @brief
   Parse the final results.

   @param[in] ret Return variant.
   @param[in] num_of_intervals Number of intervals.
   @param[in] intervals Intervals.
   @param[in] test download or upload.

   @return
   0 if the function passes.
 */
static void iperf_parse_final_results(amxc_var_t* ret, uint16_t num_of_intervals,
                                      iperf_up_down_intermediate_t* intervals, ipd_up_down_test_t test) {
    amxc_var_t* per_conn_result = NULL;
    amxc_var_t* per_conn_result_index = NULL;
    amxc_var_t* bom_time = GET_ARG(ret, UP_DOWN_RET_KEY_BOM_TIME);
    amxc_string_t* aux_string = NULL;
    uint64_t total_bytes;
    amxc_var_t* eom_time = GET_ARG(ret, UP_DOWN_RET_KEY_EOM_TIME);
    const uint32_t number_of_connections = GETP_UINT32(ret, UP_DOWN_RET_KEY_CFG "."UP_DOWN_CFG_KEY_NUM_OF_CONNECTIONS);

    amxc_string_new(&aux_string, 0);

    // Parse per Connection result
    // Create PerConnectionResult in case it's not created
    per_conn_result = amxc_var_get_key(ret, UP_DOWN_RET_KEY_PER_CONN_RES, AMXC_VAR_FLAG_DEFAULT);
    if(per_conn_result == NULL) {
        per_conn_result = amxc_var_add_key(amxc_htable_t, ret, UP_DOWN_RET_KEY_PER_CONN_RES, NULL);
    }
    for(int conn = 0; conn < (int) number_of_connections; conn++) {
        total_bytes = 0;
        for(int index = 0; index < num_of_intervals; index++) {
            total_bytes += intervals[index].conn_test_bytes[conn];
        }
        // Check if the interval is already registered
        amxc_string_setf(aux_string, "%d", conn + 1);
        per_conn_result_index = amxc_var_get_pathf(per_conn_result, AMXC_VAR_FLAG_DEFAULT, "%u", conn + 1);
        if(per_conn_result_index == NULL) {
            per_conn_result_index = amxc_var_add_key(amxc_htable_t, per_conn_result, amxc_string_get(aux_string, 0), NULL);
            amxc_var_set_pathf(per_conn_result_index, bom_time, DEFAULT_SET_PATH_FLAGS, "%s", UP_DOWN_RET_KEY_PER_CONN_BOM_TIME);
            amxc_var_set_pathf(per_conn_result_index, bom_time, DEFAULT_SET_PATH_FLAGS, "%s", UP_DOWN_RET_KEY_PER_CONN_TCP_REQ_TIME);
            amxc_var_set_pathf(per_conn_result_index, bom_time, DEFAULT_SET_PATH_FLAGS, "%s", UP_DOWN_RET_KEY_PER_CONN_TCP_RESP_TIME);
            amxc_var_set_pathf(per_conn_result_index, eom_time, DEFAULT_SET_PATH_FLAGS, "%s", UP_DOWN_RET_KEY_PER_CONN_EOM_TIME);
            if(test == ipd_up_down_download) {
                amxc_var_add_key(uint64_t, per_conn_result_index, DOWN_RET_KEY_PER_CONN_TEST_RCVD, total_bytes);
                amxc_var_add_key(uint64_t, per_conn_result_index, UP_DOWN_RET_KEY_PER_CONN_TOTAL_RCVD, total_bytes);
                amxc_var_add_key(uint64_t, per_conn_result_index, UP_DOWN_RET_KEY_PER_CONN_TOTAL_SENT, 0);
            } else if(test == ipd_up_down_upload) {
                amxc_var_add_key(uint64_t, per_conn_result_index, UP_RET_KEY_PER_CONN_TEST_SENT, total_bytes);
                amxc_var_add_key(uint64_t, per_conn_result_index, UP_DOWN_RET_KEY_PER_CONN_TOTAL_SENT, total_bytes);
                amxc_var_add_key(uint64_t, per_conn_result_index, UP_DOWN_RET_KEY_PER_CONN_TOTAL_RCVD, 0);
            }
        }
    }
    amxc_string_delete(&aux_string);
}

/**
   @brief
   Parse result from the standard output string.

   @param[in] str amxc_string_t object with the string to be parsed.
   @param[out] ret Variant with the datamodel format.
   @param[in] test download or upload.

   @return
   Test status based on the test type
 */
static int iperf_parse_result(amxc_string_t* str_stdout, amxc_var_t* ret,
                              ipd_up_down_test_t test) {
    int status = 0;
    amxc_llist_t stdout_lines;
    int line_num = 0;
    int ret_value = -1;
    int num_of_lines;
    uint16_t num_of_intervals = 0;
    iperf_up_down_intermediate_t* intervals = NULL;
    uint32_t size_of_test;
    uint64_t total_bytes = 0;

    const uint32_t time_based_test_duration = GETP_UINT32(ret, UP_DOWN_RET_KEY_CFG "."UP_DOWN_CFG_KEY_TIME_TEST_DURATION);

    //Set the status to Not_Complete
    status = iperf_status_not_complete(test);

    amxc_llist_init(&stdout_lines);
    //TODO Use this when issue 65 of libamxc is solved
    // amxc_string_split_to_llist(str_stdout, &stdout_lines, '\n');
    iperf_split_string_lines(str_stdout, &stdout_lines); //Delete after issue 65 of libamxc is solved
    num_of_lines = amxc_llist_size(&stdout_lines);
    when_false(num_of_lines, exit);

    /*Parse status*/
    status = iperf_parse_status(&stdout_lines, test);

    /*Parse local IP*/
    ret_value = iperf_parse_local_ip(ret, &stdout_lines, line_num);
    when_failed(ret_value, exit);

    /*Parse intervals and connections*/
    ret_value = iperf_parse_incremental_result(ret, &stdout_lines, line_num, test, &intervals, &num_of_intervals);
    when_failed(ret_value, exit);
    if(intervals == NULL) {
        SAH_TRACEZ_ERROR(ME, "Problem with the mem allocation of intervals");
        goto exit;
    }

    //Workaround to stop file based test in download test
    if(test == ipd_up_down_download) {
        if(time_based_test_duration == 0) {
            size_of_test = GETP_UINT32(ret, UP_DOWN_RET_KEY_CFG "."DOWN_CFG_KEY_SIZE_OF_TEST);
            if(num_of_intervals > 0) {
                total_bytes = intervals[num_of_intervals - 1].total_bytes;
            }
            if(total_bytes > (uint64_t) size_of_test) {
                status = down_test_complete;
            }
        }
    }

    /*Parse final results*/
    if(status != iperf_status_not_complete(test)) {
        iperf_parse_final_results(ret, num_of_intervals, intervals, test);
    }

exit:
    amxc_llist_clean(&stdout_lines, amxc_string_list_it_free);
    if(intervals != NULL) {
        for(int index = 0; index < num_of_intervals; index++) {
            if(intervals[index].conn_test_bytes != NULL) {
                free(intervals[index].conn_test_bytes);
            }
        }
        free(intervals);
    }
    iperf_replace_status(ret, test, status);
    return status;
}

/**
   @brief
   Check the results to see if the test passed.

   @param[in] params Input parameters.
   @param[in] test download or upload.

   @return
   Test status
 */
static int iperf_check_results(amxc_var_t* params, ipd_up_down_test_t test) {
    int status = 0;
    amxc_var_t* incremental_results;
    uint32_t number_of_intervals = 0;
    uint32_t meas_number_of_intervals = 0;

    const uint32_t time_based_test_duration = GETP_UINT32(params, UP_DOWN_RET_KEY_CFG "." UP_DOWN_CFG_KEY_TIME_TEST_DURATION);
    const uint32_t measurement_interval = GETP_UINT32(params, UP_DOWN_RET_KEY_CFG "." UP_DOWN_CFG_KEY_TIME_TEST_MEAS_INT);

    if(time_based_test_duration > 0) {
        if(measurement_interval == 0) {
            meas_number_of_intervals = 1;
        } else {
            meas_number_of_intervals = (time_based_test_duration / measurement_interval);
            if(time_based_test_duration % measurement_interval != 0) {
                meas_number_of_intervals++;
            }
        }
    }

    if(time_based_test_duration > 0) {
        incremental_results = GETP_ARG(params, UP_DOWN_RET_KEY_INCR_RES);
        amxc_var_for_each(connection, incremental_results) {
            number_of_intervals++;
        }
        if(number_of_intervals != meas_number_of_intervals) {
            status = iperf_status_error_other(test);
        }
    }

    return status;
}

/**
   @brief
   Check the test.

   @param[in] params Input parameters.
   @param[out] ret Return variant with the datamodel format.
   @param[in] test download or upload.

   @return
   Status of the test.
 */
int iperf_up_down_test_check(amxc_var_t* params,
                             amxc_var_t* ret,
                             ipd_up_down_test_t test) {
    amxp_subproc_t* proc_iperf = NULL;
    amxc_string_t string_stdout;
    int ret_value = 0;
    int status = iperf_status_not_complete(test);
    cstring_t status_string;

    const int32_t pid = GETP_INT32(params, UP_DOWN_RET_KEY_PROC "." UP_DOWN_RET_KEY_PROC_PID);
    const int32_t fd_stdout = GETP_INT32(params, UP_DOWN_RET_KEY_PROC "." UP_DOWN_RET_KEY_PROC_FD_STDOUT);
    const int32_t fd_stderr = GETP_INT32(params, UP_DOWN_RET_KEY_PROC "." UP_DOWN_RET_KEY_PROC_FD_STDERR);

    amxc_var_move(ret, params);

    amxc_string_init(&string_stdout, 0);

    iperf_read_result_file(ret, test, &string_stdout);

    proc_iperf = amxp_subproc_find(pid);

    ret_value = amxp_subproc_wait(proc_iperf, 1);
#ifdef IP_DIAGNOSTICS_UNIT_TEST
    ret_value = 1;
#endif
    if(ret_value == 1) {
        mod_ipd_read_from_fd(fd_stdout, &string_stdout);
        mod_ipd_read_from_fd(fd_stderr, &string_stdout);
        iperf_parse_result(&string_stdout, ret, test);
        // Treat the case where the process stalls but the results are provided
        status_string = (cstring_t) GETP_CHAR(ret, UP_DOWN_RET_KEY_STATUS);
        if(status_string != NULL) {
            // Check if the test has completed, but the process hasn't.
            if(strcmp(status_string, UP_DOWN_STATUS_COMPLETE) == 0) {
                status = 0;
                goto check_results;
            } else if(strcmp(status_string, UP_DOWN_STATUS_NOT_COMPLETE) != 0) {
                // Wait for 1 second to wait the process to finish.
                ret_value = amxp_subproc_wait(proc_iperf, 1000);
#ifdef IP_DIAGNOSTICS_UNIT_TEST
                ret_value = 1;
#endif
                if(ret_value == 1) {
                    SAH_TRACEZ_ERROR(ME, "Process running while output shows it's complete.");
                    status = iperf_status_error_other(test);
                    goto exit;
                }
            }
        }
        goto not_complete;
    }

    status = iperf_status_error_internal(test);
    when_failed_trace(ret_value, exit, ERROR, "Failed while running iperf process.");

    mod_ipd_read_from_fd(fd_stdout, &string_stdout);
    mod_ipd_read_from_fd(fd_stderr, &string_stdout);
    status = iperf_parse_result(&string_stdout, ret, test);
    if(status == iperf_status_not_complete(test)) {
        SAH_TRACEZ_ERROR(ME, "Process not running while output shows it's not complete.");
        status = iperf_status_error_other(test);
        goto exit;
    }

check_results:
    if(status == 0) {
        status = iperf_check_results(ret, test);
    }

exit:
    // Kill the process in case something goes wrong.
    amxp_subproc_kill(proc_iperf, SIGTERM);
    amxp_subproc_wait(proc_iperf, 5);
    iperf_kill_iperf_test(ret, test);
    amxp_subproc_delete(&proc_iperf);

    iperf_delete_result_file(ret, test);
    iperf_delete_test_script(ret, test);
    iperf_replace_status(ret, test, status);

not_complete:
    if(status == iperf_status_not_complete(test)) {
        iperf_write_result_file(ret, test, &string_stdout);
    }

    amxc_string_clean(&string_stdout);

    return status;
}