/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxc/amxc.h>

#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>

#include "mod_ipd_common.h"
#include "mod_ipd_iperf_common.h"
#include "mod_ipd_iperf_up_down.h"
#include "upload.h"
#include "download.h"

// module trace zone
#define ME "ipd-cup-s"

static void iperf_script_array_it_free(amxc_array_it_t* it) {
    free(it->data);
}

/**
   @brief
   Create the command.

   @param[in] params Input parameters.
   @param[in] test download or upload.
   @param[out] cmd String with the command.
 */
static void iperf_create_command(amxc_var_t* params, ipd_up_down_test_t test, amxc_string_t* cmd) {
    SAH_TRACEZ_IN(ME);
    uint32_t param_uint;
    uint32_t time_based_test_duration;
    uint64_t file_length;
    bool ipv4 = false;
    cstring_t param_string = NULL;

    when_null_trace(params, exit, ERROR, "Input parameter is NULL.");
    when_null_trace(cmd, exit, ERROR, "Array is not initialised.");

    //Check ProtocolVersion
    param_string = (cstring_t) GETP_CHAR(params, UP_DOWN_RET_KEY_CFG "." UP_DOWN_CFG_KEY_PROTOCOL_VERSION);
    if(strcmp(param_string, "IPv4") == 0) {
        ipv4 = true;
    }

    amxc_string_clean(cmd);
    amxc_string_appendf(cmd, "iperf3 ");
    //DownloadURL or UploadURL
    param_string = iperf_get_url(params, test);
    amxc_string_appendf(cmd, "-c %s ", param_string);

    if(ipv4) {
        amxc_string_replace(cmd, ":", " -p ", UINT32_MAX);
    } else {
        amxc_string_replace(cmd, "[", "", UINT32_MAX);
        amxc_string_replace(cmd, "]:", " -p ", UINT32_MAX);
    }

    //InterfaceIP
    param_string = (cstring_t) GETP_CHAR(params, UP_DOWN_RET_KEY_CFG "." UP_DOWN_CFG_KEY_INTERFACE_IP);
    if((param_string != NULL) && (strcmp(param_string, "") != 0)) {
        amxc_string_appendf(cmd, "-B %s ", param_string);
    }
    //DSCP
    param_uint = (uint32_t) GETP_UINT32(params, UP_DOWN_RET_KEY_CFG "." UP_DOWN_CFG_KEY_DSCP);
    if(param_uint > 0) {
        amxc_string_appendf(cmd, "--dscp %u ", param_uint);
    }
    //TimeBasedTestDuration
    time_based_test_duration = (uint32_t) GETP_UINT32(params, UP_DOWN_RET_KEY_CFG "." UP_DOWN_CFG_KEY_TIME_TEST_DURATION);
    if(time_based_test_duration > 0) {
        amxc_string_appendf(cmd, "-t %u ", time_based_test_duration);
        param_uint = (uint32_t) GETP_UINT32(params, UP_DOWN_RET_KEY_CFG "." UP_DOWN_CFG_KEY_TIME_TEST_MEAS_INT);
        if(param_uint > 0) {
            amxc_string_appendf(cmd, "-i %u ", param_uint);
        } else {
            amxc_string_appendf(cmd, "-i %u ", time_based_test_duration);
        }
    } else {
        if(test == ipd_up_down_download) {
            param_uint = (uint32_t) GETP_UINT32(params, UP_DOWN_RET_KEY_CFG "." DOWN_CFG_KEY_SIZE_OF_TEST);
            amxc_string_appendf(cmd, "-n %uM ", param_uint);
        } else if(test == ipd_up_down_upload) {
            file_length = (uint64_t) amxc_var_dyncast(uint64_t, GETP_ARG(params, UP_DOWN_RET_KEY_CFG "." UP_CFG_KEY_FILE_LENGTH));
            amxc_string_appendf(cmd, "-n %llu ", (unsigned long long int) file_length);
        }
    }
    //Set ProtocolVersion
    if(ipv4) {
        amxc_string_appendf(cmd, "-4 ");
    } else {
        amxc_string_appendf(cmd, "-6 ");
    }
    //NumberOfConnections
    param_uint = (uint32_t) GETP_UINT32(params, UP_DOWN_RET_KEY_CFG "." UP_DOWN_CFG_KEY_NUM_OF_CONNECTIONS);
    if(param_uint > 1) {
        amxc_string_appendf(cmd, "-P %u ", param_uint);
    }
    amxc_string_appendf(cmd, "--forceflush ");
    if(test == ipd_up_down_download) {
        amxc_string_appendf(cmd, "-R ");
    }
    amxc_string_appendf(cmd, "--connect-timeout 25 ");

exit:
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Create the command.

   @param[in] params Input parameters.
   @param[in] test download or upload.
   @param[in] cmd String with the command.
   @param[in] script_file Script file name.
   @param[out] sh_script Array used in the process.

   @return
   0 if it passes.
 */
static int iperf_create_sh_script(amxc_var_t* params, ipd_up_down_test_t test, amxc_string_t* cmd,
                                  amxc_array_t* sh_script) {
    int rv = -1;
    FILE* p_sh_file = NULL;
    amxc_string_t aux_string;
    cstring_t script_file;
    const uint32_t result_number = GETP_UINT32(params, UP_DOWN_RET_KEY_CFG "." UP_DOWN_CFG_KEY_RESULT_NUMBER);

    amxc_string_init(&aux_string, 1);

    when_null_trace(params, exit, ERROR, "Input parameter is NULL.");
    when_null_trace(cmd, exit, ERROR, "Command is NULL.");
    when_null_trace(sh_script, exit, ERROR, "Array is not initialised.");

    amxc_string_setf(&aux_string, "%s%u.sh", iperf_get_file_path(test, false), result_number);

    script_file = amxc_string_take_buffer(&aux_string);
    amxc_array_append_data(sh_script, strdup("sh"));
    amxc_array_append_data(sh_script, script_file);
    // Delete the shell script with the result number in order to clean up the environment
    iperf_kill_script_test(params, test);
    iperf_delete_test_script(params, test);
    p_sh_file = fopen(script_file, "w");
    when_null_trace(p_sh_file, exit, ERROR, "Unable to create the shell script.")

    fprintf(p_sh_file, "#!/bin/bash\n\n");
    fprintf(p_sh_file, "set +x\n");
    fprintf(p_sh_file, "%s\n", amxc_string_get(cmd, 0));
    fprintf(p_sh_file, "sleep 15\n");

    fclose(p_sh_file);
    chmod(script_file, 0755);

    //Remove the result file
    iperf_delete_result_file(params, test);
    rv = 0;

exit:
    amxc_string_clean(&aux_string);
    return rv;
}

/**
   @brief
   Start the test.

   @param[in] params Input parameters.
   @param[out] ret Return variant with the datamodel format.
   @param[in] test download or upload.

   @return
   0 if it passes, or 1 if it fails.
 */
int iperf_up_down_test_start(amxc_var_t* params, amxc_var_t* ret, ipd_up_down_test_t test) {
    int status = 0;
    amxc_var_t* proc_htable = NULL;
    amxp_subproc_t* proc_iperf = NULL;
    int rv = -1;
    int32_t pid = -1;
    int32_t fd_stdout = -1;
    int32_t fd_stderr = -1;
    amxc_array_t sh_script;
    amxc_string_t* cmd = NULL;
    amxc_var_t* aux_var;


    amxc_var_new(&aux_var);
    amxc_string_new(&cmd, 0);
    amxc_array_init(&sh_script, 1);

    iperf_create_command(params, test, cmd);

    amxc_var_move(ret, params);

    rv = iperf_create_sh_script(ret, test, cmd, &sh_script);
    when_failed_trace(rv, exit, ERROR, "Failed to create the shell script.");

    rv = amxp_subproc_new(&proc_iperf);
    when_failed_trace(rv, exit, ERROR, "Failed to initialize the subprocess.");

    fd_stdout = amxp_subproc_open_fd(proc_iperf, STDOUT_FILENO);
    fd_stderr = amxp_subproc_open_fd(proc_iperf, STDERR_FILENO);
    if((fd_stdout == -1) || (fd_stderr == -1)) {
        SAH_TRACEZ_ERROR(ME, "Failed to open file descriptor.");
        goto exit;
    }

    proc_htable = amxc_var_add_key(amxc_htable_t, ret, UP_DOWN_RET_KEY_PROC, NULL);
    amxc_var_add_key(int32_t, proc_htable, UP_DOWN_RET_KEY_PROC_FD_STDOUT, fd_stdout);
    amxc_var_add_key(int32_t, proc_htable, UP_DOWN_RET_KEY_PROC_FD_STDERR, fd_stderr);

#ifndef IP_DIAGNOSTICS_UNIT_TEST
    rv = amxp_subproc_astart(proc_iperf, &sh_script);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to start iperf process.");
        amxp_subproc_delete(&proc_iperf);
        goto exit;
    }
#else
    amxp_subproc_delete(&proc_iperf);
#endif

    rv = iperf_get_current_time(aux_var);
    when_failed_trace(rv, exit, ERROR, "Failed to get the current time.");
    when_null_trace(aux_var, exit, ERROR, "Failed to get the BOM time.");

    amxc_var_set_pathf(ret, aux_var, DEFAULT_SET_PATH_FLAGS, UP_DOWN_RET_KEY_BOM_TIME);
    amxc_var_set_pathf(ret, aux_var, DEFAULT_SET_PATH_FLAGS, UP_DOWN_RET_KEY_TCP_REQ_TIME);
    amxc_var_set_pathf(ret, aux_var, DEFAULT_SET_PATH_FLAGS, UP_DOWN_RET_KEY_TCP_RESP_TIME);

    pid = (int) amxp_subproc_get_pid(proc_iperf);
    amxc_var_add_key(int32_t, proc_htable, UP_DOWN_RET_KEY_PROC_PID, pid);

    //Set the status to Not_Complete
    status = iperf_status_not_complete(test);

exit:
    amxc_array_clean(&sh_script, iperf_script_array_it_free);
    amxc_var_delete(&aux_var);
    amxc_string_delete(&cmd);

    if(status == iperf_status_not_complete(test)) {
        iperf_replace_status(ret, test, status);
        return 0;
    } else {
        return 1;
    }
}
