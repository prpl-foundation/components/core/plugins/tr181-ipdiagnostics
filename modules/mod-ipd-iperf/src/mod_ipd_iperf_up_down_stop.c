/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxp/amxp.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_ipd_iperf_common.h"
#include "mod_ipd_iperf_up_down.h"
#include "upload.h"
#include "download.h"

// module trace zone
#define ME "iperf-cup-stop"

/**
   @brief
   Stop the test.

   @param[in] params Input parameters.
   @param[out] ret Return variant with the datamodel format.
   @param[in] test download or upload.

   @return
   0 if the function passes.
 */
int iperf_up_down_test_stop(amxc_var_t* params, amxc_var_t* ret, ipd_up_down_test_t test) {
    int status;
    amxp_subproc_t* proc_iperf = NULL;
    const int pid = GETP_INT32(params, UP_DOWN_RET_KEY_PROC "." UP_DOWN_RET_KEY_PROC_PID);

    proc_iperf = amxp_subproc_find(pid);

    amxc_var_move(ret, params);

    status = iperf_status_error_other(test);

    //Kill the process
    amxp_subproc_kill(proc_iperf, SIGTERM);
    amxp_subproc_wait(proc_iperf, 5);
    iperf_kill_iperf_test(ret, test);
    iperf_kill_script_test(ret, test);

    //Remove script file
    iperf_delete_test_script(ret, test);

    //Remove result file
    iperf_delete_result_file(ret, test);
    amxp_subproc_delete(&proc_iperf);

    iperf_replace_status(ret, test, status);
    return 0;
}

/**
   @brief
   Abort tests.

   @param[out] ret Return variant with the datamodel format.
   @param[in] test download or upload.

   @return
   0 if the function passes.
 */
int iperf_up_down_test_abort(amxc_var_t* ret, ipd_up_down_test_t test) {
    int status = 0;
    int rv = -1;
    amxc_string_t* aux_string = NULL;

    amxc_string_new(&aux_string, 0);

    if(test == ipd_up_down_download) {
        status = down_test_error_other;
    } else if(test == ipd_up_down_upload) {
        status = up_test_error_other;
    }
    amxc_string_setf(aux_string, "%s", iperf_get_file_path(test, false));

    rv = iperf_kill_process(amxc_string_get(aux_string, 0));
    when_failed_trace(rv, exit, ERROR, "Failed to kill the process with name: '%s'", amxc_string_get(aux_string, 0));

exit:
    amxc_string_delete(&aux_string);
    iperf_delete_result_file(NULL, test);
    iperf_delete_test_script(NULL, test);
    iperf_replace_status(ret, test, status);

    return rv;
}