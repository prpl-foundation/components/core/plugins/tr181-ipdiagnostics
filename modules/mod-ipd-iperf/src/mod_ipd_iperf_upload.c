/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include "mod_ipd_iperf_up_down.h"
#include "mod_ipd_iperf_upload.h"
#include "upload.h"

// module trace zone
#define ME "iperf-u"

const cstring_t str_up_test_status[upload_test_num_of_status] = {
    UP_DOWN_STATUS_COMPLETE,
    UP_DOWN_STATUS_ERR_HOST_NAME,
    UP_DOWN_STATUS_ERR_ROUTE,
    UP_DOWN_STATUS_ERR_INIT_CONN,
    UP_DOWN_STATUS_ERR_NO_RESPONSE,
    UP_DOWN_STATUS_ERR_PASSWORD,
    UP_DOWN_STATUS_ERR_LOGIN,
    UP_DOWN_STATUS_ERR_NO_TRANSFER,
    UP_DOWN_STATUS_ERR_NO_PASV,
    UP_STATUS_ERR_NO_CWD,
    UP_STATUS_ERR_NO_STOR,
    UP_STATUS_ERR_TRAN_COMP,
    UP_DOWN_STATUS_ERR_TIMEOUT,
    UP_DOWN_STATUS_ERR_INTERNAL,
    UP_DOWN_STATUS_ERR_OTHER,
    UP_DOWN_STATUS_NOT_COMPLETE,
    UP_DOWN_STATUS_NOT_EXECUTED
};

int mod_upload_test_start(UNUSED const char* function_name,
                          amxc_var_t* params,
                          amxc_var_t* ret) {
    return iperf_up_down_test_start(params, ret, ipd_up_down_upload);
}

int mod_upload_test_check(UNUSED const char* function_name,
                          amxc_var_t* params,
                          amxc_var_t* ret) {
    return iperf_up_down_test_check(params, ret, ipd_up_down_upload);
}

int mod_upload_test_stop(UNUSED const char* function_name,
                         amxc_var_t* params,
                         amxc_var_t* ret) {
    return iperf_up_down_test_stop(params, ret, ipd_up_down_upload);
}

int mod_upload_test_abort(UNUSED const char* function_name,
                          UNUSED amxc_var_t* params,
                          amxc_var_t* ret) {
    return iperf_up_down_test_abort(ret, ipd_up_down_upload);
}
