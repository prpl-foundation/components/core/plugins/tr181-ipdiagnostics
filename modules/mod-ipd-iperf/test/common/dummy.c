/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>

#include <yajl/yajl_gen.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxj/amxj_variant.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_function.h>
#include <amxd/amxd_object_event.h>

#include <amxo/amxo.h>

#include "mock.h"

typedef struct _dummy_app {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
} dummy_app_t;

static dummy_app_t app;

amxc_var_t* dummy_read_json_from_file(const char* fname) {
    int fd = -1;
    variant_json_t* reader = NULL;
    amxc_var_t* data = NULL;
    // create a json reader
    if(amxj_reader_new(&reader) != 0) {
        printf("Failed to create json file reader");
        goto exit;
    }

    // open the json file
    fd = open(fname, O_RDONLY);
    if(fd == -1) {
        printf("File open file %s - error 0x%8.8X\n", fname, errno);
        goto exit;
    }

    // read the json file and parse the json text
    while(amxj_read(reader, fd) > 0) {
    }

    // get the variant
    data = amxj_reader_result(reader);

    if(data == NULL) {
        printf("Invalid JSON in file %s", fname);
    }

    close(fd);
exit:
    amxj_reader_delete(&reader);

    return data;
}

int dummy_write_json_to_file(const char* fname, amxc_var_t* var) {
    int fd = -1;
    int length = 0;
    variant_json_t* writer = NULL;
    // create a json writer
    if(amxj_writer_new(&writer, var) != 0) {
        printf("Failed to create json file writer");
        goto exit;
    }

    // open the json file
    fd = open(fname, O_WRONLY | O_TRUNC);
    if(fd == -1) {
        printf("File open file %s - error 0x%8.8X\n", fname, errno);
        goto exit;
    }

    length = amxj_write(writer, fd);

    close(fd);
exit:
    amxj_writer_delete(&writer);
    return length;
}

int _dummy_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {

    switch(reason) {
    case 0:     // START
        app.dm = dm;
        app.parser = parser;
        break;
    case 1:     // STOP
        app.dm = NULL;
        app.parser = NULL;
        break;
    }

    return 0;
}

void dummy_set_file(const char* function, const char* file,
                    const char* iperf) {
    amxc_var_t* file_var = NULL;
    if(iperf == NULL) {
        file_var = amxc_var_get_pathf(&app.parser->config,
                                      AMXC_VAR_FLAG_DEFAULT,
                                      "func-return.%s",
                                      function);
    } else {
        file_var = amxc_var_get_pathf(&app.parser->config,
                                      AMXC_VAR_FLAG_DEFAULT,
                                      "func-return.%s_%s",
                                      function, iperf);
    }
    amxc_var_set(cstring_t, file_var, file);
}
