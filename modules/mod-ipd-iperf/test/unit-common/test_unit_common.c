/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <fcntl.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "../common/mock.h"
#include "test_unit_common.h"

#include "ip_diagnostics.h"

#include "mod_ipd_common.h"
#include "mod_ipd_iperf_common.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;

#define IP_DIAGNOSTICS_ODL "../../../../odl/tr181-ipdiagnostics_definition.odl"

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser,
                        amxb_get_fd(bus_ctx),
                        connection_read,
                        "dummy:/tmp/dummy.sock",
                        AMXO_BUS,
                        bus_ctx);
    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxo_parser_parse_file(&parser, IP_DIAGNOSTICS_ODL, root_obj), 0);

    handle_events();

    return 0;
}

int test_teardown(UNUSED void** state) {
    amxb_free(&bus_ctx);
    amxm_close_all();
    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

void test_can_load_module(UNUSED void** state) {
    amxm_shared_object_t* so = NULL;
    assert_int_equal(amxm_so_open(&so, "target_module", "../modules_under_test/target_module.so"), 0);
}

void test_replace_htable_uint64(UNUSED void** state) {
    amxc_var_t* htable;
    uint64_t value;
    int result = 0;

    amxc_var_new(&htable);
    amxc_var_set_type(htable, AMXC_VAR_ID_HTABLE);

    iperf_replace_htable_uint64(htable, "KEY", 0);

    value = amxc_var_dyncast(uint64_t, GETP_ARG(htable, "KEY"));
    if(value != 0) {
        result = 1;
        goto evaluate;
    }

    iperf_replace_htable_uint64(htable, "KEY", UINT32_MAX);
    value = amxc_var_dyncast(uint64_t, GETP_ARG(htable, "KEY"));
    if(value != UINT32_MAX) {
        result = 1;
        goto evaluate;
    }

evaluate:
    amxc_var_delete(&htable);
    assert_int_equal(result, 0);
}

void test_read_from_fd(UNUSED void** state) {
    amxc_string_t string;
    int fd;
    int newfd;
    int result = 0;

    fd = open("fd_test", O_RDWR | O_CREAT | O_TRUNC);
    assert_int_not_equal(fd, -1);

    amxc_string_init(&string, 0);

    write(fd, "This will be the output", 23);

    newfd = open("fd_test", O_RDWR);
    mod_ipd_read_from_fd(newfd, &string);
    close(fd);
    close(newfd);
    if(strcmp(amxc_string_get(&string, 0), "This will be the output") != 0) {
        result = 1;
        goto evaluate;
    }

evaluate:
    amxc_string_clean(&string);
    if(access("fd_test", F_OK) == 0) {
        remove("fd_test");
    }
    assert_int_equal(result, 0);
}

void test_read_from_file(UNUSED void** state) {
    FILE* p_file = NULL;
    amxc_string_t string;
    int result = 0;

    p_file = fopen("file_test", "w");
    assert_non_null(p_file);

    fprintf(p_file, "This will be the output");
    fclose(p_file);

    amxc_string_init(&string, 0);

    mod_ipd_read_from_file("file_test", &string);

    if(strcmp(amxc_string_get(&string, 0), "This will be the output") != 0) {
        result = 1;
        goto evaluate;
    }

evaluate:
    amxc_string_clean(&string);
    if(access("file_test", F_OK) == 0) {
        remove("file_test");
    }
    assert_int_equal(result, 0);
}

void test_write_to_file(UNUSED void** state) {
    amxc_string_t w_string;
    amxc_string_t r_string;
    int result = 0;

    amxc_string_init(&w_string, 0);
    amxc_string_appendf(&w_string, "This will be the output");

    mod_ipd_write_to_file("file_test", &w_string);
    amxc_string_clean(&w_string);

    amxc_string_init(&r_string, 0);
    mod_ipd_read_from_file("file_test", &r_string);

    if(strcmp(amxc_string_get(&r_string, 0), "This will be the output") != 0) {
        result = 1;
        goto evaluate;
    }

evaluate:
    amxc_string_clean(&r_string);
    if(access("file_test", F_OK) == 0) {
        remove("file_test");
    }
    assert_int_equal(result, 0);
}

void test_extract_regex_from_string(UNUSED void** state) {
    amxc_string_t string;
    amxc_string_t extracted_string;
    int ret_value_true = 0;
    int ret_value_false = 0;

    amxc_string_init(&string, 0);
    amxc_string_init(&extracted_string, 0);
    amxc_string_appendf(&string, "[aa ID]      15.00-18.00      Interval");

    ret_value_true = mod_ipd_extract_regex_from_string(&string,
                                                       "[0-9]+\\.[0-9][0-9]-[0-9]+\\.[0-9][0-9]",
                                                       &extracted_string);
    ret_value_false = mod_ipd_extract_regex_from_string(&string,
                                                        "[0-9]+\\,[0-9][0-9]-[0-9]+\\,[0-9][0-9]",
                                                        &extracted_string);
    amxc_string_clean(&string);
    amxc_string_clean(&extracted_string);

    assert_int_equal(ret_value_true, 0);
    assert_int_not_equal(ret_value_false, 0);
}

void test_split_string_lines(UNUSED void** state) {
    amxc_string_t* string = NULL;
    amxc_llist_t lines;
    int num_of_lines;

    amxc_string_new(&string, 0);
    amxc_string_appendf(string, "A\nB\nC\nD\n");

    amxc_llist_init(&lines);

    iperf_split_string_lines(string, &lines);
    amxc_string_delete(&string);

    num_of_lines = amxc_llist_size(&lines);

    amxc_llist_clean(&lines, amxc_string_list_it_free);

    assert_int_equal(num_of_lines, 4);
}

void test_parse_date_string(UNUSED void** state) {
    amxc_var_t* date = NULL;
    struct tm ptm;
    int microseconds;

    amxc_var_new(&date);
    amxc_var_set(cstring_t, date, "2008-04-09T15:01:05.123456Z");
    microseconds = iperf_parse_date_string(date, &ptm);
    amxc_var_delete(&date);

    assert_int_not_equal(microseconds, -1);

    assert_int_equal(ptm.tm_year + 1900, 2008);
    assert_int_equal(ptm.tm_mon + 1, 4);
    assert_int_equal(ptm.tm_mday, 9);
    assert_int_equal(ptm.tm_hour, 15);
    assert_int_equal(ptm.tm_min, 1);
    assert_int_equal(ptm.tm_sec, 5);
    assert_int_equal(microseconds, 123456);
}

void test_parse_local_ip(UNUSED void** state) {
    amxc_string_t* string_input = NULL;
    amxc_string_t* string_output = NULL;
    int result = 0;
    int rv = -1;

    amxc_string_new(&string_input, 0);
    amxc_string_set(string_input, "192.168.0.1 and that's it.");
    amxc_string_new(&string_output, 0);
    rv = iperf_parse_local_ip_from_line(string_input, "IPv4", string_output);
    amxc_string_delete(&string_input);
    amxc_string_delete(&string_output);
    assert_int_not_equal(rv, 0);

    amxc_string_new(&string_input, 0);
    amxc_string_set(string_input, "2001:0db8:85a3:0000:0000:8a2e:0370:7334 and that's it.");
    amxc_string_new(&string_output, 0);
    rv = iperf_parse_local_ip_from_line(string_input, "IPv6", string_output);
    amxc_string_delete(&string_input);
    amxc_string_delete(&string_output);
    assert_int_not_equal(rv, 0);

    amxc_string_new(&string_input, 0);
    amxc_string_set(string_input, "local 192.168.0 and that's it.");
    amxc_string_new(&string_output, 0);
    rv = iperf_parse_local_ip_from_line(string_input, "IPv4", string_output);
    amxc_string_delete(&string_input);
    amxc_string_delete(&string_output);
    assert_int_not_equal(rv, 0);

    amxc_string_new(&string_input, 0);
    amxc_string_set(string_input, "local 2001.:.0db8:85a3:0000:0000.:.8a2e:0370:7334 and that's it.");
    amxc_string_new(&string_output, 0);
    rv = iperf_parse_local_ip_from_line(string_input, "IPv6", string_output);
    amxc_string_delete(&string_input);
    amxc_string_delete(&string_output);
    assert_int_not_equal(rv, 0);

    amxc_string_new(&string_input, 0);
    amxc_string_set(string_input, "local 192.168.0.1 and that's it.");
    amxc_string_new(&string_output, 0);
    rv = iperf_parse_local_ip_from_line(string_input, "IPv4", string_output);
    amxc_string_delete(&string_input);
    if(strcmp(amxc_string_get(string_output, 0), "192.168.0.1") != 0) {
        result = 1;
    }
    amxc_string_delete(&string_output);
    assert_int_equal(result, 0);
    assert_int_equal(rv, 0);

    amxc_string_new(&string_input, 0);
    amxc_string_set(string_input, "local 2001:0db8:85a3:0000:0000:8a2e:0370:7334 and that's it.");
    amxc_string_new(&string_output, 0);
    rv = iperf_parse_local_ip_from_line(string_input, "IPv6", string_output);
    amxc_string_delete(&string_input);
    if(strcmp(amxc_string_get(string_output, 0), "2001:0db8:85a3:0000:0000:8a2e:0370:7334") != 0) {
        result = 1;
    }
    amxc_string_delete(&string_output);
    assert_int_equal(result, 0);
    assert_int_equal(rv, 0);
}

void test_calculate_new_ptm(UNUSED void** state) {
    struct tm ptm;
    int microseconds = 123456;

    ptm.tm_year = 108;
    ptm.tm_mon = 1;
    ptm.tm_mday = 11;
    ptm.tm_hour = 23;
    ptm.tm_min = 59;
    ptm.tm_sec = 58;
    ptm.tm_isdst = 0;

    microseconds = iperf_calculate_new_ptm(microseconds, 2, 876544, &ptm);

    assert_int_equal(ptm.tm_year, 108);
    assert_int_equal(ptm.tm_mon, 1);
    assert_int_equal(ptm.tm_mday, 12);
    assert_int_equal(ptm.tm_hour, 0);
    assert_int_equal(ptm.tm_min, 0);
    assert_int_equal(ptm.tm_sec, 1);
    assert_int_equal(microseconds, 0);
}

void test_convert_ptm_to_var(UNUSED void** state) {
    struct tm ptm;
    int microseconds = 123456;
    amxc_var_t aux_var;
    cstring_t date_str = NULL;
    int valid_str = 0;

    ptm.tm_year = 108;
    ptm.tm_mon = 1;
    ptm.tm_mday = 8;
    ptm.tm_hour = 3;
    ptm.tm_min = 2;
    ptm.tm_sec = 1;
    ptm.tm_isdst = 0;

    amxc_var_init(&aux_var);
    iperf_convert_ptm_to_var(&ptm, microseconds, &aux_var);
    date_str = (cstring_t) GET_CHAR(&aux_var, NULL);
    valid_str = 0;
    if(strcmp(date_str, "2008-02-08T03:02:01.123456Z") != 0) {
        valid_str = 1;
    }
    amxc_var_clean(&aux_var);
    assert_int_equal(valid_str, 0);

    microseconds = 1234567;
    amxc_var_init(&aux_var);
    iperf_convert_ptm_to_var(&ptm, microseconds, &aux_var);
    date_str = (cstring_t) GET_CHAR(&aux_var, NULL);
    valid_str = 0;
    if(strcmp(date_str, "2008-02-08T03:02:01.999999Z") != 0) {
        valid_str = 1;
    }
    amxc_var_clean(&aux_var);
    assert_int_equal(valid_str, 0);

    microseconds = 1;
    amxc_var_init(&aux_var);
    iperf_convert_ptm_to_var(&ptm, microseconds, &aux_var);
    date_str = (cstring_t) GET_CHAR(&aux_var, NULL);
    valid_str = 0;
    if(strcmp(date_str, "2008-02-08T03:02:01.000001Z") != 0) {
        valid_str = 1;
    }
    amxc_var_clean(&aux_var);
    assert_int_equal(valid_str, 0);
}

void test_is_valid_interval_line(UNUSED void** state) {
    amxc_string_t* string = NULL;
    int is_valid = 0;

    amxc_string_new(&string, 0);
    amxc_string_appendf(string, "[SUM]   4.00-6.00   sec  8.68 GBytes  37.3 Gbits/sec");
    is_valid = iperf_is_valid_interval_line(string);
    amxc_string_delete(&string);
    assert_int_equal(is_valid, 0);

    amxc_string_new(&string, 0);
    amxc_string_appendf(string, "4.00-6.00   sec  8.68 GBytes  37.3 Gbits/sec");
    is_valid = iperf_is_valid_interval_line(string);
    amxc_string_delete(&string);
    assert_int_not_equal(is_valid, 0);
}

void test_parse_test_bytes(UNUSED void** state) {
    amxc_string_t* string = NULL;
    uint64_t test_bytes;

    amxc_string_new(&string, 0);
    amxc_string_appendf(string, "[SUM]   4.00-6.00   sec  8.68 PBytes  37.3 Gbits/sec");
    test_bytes = iperf_parse_test_bytes(string);
    amxc_string_delete(&string);
    assert_int_equal(test_bytes, 8680000000000000);

    amxc_string_new(&string, 0);
    amxc_string_appendf(string, "[SUM]   4.00-6.00   sec  8.68 TBytes  37.3 Gbits/sec");
    test_bytes = iperf_parse_test_bytes(string);
    amxc_string_delete(&string);
    assert_int_equal(test_bytes, 8680000000000);

    amxc_string_new(&string, 0);
    amxc_string_appendf(string, "[SUM]   4.00-6.00   sec  8.68 GBytes  37.3 Gbits/sec");
    test_bytes = iperf_parse_test_bytes(string);
    amxc_string_delete(&string);
    assert_int_equal(test_bytes, 8680000000);

    amxc_string_new(&string, 0);
    amxc_string_appendf(string, "[SUM]   4.00-6.00   sec  8.6895 GBytes  37.3 Gbits/sec");
    test_bytes = iperf_parse_test_bytes(string);
    amxc_string_delete(&string);
    assert_int_equal(test_bytes, 8689000000);

    amxc_string_new(&string, 0);
    amxc_string_appendf(string, "[SUM]   4.00-6.00   sec  8.6895 MBytes  37.3 Gbits/sec");
    test_bytes = iperf_parse_test_bytes(string);
    amxc_string_delete(&string);
    assert_int_equal(test_bytes, 8689000);

    amxc_string_new(&string, 0);
    amxc_string_appendf(string, "[SUM]   4.00-6.00   sec  8.6895 KBytes  37.3 Gbits/sec");
    test_bytes = iperf_parse_test_bytes(string);
    amxc_string_delete(&string);
    assert_int_equal(test_bytes, 8689);

    amxc_string_new(&string, 0);
    amxc_string_appendf(string, "[SUM]   4.00-6.00   sec  868.95 Bytes  37.3 Gbits/sec");
    test_bytes = iperf_parse_test_bytes(string);
    amxc_string_delete(&string);
    assert_int_equal(test_bytes, 868);
}

void test_parse_start_interval(UNUSED void** state) {
    amxc_string_t* string = NULL;
    int start_interval;

    amxc_string_new(&string, 0);
    amxc_string_appendf(string, "[SUM]   4.00-6.00   sec  8.68 GBytes  37.3 Gbits/sec");
    start_interval = iperf_parse_start_interval(string);
    amxc_string_delete(&string);
    assert_int_equal(start_interval, 4);

    amxc_string_new(&string, 0);
    amxc_string_appendf(string, "[SUM]   4.15-6.00   sec  8.68 GBytes  37.3 Gbits/sec");
    start_interval = iperf_parse_start_interval(string);
    amxc_string_delete(&string);
    assert_int_equal(start_interval, 4);
}

void test_parse_end_interval(UNUSED void** state) {
    amxc_string_t* string = NULL;
    int end_interval;

    amxc_string_new(&string, 0);
    amxc_string_appendf(string, "[SUM]   4.00-6.00   sec  8.68 GBytes  37.3 Gbits/sec");
    end_interval = iperf_parse_end_interval(string);
    amxc_string_delete(&string);
    assert_int_equal(end_interval, 6);

    amxc_string_new(&string, 0);
    amxc_string_appendf(string, "[SUM]   4.00-6.15   sec  8.68 GBytes  37.3 Gbits/sec");
    end_interval = iperf_parse_end_interval(string);
    amxc_string_delete(&string);
    assert_int_equal(end_interval, 6);
}

void test_parse_end_interval_microseconds(UNUSED void** state) {
    amxc_string_t* string = NULL;
    int end_interval_microseconds;

    amxc_string_new(&string, 0);
    amxc_string_appendf(string, "[SUM]   4.00-6.00   sec  8.68 GBytes  37.3 Gbits/sec");
    end_interval_microseconds = iperf_parse_end_interval_microseconds(string);
    amxc_string_delete(&string);
    assert_int_equal(end_interval_microseconds, 0);

    amxc_string_new(&string, 0);
    amxc_string_appendf(string, "[SUM]   4.00-6.15   sec  8.68 GBytes  37.3 Gbits/sec");
    end_interval_microseconds = iperf_parse_end_interval_microseconds(string);
    amxc_string_delete(&string);
    assert_int_equal(end_interval_microseconds, 150000);

    amxc_string_new(&string, 0);
    amxc_string_appendf(string, "[SUM]   4.00-6.151   sec  8.68 GBytes  37.3 Gbits/sec");
    end_interval_microseconds = iperf_parse_end_interval_microseconds(string);
    amxc_string_delete(&string);
    assert_int_equal(end_interval_microseconds, 150000);
}