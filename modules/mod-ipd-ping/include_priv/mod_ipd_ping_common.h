/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MOD_IPD_IPPING_COMMON_H__)
#define __MOD_IPD_IPPING_COMMON_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include "ipping.h"
#include "mod_ipd_common.h"

// Regex expression
#define IPPING_REGEX_FROM_IPV4            "from\\s" MOD_IPD_REGEX_IPV4 "\\s"
#define IPPING_REGEX_FROM_IPV6            "from\\s" MOD_IPD_REGEX_IPV6 "\\s"
#define IPPING_REGEX_PACKAGES_TRANSMITTED "[0-9]+ packets transmitted"
#define IPPING_REGEX_PACKAGES_RECIEVED    "[0-9]+ received"
#define IPPING_REGEX_ERRORS               "\\+[0-9]+ errors"
#define IPPING_REGEX_INT_VAL              "[0-9]+"
#define IPPING_REGEX_RTT                  "rtt min/avg/max/mdev"
#define IPPING_REGEX_RTT_VAL              "([0-9]*\\.[0-9]+(/[0-9]*\\.[0-9]+)+)"
#define IPPING_REGEX_STAT                 "ping statistics"
#define IPPING_REGEX_PING                 "PING"

//file names
#define IPPING_SCRIPT_FILE "ipping.sh"
#define IPPING_RESULT_FILE "ipping-result"

int ipping_kill_script_test(void);
int ipping_kill_ipping_test(amxc_var_t* params);
void ipping_delete_result_file(void);
void ipping_delete_test_script(void);
ipping_test_status_t ipping_st_str_to_st_int(const cstring_t status_string);
char* ipping_find_interface_ip(amxc_string_t* ping_test_head, bool intf_empty);

#ifdef __cplusplus
}
#endif

#endif // __MOD_IPD_IPPING_COMMON_H__
