/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_types.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_ipd_ping.h"
#include "mod_ipd_ping_common.h"
#include "mod_ipd_ping_status.h"
#include "ipping.h"
#include "ip_diagnostics.h"
#include "mod_ipd_common.h"

// module trace zone
#define ME "mod-p-check"

/**
   @brief
   Write the information from a string and save to the result file.

   @param[in] string string containing the information to be written.
 */
static void ipping_write_result_file(amxc_string_t* string) {

    mod_ipd_write_to_file(IPPING_RESULT_FILE, string);

}

/**
 * @brief
 * Find the interface ip used in the current ping test
 *
 * @param ping_test_head "Ping test line with the interface data"
 * @param intf_empty "Mentionned interface boolean, false -> no interface ip directly available in the string"
 * @return char* IPAddressUsed linked to the current interface
 */
char* ipping_find_interface_ip(amxc_string_t* ping_test_head, bool intf_empty) {
    amxc_string_t extracted_string;
    amxc_string_t str_number;
    char* intf_ip = NULL;
    const char* regex = NULL;
    int rv = 0;
    bool skip = true;

    amxc_string_init(&extracted_string, 0);
    amxc_string_init(&str_number, 0);

    when_null_trace(ping_test_head, exit, ERROR, "Input amxc_string_t structure is NULL");
    skip = false;

    //Look for an IPv4 address
    regex = intf_empty ? MOD_IPD_REGEX_HOST_IPV4 : IPPING_REGEX_FROM_IPV4;
    rv = mod_ipd_extract_regex_from_string(ping_test_head, regex, &extracted_string);
    if(rv == 0) {
        rv = mod_ipd_extract_regex_from_string(&extracted_string, MOD_IPD_REGEX_IPV4, &str_number);
        goto set_intf_ip;
    }

    //Look for an IPv6 address
    regex = intf_empty ? MOD_IPD_REGEX_HOST_IPV6 : IPPING_REGEX_FROM_IPV6;
    rv = mod_ipd_extract_regex_from_string(ping_test_head, regex, &extracted_string);
    if(rv == 0) {
        rv = mod_ipd_extract_regex_from_string(&extracted_string, MOD_IPD_REGEX_IPV6, &str_number);
        goto set_intf_ip;
    }

set_intf_ip:
    if(intf_empty) {
        intf_ip = rv == 0 ? mod_ipd_find_interface_ip_address(amxc_string_get(&str_number, 0)) : strdup("");
    } else {
        intf_ip = strdup(rv == 0 ? amxc_string_get(&str_number, 0) : "");
    }

exit:
    if(skip) {
        intf_ip = strdup("");
    }
    amxc_string_clean(&extracted_string);
    amxc_string_clean(&str_number);
    return intf_ip;
}

/**
 * @brief
 * Parse the results from the string list into the output variant
 *
 * @param ret output variant
 * @param status Ping status
 * @param stdout_lines List of strings with the results
 */
static void ipping_parse_final_results(amxc_var_t* ret, ipping_test_status_t status, amxc_llist_t* stdout_lines) {
    SAH_TRACEZ_IN(ME);
    int rv = 0;
    uint32_t failure_count = 0;
    uint32_t success_count = 0;
    uint32_t number_of_repetition = 0;
    double maximum_response_time = 0;
    double minimum_response_time = 0;
    double average_response_time = 0;
    uint32_t line_num = 0;
    uint32_t number_of_lines = 0;
    amxc_string_t extracted_string;
    amxc_string_t str_number;
    amxc_string_t* line = NULL;
    amxc_llist_t* val_list = NULL;
    amxc_var_t* value = NULL;
    char* intf_ip_address = strdup("");
    char* ptr = NULL;
    bool rtt = false;

    amxc_llist_new(&val_list);
    amxc_string_init(&extracted_string, 0);
    amxc_string_init(&str_number, 0);

    when_null_trace(ret, exit, ERROR, "Null return variant");
    when_null_trace(stdout_lines, exit, ERROR, "No input string to parse for final results");
    when_true((status != ipping_test_error_complete)
              && (status != ipping_test_error_err_route)
              && (status != ipping_test_error_err_host_name)
              && (status != ipping_test_error_err_internal), exit);
    number_of_lines = amxc_llist_size(stdout_lines);
    number_of_repetition = GET_UINT32(ret, "NumberOfRepetitions");

    // Search for the statistics line of the test
    for(; line_num < number_of_lines; line_num++) {
        line = amxc_string_get_from_llist(stdout_lines, line_num);
        if(amxc_string_search(line, IPPING_REGEX_STAT, 0) != -1) {
            break;
        }
    }
    when_true_trace(line_num == number_of_lines, skip_intf, ERROR, "No statistics part of the ping test");
    line = amxc_string_get_from_llist(stdout_lines, line_num + 1);

    // Fetch number of recieved packages
    rv = mod_ipd_extract_regex_from_string(line, IPPING_REGEX_PACKAGES_RECIEVED, &extracted_string);
    when_failed_trace(rv, skip_rec_pack, ERROR, "Failed to fetch %s from %s", IPPING_REGEX_PACKAGES_RECIEVED, amxc_string_get(line, 0));
    rv = mod_ipd_extract_regex_from_string(&extracted_string, IPPING_REGEX_INT_VAL, &str_number);
    when_failed_trace(rv, skip_rec_pack, ERROR, "Failed to fetch %s from %s", IPPING_REGEX_INT_VAL, amxc_string_get(&extracted_string, 0));
    success_count = atoi(amxc_string_get(&str_number, 0));
skip_rec_pack:
    // Fetch Number of errors
    rv = mod_ipd_extract_regex_from_string(line, IPPING_REGEX_ERRORS, &extracted_string);
    when_failed_trace(rv, skip_err, ERROR, "Failed to fetch %s from %s", IPPING_REGEX_ERRORS, amxc_string_get(line, 0));
    rv = mod_ipd_extract_regex_from_string(&extracted_string, IPPING_REGEX_INT_VAL, &str_number);
    when_failed_trace(rv, skip_err, ERROR, "Failed to fetch %s from %s", IPPING_REGEX_INT_VAL, amxc_string_get(&extracted_string, 0));
    failure_count = atoi(amxc_string_get(&str_number, 0));
skip_err:
    // Fetch the rtt line of the ping test
    for(line_num = 0; line_num < number_of_lines; line_num++) {
        line = amxc_string_get_from_llist(stdout_lines, line_num);
        if(amxc_string_search(line, IPPING_REGEX_RTT, 0) != -1) {
            rtt = true;
            break;
        }
    }
    if(rtt) {
        // Fetch the values line
        rv = mod_ipd_extract_regex_from_string(line, IPPING_REGEX_RTT_VAL, &extracted_string);
        when_failed_trace(rv, exit, ERROR, "Failed to fetch %s from %s", IPPING_REGEX_RTT_VAL, amxc_string_get(line, 0));
        amxc_string_split_to_llist(&extracted_string, val_list, '/');
        minimum_response_time = strtod(amxc_string_get(amxc_string_get_from_llist(val_list, 0), 0), &ptr);
        average_response_time = strtod(amxc_string_get(amxc_string_get_from_llist(val_list, 1), 0), &ptr);
        maximum_response_time = strtod(amxc_string_get(amxc_string_get_from_llist(val_list, 2), 0), &ptr);
    }

    // Search for the PING line with the IP address of the interface
    for(line_num = 0; line_num < number_of_lines; line_num++) {
        line = amxc_string_get_from_llist(stdout_lines, line_num);
        if(amxc_string_search(line, IPPING_REGEX_PING, 0) != -1) {
            break;
        }
    }
    free(intf_ip_address);
    intf_ip_address = ipping_find_interface_ip(line, str_empty(GET_CHAR(ret, IPPING_RET_KEY_NAME)));

skip_intf:
    // Set the values into ret
    value = GET_ARG(ret, IPPING_RET_KEY_MAX);
    amxc_var_set(uint32_t, value, (uint32_t) maximum_response_time);
    value = amxc_var_get_key(ret, IPPING_RET_KEY_MAXD, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(uint32_t, value, (uint32_t) (maximum_response_time * 1000));
    value = amxc_var_get_key(ret, IPPING_RET_KEY_MIN, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(uint32_t, value, (uint32_t) minimum_response_time);
    value = amxc_var_get_key(ret, IPPING_RET_KEY_MIND, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(uint32_t, value, (uint32_t) (minimum_response_time * 1000));
    value = amxc_var_get_key(ret, IPPING_RET_KEY_AVG, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(uint32_t, value, (uint32_t) average_response_time);
    value = amxc_var_get_key(ret, IPPING_RET_KEY_AVGD, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(uint32_t, value, (uint32_t) (average_response_time * 1000));
    value = amxc_var_get_key(ret, IPPING_RET_KEY_SUCC, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(uint32_t, value, success_count);
    if(failure_count == 0) {
        failure_count = number_of_repetition - success_count;
    }
    value = amxc_var_get_key(ret, IPPING_RET_KEY_FC, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(uint32_t, value, failure_count);

exit:
    value = amxc_var_get_key(ret, IPPING_RET_KEY_IPUSD, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(cstring_t, value, (cstring_t) intf_ip_address);

    free(intf_ip_address);
    amxc_llist_delete(&val_list, amxc_string_list_it_free);
    amxc_string_clean(&extracted_string);
    amxc_string_clean(&str_number);
    SAH_TRACEZ_OUT(ME);
}

/**
 * @brief
 *  Checks the status of the test and
 *  gives out the status integer
 *
 * @param stdout_lines Output strings of the test
 * @return Status integer
 */
static ipping_test_status_t ipping_parse_status(amxc_llist_t* stdout_lines) {
    SAH_TRACEZ_IN(ME);
    ipping_test_status_t status = ipping_test_not_complete;
    int num_of_lines = amxc_llist_size(stdout_lines);

    when_false(num_of_lines > 0, exit);

    if((mod_ipd_find_line(stdout_lines, "connect: Network is unreachable") != -1) ||
       (mod_ipd_find_line(stdout_lines, "connect: Network unreachable") != -1)) {
        status = ipping_test_error_err_internal;
        goto exit;
    }
    if((mod_ipd_find_line(stdout_lines, "unknown host") != -1) ||
       (mod_ipd_find_line(stdout_lines, "Address family for hostname not supported") != -1) ||
       (mod_ipd_find_line(stdout_lines, "Name does not resolve") != -1) ||
       (mod_ipd_find_line(stdout_lines, "Try again") != -1) ||
       (mod_ipd_find_line(stdout_lines, "Unknown error") != -1)) {
        status = ipping_test_error_err_host_name;
        goto exit;
    }
    if((mod_ipd_find_line(stdout_lines, "pipe ") != -1) ||
       (mod_ipd_find_line(stdout_lines, "Name or service not known") != -1)) {
        status = ipping_test_error_err_route;
        goto exit;
    }
    if(mod_ipd_find_line(stdout_lines, "ping statistics") != -1) {
        status = ipping_test_error_complete;
        goto exit;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

/**
   @brief
   Parse result from the standard output string.

   @param[in] str amxc_string_t object with the string to be parsed.
   @param[out] ret Variant with the datamodel format.

   @return
   Test status based on the test type
 */
static ipping_test_status_t ipping_parse_result(amxc_string_t* str_stdout, amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    //Set the status to Not_Complete
    ipping_test_status_t status = ipping_test_not_complete;
    amxc_llist_t stdout_lines;
    amxc_var_t* ret_status = NULL;
    int num_of_lines = 0;

    amxc_llist_init(&stdout_lines);
    amxc_string_split_to_llist(str_stdout, &stdout_lines, '\n');
    num_of_lines = amxc_llist_size(&stdout_lines);
    when_false(num_of_lines > 0, exit);

    /*Parse status*/
    status = ipping_parse_status(&stdout_lines);
    if(status != ipping_test_not_complete) {
        ipping_parse_final_results(ret, status, &stdout_lines);
    }

exit:
    // Replace status in output variant
    ret_status = GET_ARG(ret, IPPING_RET_KEY_PST);
    amxc_var_set(cstring_t, ret_status, str_ipping_test_status[status]);
    amxc_llist_clean(&stdout_lines, amxc_string_list_it_free);
    SAH_TRACEZ_OUT(ME);

    return status;
}

/**
 * @brief
 * Check if the test is running
 *
 * @param function_name
 * @param params
 * @param ret
 * @return int
 */
int mod_ipping_test_check(UNUSED const char* function_name,
                          amxc_var_t* params,
                          amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxp_subproc_t* proc_ipping = NULL;
    amxc_string_t string_stdout;
    amxc_var_t* stat_var = NULL;
    int ret_value = 0;
    ipping_test_status_t status = ipping_test_error_err_other;
    const cstring_t status_string = NULL;
    const int32_t pid = GETP_INT32(params, IPPING_RET_KEY_PROC "." IPPING_RET_KEY_PROC_PID);
    const int32_t fd_stdout = GETP_INT32(params, IPPING_RET_KEY_PROC "." IPPING_RET_KEY_PROC_FD_STDOUT);
    const int32_t fd_stderr = GETP_INT32(params, IPPING_RET_KEY_PROC "." IPPING_RET_KEY_PROC_FD_STDERR);

    amxc_var_move(ret, params);
    amxc_string_init(&string_stdout, 0);

    when_true_trace(fd_stdout == 0, not_complete, ERROR, "No test is running");
    when_true_trace(fd_stderr == 0, not_complete, ERROR, "No test is running");
    when_true_trace(pid == 0, not_complete, ERROR, "No test is running");

    mod_ipd_read_from_file(IPPING_RESULT_FILE, &string_stdout);
    proc_ipping = amxp_subproc_find(pid);

#ifndef IP_DIAGNOSTICS_UNIT_TEST
    when_null_trace(proc_ipping, exit, ERROR, "No sub process found");
#endif

    ret_value = amxp_subproc_wait(proc_ipping, 1);
#ifdef IP_DIAGNOSTICS_UNIT_TEST
    ret_value = 1;
#endif
    if(ret_value == 1) {
        mod_ipd_read_from_fd(fd_stdout, &string_stdout);
        mod_ipd_read_from_fd(fd_stderr, &string_stdout);
        ipping_parse_result(&string_stdout, ret);
        // Treat the case where the process stalls but the results are provided
        status_string = GET_CHAR(ret, IPPING_RET_KEY_PST);
        if(status_string != NULL) {
            status = ipping_st_str_to_st_int(status_string);
            if(status < ipping_test_not_complete) {
                goto exit;
            }
        }
        goto not_complete;
    }

    status = ipping_test_error_err_internal;
    when_failed_trace(ret_value, exit, ERROR, "Failed while running ping process. with for %s", GET_CHAR(ret, IPPING_RET_KEY_CMD));

    mod_ipd_read_from_fd(fd_stdout, &string_stdout);
    mod_ipd_read_from_fd(fd_stderr, &string_stdout);
    status = ipping_parse_result(&string_stdout, ret);
    if(status == ipping_test_not_complete) {
        SAH_TRACEZ_ERROR(ME, "Process not running while output shows it's not complete.");
        status = ipping_test_error_err_other;
        goto exit;
    }

exit:
    // Kill the process in case something goes wrong.
    amxp_subproc_kill(proc_ipping, SIGTERM);
    amxp_subproc_wait(proc_ipping, 5);
    ipping_kill_ipping_test(ret);
    amxp_subproc_delete(&proc_ipping);

    ipping_delete_result_file();
    ipping_delete_test_script();
    // Replace the status in the output variant
    stat_var = GET_ARG(ret, IPPING_RET_KEY_PST);
    amxc_var_set(cstring_t, stat_var, (cstring_t) str_ipping_test_status[status]);

not_complete:
    if(status == ipping_test_not_complete) {
        ipping_write_result_file(&string_stdout);
    }

    amxc_string_clean(&string_stdout);

    SAH_TRACEZ_OUT(ME);
    return status;
}
