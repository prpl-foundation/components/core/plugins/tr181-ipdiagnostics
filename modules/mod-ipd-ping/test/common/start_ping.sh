#!/bin/bash

set +e
IS_PING_RUNNING=$(pgrep ping)
if [ ! -z "${IS_PING_RUNNING}" -a "${IS_PING_RUNNING}" != "" -a "${IS_PING_RUNNING}" != " " ]; then
	pkill ping
fi

if  ping -V | grep -q 'iputils' ; then
	echo 
    echo
    echo "Ping from IPUtils is installed"
    echo 
    echo
    exit 0
else
    echo 
    echo "No ping from IPUtils is installed, test will fail"
    echo 
    exit 1
fi