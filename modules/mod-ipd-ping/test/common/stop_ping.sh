#!/bin/bash

set +e
IS_PING_RUNNING=$(pgrep ping)
if [ ! -z "${IS_PING_RUNNING}" -a "${IS_PING_RUNNING}" != "" -a "${IS_PING_RUNNING}" != " " ]; then
	pkill ping
fi