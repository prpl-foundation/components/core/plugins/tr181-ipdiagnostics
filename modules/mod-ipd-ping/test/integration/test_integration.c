/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "../common/mock.h"
#include "test_integration.h"

#include "ip_diagnostics.h"
#include "ipping.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;

#define IP_DIAGNOSTICS_ODL "../../../../odl/tr181-ipdiagnostics_definition.odl"

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser,
                        amxb_get_fd(bus_ctx),
                        connection_read,
                        "dummy:/tmp/dummy.sock",
                        AMXO_BUS,
                        bus_ctx);
    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxo_parser_parse_file(&parser, IP_DIAGNOSTICS_ODL, root_obj), 0);

    handle_events();

    return 0;
}

int test_teardown(UNUSED void** state) {
    amxb_free(&bus_ctx);
    amxm_close_all();
    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

void test_can_load_module(UNUSED void** state) {
    amxm_shared_object_t* so = NULL;
    assert_int_equal(amxm_so_open(&so, "target_module", "../modules_under_test/target_module.so"), 0);
}

void test_ipd_ipping_start(UNUSED void** state) {
    amxc_var_t start_ret;
    amxc_var_t ret;
    amxc_var_t* data;
    int start_rv = 0;
    int end_rv = ipping_test_not_complete;

    data = dummy_read_json_from_file("../common/test_data/test_ipping.json");
    assert_non_null(data);

    amxc_var_init(&start_ret);

    start_rv = amxm_execute_function("target_module", MOD_IPD_PING_CTRL, IPD_IPPING_TEST_START, data, &start_ret);

    amxc_var_delete(&data);
    if(start_rv != 0) {
        amxc_var_clean(&start_ret);
    }

    assert_int_equal(start_rv, 0);

    amxc_var_init(&ret);

    while(end_rv == ipping_test_not_complete) {
        end_rv = amxm_execute_function("target_module", MOD_IPD_PING_CTRL, IPD_IPPING_TEST_CHECK, &start_ret, &ret);
        amxc_var_move(&start_ret, &ret);
    }
    amxc_var_dump(&start_ret, STDOUT_FILENO);

    amxc_var_clean(&start_ret);
    amxc_var_clean(&ret);

    assert_int_equal(end_rv, ipping_test_error_complete);
}

void test_ipd_ipping_stop(UNUSED void** state) {
    amxc_var_t start_ret_1;
    amxc_var_t ret;
    amxc_var_t* data_1;
    int rv = 0;
    int start_rv_1 = 0;
    int32_t pid_1;
    amxp_subproc_t* proc_iperf_1 = NULL;

    data_1 = dummy_read_json_from_file("../common/test_data/test_ipping_long.json");
    assert_non_null(data_1);

    amxc_var_init(&start_ret_1);
    amxc_var_set_type(&start_ret_1, AMXC_VAR_ID_HTABLE);

    start_rv_1 = amxm_execute_function("target_module", MOD_IPD_PING_CTRL, IPD_IPPING_TEST_START, data_1, &start_ret_1);

    amxc_var_delete(&data_1);

    pid_1 = (int32_t) GETP_INT32(&start_ret_1, IPPING_RET_KEY_PROC "." IPPING_RET_KEY_PROC_PID);

    assert_int_equal(start_rv_1, 0);

    amxc_var_init(&ret);
    amxc_var_set_type(&ret, AMXC_VAR_ID_HTABLE);
    sleep(10);

    rv = amxm_execute_function("target_module", MOD_IPD_PING_CTRL, IPD_IPPING_TEST_STOP, &start_ret_1, &ret);

    proc_iperf_1 = amxp_subproc_find(pid_1);
    amxp_subproc_kill(proc_iperf_1, SIGTERM);
    amxp_subproc_delete(&proc_iperf_1);

    amxc_var_clean(&ret);

    assert_int_equal(rv, 0);
}

