/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <fcntl.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include "../common/mock.h"
#include "test_unit_common.h"

#include "ip_diagnostics.h"

#include "mod_ipd_common.h"
#include "mod_ipd_ping_common.h"

static void file_to_string(amxc_string_t* string, const char* file_path) {
    char buff[256 + 1];
    FILE* fp = fopen(file_path, "r");

    assert_non_null(string);

    if(fp != NULL) {
        size_t newLen = fread(buff, sizeof(char), 256, fp);
        if(ferror(fp) != 0) {
            fputs("Error reading file", stderr);
        } else {
            buff[newLen++] = '\0';
        }
        fclose(fp);
        amxc_string_setf(string, "%s", buff);
        amxc_string_trim(string, NULL);
    } else {
        amxc_string_set(string, "");
    }

}

static bool move_file_content(const char* src, const char* dest) {
    FILE* fptr1 = NULL;
    FILE* fptr2 = NULL;
    int c = 0;
    bool rv = false;

    when_str_empty(src, exit);
    when_str_empty(dest, exit);

    // Open one file for reading
    fptr1 = fopen(src, "r");
    if(fptr1 == NULL) {
        printf("Cannot open file %s\n", src);
        goto exit;
    }

    // Open another file for writing
    fptr2 = fopen(dest, "w");
    if(fptr2 == NULL) {
        printf("Cannot open file %s\n", dest);
        goto exit;
    }

    // Read contents from file
    c = fgetc(fptr1);
    while(c != EOF) {
        fputc(c, fptr2);
        c = fgetc(fptr1);
    }

    rv = true;

exit:
    if(fptr1 != NULL) {
        fclose(fptr1);
    }
    if(fptr2 != NULL) {
        fclose(fptr2);
    }
    return rv;
}

void test_can_parse_ip_route_ipv4(UNUSED void** state) {
    assert_true(move_file_content("../common/test_data/test_can_parse_ip_route_ipv4", "../common/test_data/test_can_parse_ip_route"));
    char* intf_address = mod_ipd_find_interface_ip_address("172.217.168.238");
    assert_string_equal(intf_address, "192.168.99.191");
    free(intf_address);
}

void test_can_parse_ip_route_ipv6(UNUSED void** state) {
    assert_true(move_file_content("../common/test_data/test_can_parse_ip_route_ipv6", "../common/test_data/test_can_parse_ip_route"));
    char* intf_address = mod_ipd_find_interface_ip_address("2a00:1450:400e:801::200e");
    assert_string_equal(intf_address, "2a02:1802:94:99:ae91:9bff:fe3a:5d98");
    free(intf_address);
}

void test_find_interface_ipv4(UNUSED void** state) {
    amxc_string_t line;
    amxc_string_init(&line, 0);
    char* ip = NULL;

    assert_true(move_file_content("../common/test_data/test_can_parse_ip_route_ipv4", "../common/test_data/test_can_parse_ip_route"));
    file_to_string(&line, "../common/test_data/test_find_interface_ipv4");
    ip = ipping_find_interface_ip(&line, true);
    assert_string_equal(ip, "192.168.99.191");

    free(ip);
    amxc_string_clean(&line);
}

void test_find_interface_ipv6(UNUSED void** state) {
    amxc_string_t line;
    amxc_string_init(&line, 0);
    char* ip = NULL;

    assert_true(move_file_content("../common/test_data/test_can_parse_ip_route_ipv6", "../common/test_data/test_can_parse_ip_route"));
    file_to_string(&line, "../common/test_data/test_find_interface_ipv6");
    ip = ipping_find_interface_ip(&line, true);
    assert_string_equal(ip, "2a02:1802:94:99:ae91:9bff:fe3a:5d98");

    free(ip);
    amxc_string_clean(&line);
}

void test_find_interface_ipv4_from(UNUSED void** state) {
    amxc_string_t line;
    amxc_string_init(&line, 0);
    char* ip = NULL;

    file_to_string(&line, "../common/test_data/test_find_interface_ipv4_from");
    ip = ipping_find_interface_ip(&line, false);
    assert_string_equal(ip, "192.168.1.75");

    free(ip);
    amxc_string_clean(&line);
}

void test_find_interface_ipv6_from(UNUSED void** state) {
    amxc_string_t line;
    amxc_string_init(&line, 0);
    char* ip = NULL;

    file_to_string(&line, "../common/test_data/test_find_interface_ipv6_from");
    ip = ipping_find_interface_ip(&line, false);
    assert_string_equal(ip, "2a02:1802:94:4010::1");

    free(ip);
    amxc_string_clean(&line);
}
