/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MOD_IPD_TRCR_COMMON_H__)
#define __MOD_IPD_TRCR_COMMON_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include "traceroute.h"

// Regex expression
#define TRCR_REGEX_SEARCH               "traceroute to"
#define TRCR_REGEX_HOP_HOST             "\\s+([A-Za-z0-9:\\-\\:]+(\\.[A-Za-z0-9:\\-\\:]+)+)\\s"
#define TRCR_REGEX_HOP_HOSTADDR         "\\s\\(([A-Za-z0-9\\:]+(\\.[A-Za-z0-9\\:]+)+)\\)\\s+"
#define TRCR_REGEX_INT_VAL              "[0-9]+"
#define TRCR_REGEX_DOUBLE_VAL           "[0-9]*\\.[0-9]+"
#define TRCR_REGEX_MAX_NUM_HOPS         " [0-9]+ hops max"
#define TRCR_REGEX_HOP                  "\\s+[0-9]*\\.[0-9]+\\sms"
#define TRCR_REGEX_HOP_W_NAME           "\\s.*\\s\\(.*\\)\\s"
#define TRCR_REGEX_HOP_TIME_EXC         "\\s\\*\\s"
#define TRCR_REGEX_HOP_ADMIN_DENIED     "\\s!A\\s"
#define TRCR_REGEX_HOP_TMT              "\\s!T\\s"
#define TRCR_REGEX_HOP_SRC_Q            "\\s!Q\\s"
#define TRCR_REGEX_HOP_PORT_UNREACH     "\\s!U\\s"
#define TRCR_REGEX_HOP_NET_UNREACH      "\\s!N\\s"
#define TRCR_REGEX_HOP_PROT_UNREACH     "\\s!P\\s"
#define TRCR_REGEX_HOP_UNKW_P_TYPE      "\\s!\\?"

//file names
#define TRCR_SCRIPT_FILE "traceroute-test.sh"
#define TRCR_RESULT_FILE "traceroute-result"

int trcr_kill_script_test(void);
int trcr_kill_trcr_test(amxc_var_t* params);
void trcr_delete_result_file(void);
void trcr_delete_test_script(void);
trcr_test_status_t trcr_st_str_to_st_int(const cstring_t status_string);
char* trcr_find_ip_address_used(amxc_string_t* trcr_target_host);

#ifdef __cplusplus
}
#endif

#endif // __MOD_IPD_TRCR_COMMON_H__