/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_types.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_ipd_trcr.h"
#include "mod_ipd_trcr_common.h"
#include "mod_ipd_trcr_status.h"
#include "traceroute.h"
#include "ip_diagnostics.h"
#include "mod_ipd_common.h"

// module trace zone
#define ME "mod-t-check"

// The response time (in miliseconds) of traceroute test
static uint32_t _trcr_avg_responsetime;
static const cstring_t trcr_hop_id[] = {
    TRCR_REGEX_HOP_TIME_EXC,
    TRCR_REGEX_HOP_ADMIN_DENIED,
    TRCR_REGEX_HOP_TMT,
    TRCR_REGEX_HOP_SRC_Q,
    TRCR_REGEX_HOP_PORT_UNREACH,
    TRCR_REGEX_HOP_NET_UNREACH,
    TRCR_REGEX_HOP_PROT_UNREACH,
    TRCR_REGEX_HOP_UNKW_P_TYPE,
    TRCR_REGEX_HOP_W_NAME,
    TRCR_REGEX_HOP,
    NULL
};

/**
 * @brief
 * Find the interface ip used in the current traceroute test
 *
 * @param (amxc_string_t*) trcr_target_host: a string containing the target host IP address for traceroute
 * @return (char*) IPAddressUsed linked to the current interface
 */
char* trcr_find_ip_address_used(amxc_string_t* trcr_target_host) {
    amxc_string_t extracted_string;
    amxc_string_t target_ipaddress_string;
    char* intf_ip = NULL;
    int rv = 0;

    amxc_string_init(&extracted_string, 0);
    amxc_string_init(&target_ipaddress_string, 0);

    when_null_trace(trcr_target_host, exit, ERROR, "Input amxc_string_t structure is NULL");

    /* Check if the target host IP is IPv4 or IPv6 and parse the value */
    //Parse IPv4 address
    rv = mod_ipd_extract_regex_from_string(trcr_target_host, MOD_IPD_REGEX_HOST_IPV4, &extracted_string);
    if(rv == 0) {
        rv = mod_ipd_extract_regex_from_string(&extracted_string, MOD_IPD_REGEX_IPV4, &target_ipaddress_string);
        goto find_intf_ip;
    }

    //Parse IPv6 address
    rv = mod_ipd_extract_regex_from_string(trcr_target_host, MOD_IPD_REGEX_HOST_IPV6, &extracted_string);
    if(rv == 0) {
        rv = mod_ipd_extract_regex_from_string(&extracted_string, MOD_IPD_REGEX_IPV6, &target_ipaddress_string);
        goto find_intf_ip;
    }

    /* Find the interface's IP Address used to run the traceroute test */
find_intf_ip:
    intf_ip = (rv == 0) ? mod_ipd_find_interface_ip_address(amxc_string_get(&target_ipaddress_string, 0)) : strdup("");

exit:
    amxc_string_clean(&extracted_string);
    amxc_string_clean(&target_ipaddress_string);
    return intf_ip;
}

/**
 * @brief
 * Function that parses the hop info contained in the string into a variant
 *
 * @param line The hop string
 * @param hop The output hop variant
 * @return int
 */
static int trcr_hop_from_string(amxc_string_t* line, amxc_var_t* hop) {
    SAH_TRACEZ_IN(ME);
    int rv = 0;
    int ret = -1;
    uint32_t num_tries = 0;
    amxc_string_t aux_string;
    amxc_string_t extracted_string;
    amxc_string_t val;
    amxc_string_t rttimes;
    amxc_var_t* var = NULL;
    uint32_t route_trip_times_val = 0;
    uint32_t rttimes_sum = 0;
    uint8_t rttimes_cnt = 0;

    amxc_string_init(&aux_string, 0);
    amxc_string_init(&extracted_string, 0);
    amxc_string_init(&rttimes, 0);
    amxc_string_init(&val, 0);

    when_null_trace(line, exit, ERROR, "Line string is empty");
    when_null_trace(hop, exit, ERROR, "Hop variant is empty");

    amxc_var_add_key(cstring_t, hop, "Host", "");
    amxc_var_add_key(cstring_t, hop, "HostAddress", "");
    amxc_var_add_key(uint32_t, hop, "ErrorCode", 0);
    amxc_var_add_key(cstring_t, hop, "RTTimes", "");

    amxc_string_setf(&aux_string, "%s", amxc_string_get(line, 0));

    for(int i = 0; trcr_hop_id[i] != NULL; i++) {
        while(rv == 0) {
            rv = mod_ipd_extract_regex_from_string(&aux_string, trcr_hop_id[i], &extracted_string);
            if(rv != 0) {
                break;
            }
            num_tries = amxc_string_replace(&aux_string, amxc_string_get(&extracted_string, 0), "", UINT32_MAX);
            if((strcmp(trcr_hop_id[i], TRCR_REGEX_HOP_TIME_EXC) == 0)) {
                for(; num_tries != 0; num_tries--) {
                    amxc_string_appendf(&rttimes, ",");
                }
                var = amxc_var_get_key(hop, "ErrorCode", AMXC_VAR_FLAG_DEFAULT);
                amxc_var_set(uint32_t, var, 13);
                ret = 0;
            } else if((strcmp(trcr_hop_id[i], TRCR_REGEX_HOP_NET_UNREACH) == 0)) {
                var = amxc_var_get_key(hop, "ErrorCode", AMXC_VAR_FLAG_DEFAULT);
                amxc_var_set(uint32_t, var, 13);
                ret = 0;
            } else if((strcmp(trcr_hop_id[i], TRCR_REGEX_HOP) == 0)) {
                // Search for an RTTime value
                rv = mod_ipd_extract_regex_from_string(&extracted_string, TRCR_REGEX_DOUBLE_VAL, &val);
                if(rv != 0) {
                    break;
                }
                rv = mod_ipd_extract_regex_from_string(&val, TRCR_REGEX_INT_VAL, &extracted_string);
                if(rv != 0) {
                    break;
                }

                // Round trip times of less than 1 milliseconds MUST be rounded up to 1 (From BBF TR-181 data model definition)
                route_trip_times_val = atoi(amxc_string_get(&extracted_string, 0));
                if(route_trip_times_val < 1) {
                    route_trip_times_val = 1;
                }

                amxc_string_appendf(&rttimes, "%d,", route_trip_times_val);

                rttimes_cnt++;
                rttimes_sum += route_trip_times_val;
                _trcr_avg_responsetime = rttimes_sum / rttimes_cnt;
                ret = 0;
            } else if((strcmp(trcr_hop_id[i], TRCR_REGEX_HOP_W_NAME) == 0)) {
                //search for ip address and set it
                rv = mod_ipd_extract_regex_from_string(&extracted_string, TRCR_REGEX_HOP_HOSTADDR, &val);
                if(rv != 0) {
                    rv = mod_ipd_extract_regex_from_string(&extracted_string, MOD_IPD_REGEX_IPV6, &val);
                    if(rv != 0) {
                        break;
                    }
                }
                amxc_string_replace(&val, " (", "", UINT32_MAX);
                amxc_string_replace(&val, ") ", "", UINT32_MAX);
                var = amxc_var_get_key(hop, "HostAddress", AMXC_VAR_FLAG_DEFAULT);
                amxc_var_set(cstring_t, var, amxc_string_get(&val, 0));

                //Search for domain name and set it
                var = amxc_var_get_key(hop, "Host", AMXC_VAR_FLAG_DEFAULT);
                rv = mod_ipd_extract_regex_from_string(&extracted_string, TRCR_REGEX_HOP_HOST, &val);
                if(rv != 0) {
                    amxc_var_set(cstring_t, var, GET_CHAR(hop, "HostAddress"));
                    goto skip_host;
                }
                amxc_string_trim(&val, NULL);
                amxc_var_set(cstring_t, var, amxc_string_get(&val, 0));
skip_host:
                ret = 0;
            }
        }
        amxc_string_setf(&aux_string, "%s", amxc_string_get(line, 0));
        rv = 0;
    }

    //Set RTTimes string in the variant
    var = amxc_var_get_key(hop, "RTTimes", AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(cstring_t, var, amxc_string_get(&rttimes, 0));

exit:
    amxc_string_clean(&aux_string);
    amxc_string_clean(&extracted_string);
    amxc_string_clean(&val);
    amxc_string_clean(&rttimes);
    SAH_TRACEZ_OUT(ME);
    return ret;
}

/**
 * @brief
 * Function that parses the result hops of the traceroute test
 *
 * @param ret Output variant
 * @param status Status
 * @param stdout_lines List of strings that contains all the hops from the traceroute test
 */
static void trcr_parse_final_results(amxc_var_t* ret, trcr_test_status_t status, amxc_llist_t* stdout_lines) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    uint32_t number_of_lines = 0;
    uint32_t line_num = 1;
    uint32_t index = 0;
    amxc_string_t hop_index;
    amxc_string_t* line = NULL;
    amxc_var_t hop;
    amxc_var_t* responsetime_para = NULL;
    amxc_var_t* the_hop = NULL;
    amxc_var_t* hops = NULL;
    amxc_var_t* rhnoe = NULL;
    amxc_var_t* ipaddressused = NULL;
    char* ip_address_str = NULL;

    amxc_string_init(&hop_index, 0);

    when_null_trace(ret, exit, ERROR, "Null return variant");
    when_null_trace(stdout_lines, exit, ERROR, "No input string to parse for final results");
    when_true((status != trcr_test_error_complete) && (status != trcr_test_error_err_max_hop_count), exit);

    number_of_lines = amxc_llist_size(stdout_lines);
    _trcr_avg_responsetime = 0; // reset to default value

    // Parse the route hops into the variant
    // The status ensures that only a hop will be contained in each line;
    hops = GET_ARG(ret, TRCR_RET_KEY_HOPS);
    rhnoe = GET_ARG(ret, TRCR_RET_KEY_RHNOE);
    for(; line_num < number_of_lines; line_num++) {
        amxc_var_init(&hop);
        amxc_var_set_type(&hop, AMXC_VAR_ID_HTABLE);
        line = amxc_string_get_from_llist(stdout_lines, line_num);
        rv = trcr_hop_from_string(line, &hop);
        if(rv == 0) {
            index++;
            amxc_string_setf(&hop_index, "%d", index);
            the_hop = amxc_var_add_key(amxc_htable_t, hops, amxc_string_get(&hop_index, 0), NULL);
            amxc_var_copy(the_hop, &hop);
        }
        amxc_var_clean(&hop);
    }
    amxc_var_set(uint32_t, rhnoe, index);

    /* Search for the line containing "traceroute to" to find the target IP address */
    for(line_num = 0; line_num < number_of_lines; line_num++) {
        line = amxc_string_get_from_llist(stdout_lines, line_num);
        if(amxc_string_search(line, TRCR_REGEX_SEARCH, 0) != -1) {
            break;
        }
    }

    ip_address_str = trcr_find_ip_address_used(line);
    ipaddressused = amxc_var_get_key(ret, TRCR_RET_KEY_IPUSD, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(cstring_t, ipaddressused, (cstring_t) ip_address_str);

    responsetime_para = amxc_var_get_key(ret, TRCR_RET_KEY_RSPTM, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(uint32_t, responsetime_para, (uint32_t) _trcr_avg_responsetime);

exit:
    free(ip_address_str);
    amxc_string_clean(&hop_index);
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Write the information from a string and save to the result file.

   @param[in] string string containing the information to be written.
 */
static void trcr_write_result_file(amxc_string_t* string) {
    mod_ipd_write_to_file(TRCR_RESULT_FILE, string);
}

/**
 * @brief
 *  Checks the status of the test and
 *  gives out the status integer
 *
 * @param stdout_lines Output strings of the test
 * @return Status integer
 */
static trcr_test_status_t trcr_parse_status(amxc_llist_t* stdout_lines, int proc_status) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t* line = NULL;
    amxc_string_t extracted_string;
    amxc_string_t val_string;
    trcr_test_status_t status = trcr_test_not_complete;
    uint32_t num_of_lines = amxc_llist_size(stdout_lines);
    int line_num = -1;
    int ip_line_num = -1;
    int rv = -1;
    amxc_llist_it_t* first_data = NULL;

    amxc_string_init(&extracted_string, 0);
    amxc_string_init(&val_string, 0);

    when_false(num_of_lines > 0, exit);

    if(mod_ipd_find_line(stdout_lines, "traceroute: can't connect to remote host: Network is unreachable") != -1) {
        status = trcr_test_error_err_route;
        goto exit;
    }
    if(mod_ipd_find_line(stdout_lines, "traceroute: bad address") != -1) {
        status = trcr_test_error_err_host_name;
        goto exit;
    }
    if(mod_ipd_find_line(stdout_lines, "traceroute: bind: Invalid argument") != -1) {
        status = trcr_test_error_err_internal;
        goto exit;
    }
    // Handle maximum number of hops reached
    // Find the max hops number
    line_num = mod_ipd_find_line(stdout_lines, "hops max");
    if(line_num != -1) {
        // extract max hops number
        line = amxc_string_get_from_llist(stdout_lines, line_num);
        rv = mod_ipd_extract_regex_from_string(line, "\\(([^\\)]+)\\)", &extracted_string);
        when_failed_trace(rv, exit, ERROR, "Failed to fetch %s from %s", "\\(([^\\)]+)\\)", amxc_string_get(line, 0));
        amxc_string_replace(&extracted_string, "(", "", UINT32_MAX);
        amxc_string_replace(&extracted_string, ")", "", UINT32_MAX);
        // find the ip address in the list of lines
        first_data = amxc_llist_take_first(stdout_lines);
        ip_line_num = mod_ipd_find_line(stdout_lines, amxc_string_get(&extracted_string, 0));
        amxc_llist_prepend(stdout_lines, first_data);
        first_data = NULL;
        if((ip_line_num == -1) && ((mod_ipd_find_line(stdout_lines, "test done") != -1) || (proc_status == 0))) {
            status = trcr_test_error_err_max_hop_count;
            goto exit;
        }
    }
    if((proc_status == 0) || (mod_ipd_find_line(stdout_lines, "test done") != -1)) {
        status = trcr_test_error_complete;
        goto exit;
    }
exit:
    amxc_string_clean(&val_string);
    amxc_string_clean(&extracted_string);
    SAH_TRACEZ_OUT(ME);
    return status;
}

/**
 * @brief
 * Parse result from the standard output string.
 *
 * @param str_stdout amxc_string_t object with the string to be parsed.
 * @param ret Variant with the datamodel format.
 * @return trcr_test_status_t
 */
static trcr_test_status_t trcr_parse_result(amxc_string_t* str_stdout, amxc_var_t* ret, int proc_status) {
    SAH_TRACEZ_IN(ME);
    trcr_test_status_t status = trcr_test_not_complete;
    amxc_llist_t stdout_lines;
    amxc_var_t* ret_status = NULL;
    int num_of_lines = 0;

    amxc_llist_init(&stdout_lines);
    amxc_string_split_to_llist(str_stdout, &stdout_lines, '\n');
    num_of_lines = amxc_llist_size(&stdout_lines);
    when_false(num_of_lines > 0, exit);

    // Parse status and results
    status = trcr_parse_status(&stdout_lines, proc_status);
    if(status != trcr_test_not_complete) {
        trcr_parse_final_results(ret, status, &stdout_lines);
    }

exit:
    // Replace status in output variant
    ret_status = amxc_var_get_key(ret, TRCR_RET_KEY_DST, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(cstring_t, ret_status, str_trcr_test_status[status]);
    amxc_llist_clean(&stdout_lines, amxc_string_list_it_free);
    SAH_TRACEZ_OUT(ME);

    return status;
}

/**
 * @brief
 * Check if the test is running
 *
 * @param function_name
 * @param params
 * @param ret
 * @return int
 */
int mod_trcr_test_check(UNUSED const char* function_name,
                        amxc_var_t* params,
                        amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxp_subproc_t* proc_trcr = NULL;
    amxc_string_t string_stdout;
    amxc_var_t* stat_var = NULL;
    int ret_value = 0;
    trcr_test_status_t status = trcr_test_error_err_other;
    const cstring_t status_string = NULL;
    const int32_t pid = GETP_INT32(params, TRCR_RET_KEY_PROC "." TRCR_RET_KEY_PROC_PID);
    const int32_t fd_stdout = GETP_INT32(params, TRCR_RET_KEY_PROC "." TRCR_RET_KEY_PROC_FD_STDOUT);
    const int32_t fd_stderr = GETP_INT32(params, TRCR_RET_KEY_PROC "." TRCR_RET_KEY_PROC_FD_STDERR);

    amxc_var_move(ret, params);
    amxc_string_init(&string_stdout, 0);

    when_true_trace(fd_stdout == 0, not_complete, ERROR, "No test is running");
    when_true_trace(fd_stderr == 0, not_complete, ERROR, "No test is running");
    when_true_trace(pid == 0, not_complete, ERROR, "No test is running");

    mod_ipd_read_from_file(TRCR_RESULT_FILE, &string_stdout);

    proc_trcr = amxp_subproc_find(pid);
    ret_value = amxp_subproc_wait(proc_trcr, 1);

    if(ret_value == 1) {
        mod_ipd_read_from_fd(fd_stdout, &string_stdout);
        mod_ipd_read_from_fd(fd_stderr, &string_stdout);
        trcr_parse_result(&string_stdout, ret, ret_value);
        // Treat the case where the process stalls but the results are provided
        status_string = GETP_CHAR(ret, TRCR_RET_KEY_DST);
        if(status_string != NULL) {
            status = trcr_st_str_to_st_int(status_string);
            if(status < trcr_test_not_complete) {
                goto exit;
            }
        }
        goto not_complete;
    }

    status = trcr_test_error_err_internal;
    when_failed_trace(ret_value, exit, ERROR, "Failed while running traceroute process. with %s", GET_CHAR(ret, TRCR_RET_KEY_CMD));

    mod_ipd_read_from_fd(fd_stdout, &string_stdout);
    mod_ipd_read_from_fd(fd_stderr, &string_stdout);
    status = trcr_parse_result(&string_stdout, ret, ret_value);
    if(status == trcr_test_not_complete) {
        SAH_TRACEZ_ERROR(ME, "Process not running while output shows it's not complete.");
        status = trcr_test_error_err_other;
        goto exit;
    }

exit:
    // Kill the process in case something goes wrong.
    amxp_subproc_kill(proc_trcr, SIGTERM);
    amxp_subproc_wait(proc_trcr, 5);
    trcr_kill_trcr_test(ret);
    amxp_subproc_delete(&proc_trcr);

    trcr_delete_result_file();
    trcr_delete_test_script();
    // Replace the status in the output variant
    stat_var = amxc_var_get_key(ret, TRCR_RET_KEY_DST, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(cstring_t, stat_var, (cstring_t) str_trcr_test_status[status]);

not_complete:
    if(status == trcr_test_not_complete) {
        trcr_write_result_file(&string_stdout);
    }

    amxc_string_clean(&string_stdout);

    SAH_TRACEZ_OUT(ME);
    return status;
}
