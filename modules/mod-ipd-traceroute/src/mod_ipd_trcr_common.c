/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_dm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_ipd_trcr_common.h"
#include "mod_ipd_trcr.h"
#include "mod_ipd_trcr_status.h"
#include "mod_ipd_common.h"
#include "traceroute.h"

#define ME "mod-t-com"

/**
   @brief
   Delete the result file according to the test type.

   @param[in] params Input parameters.
   @param[in] test download or upload.
 */
void trcr_delete_test_script(void) {
    SAH_TRACEZ_IN(ME);

    mod_ipd_delete_filef(TRCR_SCRIPT_FILE);

    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Delete the result file according to the test type.

   @param[in] params Input parameters.
   @param[in] test download or upload.
 */
void trcr_delete_result_file(void) {
    SAH_TRACEZ_IN(ME);

    mod_ipd_delete_filef("%s", TRCR_RESULT_FILE);

    SAH_TRACEZ_OUT(ME);
}

/**
 * @brief
 * Kills the traceroute process by using the generated command of the test
 *
 */
int trcr_kill_trcr_test(amxc_var_t* params) {
    SAH_TRACEZ_IN(ME);
    int rv = 0;
    amxp_subproc_t* proc_trcr = NULL;
    const int pid = GETP_INT32(params, TRCR_RET_KEY_PROC "." TRCR_RET_KEY_PROC_PID);

    proc_trcr = amxp_subproc_find(pid);
    amxp_subproc_kill(proc_trcr, SIGKILL);
    rv = amxp_subproc_wait(proc_trcr, 1);

    if(rv == 0) {
        SAH_TRACEZ_INFO(ME, "TraceRoute test killed successfully");
    }

    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
   @brief
   Kill a specific script process.
   @return
   Return of the kill command.
 */
int trcr_kill_script_test(void) {
    SAH_TRACEZ_IN(ME);
    int rv = 0;
    amxp_subproc_t* kill_proc = NULL;

    amxp_subproc_new(&kill_proc);

    rv = amxp_subproc_start(kill_proc, (char*) "pkill", "-9", "-f", TRCR_SCRIPT_FILE, NULL);
    when_failed_trace(rv, exit, ERROR, "Failed to launch the subprocess");
    when_failed_trace(amxp_subproc_get_exitstatus(kill_proc), exit, ERROR, "Unable to kill the process");

exit:
    mod_ipd_delete_filef(TRCR_SCRIPT_FILE);

    amxp_subproc_delete(&kill_proc);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief
 * Converts the string status into its int homologue
 *
 * @param status_string IPPing string status
 * @return ipping_test_status_t
 */
trcr_test_status_t trcr_st_str_to_st_int(const cstring_t status_string) {
    SAH_TRACEZ_IN(ME);
    trcr_test_status_t status = trcr_test_none;

    when_str_empty_trace(status_string, exit, ERROR, "Status_string empty");

    for(; status < trcr_test_num_of_status; status++) {
        if(strcmp(str_trcr_test_status[status], status_string) == 0) {
            goto exit;
        }
    }
    status = trcr_test_none;
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}