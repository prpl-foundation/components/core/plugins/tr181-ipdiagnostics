/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_ipd_trcr.h"
#include "mod_ipd_trcr_common.h"
#include "mod_ipd_trcr_status.h"
#include "mod_ipd_common.h"
#include "ip_diagnostics_priv.h"
#include "ip_diagnostics.h"
#include "traceroute.h"

// module trace zone
#define ME "mod-t-start"

static void trcr_script_array_it_free(amxc_array_it_t* it) {
    free(it->data);
}

/**
 * @brief
 * Creates the necessary data and file name that will be executed
 * for the traceroute test
 *
 * @param params Input parameters
 * @param cmd String with the command
 * @param sh_script Script file name in an array
 * @return int
 */
static int trcr_create_sh_script(amxc_var_t* params,
                                 amxc_string_t* cmd,
                                 amxc_array_t* sh_script) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    FILE* p_sh_file = NULL;

    when_null_trace(params, exit, ERROR, "Input parameter is NULL.");
    when_null_trace(cmd, exit, ERROR, "Command is NULL.");
    when_null_trace(sh_script, exit, ERROR, "Array is not initialised.");

    amxc_array_append_data(sh_script, strdup("sh"));
    amxc_array_append_data(sh_script, strdup(TRCR_SCRIPT_FILE));
    // Kills the previous script avoiding any conflicts
    trcr_kill_script_test();
    p_sh_file = fopen(TRCR_SCRIPT_FILE, "w");
    when_null_trace(p_sh_file, exit, ERROR, "Unable to create the shell script.");

    fprintf(p_sh_file, "#!/bin/bash\n\n");
    fprintf(p_sh_file, "set +x\n");
    fprintf(p_sh_file, "%s\n", amxc_string_get(cmd, 0));
    fprintf(p_sh_file, "echo \"test done\"\n");

    fclose(p_sh_file);
    chmod(TRCR_SCRIPT_FILE, 0755);

    // Deletes the previous results file
    mod_ipd_delete_filef("%s", TRCR_RESULT_FILE);
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief
 * Builds the command for launching the traceroute test
 *
 * @param params Input parameters
 * @param cmd Output command of the traceroute test
 * @return traceroute status
 */
static int trcr_create_command(amxc_var_t* params, amxc_string_t* cmd) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    bool ipv6 = false;
    uint32_t param_int = 0;
    unsigned char dscp = 0;
    amxc_string_t param_string;
    amxc_var_t* command = NULL;
    amxc_var_t* aux_var = NULL;
    uint32_t expected_size = 30 + (2 * sizeof(time_t));

    when_null_trace(params, exit, ERROR, "Input parameter is NULL.");
    when_null_trace(cmd, exit, ERROR, "Command string is not initialised.");

    amxc_string_clean(cmd);
    amxc_string_init(cmd, 0);
    amxc_string_init(&param_string, 0);

    //Protocol selection
    amxc_string_setf(&param_string, "%s", GET_CHAR(params, TRCR_RET_KEY_PRTCLV));
    when_str_empty_trace(amxc_string_get(&param_string, 0), exit, ERROR, "No IP protocol specified");
    // For now only ICMP is used for traceroute
    if(strcmp(amxc_string_get(&param_string, 0), "IPv4") == 0) {
        amxc_string_appendf(cmd, "traceroute -I -4 ");
    } else if(strcmp(amxc_string_get(&param_string, 0), "IPv6") == 0) {
        amxc_string_appendf(cmd, "traceroute -I -6 ");
        ipv6 = true;
    } else if(strcmp(amxc_string_get(&param_string, 0), "Any") == 0) {
        amxc_string_appendf(cmd, "traceroute -I ");
    } else {
        SAH_TRACEZ_ERROR(ME, "No valid protocol has been specified");
        goto exit;
    }

    // Number of tries
    param_int = GET_UINT32(params, TRCR_RET_KEY_NMBOTR);
    when_false_trace(param_int > 0, exit, ERROR, "Number of tries not set");
    amxc_string_appendf(cmd, "-q %u ", param_int);
    // Timeout
    param_int = GET_UINT32(params, TRCR_RET_KEY_TMT);
    when_false_trace(param_int > 0, exit, ERROR, "Timeout not set");
    if(param_int < 1000) {
        amxc_string_appendf(cmd, "-w %u ", 1);
    } else {
        amxc_string_appendf(cmd, "-w %u ", param_int / 1000);
    }
    // DSCP
    dscp = (unsigned char) GET_UINT32(params, TRCR_RET_KEY_DSCP);
    aux_var = GET_ARG(params, TRCR_RET_KEY_DSCP);
    if(dscp == 0) {
        amxc_var_set(uint32_t, aux_var, 0);
        goto skip_dscp;
    }
    amxc_string_appendf(cmd, "-t %d ", mod_ipd_dscp_to_tos(dscp));
skip_dscp:
    // Max number of hops
    param_int = GET_UINT32(params, TRCR_RET_KEY_MHC);
    when_false_trace(param_int > 0, exit, ERROR, "Timeout not set");
    amxc_string_appendf(cmd, "-m %u ", param_int);//param_int);
    // IP Interface
    amxc_string_setf(&param_string, "%s", GET_CHAR(params, TRCR_RET_KEY_NAME));
    if(!str_empty(amxc_string_get(&param_string, 0)) && ((strcmp(amxc_string_get(&param_string, 0), "") != 0) && (GET_CHAR(params, TRCR_RET_KEY_NAME) != NULL))) {
        amxc_string_appendf(cmd, "-i %s ", amxc_string_get(&param_string, 0));
    }
    // Host
    amxc_string_setf(&param_string, "%s", GET_CHAR(params, TRCR_RET_KEY_HST));
    when_str_empty_trace(amxc_string_get(&param_string, 0), exit, ERROR, "No Host specified");
    amxc_string_appendf(cmd, "%s ", amxc_string_get(&param_string, 0));
    // Data block size, if system support USE_TIME_BITS64 , minimum block size will be 46.
    param_int = GET_UINT32(params, TRCR_RET_KEY_DTBS);
    // if data size is set to 0, then use the default minimum value of traceroute
    if(param_int != 0) {
        when_false_trace(param_int >= expected_size, exit, ERROR, "Block size too small, data block size need to be at least %u and not %d", expected_size, param_int);
        when_true_trace(param_int < ( expected_size + 26 ) && ipv6, exit, ERROR, "Block size too small when ipv6 is chosen, data block size need to be at least %u and not %d", expected_size + 26, param_int);
        amxc_string_appendf(cmd, "%u", param_int);
    }

    command = amxc_var_get_key(params, TRCR_RET_KEY_CMD, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set(cstring_t, command, amxc_string_get(cmd, 0));
    rv = amxd_status_ok;
exit:
    amxc_string_clean(&param_string);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief
 * Starts the traceroute test process
 *
 * @param function_name
 * @param params Parameters passed to the traceroute tool
 * @param ret Results of the test if immediately finished
 * @return int
 */
int mod_trcr_test_start(UNUSED const char* function_name,
                        amxc_var_t* params,
                        amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    trcr_test_status_t status = 0;
    amxc_var_t* proc_htable = NULL;
    amxp_subproc_t* proc_trcr = NULL;
    int rv = -1;
    int32_t pid = -1;
    int32_t fd_stdout = -1;
    int32_t fd_stderr = -1;
    amxc_array_t sh_script;
    amxc_string_t cmd;
    amxc_var_t* var = NULL;

    amxc_string_init(&cmd, 0);
    amxc_var_new(&var);
    amxc_array_init(&sh_script, 1);

    rv = trcr_create_command(params, &cmd);
    when_failed_trace(rv, exit, ERROR, "Could not create the command, missing or wrong parameters");

    amxc_var_move(ret, params);

    rv = trcr_create_sh_script(ret, &cmd, &sh_script);
    when_failed_trace(rv, exit, ERROR, "Failed to create the shell script.");

    rv = amxp_subproc_new(&proc_trcr);
    when_failed_trace(rv, exit, ERROR, "Failed to initialize the subprocess.");

    fd_stdout = amxp_subproc_open_fd(proc_trcr, STDOUT_FILENO);
    fd_stderr = amxp_subproc_open_fd(proc_trcr, STDERR_FILENO);
    if((fd_stdout == -1) || (fd_stderr == -1)) {
        SAH_TRACEZ_ERROR(ME, "Failed to open file descriptor.");
        goto exit;
    }

    proc_htable = amxc_var_add_key(amxc_htable_t, ret, TRCR_RET_KEY_PROC, NULL);
    amxc_var_add_key(int32_t, proc_htable, TRCR_RET_KEY_PROC_FD_STDOUT, fd_stdout);
    amxc_var_add_key(int32_t, proc_htable, TRCR_RET_KEY_PROC_FD_STDERR, fd_stderr);

    rv = amxp_subproc_astart(proc_trcr, &sh_script);

    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to start traceroute process.");
        amxp_subproc_delete(&proc_trcr);
        goto exit;
    }

    pid = amxp_subproc_get_pid(proc_trcr);
    amxc_var_add_key(int32_t, proc_htable, TRCR_RET_KEY_PROC_PID, pid);

    status = trcr_test_not_complete;

exit:
    amxc_array_clean(&sh_script, trcr_script_array_it_free);
    amxc_string_clean(&cmd);

    if(status == trcr_test_not_complete) {
        amxc_var_set(cstring_t, var, (cstring_t) str_trcr_test_status[status]);
        amxc_var_set_pathf(ret, var, DEFAULT_SET_PATH_FLAGS, TRCR_RET_KEY_DST);
    }

    amxc_var_delete(&var);
    SAH_TRACEZ_OUT(ME);
    return status == trcr_test_not_complete ? 0:1;
}