/**
 * IPDiagnostics.UploadDiagnostics from TR-181 datamodel 2.15.1
 *
 * @version 1.0
 */
%define {
    select IPDiagnostics {
        /**
         * Comma-separated list of strings. Supported UploadDiagnostics transport protocols for a CPE device. Each list item is an enumeration of:
         * - HTTP
         * - FTP (OPTIONAL)
         *
         * @version 1.0
         */
        %persistent csv_string UploadTransports;

        /**
         * Indicates the maximum number of connections that are supported by Upload Diagnostics.
         *
         * @version 1.0
         */
        %persistent uint32 UploadDiagnosticsMaxConnections = 999 {
            on action validate call check_minimum 1;
        }

        /**
         * The maximum number of instances in UploadDiagnostics().IncrementalResult that the implementation will return.
         *
         * @version 1.0
         */
        %persistent uint32 UploadDiagnosticsMaxIncrementalResult = 999 {
            on action validate call check_minimum 0;
        }

        /*
        * An object that contains the UploadDiagnostics configuration.
        *
        * @version 1.0
        */
        %persistent object '${prefix_}UploadConfig' {
            /*
             * The value MUST be the Path Name of a table row. The IP-layer interface over which the test is to be performed.
             * Example: Device.IP.Interface.1
             * 
             * If an empty string is specified, the CPE MUST use the interface as directed by its routing policy
             * (Forwarding table entries) to determine the appropriate interface.
             *
             * @version 1.0
             */
            %persistent string Interface = "${ip_intf_lo}" {
                on action validate call check_maximum_length 256;
            }

            /*
             * The [URL] for the CPE to Upload to.
             * This parameter MUST be in the form of a valid HTTP [RFC2616] or FTP [RFC959] URL.
             * 
             * - When using FTP transport, FTP binary transfer MUST be used.
             * - When using HTTP transport, persistent connections MUST be used and pipelining MUST NOT be used.
             * - When using HTTP transport the HTTP Authentication MUST NOT be used.
             *
             * @version 1.0
             */
            %persistent string UploadURL {
                on action validate call check_maximum_length 2048;
            }

            /*
             * The DiffServ code point for marking packets transmitted in the test.
             *
             * @version 1.0
             */
            %persistent uint8 DSCP = 0 {
                on action validate call check_maximum 63;
            }

            /*
             * Ethernet priority code for marking packets transmitted in the test (if applicable).
             *
             * @version 1.0
             */
            %persistent uint8 EthernetPriority = 0 {
                on action validate call check_maximum 7;
            }

            /*
             * The size of the file (in bytes) to be uploaded to the server.
             * The CPE MUST insure the appropriate number of bytes are sent.
             *
             * @version 1.0
             */
            %persistent uint64 TestFileLength = 0;

            /*
             * Controls time based testing [Section 4.3/TR-143]. When TimeBasedTestDuration > 0, TimeBasedTestDuration is the
             * duration in seconds of a time based test. If TimeBasedTestDuration is 0, the test is not based on time, but on
             * the size of the file to be uploaded.
             *
             * @version 1.0
             */
            %persistent uint16 TimeBasedTestDuration = 0 {
                on action validate call check_maximum 999;
            }

            /*
             * The measurement interval duration in seconds for objects in IncrementalResult for a time based FTP/HTTP upload test
             * (when TimeBasedTestDuration > 0). 
             *
             * For example if TimeBasedTestDuration is 90 seconds and TimeBasedTestMeasurementInterval is 10 seconds,
             * there will be 9 results in IncrementalResult, each with a 10 seconds duration.
             *
             * @version 1.0
             */
            %persistent uint16 TimeBasedTestMeasurementInterval = 0 {
                on action validate call check_maximum 999;
            }

            /*
             * This TimeBasedTestMeasurementOffset works in conjunction with TimeBasedTestMeasurementInterval to allow the interval
             * measurement to start a number of seconds after BOMTime. The test measurement interval in IncrementalResult starts at
             * time BOMTime + TimeBasedTestMeasurementOffset to allow for slow start window removal of file transfers. 
             *
             * This TimeBasedTestMeasurementOffset is in seconds.
             *
             * @version 1.0
             */
            %persistent uint8 TimeBasedTestMeasurementOffset = 0 {
                on action validate call check_maximum 255;
            }

            /*
             * Indicates the IP protocol version to be used. Enumeration of:
             * - Any (Use either IPv4 or IPv6 depending on the system preference)
             * - IPv4 (Use IPv4 for the requests)
             * - IPv6 (Use IPv6 for the requests)
             *
             * @version 1.0
             */
            %persistent string ProtocolVersion = "Any" {
                on action validate call check_is_in "IPDiagnostics.ProtocolVersionsAllowed";
            }

            /*
             * The number of connections to be used in the test. The default value SHOULD be 1.
             * NumberOfConnections MUST NOT be set to a value greater than UploadDiagnosticsMaxConnections.
             *
             * @version 1.0
             */
            %persistent uint32 NumberOfConnections = 1 {
                on action validate call check_minimum 1;
                on action validate call check_maximum 'IPDiagnostics.UploadDiagnosticsMaxConnections';
            }

            /*
             * The results must be returned in the PerConnectionResult table for every connection when set to true.
             *
             * @version 1.0
             */
            %persistent bool EnablePerConnectionResults = false;

        }

        /*
        * An object that contains the UploadDiagnostics result.
        *
        * @version 1.0
        */
        object '${prefix_}UploadResult'[] {
            counted with '${prefix_}NumberOfUploadResult';

            /*
             * Indicates the availability of diagnostics data. Enumeration of:
             * 
             * - Complete
             * - Error_CannotResolveHostName
             * - Error_NoRouteToHost
             * - Error_InitConnectionFailed
             * - Error_NoResponse
             * - Error_TransferFailed
             * - Error_PasswordRequestFailed
             * - Error_LoginFailed
             * - Error_NoTransferMode
             * - Error_NoPASV
             * - Error_IncorrectSize
             * - Error_Timeout
             * - Error_Internal
             * - Error_Other
             *
             * If the value of this parameter is anything other than Complete, the values of the other results parameters
             * for this test are indeterminate.
             *
             * @version 1.0
             */
            %read-only string Status = "Not_Executed" {
                on action validate call check_enum ["Complete",
                                                    "Error_CannotResolveHostName",
                                                    "Error_NoRouteToHost",
                                                    "Error_InitConnectionFailed",
                                                    "Error_NoResponse",
                                                    "Error_PasswordRequestFailed",
                                                    "Error_LoginFailed",
                                                    "Error_NoTransferMode",
                                                    "Error_NoPASV",
                                                    "Error_NoCWD",
                                                    "Error_NoSTOR",
                                                    "Error_NoTransferComplete",
                                                    "Error_Timeout",
                                                    "Error_Internal",
                                                    "Error_Other",
                                                    "Not_Complete",
                                                    "Not_Executed"];
            }

            /*
             * [IPAddress] Indicates which IP address was used to send the request.
             *
             * @version 1.0
             */
            %read-only string IPAddressUsed {
                on action validate call check_maximum_length 45;
            }

            /*
             * Request time in UTC, which MUST be specified to microsecond precision.
             *
             * For example: 2008-04-09T15:01:05.123456Z
             * - For HTTP this is the time at which the client sends the GET command.
             * - For FTP this is the time at which the client sends the RTRV command.
             *
             * If multiple connections are used, then ROMTime is set to the earliest ROMTime across all connections.
             *
             * @version 1.0
             */
            %read-only datetime ROMTime;

            /*
             * Begin of transmission time in UTC, which MUST be specified to microsecond precision
             *
             * For example: 2008-04-09T15:01:05.123456Z
             * - For HTTP this is the time at which the first data packet is received.
             * - For FTP this is the time at which the client receives the first data packet on the data connection.
             *
             * If multiple connections are used, then BOMTime is set to the earliest BOMTime across all connections.
             *
             * @version 1.0
             */
            %read-only datetime BOMTime;

            /*
             * End of transmission in UTC, which MUST be specified to microsecond precision.
             *
             * For example: 2008-04-09T15:01:05.123456Z
             * - For HTTP this is the time at which the last data packet is received.
             * - For FTP this is the time at which the client receives the last packet on the data connection.
             *
             * If multiple connections are used, then EOMTime is set to the latest EOMTime across all connections.
             *
             * @version 1.0
             */
            %read-only datetime EOMTime;

            /*
             * The number of bytes of the test file sent during the FTP/HTTP transaction including FTP/HTTP headers,
             * between BOMTime and EOMTime acrosss all connections.
             *
             * @version 1.0
             */
            %read-only uint64 TestBytesSent;

            /*
             * The total number of bytes (at the IP layer) received on the Interface between BOMTime and EOMTime.
             * This MAY be calculated by sampling Stats.BytesReceived on the Interface object at BOMTime and at EOMTime and subtracting. 
             * If Interface is an empty string, this parameter cannot be determined and SHOULD be 0.
             *
             * @version 1.0
             */
            %read-only uint64 TotalBytesReceived;

            /*
             * The total number of bytes (at the IP layer) sent on the Interface between BOMTime and EOMTime.
             * This MAY be calculated by sampling Stats.BytesSent on the Interface object at BOMTime and at EOMTime and subtracting.
             * If Interface is an empty string, this parameter cannot be determined and SHOULD be 0.
             *
             * @version 1.0
             */
            %read-only uint64 TotalBytesSent;

            /*
             * The number of bytes of the test file sent between the latest PerConnectionResult.{i}.BOMTime
             * and the earliest PerConnectionResult.{i}.EOMTime across all connections.
             *
             * @version 1.0
             */
            %read-only uint64 TestBytesSentUnderFullLoading;

            /*
             * The total number of bytes (at the IP layer) received in between the latest PerConnectionResult.{i}.BOMTime
             * and the earliest PerConnectionResult.{i}.EOMTime. This MAY be calculated by sampling Stats.BytesReceived on
             * the Interface object at the latest PerConnectionResult.{i}.BOMTime and at the earliest PerConnectionResult.{i}.EOMTime and subtracting.
             *
             * @version 1.0
             */
            %read-only uint64 TotalBytesReceivedUnderFullLoading;

            /*
             * The total number of bytes (at the IP layer) sent between the latest PerConnectionResult.{i}.BOMTime and the
             * earliest PerConnectionResult.{i}.EOMTime. This MAY be calculated by sampling Stats.BytesSent on the Interface
             * object at the latest PerConnectionResult.{i}.BOMTime and at the earliest PerConnectionResult.{i}.EOMTime and subtracting.
             *
             * @version 1.0
             */
            %read-only uint64 TotalBytesSentUnderFullLoading;

            /*
             * The period of time in microseconds between the latest PerConnectionResult.{i}.BOMTime and the earliest PerConnectionResult.{i}.EOMTime of the test.
             *
             * @version 1.0
             */
            %read-only uint64 PeriodOfFullLoading;

            /*
             * Request time in UTC, which MUST be specified to microsecond precision.
             *
             * For example: 2008-04-09T15:01:05.123456Z
             * - For HTTP this is the time at which the TCP socket open (SYN) was sent for the HTTP connection.
             * - For FTP this is the time at which the TCP socket open (SYN) was sent for the data connection.
             *
             * Note: Interval of 1 microsecond SHOULD be supported.
             *
             * If multiple connections are used, then TCPOpenRequestTime is set to the latest TCPOpenRequestTime across all connections.
             *
             * @version 1.0
             */
            %read-only datetime TCPOpenRequestTime;

            /*
             * Response time in UTC, which MUST be specified to microsecond precision.
             *
             * For example: 2008-04-09T15:01:05.123456Z
             * - For HTTP this is the time at which the TCP ACK to the socket opening the HTTP connection was received.
             * - For FTP this is the time at which the TCP ACK to the socket opening the data connection was received.
             *
             * Note: Interval of 1 microsecond SHOULD be supported.
             *
             * If multiple connections are used, then TCPOpenResponseTime is set to the latest TCPOpenResponseTime across all connections.
             *
             * @version 1.0
             */
            %read-only datetime TCPOpenResponseTime;

            /*
            * Results for individual connections. This table is only populated when EnablePerConnectionResults is true.
            * A new object is created for each connection specified in NumberOfConnections. Instance numbers MUST start
            * at 1 and sequentially increment as new instances are created.
            *
            * This table's Instance Numbers MUST be 1, 2, 3... (assigned sequentially without gaps).
            *
            * @version 1.0
            */
            object PerConnectionResult[] {
                /*
                * Request time in UTC, which MUST be specified to microsecond precision.
                *
                * For example: 2008-04-09T15:01:05.123456Z
                * - For HTTP this is the time at which the client sends the GET command.
                * - For FTP this is the time at which the client sends the RTRV command.
                *
                * @version 1.0
                */
                %read-only datetime ROMTime;

                /*
                * Begin of transmission time in UTC, which MUST be specified to microsecond precision.
                *
                * For example: 2008-04-09T15:01:05.123456Z
                * - For HTTP this is the time at which the first data packet is received.
                * - For FTP this is the time at which the client receives the first data packet on the data connection.
                *
                * @version 1.0
                */
                %read-only datetime BOMTime;

                /*
                * End of transmission in UTC, which MUST be specified to microsecond precision.
                *
                * For example: 2008-04-09T15:01:05.123456Z
                * - For HTTP this is the time at which the last data packet is received.
                * - For FTP this is the time at which the client receives the last packet on the data connection.
                *
                * @version 1.0
                */
                %read-only datetime EOMTime;

                /*
                * The number of bytes of the test file sent during the FTP/HTTP transaction including FTP/HTTP headers, between BOMTime and EOMTime.
                *
                * @version 1.0
                */
                %read-only uint64 TestBytesSent;

                /*
                * The total number of bytes (at the IP layer) received on the Interface between BOMTime and EOMTime.
                * This MAY be calculated by sampling Stats.BytesReceived on the Interface object at BOMTime and at EOMTime and subtracting.
                *
                * @version 1.0
                */
                %read-only uint64 TotalBytesReceived;

                /*
                * The total number of bytes (at the IP layer) sent on the Interface between BOMTime and EOMTime.
                * This MAY be calculated by sampling Stats.BytesSent on the Interface object at BOMTime and at EOMTime and subtracting.
                *
                * @version 1.0
                */
                %read-only uint64 TotalBytesSent;

                /*
                * Request time in UTC, which MUST be specified to microsecond precision.
                *
                * For example: 2008-04-09T15:01:05.123456Z
                * - For HTTP this is the time at which the TCP socket open (SYN) was sent for the HTTP connection.
                * - For FTP this is the time at which the TCP socket open (SYN) was sent for the data connection.
                *
                * @version 1.0
                */
                %read-only datetime TCPOpenRequestTime;

                /*
                * Response time in UTC, which MUST be specified to microsecond precision.
                *
                * For example: 2008-04-09T15:01:05.123456Z
                * - For HTTP this is the time at which the TCP ACK to the socket opening the HTTP connection was received.
                * - For FTP this is the time at which the TCP ACK to the socket opening the data connection was received.
                *
                * @version 1.0
                */
                %read-only datetime TCPOpenResponseTime;
            }

            /*
            * Results for time segmented tests (tests where TimeBasedTestDuration > 0 and TimeBasedTestMeasurementInterval > 0).
            * This data is totaled across all connections in the test. A new object is created every TimeBasedTestMeasurementInterval
            * after that interval has completed. Instance numbers MUST start at 1 and sequentially increment as new instances are created.
            *
            * This table's Instance Numbers MUST be 1, 2, 3... (assigned sequentially without gaps).
            *
            * @version 1.0
            */
            object IncrementalResult[] {
                /*
                * Change in the value of TestBytesSent between StartTime and EndTime.
                *
                * @version 1.0
                */
                %read-only uint64 TestBytesSent;

                /*
                * The total number of bytes (at the IP layer) received on the Interface between StartTime and EndTime.
                * This MAY be calculated by sampling Stats.BytesReceived on the Interface object at StartTime and at EndTime and subtracting.
                * If Interface is an empty string, this parameter cannot be determined and SHOULD be 0.
                *
                * @version 1.0
                */
                %read-only uint64 TotalBytesReceived;

                /*
                * The total number of bytes (at the IP layer) sent on the Interface between StartTime and EndTime.
                * This MAY be calculated by sampling Stats.BytesSent on the Interface object at StartTime and at EndTime and subtracting.
                * If Interface is an empty string, this parameter cannot be determined and SHOULD be 0.
                *
                * @version 1.0
                */
                %read-only uint64 TotalBytesSent;

                /*
                * The start time of this interval which MUST be specified to microsecond precision.
                *
                * For example: 2008-04-09T15:01:05.123456Z
                *
                * @version 1.0
                */
                %read-only datetime StartTime;

                /*
                * The end time of this interval which MUST be specified to microsecond precision.
                *
                * For example: 2008-04-09T15:01:05.123456Z
                *
                * @version 1.0
                */
                %read-only datetime EndTime;
            }

            /*
            * Process information.
            *
            * @version 1.0
            */
            %protected object Process {
                /*
                * Process ID of the process.
                *
                * @version 1.0
                */
                %read-only int32 ID = 0;
                
                /*
                * File descriptor of the standard output.
                *
                * @version 1.0
                */
                %read-only int32 FDStdOut = 0;

                /*
                * File descriptor of the standard error.
                *
                * @version 1.0
                */
                %read-only int32 FDStdErr = 0;
            }

            /*
            * An object that contains the UploadDiagnostics configuration for the specific test.
            *
            * @version 1.0
            */
            %protected object 'Config' {
                /*
                * The IP-layer interface over which the test was performed.
                *
                * @version 1.0
                */
                %read-only string Interface;

                /*
                * The [URL] for the CPE to perform the Upload to.
                *
                * @version 1.0
                */
                %read-only string UploadURL;

                /*
                * The DiffServ code point for marking packets transmitted in the test.
                *
                * @version 1.0
                */
                %read-only uint8 DSCP;

                /*
                * Ethernet priority code for marking packets transmitted in the test.
                *
                * @version 1.0
                */
                %read-only uint8 EthernetPriority;

                /*
                * The size of the file (in bytes) to be uploaded to the server.
                * The CPE MUST insure the appropriate number of bytes are sent.
                *
                * @version 1.0
                */
                %read-only uint64 TestFileLength;

                /*
                * Controls time based testing.
                *
                * @version 1.0
                */
                %read-only uint16 TimeBasedTestDuration;

                /*
                * The measurement interval duration in seconds for objects in IncrementalResult for a time based FTP/HTTP upload test
                * (when TimeBasedTestDuration > 0).
                *
                * @version 1.0
                */
                %read-only uint16 TimeBasedTestMeasurementInterval;

                /*
                * This TimeBasedTestMeasurementOffset works in conjunction with TimeBasedTestMeasurementInterval to allow the interval
                * measurement to start a number of seconds after BOMTime.
                *
                * @version 1.0
                */
                %read-only uint8 TimeBasedTestMeasurementOffset;

                /*
                * Indicates the IP protocol version used.
                *
                * @version 1.0
                */
                %read-only string ProtocolVersion;

                /*
                * The number of connections used in the test.
                *
                * @version 1.0
                */
                %read-only uint32 NumberOfConnections;

                /*
                * The results must be returned in the PerConnectionResult table for every connection when set to true.
                *
                * @version 1.0
                */
                %read-only bool EnablePerConnectionResults;
            }
        }

        /**
        * [ASYNC] This command defines the diagnostics configuration for a HTTP or FTP
        * UploadDiagnostics Test.
        *
        * @param Interface The value MUST be the Path Name of a table row. The IP-layer
        *        interface over which the test is to be performed.
        *        Example: Device.IP.Interface.1
        * @param UploadURL [MANDATORY] The [URL] for the CPE to perform the Upload to.
        *        This parameter MUST be in the form of a valid HTTP [RFC2616] or FTP
        *        [RFC959] URL.
        * @param DSCP DiffServ code point for marking packets transmitted in the test.
        * @param EthernetPriority Ethernet priority code for marking packets transmitted
        *        in the test (if applicable).
        * @param TestFileLength The size of the file (in bytes) to be uploaded to the server.
        * @param TimeBasedTestDuration Controls time based testing [Section 4.3/TR-143].
        *        When TimeBasedTestDuration > 0, TimeBasedTestDuration is the duration in
        *        seconds of a time based test. If TimeBasedTestDuration is 0, the test is
        *        not based on time, but on the size of the file to be uploaded.
        * @param TimeBasedTestMeasurementInterval The measurement interval duration in seconds
        *        for objects in IncrementalResult for a time based FTP/HTTP upload test
        *        (when TimeBasedTestDuration > 0).
        * @param TimeBasedTestMeasurementOffset This TimeBasedTestMeasurementOffset works in
        *        conjunction with TimeBasedTestMeasurementInterval to allow the interval
        *        measurement to start a number of seconds after BOMTime. The test measurement
        *        interval in IncrementalResult starts at time BOMTime + TimeBasedTestMeasurementOffset
        *        to allow for slow start window removal of file transfers.
        * @param ProtocolVersion Indicates the IP protocol version to be used. Enumeration of:
        *        - Any (Use either IPv4 or IPv6 depending on the system preference)
        *        - IPv4 (Use IPv4 for the requests)
        *        - IPv6 (Use IPv6 for the requests)
        * @param NumberOfConnections The number of connections to be used in the test.
        *        NumberOfConnections MUST NOT be set to a value greater than UploadDiagnosticsMaxConnections.
        * @param EnablePerConnectionResults The results must be returned in the PerConnectionResult
        *        table for every connection when set to true.
        * @version V1.0
        */
        %async htable UploadDiagnostics(%in string Interface,
                                   %in string UploadURL,
                                   %in uint8 DSCP,
                                   %in uint8 EthernetPriority,
                                   %in uint64 TestFileLength,
                                   %in uint16 TimeBasedTestDuration,
                                   %in uint16 TimeBasedTestMeasurementInterval,
                                   %in uint8 TimeBasedTestMeasurementOffset,
                                   %in string ProtocolVersion,
                                   %in uint32 NumberOfConnections,
                                   %in bool EnablePerConnectionResults);
    }
}
