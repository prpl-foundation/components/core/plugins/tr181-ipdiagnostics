#!/bin/ash

export PROC_NAME="$@"

# PIDS_PROC=$(pgrep $PROC_NAME 2> /dev/null)
if [ "$PROC_NAME" == "" -o "$PROC_NAME" == " " ]; then
    exit 1
fi
PIDS_PROC=$(ps ax | grep "$PROC_NAME" | grep -v grep | awk '{print $1}' | tr "\n" " ")
kill -9 $PIDS_PROC
