/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <amxc/amxc.h>
#include <amxm/amxm.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_object.h>
#include <netmodel/client.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_diagnostics.h"
#include "ip_diagnostics_priv.h"
#include "config.h"
#include "ipping.h"
#include "priv_ipping.h"
#include "traceroute.h"
#include "priv_traceroute.h"

#define ME "ipd-c"

#define ODL_DEFAULTS "?include '${odl.directory}/${name}.odl':'${odl.dm-defaults}';"

static const non_mand_param_t str_ipping_non_man_param[] = {
    {IPPING_RET_KEY_INTF, 0},
    {IPPING_RET_KEY_DSCP, 0},
    {IPPING_RET_KEY_MIN, 0},
    {IPPING_RET_KEY_MIND, 0},
    {IPPING_RET_KEY_AVG, 0},
    {IPPING_RET_KEY_AVGD, 0},
    {IPPING_RET_KEY_MAX, 0},
    {IPPING_RET_KEY_MAXD, 0},
    {IPPING_RET_KEY_SUCC, 0},
    {IPPING_RET_KEY_FC, 0},
    {NULL, 0}
};

static const non_mand_param_t str_trcr_non_man_param[] = {
    {TRCR_RET_KEY_INTF, 0},
    {TRCR_RET_KEY_NMBOTR, 3},
    {TRCR_RET_KEY_TMT, 1000},
    {TRCR_RET_KEY_DTBS, 64},
    {TRCR_RET_KEY_DSCP, 0},
    {TRCR_RET_KEY_MHC, 30},
    {TRCR_RET_KEY_RSPTM, 0},
    {NULL, 0}
};

static const cstring_t str_ipping_filtered_ret_key[] = {
    IPPING_RET_KEY_CMD,
    IPPING_RET_KEY_CTRL,
    IPPING_RET_KEY_INTF,
    IPPING_RET_KEY_NAME,
    IPPING_RET_KEY_NMBRP,
    IPPING_RET_KEY_PROC,
    IPPING_RET_KEY_PRTCLV,
    IPPING_RET_KEY_TMT,
    IPPING_RET_KEY_PST,
    NULL
};

static const cstring_t str_ipping_filtered_args_key[] = {
    IPPING_RET_KEY_AVG,
    IPPING_RET_KEY_AVGD,
    IPPING_RET_KEY_CMD,
    IPPING_RET_KEY_CTRL,
    IPPING_RET_KEY_DST,
    IPPING_RET_KEY_FC,
    IPPING_RET_KEY_IPUSD,
    IPPING_RET_KEY_MAX,
    IPPING_RET_KEY_MAXD,
    IPPING_RET_KEY_MIN,
    IPPING_RET_KEY_MIND,
    IPPING_RET_KEY_NAME,
    IPPING_RET_KEY_PROC,
    IPPING_RET_KEY_SUCC,
    NULL
};

static const cstring_t str_trcr_filtered_ret_key[] = {
    TRCR_RET_KEY_CMD,
    TRCR_RET_KEY_CTRL,
    TRCR_RET_KEY_INTF,
    TRCR_RET_KEY_NAME,
    TRCR_RET_KEY_PROC,
    TRCR_RET_KEY_PRTCLV,
    TRCR_RET_KEY_DSCP,
    TRCR_RET_KEY_DTBS,
    TRCR_RET_KEY_HST,
    TRCR_RET_KEY_MHC,
    TRCR_RET_KEY_RHNOE,
    TRCR_RET_KEY_TMT,
    TRCR_RET_KEY_NMBOTR,
    NULL
};

static const cstring_t str_trcr_filtered_args_key[] = {
    TRCR_RET_KEY_CMD,
    TRCR_RET_KEY_CTRL,
    TRCR_RET_KEY_DST,
    TRCR_RET_KEY_IPUSD,
    TRCR_RET_KEY_NAME,
    TRCR_RET_KEY_RSPTM,
    TRCR_RET_KEY_RHNOE,
    TRCR_RET_KEY_PROC,
    TRCR_RET_KEY_HOPS,
    NULL
};

static const swap_key_array_t str_ipping_swap_ret_key[] = {
    {IPPING_RET_KEY_DST, "Status"},
    {NULL, NULL}
};

static const swap_key_array_t str_trtcr_swap_ret_key[] = {
    {NULL, NULL}
};

static ip_diagnostics_t ip_diagnostics;

amxd_dm_t* ipd_get_dm(void) {
    return ip_diagnostics.dm;
}

amxo_parser_t* ipd_get_parser(void) {
    return ip_diagnostics.parser;
}

amxb_bus_ctx_t* ipd_get_ctx(void) {
    return ip_diagnostics.ctx;
}

amxb_bus_ctx_t* ip_manager_get_ctx(void) {
    return amxb_be_who_has("Device.IP.");
}

amxd_object_t* ipd_get_root_object(void) {
    return amxd_dm_findf(ipd_get_dm(), IPD ".");
}

amxd_object_t* ipd_get_ipping_object(void) {
    return amxd_dm_findf(ipd_get_dm(), IPD "."IPPING);
}

amxd_object_t* ipd_get_trcr_object(void) {
    return amxd_dm_findf(ipd_get_dm(), IPD "."TRCR);
}

cstring_t ipd_get_prefix(void) {
    amxo_parser_t* parser = ipd_get_parser();
    return (cstring_t) GETP_CHAR(&parser->config, "prefix_");
}

static ip_diagnostics_status_t ip_diagnostics_init_bus_ctx(void) {
    ip_diagnostics_status_t status = ipdiag_error;
    ip_diagnostics.ctx = amxb_be_who_has(IPD);
    if(ip_diagnostics.ctx != NULL) {
        status = ipdiag_ok;
    }
    return status;
}

static bool ipd_ud_is_mandatory_param(UNUSED const cstring_t key) {
    // Placeholder function that returns fals in case of upload-download
    return false;
}

static bool ipd_ping_is_mandatory_param(const cstring_t key) {
    bool mandatory = false;

    for(int i = 0; str_ipping_non_man_param[i].key != NULL; i++) {
        if(strcmp(str_ipping_non_man_param[i].key, key) == 0) {
            goto exit;
        }
    }
    mandatory = true;
exit:
    return mandatory;
}

static bool ipd_trcr_is_mandatory_param(const cstring_t key) {
    bool mandatory = false;

    for(int i = 0; str_trcr_non_man_param[i].key != NULL; i++) {
        if(strcmp(str_trcr_non_man_param[i].key, key) == 0) {
            goto exit;
        }
    }
    mandatory = true;
exit:
    return mandatory;
}

static void ip_diagnostics_on_delete_fd(UNUSED const char* signame,
                                        const amxc_var_t* const data,
                                        UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    int fd = -1;
    int retval;

    when_true(amxc_var_type_of(data) != AMXC_VAR_ID_FD, leave)

    fd = amxc_var_dyncast(fd_t, data);
    when_true(fd != amxb_get_fd(ip_diagnostics.ctx), leave)

    SAH_TRACEZ_WARNING(ME, "Current active connection is closed");
    retval = ip_diagnostics_init_bus_ctx();
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Unable to get new bus context");
    }

leave:
    SAH_TRACEZ_OUT(ME);
    return;
}

int ipd_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;

    ip_diagnostics.dm = dm;
    ip_diagnostics.parser = parser;

    netmodel_initialize();

    rv = ip_diagnostics_init_bus_ctx(); // sets ip_diagnostics.ctx
    when_failed_trace(rv, leave, ERROR, "Unable to initialise bus context, retval = %d", rv);

    rv = amxp_slot_connect(NULL, "connection-deleted", NULL, ip_diagnostics_on_delete_fd, NULL);
    when_failed_trace(rv, leave, ERROR, "Unable to connect to signal 'connection-deleted', retval = %d", rv);

    // load the controllers
    rv = ip_diagnostics_open_cfgctrlrs();
    when_failed_trace(rv, leave, ERROR, "Failed to load all supported modules");

    // load the defaults after the configuration module is loaded so the
    // default/saved configuration can be applied at start-up.
    amxo_parser_parse_string(parser, ODL_DEFAULTS, amxd_dm_get_root(dm));

leave:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void ipd_shutdown(void) {
    amxm_close_all();
    netmodel_cleanup();
}

amxd_trans_t* ipd_transaction_create(const amxd_object_t* const object) {
    SAH_TRACEZ_IN(ME);
    amxd_trans_t* trans = NULL;
    bool clean_trans = true;

    when_null(object, exit);
    when_failed(amxd_trans_new(&trans), exit);
    when_failed(amxd_trans_set_attr(trans, amxd_tattr_change_ro, true), clean);
    when_failed(amxd_trans_select_object(trans, object), clean);

    clean_trans = false;
clean:
    if(clean_trans == true) {
        amxd_trans_delete(&trans);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return trans;
}

amxd_status_t ipd_transaction_apply(amxd_trans_t* trans) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    when_null(trans, exit);

    status = amxd_trans_apply(trans, ipd_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to apply transaction, return %d", status);
    }

    amxd_trans_delete(&trans);

exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

/* Debugging Event Logger */
void _syslog_event(UNUSED const char* const sig_name,
                   const amxc_var_t* const data,
                   UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "Signal received - %s", sig_name);
    SAH_TRACEZ_INFO(ME, "Signal data = ");
    if(!amxc_var_is_null(data)) {
        amxc_var_log(data);
    }
}

/**
   @brief
   Search for a string in a linked list.

   @param[in] string String to be searched.
   @param[in] llist Linked list.

   @return
   false if the list doesn't contain the string.
 */
bool ipd_search_llist(cstring_t string, amxc_llist_t* llist) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t* list_string = NULL;
    bool ret = false;

    when_null_trace(string, exit, ERROR, "String is NULL.");

    amxc_llist_for_each(it, llist) {
        list_string = amxc_container_of(it, amxc_string_t, it);
        if(strcmp(string, amxc_string_get(list_string, 0)) == 0) {
            ret = true;
            break;
        }
    }

    SAH_TRACEZ_OUT(ME);

exit:
    return ret;
}

/**
   @brief
   Copy the data from the object to the variant.

   @param[in] object Object.
   @param[in] filter_list Linked list os strings with keys to be filtered (not copied).
   @param[out] var Variant.
 */
void ipd_obj_data_to_var(amxd_object_t* object, amxc_llist_t* filter_list, amxc_var_t* var) {
    SAH_TRACEZ_IN(ME);
    amxd_param_t* param = NULL;
    amxd_object_t* tmp_obj = NULL;
    amxc_var_t* tmp_var = NULL;
    cstring_t obj_name = NULL;
    cstring_t param_name = NULL;
    uint32_t index = 1;
    uint32_t inst_count = 0;
    amxc_string_t* str_index;
    amxd_object_type_t obj_type;

    amxc_string_new(&str_index, 0);

    when_null_trace(object, exit, ERROR, "The object is NULL.");
    when_null_trace(var, exit, ERROR, "Variant is not initialised.");

    obj_type = amxd_object_get_type(object);
    if(obj_type == amxd_object_template) {
        inst_count = amxd_object_get_instance_count(object);
        if(inst_count > 0) {
            amxc_llist_for_each(it, (&object->instances)) {
                tmp_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
                index = amxd_object_get_index(tmp_obj);
                amxc_string_setf(str_index, "%u", index);
                tmp_var = amxc_var_add_key(amxc_htable_t, var, amxc_string_get(str_index, 0), NULL);
                ipd_obj_data_to_var(tmp_obj, filter_list, tmp_var);
                tmp_obj = NULL;
            }
        }
    } else {
        amxc_llist_for_each(it, (&object->parameters)) {
            param = amxc_llist_it_get_data(it, amxd_param_t, it);
            param_name = (cstring_t) amxd_param_get_name(param);
            if(ipd_search_llist(param_name, filter_list) == false) {
                tmp_var = (amxc_var_t*) amxd_object_get_param_value(object, param_name);
                if((amxc_var_type_of(tmp_var) != AMXC_VAR_ID_HTABLE) && \
                   (amxc_var_type_of(tmp_var) != AMXC_VAR_ID_LIST)) {
                    amxc_var_set_pathf(var, tmp_var, DEFAULT_SET_PATH_FLAGS, "%s", param_name);
                }
            }
            param = NULL;
        }
        amxc_llist_for_each(it, (&object->objects)) {
            tmp_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
            obj_name = (cstring_t) amxd_object_get_name(tmp_obj, AMXD_OBJECT_INDEXED);
            if(ipd_search_llist(obj_name, filter_list) == false) {
                tmp_var = amxc_var_add_key(amxc_htable_t, var, obj_name, NULL);
                ipd_obj_data_to_var(tmp_obj, filter_list, tmp_var);
            }
            tmp_obj = NULL;
        }
    }
exit:
    amxc_string_delete(&str_index);
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Copy the data from the variant to the data model.

   @param[in] transaction Transaction.
   @param[in] sub_object Object name one level above.
   @param[in] values Variant with the information to be saved to the datamodel.
   @param[in] filter_list Linked list os strings with keys to be filtered.
 */
void ipd_var_to_dm(amxd_trans_t* transaction, const char* sub_object,
                   amxc_var_t* values, amxc_llist_t* filter_list) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* value = NULL;
    cstring_t key = NULL;

    when_null_trace(transaction, exit, ERROR, "Transaction is NULL.");
    when_null_trace(values, exit, ERROR, "Values variant is NULL.");

    if(sub_object != NULL) {
        amxd_trans_select_pathf(transaction, ".%s.", sub_object);
    }
    const amxc_htable_t* ht_params = amxc_var_constcast(amxc_htable_t, values);
    amxc_htable_for_each(it, ht_params) {
        key = (cstring_t) amxc_htable_it_get_key(it);
        value = (amxc_var_t*) amxc_htable_it_get_data(it, amxc_var_t, hit);
        if(ipd_search_llist(key, filter_list) == false) {
            if((amxc_var_type_of(value) != AMXC_VAR_ID_HTABLE) && \
               (amxc_var_type_of(value) != AMXC_VAR_ID_LIST) &&
               (amxc_var_type_of(value) != AMXC_VAR_ID_NULL)) {
                amxd_trans_set_param(transaction, key, value);
            }
        }
    }
    if(sub_object != NULL) {
        amxd_trans_select_pathf(transaction, ".^");
    }
exit:
    SAH_TRACEZ_OUT(ME);
}

/**
   @brief
   Fetch the interface IP address.

   @param[in] interface Interface path in string format. i.e. Device.IP.Interface.1.
   @param[in] interface Interface in variant format.

   @return
   NULL if it's not a valid interface.
 */
amxc_var_t* ipd_fetch_interface_ip(amxc_var_t* interface, amxc_var_t* protocol_version) {
    const cstring_t interface_str = GET_CHAR(interface, NULL);
    const cstring_t protocol_version_str = GET_CHAR(protocol_version, NULL);
    amxc_var_t* intf_ip = NULL;
#ifndef IP_DIAGNOSTICS_TEST
    amxc_var_t* intf_var = NULL;
    amxc_var_t* intf_var_list = NULL;
    cstring_t ip_addr = NULL;

    when_str_empty_trace(interface_str, exit, ERROR, "Interface is empty.");
    when_str_empty_trace(protocol_version_str, exit, ERROR, "Protocol Version is empty.");

    if((strcmp(protocol_version_str, "Any") == 0) || \
       (strcmp(protocol_version_str, "IPv4") == 0)) {
        intf_var_list = netmodel_getAddrs(interface_str, "ipv4", netmodel_traverse_down);
    } else {
        intf_var_list = netmodel_getAddrs(interface_str, "ipv6", netmodel_traverse_down);
    }
    when_null_trace(intf_var_list, exit, ERROR, "Failed to query the address from interface %s from NetModel.", interface_str);
    intf_var = amxc_var_get_index(intf_var_list, 0, AMXC_VAR_FLAG_DEFAULT);
    ip_addr = (cstring_t) GET_CHAR(intf_var, "Address");
#else
    const cstring_t ip_addr = "127.0.0.1";
    when_str_empty(interface_str, exit);
    when_str_empty(protocol_version_str, exit);
#endif
exit:
    if(ip_addr != NULL) {
        amxc_var_new(&intf_ip);
        amxc_var_set_type(intf_ip, AMXC_VAR_ID_CSTRING);
        amxc_var_set(cstring_t, intf_ip, ip_addr);
    }
    return intf_ip;
}

/**
   @brief
   Validate argument.

   @param[in] input Input variant.
   @param[in] cfg_obj Configuration object.
   @param[in] key key.

   @return
   NULL if it's not a valid parameter.
 */
static amxc_var_t* ipd_validate_arg(amxc_var_t* input_args, amxd_object_t* cfg_obj, const cstring_t key) {
    amxc_var_t* input = GET_ARG(input_args, key);
    amxc_var_t* cfg_var = (amxc_var_t*) amxd_object_get_param_value(cfg_obj, key);
    amxc_var_t* return_var = cfg_var;
    amxd_param_t* cfg_param = amxd_object_get_param_def(cfg_obj, key);
    amxd_status_t status;
    amxc_var_t* retval = NULL;
    bool valid_argument = false;

    amxc_var_new(&retval);

    when_true_trace((!ipd_ping_is_mandatory_param(key) && input == NULL), exit, WARNING, "Non mendatory input argument empty");
    when_null_trace(input, validate, ERROR, "Input argument %s was not passed, the default config will be taken.", key);

    return_var = input;

validate:
    when_null(return_var, exit);
    amxc_llist_for_each(it, &(cfg_param->cb_fns)) {
        amxd_dm_cb_t* cb_fn = amxc_llist_it_get_data(it, amxd_dm_cb_t, it);
        if((cb_fn->reason != action_param_validate) ||
           ( cb_fn->fn == NULL) ||
           ( !cb_fn->enable)) {
            continue;
        }
        status = cb_fn->fn(cfg_obj, cfg_param, action_param_validate, return_var, retval, cb_fn->priv);
        when_false_trace(status == amxd_status_ok, exit, ERROR, "Invalid value for the input %s.", key);
    }

    valid_argument = true;

exit:
    amxc_var_delete(&retval);
    if((return_var == NULL) || (valid_argument == false)) {
        return NULL;
    } else {
        return return_var;
    }
}

/**
   @brief
   Get/validate configuration parameters.

   @param[in] input_args Input variant.
   @param[in] cfg_obj Configuration object.
   @param[out] params Variant with the configuration object filled in.

   @return
   0 if the validation passes.
 */
int ipd_validate_cfg_params(amxc_var_t* input_args, amxd_object_t* cfg_obj, amxc_var_t* params, const cstring_t tool) {
    amxd_param_t* param = NULL;
    amxc_var_t* tmp_var = NULL;
    amxc_var_t* value = NULL;
    cstring_t param_name = NULL;
    bool (* ipd_is_mandatory_param)(const cstring_t) = NULL;
    int rv = -1;

    when_null_trace(input_args, exit, ERROR, "Input arguments is NULL.");
    when_null_trace(cfg_obj, exit, ERROR, "Configuration object is NULL.");
    when_null_trace(params, exit, ERROR, "Output variant is not initialised.");
    when_str_empty_trace(tool, exit, ERROR, "No test tool specified");

    if(strcmp(tool, IPD) == 0) {
        ipd_is_mandatory_param = &ipd_ud_is_mandatory_param;
    } else if(strcmp(tool, IPPING) == 0) {
        ipd_is_mandatory_param = &ipd_ping_is_mandatory_param;
    } else if(strcmp(tool, TRCR) == 0) {
        ipd_is_mandatory_param = &ipd_trcr_is_mandatory_param;
    } else {
        goto exit;
    }

    amxc_llist_for_each(it, (&cfg_obj->parameters)) {
        param = amxc_llist_it_get_data(it, amxd_param_t, it);
        param_name = (cstring_t) amxd_param_get_name(param);
        tmp_var = ipd_validate_arg(input_args, cfg_obj, param_name);
        value = GET_ARG(input_args, param_name);
        when_true_trace(((tmp_var == NULL && value != NULL) ||
                         (ipd_is_mandatory_param(param_name) && tmp_var == NULL)), exit, ERROR, "Failed to read %s.", param_name);
        amxc_var_set_pathf(params, tmp_var, DEFAULT_SET_PATH_FLAGS, "%s", param_name);
    }

    rv = 0;

exit:
    return rv;
}

/**
   @brief
   Get the current time.

   @param[out] var Variant with the time.

   @return
   Not 0 if the function fails.
 */
int ipd_get_current_time(amxc_var_t* var) {
    amxc_string_t* str = NULL;
    struct tm* ptm = NULL;
    time_t rawtime;
    struct timespec ts;
    int microseconds = 0;
    int rv = -1;

    amxc_string_new(&str, 0);

    when_null_trace(var, exit, ERROR, "Output variant is not initialised.");

    rawtime = time(NULL);
    when_false_trace(rawtime != -1, exit, ERROR, "The time() function failed.");
    ptm = gmtime(&rawtime);
    when_null_trace(ptm, exit, ERROR, "The localtime() function failed.");
    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    microseconds = (int) (ts.tv_nsec / 1000);
    amxc_string_setf(str, "%04d-%02d-%02dT%02d:%02d:%02d.%06dZ", 1900 + ptm->tm_year,
                     1 + ptm->tm_mon,
                     ptm->tm_mday,
                     ptm->tm_hour,
                     ptm->tm_min,
                     ptm->tm_sec,
                     microseconds);
    amxc_var_set(cstring_t, var, amxc_string_get(str, 0));
    rv = 0;

exit:
    amxc_string_delete(&str);
    return rv;
}

/**
 * @brief
 * Function that takes a config variant as input and resolves the IP interface name
 * with the linux name
 *
 * @param conf_args Config htable variant with "Name" and "Interface" fields
 * @return int status
 */
amxd_status_t ipd_resolve_ip_interface(amxc_var_t* conf_args) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_unknown_error;
    amxb_bus_ctx_t* ctx = ip_manager_get_ctx();
    const char* intf = NULL;
    amxc_var_t ip_data;
    amxc_var_t* ip_intf = NULL;
    bool empty_intf = false;
    amxc_var_t* name = NULL;
    amxc_var_t* ip_address = NULL;

    amxc_var_init(&ip_data);
    when_null_trace(conf_args, exit, ERROR, "configuration args cannot be empty");

    rv = amxd_status_invalid_arg;

    if(str_empty(GET_CHAR(conf_args, "Interface"))) {
        empty_intf = true;
    } else {
        intf = GET_CHAR(conf_args, "Interface");
    }

    amxb_get(ctx, intf, 0, &ip_data, 10);
    ip_intf = amxc_var_get_first(&ip_data);
    when_true_trace((ip_intf == NULL) && (empty_intf == false), exit, ERROR, "Could not get the name from IP Interface %s", intf);

    name = amxc_var_get_key(conf_args, "Name", AMXC_VAR_FLAG_DEFAULT);
    empty_intf ? amxc_var_set(cstring_t, name, ""): amxc_var_set(cstring_t, name, GETP_CHAR(ip_intf, "0.Name"));

    rv = amxd_status_ok;
exit:
    amxc_var_delete(&ip_address);
    amxc_var_clean(&ip_data);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
   @brief
   Retrieves the datamodel path of an instance

   @param bus context
   @param query for example "DHCPv4.Client.[Interface=='Device.IP.Interface.2.']."
   @return string on success
           NULL pointer if failed
 */
char* component_get_path_instance(amxb_bus_ctx_t* bus,
                                  const char* query) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t ret;
    const char* result = NULL;
    char* ret_str = NULL;

    amxc_var_init(&ret);
    amxb_get(bus, query, 0, &ret, 5);
    result = amxc_var_key(GETP_ARG(&ret, "0.0"));
    when_str_empty_trace(result, exit, ERROR, "No results for '%s'", query);
    SAH_TRACEZ_INFO(ME, "%s returned %s", query, result);
    ret_str = strdup(result);

exit:
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
    return ret_str;
}

/**
 * @brief
 * Function that sets the input variant to the default value of the datamodel
 * before launching the test
 *
 * @param params Input variant for the test setup
 * @param key specify in what test the parameters must be filled
 * @return amxd_status_t
 */
amxd_status_t ipd_set_non_mandatory_def_value(amxc_var_t* params, const cstring_t key) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t stat = amxd_status_unknown_error;
    const non_mand_param_t* array = NULL;
    amxc_var_t* aux_val = NULL;

    when_str_empty_trace(key, exit, ERROR, "Input key is empty");

    if(strcmp(key, IPD_IPPING_CONFIG) == 0) {
        array = str_ipping_non_man_param;
    } else if(strcmp(key, IPD_TRCR_CONFIG) == 0) {
        array = str_trcr_non_man_param;
    } else {
        goto exit;
    }
    for(int i = 1; array[i].key != NULL; i++) {
        aux_val = GET_ARG(params, array[i].key);
        if((aux_val != NULL) && (amxc_var_type_of(aux_val) == AMXC_VAR_ID_NULL)) {
            stat = amxc_var_set(uint32_t, aux_val, array[i].def_val);
            when_failed_trace(stat, exit, ERROR, "Could not set non-mandatory parameters to default value");
        }
    }
    stat = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return stat;
}

/**
 * @brief
 * Function that makes sure that the ping tool is from the iputils library
 *
 * @return true if from iputils
 * @return false if not
 */
bool ipd_ipping_is_iputils(void) {
    SAH_TRACEZ_IN(ME);
    FILE* fp = NULL;
    char version[128] = {0};
    char* vers_num = NULL;
    bool is_iputils = false;

    fp = popen("ping -V", "r");
    when_null_trace(fp, exit, ERROR, "Failed to get iputils certification");
    if(fgets(version, sizeof(version), fp) != NULL) {
        vers_num = strstr(version, IPPING_IPUTILS);
        is_iputils = vers_num != NULL;
    }
    pclose(fp);
exit:
    SAH_TRACEZ_OUT(ME);
    return is_iputils;
}

/**
 * @brief
 * Fetch the file descriptor in the datamodel related to the test
 *
 * @return int
 */
int32_t ipd_tool_get_dm_fd(const cstring_t tool) {
    amxd_object_t* ipd_tool_object = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* params_proc = NULL;
    amxd_object_t* (* ipd_get_object)(void) = NULL;
    int32_t fd = -1;

    amxc_var_new(&params);
    amxc_var_set_type(params, AMXC_VAR_ID_HTABLE);

    when_str_empty_trace(tool, exit, ERROR, "No test tool specified");

    if(strcmp(tool, IPPING) == 0) {
        ipd_get_object = &ipd_get_ipping_object;
    } else if(strcmp(tool, TRCR) == 0) {
        ipd_get_object = &ipd_get_trcr_object;
    } else {
        goto exit;
    }

    ipd_tool_object = ipd_get_object();
    ipd_obj_data_to_var(ipd_tool_object, NULL, params);
    when_null_trace(params, exit, ERROR, "Failed to get the param hash table.");
    params_proc = amxc_var_get_key(params, IPPING_RET_KEY_PROC, AMXC_VAR_FLAG_DEFAULT);

    fd = GET_INT32(params_proc, IPPING_RET_KEY_PROC_FD_STDOUT);

exit:
    amxc_var_delete(&params);
    return fd;
}

static void ipd_swap_ret_key(amxc_var_t* mod_ret, const cstring_t target, const cstring_t to_swap) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* mod_ret_aux = NULL;
    when_str_empty(target, exit);
    when_str_empty(to_swap, exit);
    when_null(mod_ret, exit);

    mod_ret_aux = amxc_var_get_key(mod_ret, target, AMXC_VAR_FLAG_DEFAULT);
    when_null_trace(mod_ret_aux, exit, ERROR, "Could not find the return key to swap %s", target);
    amxc_var_set_key(mod_ret, to_swap, mod_ret_aux, AMXC_VAR_FLAG_COPY);
    amxc_var_delete(&mod_ret_aux);

exit:
    SAH_TRACEZ_OUT(ME);
}

/**
 * @brief
 * Function that filters the unnecessary keys of the return variant
 *
 * @param mod_ret The module's return variant
 */
void ipd_filter_ret(amxc_var_t* mod_ret, bool return_var, const cstring_t tool) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* mod_ret_aux = NULL;
    const cstring_t* array = NULL;
    const swap_key_array_t* swap_array = NULL;

    if(str_empty(tool)) {
        SAH_TRACEZ_WARNING(ME, "Tools parameter empty, no filtering done");
        goto exit;
    } else if(strcmp(tool, TRCR) == 0) {
        array = return_var ? str_trcr_filtered_ret_key:str_trcr_filtered_args_key;
        swap_array = str_trtcr_swap_ret_key;
    } else if(strcmp(tool, IPPING) == 0) {
        array = return_var ? str_ipping_filtered_ret_key:str_ipping_filtered_args_key;
        swap_array = str_ipping_swap_ret_key;
    } else {
        SAH_TRACEZ_WARNING(ME, "Wrong Tools parameter, no filtering done");
        goto exit;
    }
    if(array == NULL) {
        goto exit;
    }
    for(int i = 0; array[i] != NULL; i++) {
        mod_ret_aux = amxc_var_get_key(mod_ret, array[i], AMXC_VAR_FLAG_DEFAULT);
        if(mod_ret_aux != NULL) {
            amxc_var_delete(&mod_ret_aux);
        }
    }
    if(return_var) {
        for(int i = 0; swap_array[i].target_key != NULL; i++) {
            ipd_swap_ret_key(mod_ret, swap_array[i].target_key, swap_array[i].swap_key);
        }
    }
exit:
    SAH_TRACEZ_OUT(ME);
}
