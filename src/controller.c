/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>

#include <dlfcn.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_diagnostics.h"
#include "ip_diagnostics_priv.h"
#include "tx_diag.h"
#include "config.h"

#define ME "ipd-ctrlr"

#ifdef PRPL_IP_DIAGNOSTIC_UPLOAD_DOWNLOAD
static int init_ipd_signal(const char* name);
#endif

static struct {
    const char* name;
    const char* type;
} _module_infos[] = {
    {"mod-ipd-iperf", MOD_IPD_CTRL},
    {"mod-ipd-ftx", MOD_IPD_143_CTRL},
    {"mod-ipd-ping", MOD_IPD_PING_CTRL},
    {"mod-ipd-traceroute", MOD_IPD_TRCR_CTRL},
    {NULL, NULL}
};

typedef int (* init_ipd_fn)(const char*);
static struct {
    const char* type;
    init_ipd_fn init_signal;
} _diag_signals[] = {
#ifdef PRPL_IP_DIAGNOSTIC_UPLOAD_DOWNLOAD
    {MOD_IPD_143_CTRL, init_ipd_signal},
#endif
    {MOD_IPD_PING_CTRL, NULL},
    {MOD_IPD_TRCR_CTRL, NULL},
    {NULL, NULL}
};

const char* ipd_get_controller_type(const char* ctrlr_name) {
    const char* type = NULL;

    when_str_empty(ctrlr_name, exit);
    for(uint32_t i = 0; _module_infos[i].name != NULL; ++i) {
        if(strcmp(ctrlr_name, _module_infos[i].name) == 0) {
            type = _module_infos[i].type;
            break;
        }
    }
exit:
    return type;
}

static init_ipd_fn ipd_get_controller_init_signal(const char* ctrlr_type) {
    init_ipd_fn init_signal = NULL;

    when_str_empty(ctrlr_type, exit);
    for(uint32_t i = 0; _diag_signals[i].type != NULL; ++i) {
        if(strcmp(ctrlr_type, _diag_signals[i].type) == 0) {
            init_signal = _diag_signals[i].init_signal;
            break;
        }
    }
exit:
    return init_signal;
}

// this need to be removed once fd_monitored function will be call by other diagnostics
#ifdef PRPL_IP_DIAGNOSTIC_UPLOAD_DOWNLOAD
static void fd_monitored_event_cb(int fd, void* priv) {
    amxc_var_t data;
    amxc_string_t signal_name;

    amxc_var_init(&data);
    amxc_string_init(&signal_name, 0);
    amxc_string_setf(&signal_name, "%s:fd_event", (const char*) priv);
    amxc_var_set(int32_t, &data, fd);
    amxp_signal_trigger(amxp_sigmngr_find_signal(NULL, amxc_string_get(&signal_name, 0)), &data);
    amxc_string_clean(&signal_name);
    amxc_var_clean(&data);
}

static void fd_to_monitor(UNUSED const char* const sig_name,
                          const amxc_var_t* const data,
                          void* const priv) {
    int fd = -1;

    when_null(GET_ARG(data, "fd"), exit);
    when_null(GET_ARG(data, "add"), exit);
    fd = GET_INT32(data, "fd");
    if(GET_BOOL(data, "add")) {
        amxo_connection_add(ipd_get_parser(), fd, fd_monitored_event_cb, NULL, AMXO_LISTEN, priv);
    } else {
        amxo_connection_remove(ipd_get_parser(), fd);
    }
exit:
    return;
}


static int init_ipd_signal(const char* ctrlr_name) {
    int rv = -1;
    amxc_string_t signal_name;

    amxc_string_init(&signal_name, 0);
    when_str_empty(ctrlr_name, exit);
    rv = 0;
    amxc_string_setf(&signal_name, "%s:fd_to_monitor", ctrlr_name);
    when_failed_trace(amxp_slot_connect(NULL, amxc_string_get(&signal_name, 0), NULL, fd_to_monitor, (void*) ctrlr_name), exit, WARNING, "Couldn't find signal %s", amxc_string_get(&signal_name, 0));
    amxc_string_setf(&signal_name, "%s:download_ended", ctrlr_name);
    when_failed_trace(amxp_slot_connect(NULL, amxc_string_get(&signal_name, 0), NULL, downdiag_ended, (void*) ctrlr_name), exit, WARNING, "Couldn't find signal %s", amxc_string_get(&signal_name, 0));
    amxc_string_setf(&signal_name, "%s:all_download_ended", ctrlr_name);
    when_failed_trace(amxp_slot_connect(NULL, amxc_string_get(&signal_name, 0), NULL, downdiag_all_ended, (void*) ctrlr_name), exit, WARNING, "Couldn't find signal %s", amxc_string_get(&signal_name, 0));
    amxc_string_setf(&signal_name, "%s:upload_ended", ctrlr_name);
    when_failed_trace(amxp_slot_connect(NULL, amxc_string_get(&signal_name, 0), NULL, updiag_ended, (void*) ctrlr_name), exit, WARNING, "Couldn't find signal %s", amxc_string_get(&signal_name, 0));
    amxc_string_setf(&signal_name, "%s:get_interface_stats", ctrlr_name);
    when_failed_trace(amxp_slot_connect(NULL, amxc_string_get(&signal_name, 0), NULL, txdiag_interface_stats, (void*) ctrlr_name), exit, WARNING, "Couldn't find signal %s", amxc_string_get(&signal_name, 0));
    amxc_string_setf(&signal_name, "%s:all_upload_ended", ctrlr_name);
    when_failed_trace(amxp_slot_connect(NULL, amxc_string_get(&signal_name, 0), NULL, updiag_all_ended, (void*) ctrlr_name), exit, WARNING, "Couldn't find signal %s", amxc_string_get(&signal_name, 0));
    amxc_string_setf(&signal_name, "%s:signal_ready", ctrlr_name);
    amxp_sigmngr_emit_signal(NULL, amxc_string_get(&signal_name, 0), NULL);
exit:
    amxc_string_clean(&signal_name);
    return rv;
}
#endif

static int ip_diagnostics_cfgctrlrs_init_signals(const char* ctrlr_name) {
    int rv = -1;
    const char* type = ipd_get_controller_type(ctrlr_name);
    init_ipd_fn init_signal = ipd_get_controller_init_signal(type);
    const char* static_ctrlr_name = NULL;

    for(uint32_t i = 0; _module_infos[i].name != NULL; ++i) {
        if(strcmp(ctrlr_name, _module_infos[i].name) == 0) {
            static_ctrlr_name = _module_infos[i].name;
            break;
        }
    }
    rv = (init_signal != NULL) ? (*init_signal)(static_ctrlr_name) : 0;
    return rv;
}

int ip_diagnostics_open_cfgctrlrs(void) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxc_var_t lcontrollers;
    amxc_string_t mod_path;
    amxm_shared_object_t* so = NULL;
    amxd_object_t* ip_obj = amxd_dm_findf(ipd_get_dm(), "IPDiagnostics");
    amxo_parser_t* parser = ipd_get_parser();
    const amxc_var_t* controllers = amxd_object_get_param_value(ip_obj, "SupportedControllers");
    const char* const mod_dir = GETP_CHAR(&parser->config, "mod-dir");

    amxc_var_init(&lcontrollers);
    amxc_string_init(&mod_path, 0);

    amxc_var_convert(&lcontrollers, controllers, AMXC_VAR_ID_LIST);
    amxc_var_for_each(controller, &lcontrollers) {
        const char* name = GET_CHAR(controller, NULL);
        amxc_string_setf(&mod_path, "%s/%s.so", mod_dir, name);
        rv = amxm_so_open(&so, name, amxc_string_get(&mod_path, 0));
        if(rv != 0) {
            SAH_TRACEZ_ERROR(ME, "Error while amxm_so_open %s", dlerror());
        }
        SAH_TRACEZ_INFO(ME, "Loading controller '%s' %s at %s with %d", name, rv ? "failed" : "successful", amxc_string_get(&mod_path, 0), rv);
        when_failed(rv, exit);
        when_failed(ip_diagnostics_cfgctrlrs_init_signals(name), exit);
    }

    rv = 0;

exit:
    amxc_string_clean(&mod_path);
    amxc_var_clean(&lcontrollers);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief Executes a function on the ip-diagnostics's configuration controller
 * @param obj The object from which to get the module name to use, if NULL mod-ipd-iperf is used
 * @param func_name The name of the function to execute
 * @param args variant structure of the arguments to the function. All functions expect the same fields in the variant!
 * @param ret variant structure of the return values from the function
 * @return 0 if the function executed without errors, otherwise it return -1
 */
int cfgctrlr_execute_function(amxd_object_t* obj,
                              const char* const func_name,
                              amxc_var_t* args,
                              amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    int status = -1;
    const amxc_var_t* ipd_ctrl = amxd_object_get_param_value(obj, "Controller");
    const char* ipd_ctrlr_name = NULL;
    const char* ipd_module = NULL;

    // find controller for the configuration
    if(obj == NULL) {
        // If no object was given we want to use the iperf module
        ipd_ctrlr_name = "mod-ipd-iperf";
    } else {
        ipd_ctrlr_name = GET_CHAR(ipd_ctrl, NULL);
        when_str_empty_trace(ipd_ctrlr_name, exit, ERROR, "Could not find the controller for this configuration");
    }

    ipd_module = ipd_get_controller_type(ipd_ctrlr_name);
    when_null(ipd_module, exit);

    // Call controller-specific implementation
    status = amxm_execute_function(ipd_ctrlr_name, ipd_module, func_name, args, ret);
    if(status < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not execute '%s' on the %s controller", func_name, ipd_ctrlr_name);
        goto exit;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

int cfgctrlr_ipdiagnostics_execute_function(amxd_object_t* ip_diagnostics_obj,
                                            const char* function,
                                            amxc_var_t* data,
                                            amxc_var_t* ret) {
    int rv = -1;

    when_str_empty_trace(function, exit, ERROR, "Can not execute function, empty string");
    when_null_trace(ip_diagnostics_obj, exit, ERROR, "can not execute %s, no object given", function);

    rv = cfgctrlr_execute_function(ip_diagnostics_obj, function, data, ret);
    if(rv < 0) {
        SAH_TRACEZ_ERROR(ME, "function %s failed", function);
    }

exit:
    return rv;
}
