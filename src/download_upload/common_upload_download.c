/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_object.h>
#include <netmodel/client.h>

#include "ip_diagnostics.h"
#include "ip_diagnostics_priv.h"
#include "upload_download.h"
#include "priv_upload_download.h"
#include "config.h"
#include "download.h"
#include "upload.h"

#define ME "ipd-cup"

/**
   @brief
   Get the key that indicate the maximum number of connections according
   to Upload or Download.

   @param[in] test download or upload.

   @return
   NULL if the test is not valid.
 */
const cstring_t ipd_up_down_max_conn(ipd_up_down_test_t test) {
    if(test == ipd_up_down_download) {
        return IPD_UP_DOWN_DOWNLOAD_PREFIX ""IPD_UP_DOWN_MAX_CONNECTIONS;
    } else if(test == ipd_up_down_upload) {
        return IPD_UP_DOWN_UPLOAD_PREFIX ""IPD_UP_DOWN_MAX_CONNECTIONS;
    } else {
        return NULL;
    }
}

/**
   @brief
   Get the key that indicate the maximum number of incremental results according
   to Upload or Download.

   @param[in] test download or upload.

   @return
   NULL if the test is not valid.
 */
const cstring_t ipd_up_down_max_incr_res(ipd_up_down_test_t test) {
    if(test == ipd_up_down_download) {
        return IPD_UP_DOWN_DOWNLOAD_PREFIX ""IPD_UP_DOWN_MAX_INCR_RESULT;
    } else if(test == ipd_up_down_upload) {
        return IPD_UP_DOWN_UPLOAD_PREFIX ""IPD_UP_DOWN_MAX_INCR_RESULT;
    } else {
        return NULL;
    }
}

/**
   @brief
   Get the key that indicate the number of results of according
   to Upload or Download.

   @param[in] test download or upload.

   @return
   NULL if the test is not valid.
 */
const cstring_t ipd_up_down_num_result(ipd_up_down_test_t test) {
    if(test == ipd_up_down_download) {
        return IPD_NUM_UP_DOWN_RESULT_1 ""IPD_UP_DOWN_DOWNLOAD_PREFIX ""IPD_NUM_UP_DOWN_RESULT_2;
    } else if(test == ipd_up_down_upload) {
        return IPD_NUM_UP_DOWN_RESULT_1 ""IPD_UP_DOWN_UPLOAD_PREFIX ""IPD_NUM_UP_DOWN_RESULT_2;
    } else {
        return NULL;
    }
}

/**
   @brief
   Get the key that indicates the Config according
   to Upload or Download.

   @param[in] test download or upload.

   @return
   NULL if the test is not valid.
 */
const cstring_t ipd_up_down_config(ipd_up_down_test_t test) {
    if(test == ipd_up_down_download) {
        return IPD_UP_DOWN_DOWNLOAD_PREFIX ""IPD_UP_DOWN_CONFIG;
    } else if(test == ipd_up_down_upload) {
        return IPD_UP_DOWN_UPLOAD_PREFIX ""IPD_UP_DOWN_CONFIG;
    } else {
        return NULL;
    }
}

/**
   @brief
   Get the key that indicates the Result according
   to Upload or Download.

   @param[in] test download or upload.

   @return
   NULL if the test is not valid.
 */
const cstring_t ipd_up_down_result(ipd_up_down_test_t test) {
    if(test == ipd_up_down_download) {
        return IPD_UP_DOWN_DOWNLOAD_PREFIX ""IPD_UP_DOWN_RESULT;
    } else if(test == ipd_up_down_upload) {
        return IPD_UP_DOWN_UPLOAD_PREFIX ""IPD_UP_DOWN_RESULT;
    } else {
        return NULL;
    }
}

/**
   @brief
   Get the key that indicates the function according
   to the type of test (Upload or Download) and the function(stop, start,...).

   @param[in] test download or upload.
   @param[in] function start, stop, check, abort.

   @return
   NULL if the combination of parameters are not valid.
 */
const cstring_t ipd_up_down_function(ipd_up_down_test_t test, const cstring_t function) {
    if(test == ipd_up_down_download) {
        if(strcmp(function, IPD_UP_DOWN_TEST_START) == 0) {
            return IPD_DOWNLOAD_TEST_START;
        } else if(strcmp(function, IPD_UP_DOWN_TEST_CHECK) == 0) {
            return IPD_DOWNLOAD_TEST_CHECK;
        } else if(strcmp(function, IPD_UP_DOWN_TEST_STOP) == 0) {
            return IPD_DOWNLOAD_TEST_STOP;
        } else if(strcmp(function, IPD_UP_DOWN_TEST_ABORT) == 0) {
            return IPD_DOWNLOAD_TEST_ABORT;
        } else {
            return NULL;
        }
    } else if(test == ipd_up_down_upload) {
        if(strcmp(function, IPD_UP_DOWN_TEST_START) == 0) {
            return IPD_UPLOAD_TEST_START;
        } else if(strcmp(function, IPD_UP_DOWN_TEST_CHECK) == 0) {
            return IPD_UPLOAD_TEST_CHECK;
        } else if(strcmp(function, IPD_UP_DOWN_TEST_STOP) == 0) {
            return IPD_UPLOAD_TEST_STOP;
        } else if(strcmp(function, IPD_UP_DOWN_TEST_ABORT) == 0) {
            return IPD_UPLOAD_TEST_ABORT;
        } else {
            return NULL;
        }
    } else {
        return NULL;
    }
}

/**
   @brief
   Save the incremental result to the data model.

   @param[in] ret Composite variant with the data.
   @param[in] transaction Transaction.
   @param[in] filter_list Linked list os strings with keys to be filtered.
   @param[in] ipd_result Results object.
   @param[in] max_incr_results max incremental results.

   @return
   0 if the function passes.
 */
static int ipd_up_down_save_incr_res(amxc_var_t* ret, amxd_trans_t* transaction, amxc_llist_t* filter_list,
                                     amxd_object_t* ipd_result, uint16_t max_incr_results) {
    int rv = -1;
    amxd_status_t ret_status = amxd_status_unknown_error;
    amxc_var_t* incremental_results = NULL;
    amxc_var_t* incremental_result_instance = NULL;
    amxd_object_t* ipd_incremental_results = NULL;
    uint32_t size_ipd_incremental_results = 0;
    uint32_t number_of_incr_results = 0;
    uint32_t max_number_of_incr_results;
    amxc_string_t* aux_string = NULL;

    amxc_string_new(&aux_string, 0);

    incremental_results = GET_ARG(ret, UP_DOWN_RET_KEY_INCR_RES);

    amxd_trans_select_pathf(transaction, ".%s.", UP_DOWN_RET_KEY_INCR_RES);
    ipd_incremental_results = amxd_object_findf(ipd_result, ".%s.", UP_DOWN_RET_KEY_INCR_RES);
    size_ipd_incremental_results = (uint32_t) amxc_llist_size(&(ipd_incremental_results->instances));
    amxc_var_for_each(incremental, incremental_results) {
        number_of_incr_results++;
    }

    if(number_of_incr_results > max_incr_results) {
        max_number_of_incr_results = max_incr_results;
    } else {
        max_number_of_incr_results = number_of_incr_results;
    }
    if(number_of_incr_results > size_ipd_incremental_results) {
        for(uint32_t index = size_ipd_incremental_results + 1; index <= max_number_of_incr_results; index++) {
            // add an instance - new instance is selected IPDiagnostics.${prefix_}...Result.{i}.IncrementalResult.{i}
            ret_status = amxd_trans_add_inst(transaction, 0, NULL);
            when_failed(ret_status, exit);
            amxc_string_setf(aux_string, "%u", index);

            incremental_result_instance = GET_ARG(incremental_results, amxc_string_get(aux_string, 0));

            ipd_var_to_dm(transaction, NULL, incremental_result_instance, filter_list);

            amxd_trans_select_pathf(transaction, ".^");
        }
    }
    amxd_trans_select_pathf(transaction, ".^");
    rv = 0;
exit:
    amxc_string_delete(&aux_string);
    return rv;
}

/**
   @brief
   Save the PerConnection result to the data model.

   @param[in] ret Composite variant with the data.
   @param[in] transaction Transaction.
   @param[in] filter_list Linked list os strings with keys to be filtered.
   @param[in] ipd_result Results object.
   @param[in] max_connections Max connections.

   @return
   0 if the function passes.
 */
static int ipd_up_down_save_per_conn(amxc_var_t* ret, amxd_trans_t* transaction, amxc_llist_t* filter_list,
                                     amxd_object_t* ipd_result, uint32_t max_connections) {
    int rv = -1;
    amxd_status_t ret_status = amxd_status_unknown_error;
    amxc_var_t* per_conn_results = NULL;
    amxc_var_t* per_conn_result_instance = NULL;
    amxd_object_t* ipd_per_connection = NULL;
    uint32_t size_ipd_per_connection = 0;
    uint32_t number_of_per_conn = 0;
    uint32_t max_number_of_connections;
    amxc_string_t* aux_string = NULL;

    amxc_string_new(&aux_string, 0);

    per_conn_results = GET_ARG(ret, UP_DOWN_RET_KEY_PER_CONN_RES);

    amxd_trans_select_pathf(transaction, ".%s.", UP_DOWN_RET_KEY_PER_CONN_RES);
    ipd_per_connection = amxd_object_findf(ipd_result, ".%s.", UP_DOWN_RET_KEY_PER_CONN_RES);
    size_ipd_per_connection = (uint32_t) amxc_llist_size(&(ipd_per_connection->instances));
    amxc_var_for_each(connection, per_conn_results) {
        number_of_per_conn++;
    }

    if(number_of_per_conn > max_connections) {
        max_number_of_connections = max_connections;
    } else {
        max_number_of_connections = number_of_per_conn;
    }
    if(number_of_per_conn > size_ipd_per_connection) {
        for(uint32_t index = size_ipd_per_connection + 1; index <= max_number_of_connections; index++) {
            // add an instance - new instance is selected IPDiagnostics.${prefix_}...Result.{i}.PerConnectionResult.{i}
            ret_status = amxd_trans_add_inst(transaction, 0, NULL);
            when_failed(ret_status, exit);
            amxc_string_setf(aux_string, "%u", index);

            per_conn_result_instance = GET_ARG(per_conn_results, amxc_string_get(aux_string, 0));

            ipd_var_to_dm(transaction, NULL, per_conn_result_instance, filter_list);

            amxd_trans_select_pathf(transaction, ".^");
        }
    }

    rv = 0;
exit:
    amxc_string_delete(&aux_string);
    return rv;
}

/**
   @brief
   Save the information from the variant to the datamodel.

   @param[in] ret Composite variant with the data.
   @param[in] result_instance_index Index of the object instance.
   @param[in] ipd_results Datamodel object cointaining the results.
   @param[in] test download or upload.

   @return
   Not 0 if the function fails.
 */
int ipd_up_down_save_result_to_dm(amxc_var_t* ret, uint32_t result_instance_index,
                                  amxd_object_t* ipd_results, ipd_up_down_test_t test) {
    const cstring_t key_max_incr_res = ipd_up_down_max_incr_res(test);
    const cstring_t key_down_max_conn = ipd_up_down_max_conn(test);
    amxd_status_t ret_status = amxd_status_unknown_error;
    amxc_var_t* cfg = NULL;
    amxc_var_t* proc = NULL;
    amxd_object_t* ipd_object = ipd_get_root_object();
    amxd_object_t* ipd_result = NULL;
    amxd_trans_t* transaction;
    amxc_llist_t* filter_list = NULL;
    bool en_per_conn_result = false;
    uint16_t max_incr_results = 0;
    uint32_t max_connections = 0;
    amxc_var_t* cfg_param;
    cstring_t ipd_result_path = NULL;
    int rv = 0;

    amxc_llist_new(&filter_list);
    amxc_llist_add_string(filter_list, UP_DOWN_CFG_KEY_INTERFACE_IP);
    amxc_llist_add_string(filter_list, UP_DOWN_CFG_KEY_RESULT_NUMBER);

    when_false(result_instance_index, error);

    ipd_result = amxd_object_get_instance(ipd_results, NULL, result_instance_index);

    cfg = GET_ARG(ret, UP_DOWN_RET_KEY_CFG);
    proc = GET_ARG(ret, UP_DOWN_RET_KEY_PROC);

    cfg_param = (amxc_var_t*) amxd_object_get_param_value(ipd_object, key_max_incr_res);
    max_incr_results = (uint16_t) amxc_var_dyncast(uint16_t, cfg_param);
    cfg_param = (amxc_var_t*) amxd_object_get_param_value(ipd_object, key_down_max_conn);
    max_connections = (uint32_t) GET_UINT32(cfg_param, NULL);
    en_per_conn_result = (bool) GET_BOOL(cfg, UP_DOWN_CFG_KEY_EN_CONN_RESULTS);

    transaction = ipd_transaction_create(ipd_result);
    when_null(transaction, error);

    //Add Result
    ipd_var_to_dm(transaction, NULL, ret, filter_list);

    //Add Config
    ipd_var_to_dm(transaction, UP_DOWN_RET_KEY_CFG, cfg, filter_list);

    //Add Process
    ipd_var_to_dm(transaction, UP_DOWN_RET_KEY_PROC, proc, filter_list);

    //Add incremental result
    rv = ipd_up_down_save_incr_res(ret, transaction, filter_list, ipd_result, max_incr_results);
    when_failed(rv, apply);

    //Add PerConnectionResult result
    if(en_per_conn_result == true) {
        rv = ipd_up_down_save_per_conn(ret, transaction, filter_list, ipd_result, max_connections);
        when_failed(rv, apply);
    }

apply:
    //apply transaction
    ret_status = ipd_transaction_apply(transaction);
    when_failed(ret_status, error);

    ipd_result_path = amxd_object_get_path(ipd_result, AMXD_OBJECT_INDEXED);
    amxc_var_add_key(cstring_t, ret, IPD_UP_DOWN_RESULT_URI, ipd_result_path);
    free(ipd_result_path);

    ret_status = amxd_status_ok;

error:
    amxc_llist_delete(&filter_list, amxc_string_list_it_free);
    return ret_status;
}