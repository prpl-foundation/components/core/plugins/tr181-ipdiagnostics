/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <debug/sahtrace.h>
#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_action.h>
#include <amxd/amxd_object.h>
#include "tx_diag.h"
#include "ip_diagnostics_priv.h"

#define ME "TXDIAG"

const char* get_mnemo_for_txtype(txdiag_type_t type) {
    const char* mnemo[] = {"Unknown", "Down", "Up"};

    return mnemo[type];
}

txdiag_type_t get_obj_txtype(const amxd_object_t* txobj) {
    txdiag_type_t type = tx_unknown;
    char* path = amxd_object_get_path(txobj, AMXD_OBJECT_TERMINATE);

    if(strncmp(path, DM_DOWNDIAG_PATH, strlen(DM_DOWNDIAG_PATH)) == 0) {
        type = tx_download;
    } else if(strncmp(path, DM_UPDIAG_PATH, strlen(DM_UPDIAG_PATH)) == 0) {
        type = tx_upload;
    }
    free(path);
    return type;
}

void _dm_txdiag_requested(UNUSED const char* const signame, UNUSED const amxc_var_t* const data, UNUSED void* const priv) {
    amxd_object_t* txdiag_obj = amxd_dm_signal_get_object(ipd_get_dm(), data);
    txdiag_type_t type = tx_unknown;

    when_null(txdiag_obj, exit);
    type = get_obj_txtype(txdiag_obj);
    when_true(type == tx_unknown, exit);
    SAH_TRACEZ_NOTICE(ME, "%sdiag requested using DiagnosticsState: %s", get_mnemo_for_txtype(type), signame);
    txdiag_requested(txdiag_obj, type);
exit:
    return;
}

void _dm_txdiag_canceled(UNUSED const char* const signame, const amxc_var_t* const data, UNUSED void* const priv) {
    amxd_object_t* txdiag_obj = amxd_dm_signal_get_object(ipd_get_dm(), data);
    txdiag_type_t type = tx_unknown;

    when_null(txdiag_obj, exit);
    type = get_obj_txtype(txdiag_obj);
    when_true(type == tx_unknown, exit);
    SAH_TRACEZ_NOTICE(ME, "%sdiag canceled using DiagnosticsState", get_mnemo_for_txtype(type));
    if(strcmp(GETP_CHAR(data, "parameters.DiagnosticsState.from"), "Requested") == 0) {
        txdiag_canceled(txdiag_obj, type);
    }
    if(strcmp(GET_CHAR(amxd_object_get_param_value(txdiag_obj, "DiagnosticsState"), NULL), "None") != 0) {
        txdiag_clean_results(txdiag_obj, type);
        txdiag_set_diagnostics_state(txdiag_obj, "None");
    }
exit:
    return;
}

void _dm_txdiag_changed(UNUSED const char* const signame, const amxc_var_t* const data, UNUSED void* const priv) {
    amxd_object_t* txdiag_obj = amxd_dm_signal_get_object(ipd_get_dm(), data);
    txdiag_type_t type = tx_unknown;

    when_null(txdiag_obj, exit);
    type = get_obj_txtype(txdiag_obj);
    when_true(type == tx_unknown, exit);
    when_not_null(GETP_ARG(data, "parameters.DiagnosticsState.to"), exit);
    if(strcmp(GET_CHAR(amxd_object_get_param_value(txdiag_obj, "DiagnosticsState"), NULL), "Requested") == 0) {
        SAH_TRACEZ_NOTICE(ME, "%sdiag canceled by changing parameters", get_mnemo_for_txtype(type));
        txdiag_canceled(txdiag_obj, type);
    }
    if(strcmp(GET_CHAR(amxd_object_get_param_value(txdiag_obj, "DiagnosticsState"), NULL), "None") != 0) {
        txdiag_clean_results(txdiag_obj, type);
        txdiag_set_diagnostics_state(txdiag_obj, "None");
    }
exit:
    return;
}

amxd_status_t _check_txdiag_params(amxd_object_t* object,
                                   UNUSED amxd_param_t* param,
                                   amxd_action_t reason,
                                   UNUSED const amxc_var_t* const args,
                                   UNUSED amxc_var_t* const retval,
                                   UNUSED void* priv) {
    amxd_status_t rv = amxd_status_unknown_error;
    txdiag_type_t type;

    when_true_status(reason != action_object_validate, exit, rv = amxd_status_function_not_implemented);
    type = get_obj_txtype(object);
    when_true(type == tx_unknown, exit);
    when_true_status(strcmp(GET_CHAR(amxd_object_get_param_value(object, "DiagnosticsState"), NULL), "Requested") != 0, exit, rv = amxd_status_ok);
    when_failed_status(txdiag_request_validate_params(object, type), exit, rv = amxd_status_invalid_value);
    rv = amxd_status_ok;
exit:
    return rv;
}

amxd_status_t _write_txdiagstate_params(amxd_object_t* object,
                                        amxd_param_t* param,
                                        amxd_action_t reason,
                                        const amxc_var_t* const args,
                                        amxc_var_t* const retval,
                                        void* priv) {
    amxd_status_t rv = amxd_status_unknown_error;
    txdiag_type_t type = get_obj_txtype(object);
    const char* state = GETP_CHAR(args, "parameters.DiagnosticsState");
    bool set_read_only = GET_BOOL(args, "set_read_only");

    when_true_status(reason != action_object_write, exit, rv = amxd_status_function_not_implemented);
    when_true(type == tx_unknown, exit);
    if(set_read_only || (state == NULL)) {
        rv = amxd_action_object_write(object, param, reason, args, retval, priv);
    } else {
        when_false_status(strcmp(state, "Requested") == 0 || strcmp(state, "Canceled") == 0, exit, rv = amxd_status_read_only);
        rv = amxd_action_object_write(object, param, reason, args, retval, priv);
    }
exit:
    return rv;
}
