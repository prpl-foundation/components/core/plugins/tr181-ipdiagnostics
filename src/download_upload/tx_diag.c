/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <netmodel/client.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "tx_diag.h"
#include "ip_diagnostics_priv.h"
#include "config.h"
#include "download.h"
#include "upload.h"

#define ME "TXDIAG"
#define GET_GREATER(x, y) ((x > y) ? (x) : (y))
#define GET_EARLIEST(x, y) ((amxc_ts_t*) ((y != NULL) ? ((amxc_ts_compare((amxc_ts_t*) x, (amxc_ts_t*) y) > 0) ? (y) : (x)) : (x)))
#define GET_LATEST(x, y) ((amxc_ts_t*) ((y != NULL) ? ((amxc_ts_compare((amxc_ts_t*) x, (amxc_ts_t*) y) > 0) ? (x) : (y)) : (x)))
#define GET_TIME(time, obj) ((amxc_ts_t*) amxc_var_constcast(amxc_ts_t, amxd_object_get_param_value(obj, time)))

static uint32_t _down_error_detected = 0;
static uint32_t _up_error_detected = 0;
static amxc_var_t _down_interface_stats;
static amxc_var_t _up_interface_stats;

static const cstring_t _str_down_test_status[download_test_num_of_status] = {
    UP_DOWN_STATUS_COMPLETE,
    UP_DOWN_STATUS_ERR_HOST_NAME,
    UP_DOWN_STATUS_ERR_ROUTE,
    UP_DOWN_STATUS_ERR_INIT_CONN,
    UP_DOWN_STATUS_ERR_NO_RESPONSE,
    DOWN_STATUS_ERR_TRANSFER,
    UP_DOWN_STATUS_ERR_PASSWORD,
    UP_DOWN_STATUS_ERR_LOGIN,
    UP_DOWN_STATUS_ERR_NO_TRANSFER,
    UP_DOWN_STATUS_ERR_NO_PASV,
    DOWN_STATUS_ERR_WRONG_SIZE,
    UP_DOWN_STATUS_ERR_TIMEOUT,
    UP_DOWN_STATUS_ERR_INTERNAL,
    UP_DOWN_STATUS_ERR_OTHER,
    UP_DOWN_STATUS_NOT_COMPLETE,
    UP_DOWN_STATUS_NOT_EXECUTED
};

static const cstring_t _str_up_test_status[upload_test_num_of_status] = {
    UP_DOWN_STATUS_COMPLETE,
    UP_DOWN_STATUS_ERR_HOST_NAME,
    UP_DOWN_STATUS_ERR_ROUTE,
    UP_DOWN_STATUS_ERR_INIT_CONN,
    UP_DOWN_STATUS_ERR_NO_RESPONSE,
    UP_DOWN_STATUS_ERR_PASSWORD,
    UP_DOWN_STATUS_ERR_LOGIN,
    UP_DOWN_STATUS_ERR_NO_TRANSFER,
    UP_DOWN_STATUS_ERR_NO_PASV,
    UP_STATUS_ERR_NO_CWD,
    UP_STATUS_ERR_NO_STOR,
    UP_STATUS_ERR_TRAN_COMP,
    UP_DOWN_STATUS_ERR_TIMEOUT,
    UP_DOWN_STATUS_ERR_INTERNAL,
    UP_DOWN_STATUS_ERR_OTHER,
    UP_DOWN_STATUS_NOT_COMPLETE,
    UP_DOWN_STATUS_NOT_EXECUTED
};

#define TX_TEST_STATUS(type, x) ((type == tx_download) ? _str_down_test_status[x] : _str_up_test_status[x])

static uint64_t amxc_ts_diff(const amxc_ts_t* start, const amxc_ts_t* end, uint32_t precision) {
    uint64_t diff = 0;
    uint32_t start_msec = start->nsec / (1000000000 / precision);
    uint32_t end_msec = end->nsec / (1000000000 / precision);

    when_true(amxc_ts_compare(start, end) > 0, exit);
    diff = end->sec - start->sec;
    diff *= precision;
    if(end_msec >= start_msec) {
        diff += end_msec - start_msec;
    } else {
        diff -= precision;
        diff += (start_msec + (precision - end_msec));
    }
exit:
    return diff;
}

static txdiag_type_t get_type_by_name(const char* name) {
    txdiag_type_t type = tx_unknown;

    if(strcmp(name, "Download") == 0) {
        type = tx_download;
    } else if(strcmp(name, "Upload") == 0) {
        type = tx_upload;
    }
    return type;
}

static void set_error_detected_by_type(txdiag_type_t type, uint32_t value) {
    switch(type) {
    case tx_download:
        _down_error_detected = value;
        break;
    case tx_upload:
        _up_error_detected = value;
        break;
    default:
        break;
    }
}

static uint32_t get_error_detected_by_type(txdiag_type_t type) {
    uint32_t value = 0;
    switch(type) {
    case tx_download:
        value = _down_error_detected;
        break;
    case tx_upload:
        value = _up_error_detected;
        break;
    default:
        break;
    }
    return value;
}

static amxc_var_t* get_interface_stats_by_type(txdiag_type_t type) {
    amxc_var_t* stats = NULL;

    switch(type) {
    case tx_download:
        stats = &_down_interface_stats;
        break;
    case tx_upload:
        stats = &_up_interface_stats;
        break;
    default:
        break;
    }
    return stats;
}

void txdiag_interface_stats(UNUSED const char* const sig_name, const amxc_var_t* const data, UNUSED void* const priv) {
    int rv = 0;
    const char* type = GET_CHAR(data, "type");
    char* index = NULL;
    amxd_object_t* txdiag_obj = amxd_dm_findf(ipd_get_dm(), "IPDiagnostics.%sDiagnostics.", type);
    amxc_var_t ret;
    amxc_string_t path;
    amxc_var_t* index_var = NULL;
    amxc_var_t* interface_stats = NULL;

    amxc_var_init(&ret);
    amxc_string_init(&path, 0);
    index = amxc_var_dyncast(cstring_t, GET_ARG(data, "index"));
    when_null_trace(txdiag_obj, exit, ERROR, "Couldn't find IPDiagnostics.%sDiagnostics.", type);
    interface_stats = get_interface_stats_by_type(get_type_by_name(type));
    when_null_trace(interface_stats, exit, ERROR, "Type is unknown: %s", type);
    when_str_empty(GET_CHAR(amxd_object_get_param_value(txdiag_obj, "Interface"), NULL), exit);
    amxc_string_set(&path, GET_CHAR(amxd_object_get_param_value(txdiag_obj, "Interface"), NULL));
    amxc_string_appendf(&path, "%s%s", (*amxc_string_get(&path, amxc_string_text_length(&path) - 1) == '.') ? "" : ".", "Stats.");
    rv = amxb_get(ipd_get_ctx(), amxc_string_get(&path, 0), 0, &ret, 1);
    when_true_trace(rv != amxd_status_ok, exit, WARNING, "Couldn't get Interface stats");
    index_var = GET_ARG(interface_stats, index);
    if(index_var == NULL) {
        index_var = amxc_var_add_new_key(interface_stats, index);
        amxc_var_set_type(index_var, AMXC_VAR_ID_HTABLE);
    }
    amxc_var_move(amxc_var_add_new_key(index_var, GET_CHAR(data, "time")), GETP_ARG(&ret, "0.0."));
exit:
    amxc_var_clean(&ret);
    amxc_string_clean(&path);
    free(index);
    return;
}

static void update_txdiag_error(amxd_object_t* txdiag_obj, txdiag_type_t type, const amxc_var_t* data) {
    uint32_t error_code = (get_error_detected_by_type(type) == 0) ? GET_UINT32(data, "error_code") : get_error_detected_by_type(type);

    SAH_TRACEZ_ERROR(ME, "%sloadDiagnostics connection ended with code %u", get_mnemo_for_txtype(type), error_code);
    txdiag_set_diagnostics_state(txdiag_obj, TX_TEST_STATUS(type, error_code));
}

static amxc_var_t* get_interface_stats_by_index(txdiag_type_t type, uint32_t index) {
    amxc_string_t path;
    amxc_var_t* ret = NULL;

    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "%d.", index);
    ret = GETP_ARG(get_interface_stats_by_type(type), amxc_string_get(&path, 0));
    amxc_string_clean(&path);
    return ret;
}

static void update_txdiag_result(amxd_object_t* result_obj, txdiag_type_t type, const amxc_var_t* const data) {
    amxd_trans_t trans;
    amxd_status_t rv = 0;
    const char* test_bytes_tx = (type == tx_download) ? "TestBytesReceived" : "TestBytesSent";
    amxc_var_t* stats = NULL;

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, result_obj);
    if(amxd_object_get_type(result_obj) == amxd_object_template) {
        amxd_trans_add_inst(&trans, GET_UINT32(data, "index"), NULL);
        amxd_trans_set_uint32_t(&trans, "BytesUnderFullLoading", GET_UINT32(data, "BytesUnderFullLoading"));
    }
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    if(amxd_object_get_param_def(result_obj, "IPAddressUsed") != NULL) {
        amxd_trans_set_cstring_t(&trans, "IPAddressUsed", GET_CHAR(data, "IPAddressUsed"));
    }
    amxd_trans_set_amxc_ts_t(&trans, "BOMTime", (amxc_ts_t*) amxc_var_constcast(amxc_ts_t, GET_ARG(data, "BOMTime")));
    amxd_trans_set_amxc_ts_t(&trans, "ROMTime", (amxc_ts_t*) amxc_var_constcast(amxc_ts_t, GET_ARG(data, "ROMTime")));
    amxd_trans_set_amxc_ts_t(&trans, "EOMTime", (amxc_ts_t*) amxc_var_constcast(amxc_ts_t, GET_ARG(data, "EOMTime")));
    amxd_trans_set_amxc_ts_t(&trans, "TCPOpenRequestTime", (amxc_ts_t*) amxc_var_constcast(amxc_ts_t, GET_ARG(data, "TCPOpenRequestTime")));
    amxd_trans_set_amxc_ts_t(&trans, "TCPOpenResponseTime", (amxc_ts_t*) amxc_var_constcast(amxc_ts_t, GET_ARG(data, "TCPOpenResponseTime")));
    amxd_trans_set_uint32_t(&trans, test_bytes_tx, GET_UINT32(data, test_bytes_tx));
    stats = get_interface_stats_by_index(type, GET_UINT32(data, "index"));
    amxd_trans_set_uint32_t(&trans, "TotalBytesReceived", GETP_UINT32(stats, "EOM.BytesReceived") - GETP_UINT32(stats, "BOM.BytesReceived"));
    amxd_trans_set_uint32_t(&trans, "TotalBytesSent", GETP_UINT32(stats, "EOM.BytesSent") - GETP_UINT32(stats, "BOM.BytesSent"));
    rv = amxd_trans_apply(&trans, ipd_get_dm());
    if(rv != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Couldn't apply test result to the datamodel (amxd_status_t: %d)", rv);
    }
    txdiag_set_diagnostics_state(result_obj, TX_TEST_STATUS(type, GET_UINT32(data, "error_code")));
    amxd_trans_clean(&trans);
}

void txdiag_clean_results(amxd_object_t* txdiag_obj, txdiag_type_t type) {
    amxd_trans_t trans;
    amxd_status_t rv = 0;
    amxd_object_t* pc_results = amxd_object_get_child(txdiag_obj, "PerConnectionResult");
    amxd_object_t* inc_results = amxd_object_get_child(txdiag_obj, "IncrementalResult");
    amxc_ts_t null_time = {0};

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, txdiag_obj);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_cstring_t(&trans, "IPAddressUsed", "");
    amxd_trans_set_amxc_ts_t(&trans, "ROMTime", &null_time);
    amxd_trans_set_amxc_ts_t(&trans, "BOMTime", &null_time);
    amxd_trans_set_amxc_ts_t(&trans, "EOMTime", &null_time);
    amxd_trans_set_uint64_t(&trans, (type == tx_download) ? "TestBytesReceived" : "TestBytesSent", 0);
    amxd_trans_set_uint64_t(&trans, "TotalBytesReceived", 0);
    amxd_trans_set_uint64_t(&trans, "TotalBytesSent", 0);
    amxd_trans_set_uint64_t(&trans, (type == tx_download) ? "TestBytesReceivedUnderFullLoading" : "TestBytesSentUnderFullLoading", 0);
    amxd_trans_set_uint64_t(&trans, "TotalBytesReceivedUnderFullLoading", 0);
    amxd_trans_set_uint64_t(&trans, "TotalBytesSentUnderFullLoading", 0);
    amxd_trans_set_uint64_t(&trans, "PeriodOfFullLoading", 0);
    amxd_trans_set_amxc_ts_t(&trans, "TCPOpenRequestTime", &null_time);
    amxd_trans_set_amxc_ts_t(&trans, "TCPOpenResponseTime", &null_time);
    if(amxd_object_get_instance_count(pc_results) > 0) {
        amxd_trans_select_object(&trans, pc_results);
        amxd_object_for_each(instance, it, pc_results) {
            amxd_object_t* pc_result = amxc_container_of(it, amxd_object_t, it);
            amxd_trans_del_inst(&trans, amxd_object_get_index(pc_result), NULL);
        }
    }
    if(amxd_object_get_instance_count(inc_results) > 0) {
        amxd_trans_select_object(&trans, inc_results);
        amxd_object_for_each(instance, it, inc_results) {
            amxd_object_t* inc_result = amxc_container_of(it, amxd_object_t, it);
            amxd_trans_del_inst(&trans, amxd_object_get_index(inc_result), NULL);
        }
    }
    rv = amxd_trans_apply(&trans, ipd_get_dm());
    if(rv != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Couldn't clean results from the datamodel (amxd_status_t: %d)", rv);
    }
    amxd_trans_clean(&trans);
}

static void txdiag_get_request_args(amxc_var_t* request_args, amxd_object_t* txdiag_obj, txdiag_type_t type) {
    bool per_connection_results_enabled = GET_BOOL(amxd_object_get_param_value(txdiag_obj, "EnablePerConnectionResults"), NULL);
    uint16_t time_based_test_duration = amxc_var_dyncast(uint16_t, amxd_object_get_param_value(txdiag_obj, "TimeBasedTestDuration"));
    amxc_var_t* intf = NULL;

    amxc_var_set_type(request_args, AMXC_VAR_ID_HTABLE);
    intf = netmodel_getFirstParameter(GET_CHAR(amxd_object_get_param_value(txdiag_obj, "Interface"), NULL), "NetDevName", "netdev-bound", "down");
    if(intf != NULL) {
        amxc_var_add_key(cstring_t, request_args, "Interface", GET_CHAR(intf, NULL));
    }
    switch(type) {
    case tx_download:
        amxc_var_add_key(cstring_t, request_args, "DownloadURL", GET_CHAR(amxd_object_get_param_value(txdiag_obj, "DownloadURL"), NULL));
        break;
    case tx_upload:
        amxc_var_add_key(cstring_t, request_args, "UploadURL", GET_CHAR(amxd_object_get_param_value(txdiag_obj, "UploadURL"), NULL));
        amxc_var_add_key(uint64_t, request_args, "TestFileLength", amxc_var_constcast(uint64_t, amxd_object_get_param_value(txdiag_obj, "TestFileLength")));
        break;
    default:
        break;
    }
    amxc_var_add_key(uint8_t, request_args, "DSCP", amxc_var_dyncast(uint8_t, amxd_object_get_param_value(txdiag_obj, "DSCP")));
    amxc_var_add_key(uint8_t, request_args, "EthernetPriority", amxc_var_dyncast(uint8_t, amxd_object_get_param_value(txdiag_obj, "EthernetPriority")));
    amxc_var_add_key(uint16_t, request_args, "TimeBasedTestDuration", time_based_test_duration);
    if(time_based_test_duration > 0) {
        amxc_var_add_key(uint16_t, request_args, "TimeBasedTestMeasurementInterval", amxc_var_dyncast(uint16_t, amxd_object_get_param_value(txdiag_obj, "TimeBasedTestMeasurementInterval")));
        amxc_var_add_key(uint8_t, request_args, "TimeBasedTestMeasurementOffset", amxc_var_dyncast(uint8_t, amxd_object_get_param_value(txdiag_obj, "TimeBasedTestMeasurementOffset")));
    }
    amxc_var_add_key(cstring_t, request_args, "ProtocolVersion", GET_CHAR(amxd_object_get_param_value(txdiag_obj, "ProtocolVersion"), NULL));
    if(per_connection_results_enabled) {
        amxc_var_add_key(uint32_t, request_args, "NumberOfConnections", amxc_var_dyncast(uint32_t, amxd_object_get_param_value(txdiag_obj, "NumberOfConnections")));
    }
    amxc_var_delete(&intf);
}

int txdiag_request_validate_params(amxd_object_t* txdiag_obj, txdiag_type_t type) {
    int rv = -1;
    amxc_string_t param;

    amxc_string_init(&param, 0);
    when_null(txdiag_obj, exit);
    amxc_string_setf(&param, "%sloadURL", get_mnemo_for_txtype(type));
    when_str_empty_trace(GET_CHAR(amxd_object_get_param_value(txdiag_obj, amxc_string_get(&param, 0)), NULL), exit, ERROR, "%s shouldn't be empty", amxc_string_get(&param, 0));
    if(type == tx_upload) {
        when_true_trace(GET_UINT32(amxd_object_get_param_value(txdiag_obj, "TestFileLength"), NULL) == 0, exit, ERROR, "TestFileLength shoudn't be 0");
    }
    rv = 0;
exit:
    amxc_string_clean(&param);
    return rv;
}

void txdiag_requested(amxd_object_t* txdiag_obj, txdiag_type_t type) {
    amxc_var_t request_params;
    amxc_var_t ret;
    amxc_var_t* interface_stats = get_interface_stats_by_type(type);

    amxc_var_init(&request_params);
    amxc_var_init(&ret);
    when_true(type == tx_unknown, exit);
    when_null(txdiag_obj, exit);
    amxc_var_init(interface_stats);
    amxc_var_set_type(interface_stats, AMXC_VAR_ID_HTABLE);
    txdiag_clean_results(txdiag_obj, type);
    set_error_detected_by_type(type, 0);
    when_failed(txdiag_request_validate_params(txdiag_obj, type), exit);
    txdiag_get_request_args(&request_params, txdiag_obj, type);
    cfgctrlr_execute_function(txdiag_obj, (type == tx_download) ? IPD_DOWNLOAD_TEST_START : IPD_UPLOAD_TEST_START, &request_params, &ret);
exit:
    amxc_var_clean(&request_params);
    amxc_var_clean(&ret);
}

void txdiag_canceled(amxd_object_t* txdiag_obj, txdiag_type_t type) {
    amxc_var_t request_params;
    amxc_var_t ret;
    amxc_var_t* interface_stats = get_interface_stats_by_type(type);

    amxc_var_init(&request_params);
    amxc_var_init(&ret);
    when_true(type == tx_unknown, exit);
    when_null(txdiag_obj, exit);
    cfgctrlr_execute_function(txdiag_obj, (type == tx_download) ? IPD_DOWNLOAD_TEST_ABORT : IPD_UPLOAD_TEST_ABORT, &request_params, &ret);
exit:
    amxc_var_clean(interface_stats);
    amxc_var_clean(&request_params);
    amxc_var_clean(&ret);
}

void txdiag_set_diagnostics_state(amxd_object_t* txdiag, const char* state) {
    amxd_param_t* diag_state = NULL;
    amxc_var_t* parameter = NULL;
    amxc_var_t* parameters = NULL;
    amxc_var_t notification;
    char* path = NULL;

    amxc_var_init(&notification);
    when_null(txdiag, exit);
    when_str_empty(state, exit);
    diag_state = amxd_object_get_param_def(txdiag, "DiagnosticsState");
    when_null(diag_state, exit);
    path = amxd_object_get_path(txdiag, AMXD_OBJECT_TERMINATE);
    when_str_empty(path, exit);
    amxc_var_set_type(&notification, AMXC_VAR_ID_HTABLE);
    parameters = amxc_var_add_key(amxc_htable_t, &notification, "parameters", NULL);
    parameter = amxc_var_add_key(amxc_htable_t, parameters, "DiagnosticsState", NULL);
    amxc_var_add_key(cstring_t, parameter, "from", GET_CHAR(&diag_state->value, NULL));
    amxc_var_add_key(cstring_t, parameter, "to", state);
    amxc_var_set_cstring_t(&diag_state->value, state);
    amxc_var_add_key(cstring_t, &notification, "path", path);
    amxc_var_add_key(cstring_t, &notification, "eobject", path);
    amxc_var_add_key(cstring_t, &notification, "object", path);
    amxc_var_add_key(cstring_t, &notification, "notification", "dm:object-changed");
    amxd_object_send_signal(txdiag, "dm:object-changed", &notification, false);
exit:
    free(path);
    amxc_var_clean(&notification);
}

void downdiag_ended(UNUSED const char* const sig_name,
                    const amxc_var_t* const data,
                    UNUSED void* const priv) {
    amxd_object_t* downdiag_obj = amxd_dm_findf(ipd_get_dm(), "%s", DM_DOWNDIAG_PATH);

    if(GET_UINT32(data, "error_code") == 0) {
        if(GET_UINT32(data, "index") == 0) {
            update_txdiag_result(downdiag_obj, tx_download, data);
            amxc_var_clean(&_down_interface_stats);
        } else {
            update_txdiag_result(amxd_dm_findf(ipd_get_dm(), "%sPerConnectionResult.", DM_DOWNDIAG_PATH), tx_download, data);
        }
    } else {
        if(GET_UINT32(data, "index") == 0) {
            update_txdiag_error(downdiag_obj, tx_download, data);
            amxc_var_clean(&_down_interface_stats);
        } else {
            _down_error_detected = GET_UINT32(data, "error_code");
        }
    }
}

void downdiag_all_ended(UNUSED const char* const sig_name,
                        const amxc_var_t* const data,
                        UNUSED void* const priv) {
    amxd_object_t* downdiag_obj = amxd_dm_findf(ipd_get_dm(), "%s", DM_DOWNDIAG_PATH);
    amxd_trans_t trans;
    amxd_status_t rv = 0;
    uint32_t test_bytes_received = 0;
    uint32_t test_bytes_received_under_fullloading = 0;
    uint32_t total_bytes_received_under_fullloading = 0;
    uint32_t total_bytes_sent_under_fullloading = 0;
    uint64_t total_bytes_received = 0;
    uint64_t total_bytes_sent = 0;
    amxc_ts_t* tcp_req = NULL;
    amxc_ts_t* tcp_resp = NULL;
    amxc_ts_t* rom_time = NULL;
    amxc_ts_t* bom_time = NULL;
    amxc_ts_t* eom_time = NULL;
    amxd_object_t* latest_bom_result = NULL;
    amxd_object_t* earliest_eom_result = NULL;

    amxd_trans_init(&trans);
    when_true_status(_down_error_detected != 0, exit, update_txdiag_error(downdiag_obj, tx_download, data));
    amxd_trans_select_object(&trans, downdiag_obj);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_object_for_each(instance, it, amxd_object_get(downdiag_obj, "PerConnectionResult")) {
        amxd_object_t* result = amxc_container_of(it, amxd_object_t, it);

        test_bytes_received += GET_UINT32(amxd_object_get_param_value(result, "TestBytesReceived"), NULL);
        test_bytes_received_under_fullloading += GET_UINT32(amxd_object_get_param_value(result, "BytesUnderFullLoading"), NULL);
        total_bytes_received += amxc_var_constcast(uint64_t, amxd_object_get_param_value(result, "TotalBytesReceived"));
        total_bytes_sent += amxc_var_constcast(uint64_t, amxd_object_get_param_value(result, "TotalBytesSent"));
        tcp_req = GET_LATEST(GET_TIME("TCPOpenRequestTime", result), tcp_req);
        tcp_resp = GET_LATEST(GET_TIME("TCPOpenResponseTime", result), tcp_resp);
        rom_time = GET_EARLIEST(GET_TIME("ROMTime", result), rom_time);
        bom_time = GET_EARLIEST(GET_TIME("BOMTime", result), bom_time);
        eom_time = GET_LATEST(GET_TIME("EOMTime", result), eom_time);
        latest_bom_result = (latest_bom_result != NULL) ? (amxc_ts_compare(GET_TIME("BOMTime", result), GET_TIME("BOMTime", latest_bom_result)) > 0 ? result : latest_bom_result)  : (result);
        earliest_eom_result = (earliest_eom_result != NULL) ? (amxc_ts_compare(GET_TIME("EOMTime", result), GET_TIME("EOMTime", earliest_eom_result)) > 0 ? earliest_eom_result : result)  : (result);
    }
    total_bytes_received_under_fullloading = GETP_UINT32(get_interface_stats_by_index(tx_download, amxd_object_get_index(earliest_eom_result)), "EOM.BytesReceived");
    total_bytes_received_under_fullloading -= GETP_UINT32(get_interface_stats_by_index(tx_download, amxd_object_get_index(latest_bom_result)), "BOM.BytesReceived");
    total_bytes_sent_under_fullloading = GETP_UINT32(get_interface_stats_by_index(tx_download, amxd_object_get_index(earliest_eom_result)), "EOM.BytesSent");
    total_bytes_sent_under_fullloading -= GETP_UINT32(get_interface_stats_by_index(tx_download, amxd_object_get_index(latest_bom_result)), "BOM.BytesSent");
    amxd_trans_set_uint32_t(&trans, "TestBytesReceived", test_bytes_received);
    amxd_trans_set_amxc_ts_t(&trans, "TCPOpenRequestTime", tcp_req);
    amxd_trans_set_amxc_ts_t(&trans, "TCPOpenResponseTime", tcp_resp);
    amxd_trans_set_amxc_ts_t(&trans, "ROMTime", rom_time);
    amxd_trans_set_amxc_ts_t(&trans, "BOMTime", bom_time);
    amxd_trans_set_amxc_ts_t(&trans, "EOMTime", eom_time);
    amxd_trans_set_cstring_t(&trans, "IPAddressUsed", GET_CHAR(data, "IPAddressUsed"));
    amxd_trans_set_uint64_t(&trans, "PeriodOfFullLoading", amxc_ts_diff(GET_TIME("BOMTime", latest_bom_result), GET_TIME("EOMTime", earliest_eom_result), 1000000));
    amxd_trans_set_uint64_t(&trans, "TestBytesReceivedUnderFullLoading", test_bytes_received_under_fullloading);
    amxd_trans_set_uint64_t(&trans, "TotalBytesReceivedUnderFullLoading", total_bytes_received_under_fullloading);
    amxd_trans_set_uint64_t(&trans, "TotalBytesSentUnderFullLoading", total_bytes_sent_under_fullloading);
    amxd_trans_set_uint64_t(&trans, "TotalBytesSent", total_bytes_sent);
    amxd_trans_set_uint64_t(&trans, "TotalBytesReceived", total_bytes_received);
    rv = amxd_trans_apply(&trans, ipd_get_dm());
    if(rv != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Couldn't apply result to the datamodel (amxd_status_t: %d)", rv);
    }
    txdiag_set_diagnostics_state(downdiag_obj, _str_down_test_status[GET_UINT32(data, "error_code")]);
exit:
    amxc_var_clean(&_down_interface_stats);
    amxd_trans_clean(&trans);
}

void updiag_ended(UNUSED const char* const sig_name,
                  const amxc_var_t* const data,
                  UNUSED void* const priv) {
    amxd_object_t* updiag_obj = amxd_dm_findf(ipd_get_dm(), "%s", DM_UPDIAG_PATH);

    if(GET_UINT32(data, "error_code") == 0) {
        if(GET_UINT32(data, "index") == 0) {
            update_txdiag_result(updiag_obj, tx_upload, data);
            amxc_var_clean(&_up_interface_stats);
        } else {
            update_txdiag_result(amxd_dm_findf(ipd_get_dm(), "%sPerConnectionResult.", DM_UPDIAG_PATH), tx_upload, data);
        }
    } else {
        if(GET_UINT32(data, "index") == 0) {
            update_txdiag_error(updiag_obj, tx_upload, data);
            amxc_var_clean(&_up_interface_stats);
        } else {
            _up_error_detected = GET_UINT32(data, "error_code");
        }
    }
}

void updiag_all_ended(UNUSED const char* const sig_name,
                      const amxc_var_t* const data,
                      UNUSED void* const priv) {
    amxd_object_t* updiag_obj = amxd_dm_findf(ipd_get_dm(), "%s", DM_UPDIAG_PATH);
    amxd_trans_t trans;
    amxd_status_t rv = 0;
    uint32_t test_bytes_sent = 0;
    uint32_t test_bytes_sent_under_fullloading = 0;
    uint32_t total_bytes_received_under_fullloading = 0;
    uint32_t total_bytes_sent_under_fullloading = 0;
    uint64_t total_bytes_received = 0;
    uint64_t total_bytes_sent = 0;
    amxc_ts_t* tcp_req = NULL;
    amxc_ts_t* tcp_resp = NULL;
    amxc_ts_t* rom_time = NULL;
    amxc_ts_t* bom_time = NULL;
    amxc_ts_t* eom_time = NULL;
    amxd_object_t* latest_bom_result = NULL;
    amxd_object_t* earliest_eom_result = NULL;

    amxd_trans_init(&trans);
    when_true_status(_up_error_detected != 0, exit, update_txdiag_error(updiag_obj, tx_upload, data));
    amxd_trans_select_object(&trans, updiag_obj);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxc_var_log(get_interface_stats_by_type(tx_upload));
    amxd_object_for_each(instance, it, amxd_object_get(updiag_obj, "PerConnectionResult")) {
        amxd_object_t* result = amxc_container_of(it, amxd_object_t, it);

        test_bytes_sent += GET_UINT32(amxd_object_get_param_value(result, "TestBytesSent"), NULL);
        test_bytes_sent_under_fullloading += GET_UINT32(amxd_object_get_param_value(result, "BytesUnderFullLoading"), NULL);
        total_bytes_received += amxc_var_constcast(uint64_t, amxd_object_get_param_value(result, "TotalBytesReceived"));
        total_bytes_sent += amxc_var_constcast(uint64_t, amxd_object_get_param_value(result, "TotalBytesSent"));
        tcp_req = GET_LATEST(GET_TIME("TCPOpenRequestTime", result), tcp_req);
        tcp_resp = GET_LATEST(GET_TIME("TCPOpenResponseTime", result), tcp_resp);
        rom_time = GET_EARLIEST(GET_TIME("ROMTime", result), rom_time);
        bom_time = GET_EARLIEST(GET_TIME("BOMTime", result), bom_time);
        eom_time = GET_LATEST(GET_TIME("EOMTime", result), eom_time);
        latest_bom_result = (latest_bom_result != NULL) ? (amxc_ts_compare(GET_TIME("BOMTime", result), GET_TIME("BOMTime", latest_bom_result)) > 0 ? result : latest_bom_result)  : (result);
        earliest_eom_result = (earliest_eom_result != NULL) ? (amxc_ts_compare(GET_TIME("EOMTime", result), GET_TIME("EOMTime", earliest_eom_result)) > 0 ? earliest_eom_result : result)  : (result);
    }
    total_bytes_received_under_fullloading = GETP_UINT32(get_interface_stats_by_index(tx_upload, amxd_object_get_index(earliest_eom_result)), "EOM.BytesReceived");
    total_bytes_received_under_fullloading -= GETP_UINT32(get_interface_stats_by_index(tx_upload, amxd_object_get_index(latest_bom_result)), "BOM.BytesReceived");
    total_bytes_sent_under_fullloading = GETP_UINT32(get_interface_stats_by_index(tx_upload, amxd_object_get_index(earliest_eom_result)), "EOM.BytesSent");
    total_bytes_sent_under_fullloading -= GETP_UINT32(get_interface_stats_by_index(tx_upload, amxd_object_get_index(latest_bom_result)), "BOM.BytesSent");
    amxd_trans_set_uint32_t(&trans, "TestBytesSent", test_bytes_sent);
    amxd_trans_set_amxc_ts_t(&trans, "TCPOpenRequestTime", tcp_req);
    amxd_trans_set_amxc_ts_t(&trans, "TCPOpenResponseTime", tcp_resp);
    amxd_trans_set_amxc_ts_t(&trans, "ROMTime", rom_time);
    amxd_trans_set_amxc_ts_t(&trans, "BOMTime", bom_time);
    amxd_trans_set_amxc_ts_t(&trans, "EOMTime", eom_time);
    amxd_trans_set_cstring_t(&trans, "IPAddressUsed", GET_CHAR(data, "IPAddressUsed"));
    amxd_trans_set_uint64_t(&trans, "PeriodOfFullLoading", amxc_ts_diff(GET_TIME("BOMTime", latest_bom_result), GET_TIME("EOMTime", earliest_eom_result), 1000000));
    amxd_trans_set_uint64_t(&trans, "TestBytesSentUnderFullLoading", test_bytes_sent_under_fullloading);
    amxd_trans_set_uint64_t(&trans, "TotalBytesReceivedUnderFullLoading", total_bytes_received_under_fullloading);
    amxd_trans_set_uint64_t(&trans, "TotalBytesSentUnderFullLoading", total_bytes_sent_under_fullloading);
    amxd_trans_set_uint64_t(&trans, "TotalBytesSent", total_bytes_sent);
    amxd_trans_set_uint64_t(&trans, "TotalBytesReceived", total_bytes_received);
    rv = amxd_trans_apply(&trans, ipd_get_dm());
    if(rv != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Couldn't apply result to the datamodel (amxd_status_t: %d)", rv);
    }
    txdiag_set_diagnostics_state(updiag_obj, _str_up_test_status[GET_UINT32(data, "error_code")]);
exit:
    amxc_var_clean(&_up_interface_stats);
    amxd_trans_clean(&trans);
}