/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_object.h>
#include <netmodel/client.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_diagnostics.h"
#include "ip_diagnostics_priv.h"
#include "priv_upload_download.h"
#include "config.h"
#include "download.h"
#include "upload.h"

#define ME "ipd-cup-c"

/**
   @brief
   Abort all tests in the scheduler for safety.

   @param[in] sch Scheduler.
 */
static void ipd_up_down_scheduler_abort(ipd_up_down_scheduler_t* sch) {
    for(int index = 0; index < IPD_MAX_UP_DOWN_RESULTS; index++) {
        sch[index].fd = 0;
        sch[index].instance_index = 0;
    }
}

/**
   @brief
   Free a test from the scheduler.

   @param[in] fd File descriptor of the test.
   @param[in] sch Scheduler.
 */
static void ipd_up_down_scheduler_free_test(int32_t fd, ipd_up_down_scheduler_t* sch) {
    for(int index = 0; index < IPD_MAX_UP_DOWN_RESULTS; index++) {
        if(sch[index].fd == fd) {
            sch[index].fd = 0;
            sch[index].instance_index = 0;
        }
    }
}

/**
   @brief
   Get the params from the datamodel

   @param[in] ipd_results Results object.
   @param[in] result_instance_index Results instance index.

   @return
   NULL if it fails.
 */
static amxc_var_t* ipd_up_down_get_params_from_dm(amxd_object_t* ipd_results, uint32_t result_instance_index) {
    amxd_object_t* ipd_result_instance = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* params_cfg = NULL;

    when_false_trace((result_instance_index > 0), error, ERROR, "Result number is 0, test check will be aborted");

    ipd_result_instance = amxd_object_get_instance(ipd_results, NULL, result_instance_index);
    when_null_trace(ipd_result_instance, error, ERROR, "Failed to fetch object with result number =  '%d'.", result_instance_index);

    amxc_var_new(&params);
    amxc_var_set_type(params, AMXC_VAR_ID_HTABLE);
    ipd_obj_data_to_var(ipd_result_instance, NULL, params);

    params_cfg = amxc_var_get_key(params, UP_DOWN_RET_KEY_CFG, AMXC_VAR_FLAG_DEFAULT);
    if(params_cfg == NULL) {
        amxc_var_delete(&params);
        params = NULL;
        SAH_TRACEZ_ERROR(ME, "Failed to get the Config hash table.");
        goto error;
    }
    amxc_var_add_key(uint32_t, params_cfg, UP_DOWN_CFG_KEY_RESULT_NUMBER, result_instance_index);

error:
    return params;
}

/**
   @brief
   Abort all non completed tests.

   @param[in] ipd_results Datamodel object cointaining the results.
   @param[in] test download or upload.
   @param[in] sch Scheduler.
 */
static void ipd_up_down_abort_all_tests(amxd_object_t* ipd_results, ipd_up_down_test_t test,
                                        ipd_up_down_scheduler_t* sch) {
    const cstring_t function_stop = ipd_up_down_function(test, IPD_UP_DOWN_TEST_STOP);
    amxd_object_t* ipd_object = ipd_get_root_object();
    amxd_object_t* tmp_obj = NULL;
    amxd_object_t* tmp_obj_proc = NULL;
    uint32_t instance_index = 0;
    int32_t fd = -1;
    amxc_var_t* status_param = NULL;
    amxc_var_t* fd_var = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* mod_ret = NULL;
    cstring_t status = NULL;

    amxc_var_new(&mod_ret);
    amxc_var_set_type(mod_ret, AMXC_VAR_ID_HTABLE);

    when_null_trace(ipd_results, exit, ERROR, "Object is NULL.");

    amxc_llist_for_each(it, &(ipd_results->instances)) {
        tmp_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
        status_param = (amxc_var_t*) amxd_object_get_param_value(tmp_obj, UP_DOWN_RET_KEY_STATUS);
        status = (cstring_t) GET_CHAR(status_param, NULL);
        if(strcmp(status, UP_DOWN_STATUS_NOT_COMPLETE) == 0) {
            tmp_obj_proc = amxd_object_get(tmp_obj, UP_DOWN_RET_KEY_PROC);
            fd_var = (amxc_var_t*) amxd_object_get_param_value(tmp_obj_proc, UP_DOWN_RET_KEY_PROC_FD_STDOUT);
            fd = (int32_t) GET_INT32(fd_var, NULL);
            if(fd != -1) {
                amxo_connection_remove(ipd_get_parser(), fd);
            }

            instance_index = amxd_object_get_index(tmp_obj);
            params = ipd_up_down_get_params_from_dm(ipd_results, instance_index);
            if(params != NULL) {
                cfgctrlr_ipdiagnostics_execute_function(ipd_object, function_stop, params, mod_ret);
                ipd_up_down_save_result_to_dm(mod_ret, instance_index, ipd_results, test);
            }
        }
    }

exit:
    amxc_var_delete(&params);
    amxc_var_delete(&mod_ret);
    ipd_up_down_scheduler_abort(sch);
}

/**
   @brief
   Check function

   @param[in] fd File descriptor of the test.
   @param[in] priv Any object or variable to be used by the check function.
   @param[in] test download or upload.
   @param[in] sch Scheduler.
 */
void ipd_up_down_check(int fd, void* priv, ipd_up_down_test_t test, ipd_up_down_scheduler_t* sch) {
    const cstring_t function_check = ipd_up_down_function(test, IPD_UP_DOWN_TEST_CHECK);
    const cstring_t key_result = ipd_up_down_result(test);
    amxd_object_t* ipd_object = NULL;
    amxd_object_t* ipd_results = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* mod_ret = NULL;
    uint32_t* result_instance_index = (uint32_t*) priv;
    int status = -1;
    bool abort_flag = true;
    cstring_t prefix;
    cstring_t status_str;

    amxc_var_new(&mod_ret);
    amxc_var_set_type(mod_ret, AMXC_VAR_ID_HTABLE);

    ipd_object = ipd_get_root_object();
    when_null_trace(ipd_object, abort, ERROR, "Failed to fetch object IPDiagnostics. from datamodel.");

    prefix = ipd_get_prefix();
    ipd_results = amxd_object_findf(ipd_object, "%s%s.", prefix, key_result);
    when_null_trace(ipd_results, abort, ERROR, "Failed to fetch object IPDiagnostics.%s%s. from datamodel.", prefix, key_result);

    when_null_trace(priv, abort, ERROR, "The priv struct is NULL, impossible to check the test status.");

    params = ipd_up_down_get_params_from_dm(ipd_results, *result_instance_index);
    when_null_trace(params, abort, ERROR, "Failed to get the parameters to run the check.");

    abort_flag = false;

abort:
    if(abort_flag == true) {
        SAH_TRACEZ_ERROR(ME, "Aborting all tests.");
        ipd_up_down_abort_all_tests(ipd_results, test, sch);
    } else {
        status = cfgctrlr_ipdiagnostics_execute_function(ipd_object, function_check, params, mod_ret);

        ipd_up_down_save_result_to_dm(mod_ret, *result_instance_index, ipd_results, test);
        status_str = (cstring_t) GET_CHAR(mod_ret, UP_DOWN_RET_KEY_STATUS);
        if((status < 0) || (strcmp(status_str, UP_DOWN_STATUS_NOT_COMPLETE) != 0)) {
            amxo_connection_remove(ipd_get_parser(), fd);
            ipd_up_down_scheduler_free_test(fd, sch);
        }
    }

    amxc_var_delete(&params);
    amxc_var_delete(&mod_ret);
}