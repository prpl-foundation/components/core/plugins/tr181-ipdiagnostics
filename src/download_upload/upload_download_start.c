/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_object.h>
#include <netmodel/client.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_diagnostics.h"
#include "ip_diagnostics_priv.h"
#include "priv_upload_download.h"
#include "config.h"
#include "download.h"
#include "upload.h"

#define ME "ipd-cup-s"

/**
   @brief
   Schedule test and return the internal index of the scheduler.

   This scheduler makes sure that all tests are handled appropriately,
   to avoid memory leaks.

   @param[in] fd File descriptor of the running test.
   @param[in] instance_index Index of the result instance on DM.
   @param[in] up_down_check Pointer to the check function.
   @param[in] sch Scheduler.

   @return
   -1 if the scheduler fails, or the internal index of the scheduler.
 */
static int ipd_up_down_scheduler_alloc(int32_t fd, uint32_t instance_index, ipd_up_down_test_t test,
                                       ipd_up_down_scheduler_t* sch) {
    int internal_index = -1;
    for(int index = 0; index < IPD_MAX_UP_DOWN_RESULTS; index++) {
        if((sch[index].fd > 0) && (sch[index].instance_index != 0)) {
            ipd_up_down_check(sch[index].fd, &(sch[index].instance_index), test, sch);
        }
    }
    for(int index = 0; index < IPD_MAX_UP_DOWN_RESULTS; index++) {
        if(sch[index].fd <= 0) {
            internal_index = index;
            sch[index].fd = fd;
            sch[index].instance_index = instance_index;
            break;
        }
    }
    return internal_index;
}

/**
   @brief
   Allocate and return the available result instance index, if the
   slots are full (more than 5) than reserve a new position.

   @param[in] ipd_results Results object.

   @return
   0 if it fails.
 */
static uint32_t ipd_up_down_alloc_index(amxd_object_t* ipd_results) {
    uint32_t result_instance_index = 0;
    uint32_t del_instance_index = 0;
    amxd_status_t ret_status = amxd_status_unknown_error;
    amxd_trans_t* transaction;
    uint32_t size_ipd_results = 0;
    bool found_object = false;
    amxd_object_t* tmp_obj = NULL;
    amxc_var_t* status_param = NULL;
    cstring_t status = NULL;

    size_ipd_results = (uint32_t) amxc_llist_size(&(ipd_results->instances));

    transaction = ipd_transaction_create(ipd_results);
    when_null(transaction, error);

    //Handle previous Results
    if(size_ipd_results >= IPD_MAX_UP_DOWN_RESULTS) {
        amxc_llist_for_each(it, &(ipd_results->instances)) {
            tmp_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
            del_instance_index = amxd_object_get_index(tmp_obj);
            status_param = (amxc_var_t*) amxd_object_get_param_value(tmp_obj, UP_DOWN_RET_KEY_STATUS);
            status = (cstring_t) GET_CHAR(status_param, NULL);
            if(strcmp(status, UP_DOWN_STATUS_NOT_COMPLETE) != 0) {
                found_object = true;
                break;
            }
        }

        if(found_object == false) {
            SAH_TRACEZ_ERROR(ME, "It's not possible to run more than 5 tests simultaneously.");
            goto clean;
        }

        ret_status = amxd_trans_del_inst(transaction, del_instance_index, NULL);
        when_failed(ret_status, clean);
    }

    // add an instance - new instance is selected IPDiagnostics.${prefix_}...Result.{i}.
    ret_status = amxd_trans_add_inst(transaction, 0, NULL);
    when_failed(ret_status, clean);

    ret_status = amxd_trans_apply(transaction, ipd_get_dm());
    when_failed(ret_status, clean);

    tmp_obj = amxd_object_findf(ipd_results,
                                "["UP_DOWN_RET_KEY_STATUS "=='"UP_DOWN_STATUS_NOT_EXECUTED "']");
    when_null(tmp_obj, clean);
    result_instance_index = amxd_object_get_index(tmp_obj);

clean:
    amxd_trans_delete(&transaction);
error:
    return result_instance_index;
}

/**
   @brief
   Prepare to start the test.

   @param[in] args Input variant.
   @param[in] test download or upload.
   @param[out] mod_args Variant with the configuration object filled in.

   @return
   0 if the function passes.
 */
static int ipd_up_down_validate_input_args(amxc_var_t* args, ipd_up_down_test_t test,
                                           amxc_var_t* mod_args) {
    const cstring_t key_config = ipd_up_down_config(test);
    int rv = -1;
    amxd_object_t* ipd_object = NULL;
    amxd_object_t* ipd_config = NULL;
    amxc_var_t* cfg_param = NULL;
    amxc_var_t* mod_args_cfg = NULL;
    amxc_var_t* time_based_test_duration;
    amxc_var_t* interface = NULL;
    amxc_var_t* protocol_version = NULL;
    cstring_t prefix;

    ipd_object = ipd_get_root_object();

    prefix = ipd_get_prefix();
    ipd_config = amxd_object_findf(ipd_object, "%s%s.", prefix, key_config);
    when_null_trace(ipd_config, exit, ERROR, "Failed to fetch object IPDiagnostics.%s%s. from datamodel.", prefix, key_config);

    mod_args_cfg = amxc_var_add_key(amxc_htable_t, mod_args, UP_DOWN_RET_KEY_CFG, NULL);
    rv = ipd_validate_cfg_params(args, ipd_config, mod_args_cfg, IPD);
    when_failed_trace(rv, exit, ERROR, "Failed to validate the parameters.");

    //Add TestFileLength to the params (only valid in UploadDiagnostics)
    rv = -1;
    if(test == ipd_up_down_upload) {
        time_based_test_duration = GET_ARG(mod_args_cfg, UP_DOWN_CFG_KEY_TIME_TEST_DURATION);
        cfg_param = GET_ARG(mod_args_cfg, UP_CFG_KEY_FILE_LENGTH);
        if(GET_UINT32(time_based_test_duration, NULL) == 0) {
            when_false_trace(amxc_var_dyncast(uint64_t, cfg_param) > 0, exit, ERROR, "TestFileLength must be bigger than 0 when no TimeBasedTestDuration is set.")
        }
    }

    //Add SizeOfTest to the params (only valid in DownloadDiagnostics)
    if(test == ipd_up_down_download) {
        cfg_param = (amxc_var_t*) amxd_object_get_param_value(ipd_config, DOWN_CFG_KEY_SIZE_OF_TEST);
        amxc_var_set_pathf(mod_args, cfg_param, DEFAULT_SET_PATH_FLAGS, UP_DOWN_RET_KEY_CFG "."DOWN_CFG_KEY_SIZE_OF_TEST);
    }

    //Fetch IP from the interface
    interface = GET_ARG(mod_args_cfg, UP_DOWN_CFG_KEY_INTERFACE);
    protocol_version = GET_ARG(mod_args_cfg, UP_DOWN_CFG_KEY_PROTOCOL_VERSION);
    cfg_param = ipd_fetch_interface_ip(interface, protocol_version);
    when_null_trace(cfg_param, exit, ERROR, "Failed to get the interface IP.");
    amxc_var_set_pathf(mod_args, cfg_param, DEFAULT_SET_PATH_FLAGS, UP_DOWN_RET_KEY_CFG "."UP_DOWN_CFG_KEY_INTERFACE_IP);
    amxc_var_delete(&cfg_param);

    rv = 0;
exit:
    return rv;
}

/**
   @brief
   Start the test on the module.

   @param[in] test download or upload.
   @param[in] instance_index Index of the result instance on DM.
   @param[in] sch Scheduler.
   @param[in] check_function pointer to the check function.
   @param[in] mod_args Input variant to be sent to the module.
   @param[out] mod_ret Output variant from the module.

   @return
   0 if the function passes.
 */
static int ipd_up_down_mod_start(ipd_up_down_test_t test, uint32_t instance_index,
                                 ipd_up_down_scheduler_t* sch, void (* check_function)(int, void*),
                                 amxc_var_t* mod_args, amxc_var_t* mod_ret) {
    const cstring_t function_check = ipd_up_down_function(test, IPD_UP_DOWN_TEST_CHECK);
    const cstring_t function_start = ipd_up_down_function(test, IPD_UP_DOWN_TEST_START);
    const cstring_t function_stop = ipd_up_down_function(test, IPD_UP_DOWN_TEST_STOP);
    amxd_object_t* ipd_object = NULL;
    amxc_var_t* fd_var;
    amxc_var_t* status_var = NULL;
    cstring_t status_str;
    int int_sch_index;
    int rv = -1;
    int32_t fd = -1;

    amxc_var_new(&status_var);
    amxc_var_set(cstring_t, status_var, UP_DOWN_STATUS_ERR_OTHER);

    ipd_object = ipd_get_root_object();

    rv = cfgctrlr_ipdiagnostics_execute_function(ipd_object, function_start, mod_args, mod_ret);
    fd_var = GETP_ARG(mod_ret, UP_DOWN_RET_KEY_PROC "." UP_DOWN_RET_KEY_PROC_FD_STDOUT);
    if((rv != 0) || (fd_var == NULL)) {
        SAH_TRACEZ_ERROR(ME, "Failed to execute function %s.", function_start);
        amxc_var_set_pathf(mod_ret, status_var, DEFAULT_SET_PATH_FLAGS, UP_DOWN_RET_KEY_STATUS);
        goto exit;
    }

    fd = GET_INT32(fd_var, NULL);
    if(fd < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to execute function %s.", function_start);
        amxc_var_set_pathf(mod_ret, status_var, DEFAULT_SET_PATH_FLAGS, UP_DOWN_RET_KEY_STATUS);
        goto exit;
    }

    //Check and parse the test status on module
#ifndef IP_DIAGNOSTICS_UNIT_TEST
    amxc_var_move(mod_args, mod_ret);
    cfgctrlr_ipdiagnostics_execute_function(ipd_object, function_check, mod_args, mod_ret);
#else
    (void) function_check;
#endif
    status_str = (cstring_t) GET_CHAR(mod_ret, UP_DOWN_RET_KEY_STATUS);
    if(strcmp(status_str, UP_DOWN_STATUS_NOT_COMPLETE) == 0) {

        //Allocate the file descriptor and instance index in the internal scheduler
        int_sch_index = ipd_up_down_scheduler_alloc(fd, instance_index, test, sch);
        if((int_sch_index < 0) || (int_sch_index >= IPD_MAX_UP_DOWN_RESULTS)) {
            SAH_TRACEZ_ERROR(ME, "Failed to schedule the test internally.");
            //Stop the test on module
            amxc_var_move(mod_args, mod_ret);
            cfgctrlr_ipdiagnostics_execute_function(ipd_object, function_stop, mod_args, mod_ret);
            goto exit;
        }
        amxo_connection_add(ipd_get_parser(), fd, check_function, NULL, AMXO_CUSTOM, &(sch[int_sch_index].instance_index));
    }

    rv = 0;
exit:
    amxc_var_delete(&status_var);
    return rv;
}

/**
   @brief
   Filter ret variant.

   @param[in] ret Composite variant with the data to be filtered.
 */
static void ipd_up_down_filter_ret(amxc_var_t* ret) {
    amxc_var_t* ret_proc = amxc_var_get_key(ret, UP_DOWN_RET_KEY_PROC, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_t* ret_cfg = amxc_var_get_key(ret, UP_DOWN_RET_KEY_CFG, AMXC_VAR_FLAG_DEFAULT);
    if(ret_proc != NULL) {
        amxc_var_delete(&ret_proc);
    }
    if(ret_cfg != NULL) {
        amxc_var_delete(&ret_cfg);
    }
}

/**
   @brief
   Start function.

   @param[in] test download or upload.
   @param[in] sch Scheduler.
   @param[in] check_function pointer to the check function.
   @param[in] args Input arguments.
   @param[out] ret Output variant.

   @return
   amxd_status_t
 */
amxd_status_t ipd_up_down_start(ipd_up_down_test_t test, ipd_up_down_scheduler_t* sch,
                                void (* check_function)(int, void*),
                                amxc_var_t* args, amxc_var_t* ret) {
    const cstring_t key_result = ipd_up_down_result(test);
    amxd_status_t ret_status = amxd_status_object_not_found;
    amxd_object_t* ipd_object = NULL;
    amxd_object_t* ipd_results = NULL;
    amxc_var_t* mod_args = NULL;
    amxc_var_t* mod_args_cfg = NULL;
    amxc_var_t* mod_ret = NULL;
    amxc_var_t* aux_var = NULL;
    cstring_t prefix;
    int ret_val = -1;
    uint32_t result_instance_index;

    amxc_var_new(&mod_args);
    amxc_var_set_type(mod_args, AMXC_VAR_ID_HTABLE);
    amxc_var_new(&mod_ret);
    amxc_var_set_type(mod_ret, AMXC_VAR_ID_HTABLE);
    amxc_var_new(&aux_var);

    ipd_object = ipd_get_root_object();

    prefix = ipd_get_prefix();
    ipd_results = amxd_object_findf(ipd_object, "%s%s.", prefix, key_result);
    when_null_trace(ipd_results, exit, ERROR, "Failed to fetch object IPDiagnostics.%s%s. from datamodel.", prefix, key_result);

    //Validate input arguments
    ret_status = amxd_status_invalid_arg;
    ret_val = ipd_up_down_validate_input_args(args, test, mod_args);
    when_failed(ret_val, exit);

    ret_status = amxd_status_unknown_error;
    //Get the instance number and allocate the result instance on DM
    result_instance_index = ipd_up_down_alloc_index(ipd_results);
    when_false_trace(result_instance_index, exit, ERROR, "Failed to get a result index.");
    mod_args_cfg = amxc_var_get_key(mod_args, UP_DOWN_RET_KEY_CFG, AMXC_VAR_FLAG_DEFAULT);
    amxc_var_add_key(uint32_t, mod_args_cfg, UP_DOWN_CFG_KEY_RESULT_NUMBER, result_instance_index);

    //Save the ROM time.
    ret_val = ipd_get_current_time(aux_var);
    when_failed_trace(ret_val, exit, ERROR, "Failed to get the current time.");
    when_null_trace(aux_var, exit, ERROR, "Failed to get the ROM time.");
    amxc_var_set_pathf(mod_args, aux_var, DEFAULT_SET_PATH_FLAGS, UP_DOWN_RET_KEY_ROM_TIME);
    amxc_var_add_key(cstring_t, mod_args, UP_DOWN_RET_KEY_STATUS, UP_DOWN_STATUS_ERR_OTHER);

    //Start test on module
    ret_status = amxd_status_ok;
    ret_val = ipd_up_down_mod_start(test, result_instance_index, sch, check_function, mod_args, mod_ret);
    when_failed(ret_val, save);

save:
    ipd_up_down_save_result_to_dm(mod_ret, result_instance_index, ipd_results, test);
    ipd_up_down_filter_ret(mod_ret);
exit:
    amxc_var_move(ret, mod_ret);
    amxc_var_delete(&mod_args);
    amxc_var_delete(&mod_ret);
    amxc_var_delete(&aux_var);
    return ret_status;
}