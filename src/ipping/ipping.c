/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_function.h>
#include <amxd/amxd_object.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_diagnostics_priv.h"
#include "ip_diagnostics.h"
#include "priv_ipping.h"
#include "ipping.h"

#define ME "ipd-ipping"

/**********************************************************
* Macro definitions
**********************************************************/

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Variable declarations
**********************************************************/

static ipd_ipping_scheduler_t scheduler = {.fd_stdout = 0,
    .fd_stderr = 0,
    .pid = 0,
    .call_id = 0,
    .prev_test = false};

/**********************************************************
* Functions
**********************************************************/
void ipd_ipping_check(int fd, void* priv) {
    ipd_ipping_sched_check(fd, priv, &scheduler);
}

amxd_status_t _IPPing(UNUSED amxd_object_t* object, amxd_function_t* func, amxc_var_t* args, amxc_var_t* ret) {
    amxd_status_t ret_status;
    ret_status = ipd_ipping_start(ipd_ipping_check, &scheduler, args, ret);
    if(ret_status == amxd_status_deferred) {
        amxd_function_defer(func, &(scheduler.call_id), ret, NULL, NULL);
    }
    return ret_status;
}

void _ipd_ipping_check_dst(UNUSED const char* const event_name,
                           const amxc_var_t* const event_data,
                           UNUSED void* const priv) {
    amxd_status_t ret_status = amxd_status_ok;
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* ipd_ping_object = NULL;
    const cstring_t dst = GETP_CHAR(event_data, "parameters.DiagnosticsState.to");

    amxc_var_init(&args);
    amxc_var_init(&ret);

    ipd_ping_object = ipd_get_ipping_object();
    when_null_trace(ipd_ping_object, exit, ERROR, "Failed to fetch object IPDiagnostics.IPPing. from datamodel.");
    ipd_obj_data_to_var(ipd_ping_object, NULL, &args);

    if(strcmp(dst, IPPING_STATUS_CANCELED) == 0) {
        ipd_ipping_stop(&scheduler);
    } else if(strcmp(dst, IPPING_STATUS_REQUESTED) == 0) {
        ipd_filter_ret(&args, false, IPPING);
        ret_status = ipd_ipping_start(ipd_ipping_check, &scheduler, &args, &ret);
        when_false_trace(ret_status == amxd_status_deferred, exit, ERROR, "Could not launch the ping test with %d error code", ret_status);
    }

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    return;
}
