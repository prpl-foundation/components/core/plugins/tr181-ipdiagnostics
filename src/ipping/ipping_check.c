/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_diagnostics_priv.h"
#include "ip_diagnostics.h"
#include "ipping.h"
#include "priv_ipping.h"
#include "config.h"

#define ME "ipd-ipping-c"

/**
 * @brief
 * Function that saves the result of a test to the datamodel of the ipping object
 *
 * @param ret Result to save
 * @param ipd_ipping Object to save the result in
 * @return int
 */
amxd_status_t ipd_ipping_save_result_to_dm(amxc_var_t* ret, amxd_object_t* ipd_ipping) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* proc = NULL;
    amxd_trans_t* transaction = NULL;
    amxc_llist_t* filter_list = NULL;
    amxd_status_t ret_status = amxd_status_unknown_error;

    amxc_llist_new(&filter_list);
    amxc_llist_add_string(filter_list, IPPING_RET_KEY_PROC);
    amxc_llist_add_string(filter_list, IPPING_RET_KEY_DST);
    amxc_llist_add_string(filter_list, "ConnectionAdded");

    proc = GET_ARG(ret, IPPING_RET_KEY_PROC);

    transaction = ipd_transaction_create(ipd_ipping);
    when_null_trace(transaction, exit, ERROR, "Could not create transaction");

    //Add results
    ipd_var_to_dm(transaction, NULL, ret, filter_list);
    //Add proc
    ipd_var_to_dm(transaction, IPPING_RET_KEY_PROC, proc, filter_list);

    //apply transaction
    ret_status = ipd_transaction_apply(transaction);
    when_failed(ret_status, exit);
    ret_status = amxd_status_ok;

exit:
    amxc_llist_delete(&filter_list, amxc_string_list_it_free);
    SAH_TRACEZ_OUT(ME);
    return ret_status;
}

/**
 * @brief
 *  Function that only saves the value of "DiagnosticState" to the dm
 *
 * @param ret Return variant containing the status to save to the DM
 * @param ipd_ipping The object containing the dm
 * @return amxd_status_t
 */
amxd_status_t ipd_ipping_save_status_to_dm(amxc_var_t* ret, amxd_object_t* ipd_ipping) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t ret_status = amxd_status_unknown_error;
    amxd_trans_t* transaction = NULL;

    transaction = ipd_transaction_create(ipd_ipping);

    amxd_trans_set_param(transaction, IPPING_RET_KEY_DST, GET_ARG(ret, IPPING_RET_KEY_PST));

    //apply transaction
    ret_status = ipd_transaction_apply(transaction);
    when_failed_trace(ret_status, exit, ERROR, "Could not update %s with %s", IPPING_RET_KEY_DST, GET_CHAR(ret, IPPING_RET_KEY_PST));
    ret_status = amxd_status_ok;

exit:
    SAH_TRACEZ_OUT(ME);
    return ret_status;
}

/**
 * @brief
 * Function that checks for a previous test and updates the status in the dm
 *
 * @param fd File descriptor of the test
 * @param priv private data that you want to pass to the function
 */
void ipd_ipping_sched_check(int fd, UNUSED void* priv, ipd_ipping_scheduler_t* sch) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* mod_ret = NULL;
    amxd_object_t* ipd_ping_object = NULL;
    amxc_var_t* params = NULL;
    const cstring_t status_str = NULL;
    int status = -1;
    bool abort_flag = true;

    amxc_var_new(&mod_ret);
    amxc_var_new(&params);
    amxc_var_set_type(mod_ret, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(params, AMXC_VAR_ID_HTABLE);

    ipd_ping_object = ipd_get_ipping_object();
    when_null_trace(ipd_ping_object, exit, ERROR, "Failed to fetch object IPDiagnostics.IPPing. from datamodel.");
    ipd_obj_data_to_var(ipd_ping_object, NULL, params);
    when_null_trace(params, exit, ERROR, "Failed to get the parameters to run the check.");

    abort_flag = false;

exit:
    if(abort_flag) {
        SAH_TRACEZ_ERROR(ME, "Aborting the ongoing ping test.");
        if(fd > 0) {
            amxo_connection_remove(ipd_get_parser(), fd);
        }
    } else {

        status = cfgctrlr_ipdiagnostics_execute_function(ipd_ping_object, IPD_IPPING_TEST_CHECK, params, mod_ret);

        if(!sch->prev_test) {
            ipd_ipping_save_result_to_dm(mod_ret, ipd_ping_object);
        }
        status_str = GET_CHAR(mod_ret, IPPING_RET_KEY_PST);
        if(!str_empty(status_str) && ((status < ipping_test_not_complete) || (strcmp(status_str, IPPING_STATUS_NOT_COMPLETE) != 0))) {
            amxo_connection_remove(ipd_get_parser(), fd);
            sch->fd_stderr = 0;
            sch->fd_stdout = 0;
            sch->pid = 0;
            if(!sch->prev_test) {
                amxc_var_set(cstring_t, GET_ARG(mod_ret, IPPING_RET_KEY_DST), GET_CHAR(mod_ret, IPPING_RET_KEY_PST));
                ipd_ipping_save_status_to_dm(mod_ret, ipd_ping_object);
                ipd_filter_ret(mod_ret, true, IPPING);
                amxd_function_deferred_done(sch->call_id, amxd_status_ok, NULL, mod_ret);
            }
        }
    }
    amxc_var_delete(&params);
    amxc_var_delete(&mod_ret);
    SAH_TRACEZ_OUT(ME);
}
