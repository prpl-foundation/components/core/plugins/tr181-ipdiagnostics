/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/time.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_function.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_action.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_diagnostics_priv.h"
#include "ip_diagnostics.h"
#include "priv_traceroute.h"
#include "traceroute.h"

#define ME "ipd-trcr"

/**********************************************************
* Variable declarations
**********************************************************/

static ipd_trcr_scheduler_t scheduler = {.fd_stdout = 0,
    .fd_stderr = 0,
    .pid = 0,
    .call_id = 0};

/**********************************************************
* Functions
**********************************************************/
void ipd_trcr_check(int fd, void* priv) {
    ipd_trcr_sched_check(fd, priv, &scheduler);
}

amxd_status_t _TraceRoute(UNUSED amxd_object_t* object, amxd_function_t* func, amxc_var_t* args, amxc_var_t* ret) {
    amxd_status_t ret_status;
    ret_status = ipd_trcr_start(ipd_trcr_check, &scheduler, args, ret);
    if(ret_status == amxd_status_deferred) {
        amxd_function_defer(func, &(scheduler.call_id), ret, NULL, NULL);
    }
    return ret_status;
}

void _ipd_trcr_check_dst(UNUSED const char* const event_name,
                         const amxc_var_t* const event_data,
                         UNUSED void* const priv) {
    amxd_status_t ret_status = amxd_status_ok;
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* ipd_trcr_object = NULL;
    const cstring_t dst = GETP_CHAR(event_data, "parameters.DiagnosticsState.to");

    amxc_var_init(&args);
    amxc_var_init(&ret);

    ipd_trcr_object = ipd_get_trcr_object();
    when_null_trace(ipd_trcr_object, exit, ERROR, "Failed to fetch object IPDiagnostics.TraceRoute. from datamodel.");
    ipd_obj_data_to_var(ipd_trcr_object, NULL, &args);

    if(strcmp(dst, TRCR_STATUS_CANCELED) == 0) {
        ipd_trcr_stop(&scheduler);
    } else if(strcmp(dst, TRCR_STATUS_REQUESTED) == 0) {
        ipd_filter_ret(&args, false, TRCR);
        ret_status = ipd_trcr_start(ipd_trcr_check, &scheduler, &args, &ret);
        when_false_trace(ret_status == amxd_status_deferred, exit, ERROR, "Could not launch the traceroute test with %d error code", ret_status);
    }

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    return;
}

amxd_status_t _set_traceroute_minimum_data_size(amxd_object_t* object,
                                                UNUSED amxd_param_t* param,
                                                UNUSED amxd_action_t reason,
                                                amxc_var_t* const args,
                                                UNUSED amxc_var_t* const retval,
                                                UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    uint32_t data_size = 0;
    const char* ip_version = NULL;
    uint32_t expected_size = 30 + (2 * sizeof(time_t));

    when_true_status(reason != action_param_write, exit, status = amxd_status_function_not_implemented);
    when_null_trace(object, exit, ERROR, "Cannot get TraceRoute object");

    ip_version = amxd_object_get_cstring_t(object, "ProtocolVersion", NULL);
    when_str_empty_trace(ip_version, exit, ERROR, "No ip version given");

    data_size = GET_UINT32(args, NULL);
    when_false_trace(data_size > 0, exit, ERROR, "Cannot get the data size for validation of minimum");

    if(data_size < expected_size) {
        amxc_var_set_uint32_t(args, expected_size);
        SAH_TRACEZ_ERROR(ME, "setting default value of data block size to %u", expected_size);
    }
    if((data_size < ( expected_size + 26 )) && (strcmp(ip_version, "IPv6") == 0)) {
        amxc_var_set_uint32_t(args, expected_size + 26);
        SAH_TRACEZ_ERROR(ME, "setting default value of data block size to %u as ipv6 protocol is chosen", expected_size + 26);
    }

    status = amxd_action_object_write(object, param, reason, args, retval, priv);
exit:
    return status;
}

void _set_traceroute_minimum_data_size_validation(UNUSED const char* const event_name,
                                                  UNUSED const amxc_var_t* const event_data,
                                                  UNUSED void* const priv) {
    uint64_t data_size = 0;
    amxd_trans_t trans;
    const char* ip_version = NULL;
    amxd_object_t* tracerout_obj = NULL;
    uint32_t expected_size = 30 + (2 * sizeof(time_t));

    amxd_trans_init(&trans);

    tracerout_obj = ipd_get_trcr_object();
    when_null_trace(tracerout_obj, exit, ERROR, "Cannot get TraceRoute object");

    amxd_trans_select_object(&trans, tracerout_obj);

    data_size = amxd_object_get_uint32_t(tracerout_obj, "DataBlockSize", NULL);

    ip_version = GETP_CHAR(event_data, "parameters.ProtocolVersion.to");
    when_str_empty_trace(ip_version, exit, ERROR, "Cannot get the ip version for validation of minimum data block size");

    if(data_size < expected_size) {
        amxd_trans_set_uint32_t(&trans, "DataBlockSize", expected_size);
        SAH_TRACEZ_ERROR(ME, "setting default value of data block size to %u", expected_size);
    }
    if((data_size < ( expected_size + 26 )) && (strcmp(ip_version, "IPv6") == 0)) {
        amxd_trans_set_uint32_t(&trans, "DataBlockSize", ( expected_size + 26 ));
        SAH_TRACEZ_ERROR(ME, "setting default value of data block size to %u as ipv6 protocol is chosen", expected_size + 26);
    }
    when_failed_trace(amxd_trans_apply(&trans, ipd_get_dm()), exit, ERROR, "Unable to change the data block size in the DM");
exit:
    amxd_trans_clean(&trans);
    return;
}