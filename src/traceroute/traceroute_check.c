/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_diagnostics_priv.h"
#include "ip_diagnostics.h"
#include "traceroute.h"
#include "priv_traceroute.h"
#include "config.h"

#define ME "ipd-trcr-c"

/**
 * @brief
 * Function that deletes the previous hops in the datamodel and then
 * sets the new ones
 *
 * @param transaction Transaction parameter to the datamodel
 * @param values variant with the hops
 * @return int
 */
static int ipd_trcr_save_hops(amxc_var_t* values,
                              amxd_object_t* trcr_obj,
                              const cstring_t status) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    amxd_status_t ret_status = amxd_status_unknown_error;
    amxd_object_t* hops_obj = NULL;
    amxc_string_t aux_string;
    amxd_trans_t* transaction = NULL;

    // Delete the previous hops from the datamodel
    hops_obj = amxd_object_findf(ipd_get_trcr_object(), ".%s.", TRCR_RET_KEY_HOPS);
    when_null_trace(hops_obj, exit, ERROR, "Could not get the RouteHops object from dm");
    amxd_object_for_each(instance, it, hops_obj) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        transaction = ipd_transaction_create(trcr_obj);
        amxd_trans_select_pathf(transaction, ".%s.", TRCR_RET_KEY_HOPS);
        ret_status = amxd_trans_del_inst(transaction, instance->index, NULL);
        when_failed(ret_status, exit);
        ipd_transaction_apply(transaction);
    }
    // Add each hop to the datamodel
    if(str_empty(status) || ((strcmp(status, TRCR_STATUS_COMPLETE) != 0) && (strcmp(status, TRCR_STATUS_ERR_MAX_HOP_COUNT) != 0))) {
        goto exit;
    }
    amxc_var_for_each(the_hop, values) {
        const char* name = amxc_var_key(the_hop);
        // Add a new instance
        transaction = ipd_transaction_create(trcr_obj);
        amxd_trans_select_pathf(transaction, ".%s.", TRCR_RET_KEY_HOPS);

        amxc_string_init(&aux_string, 0);

        amxc_string_setf(&aux_string, "hop_%s", name);

        ret_status = amxd_trans_add_inst(transaction, atoi(name), amxc_string_get(&aux_string, 0));
        when_failed_trace(ret_status, exit, ERROR, "Could not add an instance in the transaction");

        ipd_transaction_apply(transaction);

        // Puts the hop values inside
        transaction = ipd_transaction_create(trcr_obj);
        amxd_trans_select_pathf(transaction, ".%s.", TRCR_RET_KEY_HOPS);

        ipd_var_to_dm(transaction, amxc_string_get(&aux_string, 0), the_hop, NULL);
        amxc_string_clean(&aux_string);
        ipd_transaction_apply(transaction);
    }
    rv = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief
 * Function that saves the result of a test to the datamodel of the traceroute  object
 *
 * @param ret Result to save
 * @param ipd_trcr Object to save the result in
 * @return int
 */
int ipd_trcr_save_result_to_dm(amxc_var_t* ret, amxd_object_t* ipd_trcr) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* proc = NULL;
    amxc_var_t* hops = NULL;
    amxd_trans_t* transaction = NULL;
    amxc_llist_t* filter_list = NULL;
    amxd_status_t ret_status = amxd_status_unknown_error;
    const cstring_t status = GET_CHAR(ret, TRCR_RET_KEY_DST);
    int rv = 0;

    amxc_llist_new(&filter_list);
    amxc_llist_add_string(filter_list, TRCR_RET_KEY_PROC);

    proc = GET_ARG(ret, TRCR_RET_KEY_PROC);
    hops = GET_ARG(ret, TRCR_RET_KEY_HOPS);

    transaction = ipd_transaction_create(ipd_trcr);
    when_null_trace(transaction, exit, ERROR, "Could not create transaction");

    //Add results
    ipd_var_to_dm(transaction, NULL, ret, filter_list);
    ret_status = ipd_transaction_apply(transaction);
    when_failed(ret_status, exit);
    transaction = ipd_transaction_create(ipd_trcr);
    when_null_trace(transaction, exit, ERROR, "Could not create transaction");

    //Add proc
    ipd_var_to_dm(transaction, TRCR_RET_KEY_PROC, proc, filter_list);
    ret_status = ipd_transaction_apply(transaction);
    when_failed(ret_status, exit);

    //Add route hops
    rv = ipd_trcr_save_hops(hops, ipd_trcr, status);
    when_failed(rv, exit);

    ret_status = amxd_status_ok;

exit:
    amxc_llist_delete(&filter_list, amxc_string_list_it_free);
    SAH_TRACEZ_OUT(ME);
    return ret_status;
}

/**
 * @brief
 * Function that checks for a previous test and updates the status in the dm
 *
 * @param fd File descriptor of the test
 * @param priv private data that you want to pass to the function
 */
void ipd_trcr_sched_check(int fd, UNUSED void* priv, ipd_trcr_scheduler_t* sch) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* mod_ret = NULL;
    amxd_object_t* ipd_trcr_object = NULL;
    amxc_var_t* params = NULL;
    bool abort_flag = true;

    amxc_var_new(&mod_ret);
    amxc_var_new(&params);
    amxc_var_set_type(mod_ret, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(params, AMXC_VAR_ID_HTABLE);

    ipd_trcr_object = ipd_get_trcr_object();
    when_null_trace(ipd_trcr_object, exit, ERROR, "Failed to fetch object IPDiagnostics.TraceRoute. from datamodel.");
    ipd_obj_data_to_var(ipd_trcr_object, NULL, params);
    when_null_trace(params, exit, ERROR, "Failed to get the parameters to run the check.");

    abort_flag = false;

exit:
    if(abort_flag) {
        SAH_TRACEZ_ERROR(ME, "Aborting the ongoing trcr test.");
        if(fd > 0) {
            amxo_connection_remove(ipd_get_parser(), fd);
        }
    } else {
        const cstring_t status_str = NULL;
        int status = -1;

        status = cfgctrlr_ipdiagnostics_execute_function(ipd_trcr_object, IPD_TRCR_TEST_CHECK, params, mod_ret);

        ipd_trcr_save_result_to_dm(mod_ret, ipd_trcr_object);
        status_str = GET_CHAR(mod_ret, TRCR_RET_KEY_DST);
        if((status < trcr_test_not_complete) || (strcmp(status_str, TRCR_STATUS_NOT_COMPLETE) != 0)) {
            amxo_connection_remove(ipd_get_parser(), fd);
            sch->fd_stderr = 0;
            sch->fd_stdout = 0;
            sch->pid = 0;
            ipd_filter_ret(mod_ret, true, TRCR);
            amxd_function_deferred_done(sch->call_id, amxd_status_ok, NULL, mod_ret);
        }
    }
    amxc_var_delete(&params);
    amxc_var_delete(&mod_ret);
    SAH_TRACEZ_OUT(ME);
}