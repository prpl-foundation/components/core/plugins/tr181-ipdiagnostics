/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_diagnostics_priv.h"
#include "ip_diagnostics.h"
#include "traceroute.h"
#include "priv_traceroute.h"
#include "config.h"

#define ME "ipd-trcr-s"

/**
 * @brief
 * Start the ping test on the module and links a check_function to the process
 *
 * @param check_function Check function triggered when output is available
 * @param mod_args Input args of the test
 * @param mod_ret Output args of the test
 * @return int
 */
static int ipd_trcr_mod_start(ipd_trcr_scheduler_t* sch,
                              void (* check_function)(int, void*),
                              amxc_var_t* mod_args,
                              amxc_var_t* mod_ret) {
    amxd_object_t* ipd_trcr_object = NULL;
    amxc_var_t* fd_var = NULL;
    amxc_var_t* fd_err_var = NULL;
    amxc_var_t* pid_var = NULL;
    amxc_var_t* status_var = NULL;
    const cstring_t status_str = NULL;
    int rv = -1;
    int32_t fd = -1;
    int32_t fd_err = -1;
    int32_t pid = -1;

    amxc_var_new(&status_var);
    amxc_var_set(cstring_t, status_var, TRCR_STATUS_ERR_OTHER);

    ipd_trcr_object = ipd_get_trcr_object();

    //First check if a TraceRoute test is already running
    //Fetch the fd from the datamodel
    fd = ipd_tool_get_dm_fd(TRCR);
    check_function(fd, NULL);
    if(sch->fd_stdout > 0) {
        ipd_trcr_stop(sch);
    }

    rv = cfgctrlr_ipdiagnostics_execute_function(ipd_trcr_object, IPD_TRCR_TEST_START, mod_args, mod_ret);

    fd_var = GETP_ARG(mod_ret, TRCR_RET_KEY_PROC "." TRCR_RET_KEY_PROC_FD_STDOUT);
    fd_err_var = GETP_ARG(mod_ret, TRCR_RET_KEY_PROC "." TRCR_RET_KEY_PROC_FD_STDERR);
    pid_var = GETP_ARG(mod_ret, TRCR_RET_KEY_PROC "."TRCR_RET_KEY_PROC_PID);
    if((rv != 0) || (fd_var == NULL)) {
        SAH_TRACEZ_ERROR(ME, "Failed to execute function %s.", IPD_TRCR_TEST_START);
        amxc_var_set_pathf(mod_ret, status_var, DEFAULT_SET_PATH_FLAGS, TRCR_RET_KEY_DST);
        goto exit;
    }
    fd = GET_INT32(fd_var, NULL);
    fd_err = GET_INT32(fd_err_var, NULL);
    pid = GET_INT32(pid_var, NULL);
    if(fd <= 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to execute function %s.", IPD_TRCR_TEST_START);
        amxc_var_set_pathf(mod_ret, status_var, DEFAULT_SET_PATH_FLAGS, TRCR_RET_KEY_DST);
        goto exit;
    }

    amxc_var_move(mod_args, mod_ret);
    cfgctrlr_ipdiagnostics_execute_function(ipd_trcr_object, IPD_TRCR_TEST_CHECK, mod_args, mod_ret);

    status_str = GET_CHAR(mod_ret, TRCR_RET_KEY_DST);
    if(!str_empty(status_str) && (strcmp(status_str, TRCR_STATUS_NOT_COMPLETE) == 0)) {
        sch->fd_stderr = fd_err;
        sch->fd_stdout = fd;
        sch->pid = pid;
        amxo_connection_add(ipd_get_parser(), fd, check_function, NULL, AMXO_CUSTOM, NULL);
    } else {
        ipd_trcr_save_result_to_dm(mod_ret, ipd_trcr_object);
        ipd_filter_ret(mod_ret, true, TRCR);
        amxd_function_deferred_done(sch->call_id, amxd_status_ok, NULL, mod_ret);
    }
    rv = 0;
exit:
    amxc_var_delete(&status_var);
    return rv;
}

/**
 * @brief
 * Checks each input parameters and validates it with the requirements
 * of the datamodel
 *
 * @param args
 * @param mod_args
 * @return int
 */
static int ipd_trcr_validate_input_args(amxc_var_t* args,
                                        amxc_var_t* mod_args) {
    SAH_TRACEZ_IN(ME);
    const cstring_t key_result = IPD_TRCR_CONFIG;
    int rv = -1;
    amxd_object_t* ipd_object = NULL;
    amxd_object_t* ipd_config = NULL;

    ipd_object = ipd_get_root_object();
    ipd_config = amxd_object_findf(ipd_object, "%s.", key_result);
    when_null_trace(ipd_config, exit, ERROR, "Failed to fetch object IPDiagnostics.%s. from datamodel.", key_result);

    rv = ipd_validate_cfg_params(args, ipd_config, mod_args, TRCR);
    when_failed_trace(rv, exit, ERROR, "Failed to validate the parameters.");

    //Sets the non mendatory parameters to default value
    rv = ipd_set_non_mandatory_def_value(mod_args, IPD_TRCR_CONFIG);
    when_failed_trace(rv, exit, ERROR, "Problem while setting default parameters");

    // If the interface is set, find the linux interface linked to it
    rv = ipd_resolve_ip_interface(mod_args);
    when_true_trace(rv == amxd_status_unknown_error, exit, ERROR, "Could not resolve the ip interface with error code %d", rv);

    if(rv == amxd_status_invalid_arg) {
        SAH_TRACEZ_WARNING(ME, "No IP interface specified, PING test will use the default routing configuration");
#ifdef IP_DIAGNOSTICS_UNIT_TEST
        rv = amxd_status_ok;
#endif
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

/**
 * @brief
 * Start function of the traceroute test
 *
 * @param check_function function that will be linked to the process to monitor its
 *  state and parse data into the datamodel
 * @param sch Scheduler, structure that stores the file descriptor of the ongoing process
 * @param args Input arguments for the test
 * @param ret Output arguments from the test
 * @return amxd_status_t
 */
amxd_status_t ipd_trcr_start(UNUSED void (* check_function)(int, void*),
                             ipd_trcr_scheduler_t* sch,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t ret_status = amxd_status_unknown_error;
    amxc_var_t mod_args;
    amxc_var_t mod_ret;
    int ret_val = -1;

    amxc_var_init(&mod_args);
    amxc_var_init(&mod_ret);
    amxc_var_set_type(&mod_args, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&mod_ret, AMXC_VAR_ID_HTABLE);

    //Validate input arguments
    ret_status = amxd_status_invalid_arg;
    ret_val = ipd_trcr_validate_input_args(args, &mod_args);
    when_failed(ret_val, exit);

    //Start test on the module
    ret_status = amxd_status_deferred;
    ret_val = ipd_trcr_mod_start(sch, check_function, &mod_args, &mod_ret);

    if(ret_val != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Could not launch the test properly, error: %s", GET_CHAR(&mod_ret, TRCR_RET_KEY_DST));
    }

    ipd_trcr_save_result_to_dm(&mod_ret, ipd_get_trcr_object());

exit:
    amxc_var_move(ret, &mod_ret);
    ipd_filter_ret(ret, true, TRCR);
    amxc_var_clean(&mod_args);
    amxc_var_clean(&mod_ret);
    SAH_TRACEZ_OUT(ME);
    return ret_status;
}