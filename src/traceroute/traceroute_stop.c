/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_function.h>
#include <amxd/amxd_object.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "ip_diagnostics_priv.h"
#include "ip_diagnostics.h"
#include "traceroute.h"
#include "priv_traceroute.h"
#include "config.h"

#define ME "ipd-trcr-stp"

/**
 * @brief
 * Stop function for IP Ping test
 *
 * @param sched scheduler (Verify that the test has finished before launching
 *              another one)
 * @return amxd_status_t
 */
amxd_status_t ipd_trcr_stop(ipd_trcr_scheduler_t* sch) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* ipd_trcr_object = NULL;
    amxc_var_t* params = NULL;
    amxc_var_t* ret = NULL;
    amxc_var_t* proc = NULL;
    const cstring_t status = NULL;
    int32_t fd = -1;
    int32_t rv = -1;

    amxc_var_new(&ret);
    amxc_var_new(&params);
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(params, AMXC_VAR_ID_HTABLE);

    ipd_trcr_object = ipd_get_trcr_object();
    when_null_trace(ipd_trcr_object, exit, ERROR, "Failed to fetch object IPDiagnostics.TraceRoute. from datamodel.");
    ipd_obj_data_to_var(ipd_trcr_object, NULL, params);
    when_null_trace(params, exit, ERROR, "Failed to get the parameters to run the stop.");

    status = GET_CHAR(params, TRCR_RET_KEY_DST);
    when_str_empty(status, exit);

    when_true_trace(sch->fd_stderr == 0, exit, ERROR, "Test not running");
    when_true_trace(sch->fd_stdout == 0, exit, ERROR, "Test not running");
    when_true_trace(sch->pid == 0, exit, ERROR, "Test not running");

    proc = GET_ARG(params, TRCR_RET_KEY_PROC);
    fd = GET_INT32(proc, TRCR_RET_KEY_PROC_FD_STDOUT);
    if(fd != -1) {
        amxo_connection_remove(ipd_get_parser(), fd);
    }
    cfgctrlr_ipdiagnostics_execute_function(ipd_trcr_object, IPD_TRCR_TEST_STOP, params, ret);
    ipd_trcr_save_result_to_dm(ret, ipd_trcr_object);

    sch->fd_stderr = 0;
    sch->fd_stdout = 0;
    sch->pid = 0;
    rv = 0;

exit:
    ipd_filter_ret(ret, true, TRCR);
    amxd_function_deferred_done(sch->call_id, amxd_status_ok, NULL, ret);
    amxc_var_delete(&ret);
    amxc_var_delete(&params);
    SAH_TRACEZ_OUT(ME);
    return rv;
}