#!/bin/bash

set +e
IS_IPERF_RUNNING=$(pgrep iperf3)
if [ ! -z "${IS_IPERF_RUNNING}" -a "${IS_IPERF_RUNNING}" != "" -a "${IS_IPERF_RUNNING}" != " " ]; then
	pkill iperf3
fi

iperf3 -s -D &

IPERF=$(iperf3 --version)
if [ -z "${IPERF}" -o "${IPERF}" == "" -o "${IPERF}" == "" ]; then
	echo 
    echo
    echo "ERROR: Tests will fail because iperf3 must be installed."
    echo 
    echo
    exit 1
else
    echo 
    echo "Running iperf3 server on localhost."
    echo 
    exit 0
fi