#!/bin/bash

set +e
IS_IPERF_RUNNING=$(pgrep iperf3)
if [ ! -z "${IS_IPERF_RUNNING}" -a "${IS_IPERF_RUNNING}" != "" -a "${IS_IPERF_RUNNING}" != " " ]; then
	pkill iperf3
fi
