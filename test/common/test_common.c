/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <regex.h>

#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "upload_download.h"
#include "test_common.h"

void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

void dump_object(amxd_object_t* object, int index) {
    amxd_param_t* param = NULL;
    amxd_object_t* tmp_obj = NULL;
    amxc_var_t* tmp_var = NULL;
    cstring_t obj_name = NULL;
    cstring_t param_name = NULL;
    cstring_t param_value = NULL;
    index++;

    if(object == NULL) {
        printf("The object is NULL.\n");
    } else {
        amxc_llist_for_each(it, (&object->parameters)) {
            param = amxc_llist_it_get_data(it, amxd_param_t, it);
            param_name = (cstring_t) amxd_param_get_name(param);
            tmp_var = (amxc_var_t*) amxd_object_get_param_value(object, param_name);
            param_value = amxc_var_dyncast(cstring_t, tmp_var);
            printf("param_name[%d]: %s = %s\n", index, param_name, param_value);
            free(param_value);
            param = NULL;
        }
        amxc_llist_for_each(it, (&object->objects)) {
            tmp_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
            obj_name = (cstring_t) amxd_object_get_name(tmp_obj, AMXD_OBJECT_INDEXED);
            printf("obj_name[%d]: %s\n", index, obj_name);
            dump_object(tmp_obj, index);
            tmp_obj = NULL;
        }
        amxc_llist_for_each(it, (&object->instances)) {
            tmp_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
            obj_name = (cstring_t) amxd_object_get_name(tmp_obj, AMXD_OBJECT_INDEXED);
            printf("instance_name[%d]: %s\n", index, obj_name);
            dump_object(tmp_obj, index);
            tmp_obj = NULL;
        }
    }
    fflush(stdout);
}

/**
   @brief
   Extract a substring with a regular expression match.

   @param[in] str Source string containing the information to be searched.
   @param[in] regex Regular expression.
   @param[out] extracted_str Substring matched.

   @return
   0 if there is a match with the regex. Not 0 if there isn't a match.
 */
int iperf_extract_regex_from_string(amxc_string_t* str,
                                    const cstring_t regex,
                                    amxc_string_t* extracted_str) {
    regex_t finder;
    size_t match_size;
    cstring_t substr;
    int ret = -1;
    int cflags = REG_EXTENDED;
    regmatch_t pmatch[2] = {{-1, -1}, {-1, -1}};

    when_null(str, exit);
    when_false(str->length > 0, exit);
    when_null(regex, exit);

    ret = regcomp(&finder, regex, cflags);
    when_failed(ret, clean);

    ret = regexec(&finder, amxc_string_get(str, 0), 1, pmatch, 0);
    when_failed(ret, clean);

    match_size = pmatch[0].rm_eo - pmatch[0].rm_so;
    substr = (cstring_t) calloc(1, match_size + 1);
    for(int index = 0; index < (int) match_size; index++) {
        substr[index] = amxc_string_get(str, 0)[index + pmatch[0].rm_so];
    }
    substr[match_size] = '\0';

    amxc_string_set(extracted_str, substr);
    free(substr);

clean:
    regfree(&finder);
exit:
    return ret;
}

/**
   @brief
   Fetch the instance number from the URI.

   @param[in] uri URI.

   @return
   0 when it fails.
 */
uint32_t parse_instance_index_from_uri(cstring_t uri) {
    uint32_t instance_index = 0;
    amxc_string_t string_uri;
    amxc_string_t extracted_string;
    amxc_string_t partial_string;
    int rv;

    amxc_string_init(&string_uri, 0);
    amxc_string_init(&partial_string, 0);
    amxc_string_init(&extracted_string, 0);
    amxc_string_set(&string_uri, uri);

    rv = iperf_extract_regex_from_string(&string_uri, "\\.[0-9]+$", &partial_string);
    when_false((rv == 0), exit);
    rv = iperf_extract_regex_from_string(&partial_string, "[0-9]+", &extracted_string);
    when_false((rv == 0), exit);

    instance_index = atoi(amxc_string_get(&extracted_string, 0));

exit:
    amxc_string_clean(&string_uri);
    amxc_string_clean(&extracted_string);
    amxc_string_clean(&partial_string);
    return instance_index;
}

/**
   @brief
   Read the information from the file and save to a string.

   @param[in] fname file name.
   @param[out] string string containing the information of the file appended.
 */
static void read_from_file(const char* fname, amxc_string_t* string) {
    FILE* p_file = NULL;
    cstring_t buffer = NULL;
    long length;

    p_file = fopen(fname, "rb");

    if(p_file != NULL) {
        fseek(p_file, 0, SEEK_END);
        length = ftell(p_file);
        fseek(p_file, 0, SEEK_SET);
        buffer = calloc(1, length + 1);
        if(buffer == NULL) {
            return;
        }
        fread(buffer, 1, length, p_file);
        fclose(p_file);
    }

    if(buffer != NULL) {
        amxc_string_clean(string);
        amxc_string_init(string, 0);
        amxc_string_append(string, buffer, length);
        free(buffer);
    }
}

/**
   @brief
   Write string to a file.

   @param[in] fname file name.
   @param[in] string string containing the information to be written.
 */
static void write_to_file(const char* fname, amxc_string_t* string) {
    FILE* p_file;

    if(access(fname, F_OK) == 0) {
        remove(fname);
    }

    p_file = fopen(fname, "w");
    fputs(amxc_string_get(string, 0), p_file);
    fclose(p_file);
}

static void delete_file(const cstring_t filename) {
    if(access(filename, F_OK) == 0) {
        remove(filename);
    }
}

/**
   @brief
   Copy the information from an iperf test data.

   @param[in] result_number Result number.
   @param[in] test download or upload.
   @param[out] test_name Test name.
 */
void iperf_copy_result_file(uint32_t result_number, ipd_up_down_test_t test,
                            const cstring_t test_name) {
    amxc_string_t* testdata_file = NULL;
    amxc_string_t* result_file = NULL;
    amxc_string_t* file_content = NULL;

    if(result_number != 0) {
        amxc_string_new(&testdata_file, 0);
        amxc_string_new(&result_file, 0);
        amxc_string_new(&file_content, 0);

        if(test == ipd_up_down_download) {
            amxc_string_setf(result_file, "/tmp/iperf-download-result-%d", result_number);
        } else if(test == ipd_up_down_upload) {
            amxc_string_setf(result_file, "/tmp/iperf-upload-result-%d", result_number);
        }
        amxc_string_setf(testdata_file, "test_data/iperf/%s", test_name);

        read_from_file(amxc_string_get(testdata_file, 0), file_content);
        delete_file(amxc_string_get(result_file, 0));
        write_to_file(amxc_string_get(result_file, 0), file_content);

        amxc_string_delete(&result_file);
        amxc_string_delete(&file_content);
        amxc_string_delete(&testdata_file);
    }
}

/**
   @brief
   Copy the information from an ipping test data.

   @param[in] test_name Test name.
 */
void ipping_copy_result_file(const cstring_t test_name) {
    amxc_string_t* testdata_file = NULL;
    amxc_string_t* result_file = NULL;
    amxc_string_t* file_content = NULL;

    amxc_string_new(&testdata_file, 0);
    amxc_string_new(&result_file, 0);
    amxc_string_new(&file_content, 0);

    amxc_string_set(result_file, "ipping-result");
    amxc_string_setf(testdata_file, "test_data/ping/%s", test_name);

    read_from_file(amxc_string_get(testdata_file, 0), file_content);
    delete_file(amxc_string_get(result_file, 0));
    write_to_file(amxc_string_get(result_file, 0), file_content);

    amxc_string_delete(&result_file);
    amxc_string_delete(&file_content);
    amxc_string_delete(&testdata_file);
}

/**
   @brief
   Copy the information from an traceroute test data.

   @param[in] test_name Test name.
 */
void trcr_copy_result_file(const cstring_t test_name) {
    amxc_string_t* testdata_file = NULL;
    amxc_string_t* result_file = NULL;
    amxc_string_t* file_content = NULL;

    amxc_string_new(&testdata_file, 0);
    amxc_string_new(&result_file, 0);
    amxc_string_new(&file_content, 0);

    amxc_string_set(result_file, "traceroute-result");
    amxc_string_setf(testdata_file, "test_data/traceroute/%s", test_name);

    read_from_file(amxc_string_get(testdata_file, 0), file_content);
    delete_file(amxc_string_get(result_file, 0));
    write_to_file(amxc_string_get(result_file, 0), file_content);

    amxc_string_delete(&result_file);
    amxc_string_delete(&file_content);
    amxc_string_delete(&testdata_file);
}
