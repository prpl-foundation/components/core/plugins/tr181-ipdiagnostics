/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxb/amxb_connect.h>
#include <amxo/amxo.h>

#include "ip_diagnostics.h"
#include "ip_diagnostics_priv.h"
#include "ipping.h"
#include "priv_ipping.h"
#include "traceroute.h"
#include "priv_traceroute.h"
#include "priv_upload_download.h"
#include "rpc.h"
#include "upload_download.h"
#include "upload.h"
#include "download.h"
#include "test_common.h"
#include "test_common_rpc.h"

static void init_variables(amxc_var_t* args, amxc_var_t* ret) {
    amxc_var_clean(args);
    amxc_var_clean(ret);
    amxc_var_init(args);
    amxc_var_init(ret);
}

uint32_t get_num_of_result_from_dm(ipd_up_down_test_t test, amxd_dm_t* dm) {
    amxd_object_t* ipd_object = NULL;
    amxc_var_t* var_num_of_result = NULL;
    amxc_string_t* str_num_result;
    cstring_t prefix = ipd_get_prefix();
    uint32_t num_of_result = 0;

    amxc_string_new(&str_num_result, 0);
    amxc_string_setf(str_num_result, "%s%s", prefix, ipd_up_down_num_result(test));
    ipd_object = amxd_dm_findf(dm, "IPDiagnostics.");
    var_num_of_result = (amxc_var_t*) amxd_object_get_param_value(ipd_object, amxc_string_get(str_num_result, 0));
    num_of_result = (uint32_t) GET_UINT32(var_num_of_result, NULL);
    amxc_string_delete(&str_num_result);
    return num_of_result;
}

static amxd_object_t* get_test_object_from_dm(uint32_t instance_index, ipd_up_down_test_t test, amxd_dm_t* dm) {
    cstring_t prefix = ipd_get_prefix();
    amxd_object_t* ipd_results = amxd_dm_findf(dm, "IPDiagnostics.%s%s", prefix, ipd_up_down_result(test));
    return amxd_object_get_instance(ipd_results, NULL, instance_index);
}

uint32_t size_of_results(ipd_up_down_test_t test, amxd_dm_t* dm) {
    cstring_t prefix = ipd_get_prefix();
    amxd_object_t* ipd_results = amxd_dm_findf(dm, "IPDiagnostics.%s%s.", prefix, ipd_up_down_result(test));
    uint32_t size_ipd_results = (uint32_t) amxc_llist_size(&(ipd_results->instances));
    return size_ipd_results;
}

cstring_t get_test_status_from_dm(uint32_t instance_index, ipd_up_down_test_t test, amxd_dm_t* dm) {
    amxd_object_t* ipd_result = get_test_object_from_dm(instance_index, test, dm);
    amxc_var_t* cfg_param = (amxc_var_t*) amxd_object_get_param_value(ipd_result, UP_DOWN_RET_KEY_STATUS);
    cstring_t status = (cstring_t) GET_CHAR(cfg_param, NULL);
    return status;
}

cstring_t get_ping_test_status_from_dm(amxd_dm_t* dm) {
    amxd_object_t* ipd_ipping_object = amxd_dm_findf(dm, "IPDiagnostics.IPPing.");
    amxc_var_t* data = (amxc_var_t*) amxd_object_get_param_value(ipd_ipping_object, IPPING_RET_KEY_PST);
    cstring_t stat = (cstring_t) GET_CHAR(data, NULL);
    return stat;
}

cstring_t get_trcr_test_status_from_dm(amxd_dm_t* dm) {
    amxd_object_t* ipd_trcr_object = amxd_dm_findf(dm, "IPDiagnostics.TraceRoute.");
    amxc_var_t* data = (amxc_var_t*) amxd_object_get_param_value(ipd_trcr_object, TRCR_RET_KEY_DST);
    cstring_t stat = (cstring_t) GET_CHAR(data, NULL);
    return stat;
}

int32_t get_test_fd_from_dm(uint32_t instance_index, ipd_up_down_test_t test, amxd_dm_t* dm) {
    amxd_object_t* ipd_result = get_test_object_from_dm(instance_index, test, dm);
    amxd_object_t* ipd_result_cfg = amxd_object_get(ipd_result, UP_DOWN_RET_KEY_PROC);
    amxc_var_t* cfg_param = (amxc_var_t*) amxd_object_get_param_value(ipd_result_cfg, UP_DOWN_RET_KEY_PROC_FD_STDOUT);
    int32_t fd = (int32_t) GET_INT32(cfg_param, NULL);
    return fd;
}

int32_t get_ping_test_fd_from_dm(amxd_dm_t* dm) {
    amxd_object_t* ipd_ipping_object = amxd_dm_findf(dm, "IPDiagnostics.IPPing.");
    amxc_var_t* data = (amxc_var_t*) amxd_object_get_param_value(ipd_ipping_object, IPPING_RET_KEY_PROC "."IPPING_RET_KEY_PROC_FD_STDOUT);
    int32_t fd = GET_INT32(data, NULL);
    return fd;
}

int32_t get_trcr_test_fd_from_dm(amxd_dm_t* dm) {
    amxd_object_t* ipd_trcr_object = amxd_dm_findf(dm, "IPDiagnostics.TraceRoute.");
    amxc_var_t* data = (amxc_var_t*) amxd_object_get_param_value(ipd_trcr_object, TRCR_RET_KEY_PROC "."TRCR_RET_KEY_PROC_FD_STDOUT);
    int32_t fd = GET_INT32(data, NULL);
    return fd;
}

uint64_t get_test_period_from_dm(uint32_t instance_index, ipd_up_down_test_t test, amxd_dm_t* dm) {
    amxd_object_t* ipd_result = get_test_object_from_dm(instance_index, test, dm);
    amxc_var_t* cfg_param = (amxc_var_t*) amxd_object_get_param_value(ipd_result, UP_DOWN_RET_KEY_PERIOD_FULL);
    return amxc_var_dyncast(uint64_t, cfg_param);
}

uint32_t get_num_incr_result_from_dm(uint32_t instance_index, ipd_up_down_test_t test, amxd_dm_t* dm) {
    amxd_object_t* ipd_result = get_test_object_from_dm(instance_index, test, dm);
    amxd_object_t* ipd_incremental_results = amxd_object_findf(ipd_result, ".%s.", UP_DOWN_RET_KEY_INCR_RES);
    uint32_t size_ipd_incremental_results = (uint32_t) amxc_llist_size(&(ipd_incremental_results->instances));
    return size_ipd_incremental_results;
}

uint32_t get_num_connection_from_dm(uint32_t instance_index, ipd_up_down_test_t test, amxd_dm_t* dm) {
    amxd_object_t* ipd_result = get_test_object_from_dm(instance_index, test, dm);
    amxd_object_t* ipd_per_connection = amxd_object_findf(ipd_result, ".%s.", UP_DOWN_RET_KEY_PER_CONN_RES);
    uint32_t size_ipd_per_connection = (uint32_t) amxc_llist_size(&(ipd_per_connection->instances));
    return size_ipd_per_connection;
}

amxd_status_t ipd_download_diagnostics(amxd_dm_t* dm,
                                       amxc_var_t* args,
                                       amxc_var_t* ret,
                                       cstring_t download_url,
                                       uint8_t* dscp,
                                       uint8_t* eth_priority,
                                       uint16_t* test_duration,
                                       uint16_t* test_interval,
                                       uint8_t* test_offset,
                                       cstring_t protocol_version,
                                       uint32_t* num_of_connections,
                                       bool* en_per_connection) {
    amxd_status_t ret_status = amxd_status_ok;

    amxd_object_t* ipd = amxd_dm_findf(dm, "IPDiagnostics.");
    if(ipd == NULL) {
        ret_status = amxd_status_object_not_found;
        goto exit;
    }

    init_variables(args, ret);

    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, args, UP_DOWN_CFG_KEY_INTERFACE, "TEST");
    if(download_url != NULL) {
        amxc_var_add_key(cstring_t, args, DOWN_CFG_KEY_DOWNLOAD_URL, download_url);
    }
    if(dscp != NULL) {
        amxc_var_add_key(uint8_t, args, UP_DOWN_CFG_KEY_DSCP, *dscp);
    }
    if(eth_priority != NULL) {
        amxc_var_add_key(uint8_t, args, UP_DOWN_CFG_KEY_ETH_PRIORITY, *eth_priority);
    }
    if(test_duration != NULL) {
        amxc_var_add_key(uint16_t, args, UP_DOWN_CFG_KEY_TIME_TEST_DURATION, *test_duration);
    }
    if(test_interval != NULL) {
        amxc_var_add_key(uint16_t, args, UP_DOWN_CFG_KEY_TIME_TEST_MEAS_INT, *test_interval);
    }
    if(test_offset != NULL) {
        amxc_var_add_key(uint8_t, args, UP_DOWN_CFG_KEY_TIME_TEST_MEAS_OFFSET, *test_offset);
    }
    if(protocol_version != NULL) {
        amxc_var_add_key(cstring_t, args, UP_DOWN_CFG_KEY_PROTOCOL_VERSION, protocol_version);
    }
    if(num_of_connections != NULL) {
        amxc_var_add_key(uint32_t, args, UP_DOWN_CFG_KEY_NUM_OF_CONNECTIONS, *num_of_connections);
    }
    if(en_per_connection != NULL) {
        amxc_var_add_key(bool, args, UP_DOWN_CFG_KEY_EN_CONN_RESULTS, *en_per_connection);
    }

    ret_status = amxd_object_invoke_function(ipd, "DownloadDiagnostics", args, ret);
    if(ret_status != amxd_status_ok) {
        goto exit;
    }

exit:
    return ret_status;
}

amxd_status_t ipd_upload_diagnostics(amxd_dm_t* dm,
                                     amxc_var_t* args,
                                     amxc_var_t* ret,
                                     cstring_t upload_url,
                                     uint8_t* dscp,
                                     uint8_t* eth_priority,
                                     uint64_t* file_length,
                                     uint16_t* test_duration,
                                     uint16_t* test_interval,
                                     uint8_t* test_offset,
                                     cstring_t protocol_version,
                                     uint32_t* num_of_connections,
                                     bool* en_per_connection) {
    amxd_status_t ret_status = amxd_status_ok;

    amxd_object_t* ipd = amxd_dm_findf(dm, "IPDiagnostics.");
    if(ipd == NULL) {
        ret_status = amxd_status_object_not_found;
        goto exit;
    }

    init_variables(args, ret);

    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, args, UP_DOWN_CFG_KEY_INTERFACE, "TEST");
    if(upload_url != NULL) {
        amxc_var_add_key(cstring_t, args, UP_CFG_KEY_UPLOAD_URL, upload_url);
    }
    if(dscp != NULL) {
        amxc_var_add_key(uint8_t, args, UP_DOWN_CFG_KEY_DSCP, *dscp);
    }
    if(eth_priority != NULL) {
        amxc_var_add_key(uint8_t, args, UP_DOWN_CFG_KEY_ETH_PRIORITY, *eth_priority);
    }
    if(file_length != NULL) {
        amxc_var_add_key(uint64_t, args, UP_CFG_KEY_FILE_LENGTH, *file_length);
    }
    if(test_duration != NULL) {
        amxc_var_add_key(uint16_t, args, UP_DOWN_CFG_KEY_TIME_TEST_DURATION, *test_duration);
    }
    if(test_interval != NULL) {
        amxc_var_add_key(uint16_t, args, UP_DOWN_CFG_KEY_TIME_TEST_MEAS_INT, *test_interval);
    }
    if(test_offset != NULL) {
        amxc_var_add_key(uint8_t, args, UP_DOWN_CFG_KEY_TIME_TEST_MEAS_OFFSET, *test_offset);
    }
    if(protocol_version != NULL) {
        amxc_var_add_key(cstring_t, args, UP_DOWN_CFG_KEY_PROTOCOL_VERSION, protocol_version);
    }
    if(num_of_connections != NULL) {
        amxc_var_add_key(uint32_t, args, UP_DOWN_CFG_KEY_NUM_OF_CONNECTIONS, *num_of_connections);
    }
    if(en_per_connection != NULL) {
        amxc_var_add_key(bool, args, UP_DOWN_CFG_KEY_EN_CONN_RESULTS, *en_per_connection);
    }

    ret_status = amxd_object_invoke_function(ipd, "UploadDiagnostics", args, ret);
    if(ret_status != amxd_status_ok) {
        goto exit;
    }

exit:
    return ret_status;
}

amxd_status_t ipd_ipping_diagnostics(amxd_dm_t* dm,
                                     amxc_var_t* args,
                                     amxc_var_t* ret,
                                     cstring_t interface,
                                     cstring_t protocolversion,
                                     cstring_t host,
                                     uint32_t* numberofrepetition,
                                     uint32_t* timeout,
                                     uint32_t* datablocksize,
                                     uint8_t* dscp) {
    amxd_status_t ret_status = amxd_status_ok;

    amxd_object_t* ipd = amxd_dm_findf(dm, "IPDiagnostics.");
    if(ipd == NULL) {
        ret_status = amxd_status_object_not_found;
        goto exit;
    }
    init_variables(args, ret);

    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);

    if(!str_empty(interface)) {
        amxc_var_add_key(cstring_t, args, IPPING_RET_KEY_INTF, interface);
    }
    if(!str_empty(protocolversion)) {
        amxc_var_add_key(cstring_t, args, IPPING_RET_KEY_PRTCLV, protocolversion);
    }
    if(!str_empty(host)) {
        amxc_var_add_key(cstring_t, args, IPPING_RET_KEY_HST, host);
    }
    if(numberofrepetition != NULL) {
        amxc_var_add_key(uint32_t, args, IPPING_RET_KEY_NMBRP, *numberofrepetition);
    }
    if(timeout != NULL) {
        amxc_var_add_key(uint32_t, args, IPPING_RET_KEY_TMT, *timeout);
    }
    if(datablocksize != NULL) {
        amxc_var_add_key(uint32_t, args, IPPING_RET_KEY_DTBS, *datablocksize);
    }
    if(dscp != NULL) {
        amxc_var_add_key(uint8_t, args, IPPING_RET_KEY_DSCP, *dscp);
    }

    ret_status = amxd_object_invoke_function(ipd, "IPPing", args, ret);
    if(ret_status != amxd_status_ok) {
        goto exit;
    }

exit:
    return ret_status;
}

amxd_status_t ipd_trcr_diagnostics(amxd_dm_t* dm,
                                   amxc_var_t* args,
                                   amxc_var_t* ret,
                                   cstring_t interface,
                                   cstring_t protocolversion,
                                   cstring_t host,
                                   uint32_t* numberoftries,
                                   uint32_t* timeout,
                                   uint32_t* datablocksize,
                                   uint8_t* dscp,
                                   uint32_t* maxhopcount) {
    amxd_status_t ret_status = amxd_status_ok;

    amxd_object_t* ipd = amxd_dm_findf(dm, "IPDiagnostics.");
    if(ipd == NULL) {
        ret_status = amxd_status_object_not_found;
        goto exit;
    }
    init_variables(args, ret);
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);

    if(!str_empty(interface)) {
        amxc_var_add_key(cstring_t, args, TRCR_RET_KEY_INTF, interface);
    }
    if(!str_empty(protocolversion)) {
        amxc_var_add_key(cstring_t, args, TRCR_RET_KEY_PRTCLV, protocolversion);
    }
    if(!str_empty(host)) {
        amxc_var_add_key(cstring_t, args, TRCR_RET_KEY_HST, host);
    }
    if(numberoftries != NULL) {
        amxc_var_add_key(uint32_t, args, TRCR_RET_KEY_NMBOTR, *numberoftries);
    }
    if(timeout != NULL) {
        amxc_var_add_key(uint32_t, args, TRCR_RET_KEY_TMT, *timeout);
    }
    if(datablocksize != NULL) {
        amxc_var_add_key(uint32_t, args, TRCR_RET_KEY_DTBS, *datablocksize);
    }
    if(dscp != NULL) {
        amxc_var_add_key(uint8_t, args, TRCR_RET_KEY_DSCP, *dscp);
    }
    if(maxhopcount != NULL) {
        amxc_var_add_key(uint32_t, args, TRCR_RET_KEY_MHC, *maxhopcount);
    }
    ret_status = amxd_object_invoke_function(ipd, "TraceRoute", args, ret);
    if(ret_status != amxd_status_ok) {
        goto exit;
    }
exit:
    return ret_status;
}