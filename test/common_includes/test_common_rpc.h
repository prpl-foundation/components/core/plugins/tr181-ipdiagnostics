/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __TEST_COMMON_RPC_H__
#define __TEST_COMMON_RPC_H__

#ifdef __cplusplus
extern "C" {
#endif

uint32_t get_num_of_result_from_dm(ipd_up_down_test_t test, amxd_dm_t* dm);
uint32_t size_of_results(ipd_up_down_test_t test, amxd_dm_t* dm);
cstring_t get_test_status_from_dm(uint32_t instance_index, ipd_up_down_test_t test, amxd_dm_t* dm);
cstring_t get_ping_test_status_from_dm(amxd_dm_t* dm);
cstring_t get_trcr_test_status_from_dm(amxd_dm_t* dm);
int32_t get_test_fd_from_dm(uint32_t instance_index, ipd_up_down_test_t test, amxd_dm_t* dm);
int32_t get_ping_test_fd_from_dm(amxd_dm_t* dm);
int32_t get_trcr_test_fd_from_dm(amxd_dm_t* dm);
uint64_t get_test_period_from_dm(uint32_t instance_index, ipd_up_down_test_t test, amxd_dm_t* dm);
uint32_t get_num_incr_result_from_dm(uint32_t instance_index, ipd_up_down_test_t test, amxd_dm_t* dm);
uint32_t get_num_connection_from_dm(uint32_t instance_index, ipd_up_down_test_t test, amxd_dm_t* dm);
amxd_status_t ipd_download_diagnostics(amxd_dm_t* dm,
                                       amxc_var_t* args,
                                       amxc_var_t* ret,
                                       cstring_t download_url,
                                       uint8_t* dscp,
                                       uint8_t* eth_priority,
                                       uint16_t* test_duration,
                                       uint16_t* test_interval,
                                       uint8_t* test_offset,
                                       cstring_t protocol_version,
                                       uint32_t* num_of_connections,
                                       bool* en_per_connection);
amxd_status_t ipd_upload_diagnostics(amxd_dm_t* dm,
                                     amxc_var_t* args,
                                     amxc_var_t* ret,
                                     cstring_t upload_url,
                                     uint8_t* dscp,
                                     uint8_t* eth_priority,
                                     uint64_t* file_length,
                                     uint16_t* test_duration,
                                     uint16_t* test_interval,
                                     uint8_t* test_offset,
                                     cstring_t protocol_version,
                                     uint32_t* num_of_connections,
                                     bool* en_per_connection);
amxd_status_t ipd_ipping_diagnostics(amxd_dm_t* dm,
                                     amxc_var_t* args,
                                     amxc_var_t* ret,
                                     cstring_t interface,
                                     cstring_t protocolversion,
                                     cstring_t host,
                                     uint32_t* numberofrepetition,
                                     uint32_t* timeout,
                                     uint32_t* datablocksize,
                                     uint8_t* dscp);
amxd_status_t ipd_trcr_diagnostics(amxd_dm_t* dm,
                                   amxc_var_t* args,
                                   amxc_var_t* ret,
                                   cstring_t interface,
                                   cstring_t protocolversion,
                                   cstring_t host,
                                   uint32_t* numberoftries,
                                   uint32_t* timeout,
                                   uint32_t* datablocksize,
                                   uint8_t* dscp,
                                   uint32_t* maxhopcount);

#ifdef __cplusplus
}
#endif

#endif // __TEST_COMMON_RPC_H__