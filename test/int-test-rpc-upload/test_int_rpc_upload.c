/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxb/amxb_connect.h>
#include <amxo/amxo.h>

#include "test_int_rpc_upload.h"
#include "ip_diagnostics.h"
#include "ip_diagnostics_priv.h"
#include "priv_upload_download.h"
#include "rpc.h"
#include "upload_download.h"
#include "upload.h"
#include "dummy_backend.h"
#include "test_common.h"
#include "test_common_rpc.h"

static const char* odl_defs = "../../odl/tr181-ipdiagnostics_definition.odl";
static const char* mock_odl_defs = "../mock_odl/mock.odl";

static ipd_up_down_test_t test_type = ipd_up_down_upload;
static amxd_dm_t dm;
static amxo_parser_t parser;
static amxc_var_t args;
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxc_var_t ret;

static void clean_variables() {
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static void common_end_test() {
    handle_events();
    clean_variables();
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    amxd_object_t* ipd = NULL;
    amxd_object_t* ipd_config = NULL;
    int ret_value = 0;
    amxd_trans_t transaction;
    cstring_t prefix;
    amxc_string_t upload_cfg_key;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser,
                        amxb_get_fd(bus_ctx),
                        connection_read,
                        "dummy:/tmp/dummy.sock",
                        AMXO_BUS,
                        bus_ctx);
    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxo_resolver_ftab_add(&parser,
                                            "IPDiagnostics.UploadDiagnostics",
                                            AMXO_FUNC(_UploadDiagnostics)
                                            ), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, mock_odl_defs, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    // empty signal handler for 'amxp_slot_connect'
    amxp_sigmngr_add_signal(NULL, "connection-deleted");

    assert_int_equal(_ip_diagnostics_main(AMXO_START, &dm, &parser), 0);

    handle_events();

    prefix = ipd_get_prefix();
    amxc_string_init(&upload_cfg_key, 1);
    amxc_string_appendf(&upload_cfg_key, "%sUploadConfig", prefix);
    ipd = amxd_object_get(root_obj, "IPDiagnostics");
    ipd_config = amxd_object_get(ipd, amxc_string_get(&upload_cfg_key, 0));
    amxc_string_clean(&upload_cfg_key);
    if(ipd_config == NULL) {
        ret_value = 1;
        assert_int_equal(ret_value, 0);
    }

    ret_value = amxd_trans_init(&transaction);
    assert_int_equal(ret_value, 0);

    ret_value = amxd_trans_select_object(&transaction, ipd_config);
    assert_int_equal(ret_value, 0);

    amxd_trans_set_value(cstring_t, &transaction, UP_CFG_KEY_UPLOAD_URL, "127.0.0.1");

    ret_value = amxd_trans_apply(&transaction, &dm);
    assert_int_equal(ret_value, 0);

    amxd_trans_clean(&transaction);

    return 0;
}

int test_teardown(UNUSED void** state) {
    assert_int_equal(_ip_diagnostics_main(1, &dm, &parser), 0);

    amxb_free(&bus_ctx);
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);
    clean_variables();

    test_unregister_dummy_be();
    return 0;
}

void test_int_upload_invalid_dscp(UNUSED void** state) {
    uint8_t dscp = 64;
    cstring_t uri;
    uint32_t instance_index = 0;
    amxd_status_t status;
    uint32_t num_of_up_results = 0;
    uint32_t size_of_up_results = 0;

    status = ipd_upload_diagnostics(&dm, &args, &ret, NULL, &dscp, NULL, NULL, NULL,
                                    NULL, NULL, NULL, NULL, NULL);
    uri = (cstring_t) GET_CHAR(&ret, IPD_UP_DOWN_RESULT_URI);
    instance_index = parse_instance_index_from_uri(uri);
    common_end_test();
    assert_int_equal(status, amxd_status_invalid_arg);
    assert_int_equal(instance_index, 0);

    handle_events();
    num_of_up_results = get_num_of_result_from_dm(test_type, &dm);
    size_of_up_results = size_of_results(test_type, &dm);

    assert_int_equal(num_of_up_results, 0);
    assert_int_equal(size_of_up_results, num_of_up_results);
}

void test_int_upload_invalid_eth_prio(UNUSED void** state) {
    uint8_t eth_prio = 8;
    cstring_t uri;
    uint32_t instance_index = 0;
    amxd_status_t status;
    uint32_t num_of_up_results = 0;
    uint32_t size_of_up_results = 0;

    status = ipd_upload_diagnostics(&dm, &args, &ret, NULL, NULL, &eth_prio, NULL, NULL,
                                    NULL, NULL, NULL, NULL, NULL);
    uri = (cstring_t) GET_CHAR(&ret, IPD_UP_DOWN_RESULT_URI);
    instance_index = parse_instance_index_from_uri(uri);
    common_end_test();
    assert_int_equal(status, amxd_status_invalid_arg);
    assert_int_equal(instance_index, 0);

    handle_events();
    num_of_up_results = get_num_of_result_from_dm(test_type, &dm);
    size_of_up_results = size_of_results(test_type, &dm);

    assert_int_equal(num_of_up_results, 0);
    assert_int_equal(size_of_up_results, num_of_up_results);
}

void test_int_upload_invalid_test_duration(UNUSED void** state) {
    uint16_t test_duration = 1000;
    cstring_t uri;
    uint32_t instance_index = 0;
    amxd_status_t status;
    uint32_t num_of_up_results = 0;
    uint32_t size_of_up_results = 0;

    status = ipd_upload_diagnostics(&dm, &args, &ret, NULL, NULL, NULL, NULL, &test_duration,
                                    NULL, NULL, NULL, NULL, NULL);
    uri = (cstring_t) GET_CHAR(&ret, IPD_UP_DOWN_RESULT_URI);
    instance_index = parse_instance_index_from_uri(uri);
    common_end_test();
    assert_int_equal(status, amxd_status_invalid_arg);
    assert_int_equal(instance_index, 0);

    handle_events();
    num_of_up_results = get_num_of_result_from_dm(test_type, &dm);
    size_of_up_results = size_of_results(test_type, &dm);

    assert_int_equal(num_of_up_results, 0);
    assert_int_equal(size_of_up_results, num_of_up_results);
}

void test_int_upload_invalid_test_interval(UNUSED void** state) {
    uint16_t test_interval = 1000;
    cstring_t uri;
    uint32_t instance_index = 0;
    amxd_status_t status;
    uint32_t num_of_up_results = 0;
    uint32_t size_of_up_results = 0;

    status = ipd_upload_diagnostics(&dm, &args, &ret, NULL, NULL, NULL, NULL, NULL,
                                    &test_interval, NULL, NULL, NULL, NULL);
    uri = (cstring_t) GET_CHAR(&ret, IPD_UP_DOWN_RESULT_URI);
    instance_index = parse_instance_index_from_uri(uri);
    common_end_test();
    assert_int_equal(status, amxd_status_invalid_arg);
    assert_int_equal(instance_index, 0);

    handle_events();
    num_of_up_results = get_num_of_result_from_dm(test_type, &dm);
    size_of_up_results = size_of_results(test_type, &dm);

    assert_int_equal(num_of_up_results, 0);
    assert_int_equal(size_of_up_results, num_of_up_results);
}

void test_int_upload_invalid_protocol(UNUSED void** state) {
    const cstring_t protocol = "NOT_VALID";
    cstring_t uri;
    uint32_t instance_index = 0;
    amxd_status_t status;
    uint32_t num_of_up_results = 0;
    uint32_t size_of_up_results = 0;

    status = ipd_upload_diagnostics(&dm, &args, &ret, NULL, NULL, NULL, NULL, NULL,
                                    NULL, NULL, (cstring_t) protocol, NULL, NULL);
    uri = (cstring_t) GET_CHAR(&ret, IPD_UP_DOWN_RESULT_URI);
    instance_index = parse_instance_index_from_uri(uri);
    common_end_test();
    assert_int_equal(status, amxd_status_invalid_arg);
    assert_int_equal(instance_index, 0);

    handle_events();
    num_of_up_results = get_num_of_result_from_dm(test_type, &dm);
    size_of_up_results = size_of_results(test_type, &dm);

    assert_int_equal(num_of_up_results, 0);
    assert_int_equal(size_of_up_results, num_of_up_results);
}

void test_int_upload_not_existing_server(UNUSED void** state) {
    const cstring_t upload_url = "192.168.168.100";
    uint64_t file_length = 1000000000;
    int32_t fd = -1;
    cstring_t uri;
    uint32_t instance_index = 0;
    cstring_t str_status = NULL;
    uint32_t num_of_up_results = 0;
    uint32_t size_of_up_results = 0;
    uint32_t number_incr_results = 0;
    uint32_t number_connections_report = 0;
    amxc_string_t* result_file = NULL;
    amxc_string_t* script_file = NULL;
    int result_file_exists = 0;
    int script_file_exists = 0;
    amxd_status_t status;

    status = ipd_upload_diagnostics(&dm, &args, &ret, (cstring_t) upload_url, NULL, NULL, &file_length,
                                    NULL, NULL, NULL, NULL, NULL, NULL);
    uri = (cstring_t) GET_CHAR(&ret, IPD_UP_DOWN_RESULT_URI);
    instance_index = parse_instance_index_from_uri(uri);
    assert_int_equal(status, amxd_status_ok);
    assert_int_equal(instance_index, 1);

    amxc_string_new(&result_file, 0);
    amxc_string_new(&script_file, 0);
    amxc_string_appendf(result_file, "/tmp/iperf-upload-result-%d", instance_index);
    amxc_string_appendf(script_file, "/tmp/iperf-upload-%d.sh", instance_index);
    fd = get_test_fd_from_dm(instance_index, test_type, &dm);

    str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    while(strcmp(str_status, UP_DOWN_STATUS_NOT_COMPLETE) == 0) {
        ipd_upload_check(fd, &instance_index);
        handle_events();
        str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    }
    num_of_up_results = get_num_of_result_from_dm(test_type, &dm);
    size_of_up_results = size_of_results(test_type, &dm);
    number_incr_results = get_num_incr_result_from_dm(instance_index, test_type, &dm);
    number_connections_report = get_num_connection_from_dm(instance_index, test_type, &dm);

    common_end_test();

    result_file_exists = access(amxc_string_get(result_file, 0), F_OK);
    script_file_exists = access(amxc_string_get(script_file, 0), F_OK);
    amxc_string_delete(&result_file);
    amxc_string_delete(&script_file);
    assert_int_not_equal(result_file_exists, 0);
    assert_int_not_equal(script_file_exists, 0);

    assert_string_equal(str_status, UP_DOWN_STATUS_ERR_OTHER);
    assert_int_equal(num_of_up_results, 1);
    assert_int_equal(size_of_up_results, num_of_up_results);
    assert_int_equal(number_incr_results, 0);
    assert_int_equal(number_connections_report, 0);
}

void test_int_upload_file_based(UNUSED void** state) {
    uint64_t file_length = 1000000000;
    int32_t fd = -1;
    cstring_t uri;
    uint32_t instance_index = 0;
    cstring_t str_status = NULL;
    uint32_t num_of_up_results = 0;
    uint32_t size_of_up_results = 0;
    uint32_t number_incr_results = 0;
    uint32_t number_connections_report = 0;
    amxc_string_t* result_file = NULL;
    amxc_string_t* script_file = NULL;
    int result_file_exists = 0;
    int script_file_exists = 0;
    amxd_status_t status;

    status = ipd_upload_diagnostics(&dm, &args, &ret, NULL, NULL, NULL, &file_length, NULL, NULL,
                                    NULL, NULL, NULL, NULL);

    uri = (cstring_t) GET_CHAR(&ret, IPD_UP_DOWN_RESULT_URI);
    instance_index = parse_instance_index_from_uri(uri);
    assert_int_equal(status, amxd_status_ok);
    assert_int_equal(instance_index, 2);

    amxc_string_new(&result_file, 0);
    amxc_string_new(&script_file, 0);
    amxc_string_appendf(result_file, "/tmp/iperf-upload-result-%d", instance_index);
    amxc_string_appendf(script_file, "/tmp/iperf-upload-%d.sh", instance_index);
    fd = get_test_fd_from_dm(instance_index, test_type, &dm);

    str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    while(strcmp(str_status, UP_DOWN_STATUS_NOT_COMPLETE) == 0) {
        ipd_upload_check(fd, &instance_index);
        handle_events();
        str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    }
    num_of_up_results = get_num_of_result_from_dm(test_type, &dm);
    size_of_up_results = size_of_results(test_type, &dm);
    number_incr_results = get_num_incr_result_from_dm(instance_index, test_type, &dm);
    number_connections_report = get_num_connection_from_dm(instance_index, test_type, &dm);

    common_end_test();

    result_file_exists = access(amxc_string_get(result_file, 0), F_OK);
    script_file_exists = access(amxc_string_get(script_file, 0), F_OK);
    amxc_string_delete(&result_file);
    amxc_string_delete(&script_file);
    assert_int_not_equal(result_file_exists, 0);
    assert_int_not_equal(script_file_exists, 0);

    assert_string_equal(str_status, UP_DOWN_STATUS_COMPLETE);
    assert_int_equal(num_of_up_results, 2);
    assert_int_equal(size_of_up_results, num_of_up_results);
    assert_int_equal(number_incr_results, 1);
    assert_int_equal(number_connections_report, 0);
}

void test_int_upload_file_based_mult_conn_disable(UNUSED void** state) {
    uint64_t file_length = 1000000000;
    uint32_t num_of_connections = 10;
    int32_t fd = -1;
    cstring_t uri;
    uint32_t instance_index = 0;
    cstring_t str_status = NULL;
    uint32_t num_of_up_results = 0;
    uint32_t size_of_up_results = 0;
    uint32_t number_incr_results = 0;
    uint32_t number_connections_report = 0;
    amxc_string_t* result_file = NULL;
    amxc_string_t* script_file = NULL;
    int result_file_exists = 0;
    int script_file_exists = 0;
    amxd_status_t status;

    status = ipd_upload_diagnostics(&dm, &args, &ret, NULL, NULL, NULL, &file_length, NULL, NULL,
                                    NULL, NULL, &num_of_connections, NULL);

    uri = (cstring_t) GET_CHAR(&ret, IPD_UP_DOWN_RESULT_URI);
    instance_index = parse_instance_index_from_uri(uri);
    assert_int_equal(status, amxd_status_ok);
    assert_int_equal(instance_index, 3);

    amxc_string_new(&result_file, 0);
    amxc_string_new(&script_file, 0);
    amxc_string_appendf(result_file, "/tmp/iperf-upload-result-%d", instance_index);
    amxc_string_appendf(script_file, "/tmp/iperf-upload-%d.sh", instance_index);
    fd = get_test_fd_from_dm(instance_index, test_type, &dm);

    str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    while(strcmp(str_status, UP_DOWN_STATUS_NOT_COMPLETE) == 0) {
        ipd_upload_check(fd, &instance_index);
        handle_events();
        str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    }
    num_of_up_results = get_num_of_result_from_dm(test_type, &dm);
    size_of_up_results = size_of_results(test_type, &dm);
    number_incr_results = get_num_incr_result_from_dm(instance_index, test_type, &dm);
    number_connections_report = get_num_connection_from_dm(instance_index, test_type, &dm);

    common_end_test();

    result_file_exists = access(amxc_string_get(result_file, 0), F_OK);
    script_file_exists = access(amxc_string_get(script_file, 0), F_OK);
    amxc_string_delete(&result_file);
    amxc_string_delete(&script_file);
    assert_int_not_equal(result_file_exists, 0);
    assert_int_not_equal(script_file_exists, 0);

    assert_string_equal(str_status, UP_DOWN_STATUS_COMPLETE);
    assert_int_equal(num_of_up_results, 3);
    assert_int_equal(size_of_up_results, num_of_up_results);
    assert_int_equal(number_incr_results, 1);
    assert_int_equal(number_connections_report, 0);
}

void test_int_upload_file_based_mult_conn_enable(UNUSED void** state) {
    uint64_t file_length = 1000000000;
    uint32_t num_of_connections = 10;
    bool en_per_connection = true;
    int32_t fd = -1;
    cstring_t uri;
    uint32_t instance_index = 0;
    cstring_t str_status = NULL;
    uint32_t num_of_up_results = 0;
    uint32_t size_of_up_results = 0;
    uint32_t number_incr_results = 0;
    uint32_t number_connections_report = 0;
    amxc_string_t* result_file = NULL;
    amxc_string_t* script_file = NULL;
    int result_file_exists = 0;
    int script_file_exists = 0;
    amxd_status_t status;

    status = ipd_upload_diagnostics(&dm, &args, &ret, NULL, NULL, NULL, &file_length, NULL, NULL,
                                    NULL, NULL, &num_of_connections, &en_per_connection);

    uri = (cstring_t) GET_CHAR(&ret, IPD_UP_DOWN_RESULT_URI);
    instance_index = parse_instance_index_from_uri(uri);
    assert_int_equal(status, amxd_status_ok);
    assert_int_equal(instance_index, 4);

    amxc_string_new(&result_file, 0);
    amxc_string_new(&script_file, 0);
    amxc_string_appendf(result_file, "/tmp/iperf-upload-result-%d", instance_index);
    amxc_string_appendf(script_file, "/tmp/iperf-upload-%d.sh", instance_index);
    fd = get_test_fd_from_dm(instance_index, test_type, &dm);

    str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    while(strcmp(str_status, UP_DOWN_STATUS_NOT_COMPLETE) == 0) {
        ipd_upload_check(fd, &instance_index);
        handle_events();
        str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    }
    num_of_up_results = get_num_of_result_from_dm(test_type, &dm);
    size_of_up_results = size_of_results(test_type, &dm);
    number_incr_results = get_num_incr_result_from_dm(instance_index, test_type, &dm);
    number_connections_report = get_num_connection_from_dm(instance_index, test_type, &dm);

    common_end_test();

    result_file_exists = access(amxc_string_get(result_file, 0), F_OK);
    script_file_exists = access(amxc_string_get(script_file, 0), F_OK);
    amxc_string_delete(&result_file);
    amxc_string_delete(&script_file);
    assert_int_not_equal(result_file_exists, 0);
    assert_int_not_equal(script_file_exists, 0);

    assert_string_equal(str_status, UP_DOWN_STATUS_COMPLETE);
    assert_int_equal(num_of_up_results, 4);
    assert_int_equal(size_of_up_results, num_of_up_results);
    assert_int_equal(number_incr_results, 1);
    assert_int_equal(number_connections_report, num_of_connections);
}

void test_int_upload_time_based_zero_interval(UNUSED void** state) {
    uint16_t test_duration = 4;
    int32_t fd = -1;
    cstring_t uri;
    uint32_t instance_index = 0;
    cstring_t str_status = NULL;
    uint32_t num_of_up_results = 0;
    uint32_t size_of_up_results = 0;
    uint32_t number_incr_results = 0;
    uint32_t number_connections_report = 0;
    amxc_string_t* result_file = NULL;
    amxc_string_t* script_file = NULL;
    int result_file_exists = 0;
    int script_file_exists = 0;
    amxd_status_t status;
    uint64_t test_period = 0;

    status = ipd_upload_diagnostics(&dm, &args, &ret, NULL, NULL, NULL, NULL, &test_duration, NULL,
                                    NULL, NULL, NULL, NULL);

    uri = (cstring_t) GET_CHAR(&ret, IPD_UP_DOWN_RESULT_URI);
    instance_index = parse_instance_index_from_uri(uri);
    assert_int_equal(status, amxd_status_ok);
    assert_int_equal(instance_index, 5);

    amxc_string_new(&result_file, 0);
    amxc_string_new(&script_file, 0);
    amxc_string_appendf(result_file, "/tmp/iperf-upload-result-%d", instance_index);
    amxc_string_appendf(script_file, "/tmp/iperf-upload-%d.sh", instance_index);
    fd = get_test_fd_from_dm(instance_index, test_type, &dm);

    str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    while(strcmp(str_status, UP_DOWN_STATUS_NOT_COMPLETE) == 0) {
        ipd_upload_check(fd, &instance_index);
        handle_events();
        str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    }
    num_of_up_results = get_num_of_result_from_dm(test_type, &dm);
    size_of_up_results = size_of_results(test_type, &dm);
    number_incr_results = get_num_incr_result_from_dm(instance_index, test_type, &dm);
    number_connections_report = get_num_connection_from_dm(instance_index, test_type, &dm);
    test_period = get_test_period_from_dm(instance_index, test_type, &dm);

    common_end_test();

    result_file_exists = access(amxc_string_get(result_file, 0), F_OK);
    script_file_exists = access(amxc_string_get(script_file, 0), F_OK);
    amxc_string_delete(&result_file);
    amxc_string_delete(&script_file);
    assert_int_not_equal(result_file_exists, 0);
    assert_int_not_equal(script_file_exists, 0);

    assert_string_equal(str_status, UP_DOWN_STATUS_COMPLETE);
    assert_int_equal(num_of_up_results, 5);
    assert_int_equal(size_of_up_results, num_of_up_results);
    assert_int_equal(number_incr_results, 1);
    assert_int_equal(number_connections_report, 0);
    assert_int_equal(test_period, test_duration * 1000000);
}

void test_int_upload_time_based_odd_interval(UNUSED void** state) {
    uint16_t test_duration = 4;
    uint16_t test_interval = 3;
    int32_t fd = -1;
    cstring_t uri;
    uint32_t instance_index = 0;
    cstring_t str_status = NULL;
    uint32_t num_of_up_results = 0;
    uint32_t size_of_up_results = 0;
    uint32_t number_incr_results = 0;
    uint32_t number_connections_report = 0;
    amxc_string_t* result_file = NULL;
    amxc_string_t* script_file = NULL;
    int result_file_exists = 0;
    int script_file_exists = 0;
    amxd_status_t status;
    uint64_t test_period = 0;

    status = ipd_upload_diagnostics(&dm, &args, &ret, NULL, NULL, NULL, NULL, &test_duration, &test_interval,
                                    NULL, NULL, NULL, NULL);

    uri = (cstring_t) GET_CHAR(&ret, IPD_UP_DOWN_RESULT_URI);
    instance_index = parse_instance_index_from_uri(uri);
    assert_int_equal(status, amxd_status_ok);
    assert_int_equal(instance_index, 6);

    amxc_string_new(&result_file, 0);
    amxc_string_new(&script_file, 0);
    amxc_string_appendf(result_file, "/tmp/iperf-upload-result-%d", instance_index);
    amxc_string_appendf(script_file, "/tmp/iperf-upload-%d.sh", instance_index);
    fd = get_test_fd_from_dm(instance_index, test_type, &dm);

    str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    while(strcmp(str_status, UP_DOWN_STATUS_NOT_COMPLETE) == 0) {
        ipd_upload_check(fd, &instance_index);
        handle_events();
        str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    }
    num_of_up_results = get_num_of_result_from_dm(test_type, &dm);
    size_of_up_results = size_of_results(test_type, &dm);
    number_incr_results = get_num_incr_result_from_dm(instance_index, test_type, &dm);
    number_connections_report = get_num_connection_from_dm(instance_index, test_type, &dm);
    test_period = get_test_period_from_dm(instance_index, test_type, &dm);

    common_end_test();

    result_file_exists = access(amxc_string_get(result_file, 0), F_OK);
    script_file_exists = access(amxc_string_get(script_file, 0), F_OK);
    amxc_string_delete(&result_file);
    amxc_string_delete(&script_file);
    assert_int_not_equal(result_file_exists, 0);
    assert_int_not_equal(script_file_exists, 0);

    assert_string_equal(str_status, UP_DOWN_STATUS_COMPLETE);
    assert_int_equal(num_of_up_results, 5);
    assert_int_equal(size_of_up_results, num_of_up_results);
    assert_int_equal(number_incr_results, 2);
    assert_int_equal(number_connections_report, 0);
    assert_int_equal(test_period, test_duration * 1000000);
}

void test_int_upload_time_based_even_interval(UNUSED void** state) {
    uint16_t test_duration = 4;
    uint16_t test_interval = 2;
    int32_t fd = -1;
    cstring_t uri;
    uint32_t instance_index = 0;
    cstring_t str_status = NULL;
    uint32_t num_of_up_results = 0;
    uint32_t size_of_up_results = 0;
    uint32_t number_incr_results = 0;
    uint32_t number_connections_report = 0;
    amxc_string_t* result_file = NULL;
    amxc_string_t* script_file = NULL;
    int result_file_exists = 0;
    int script_file_exists = 0;
    amxd_status_t status;
    uint64_t test_period = 0;

    status = ipd_upload_diagnostics(&dm, &args, &ret, NULL, NULL, NULL, NULL, &test_duration, &test_interval,
                                    NULL, NULL, NULL, NULL);

    uri = (cstring_t) GET_CHAR(&ret, IPD_UP_DOWN_RESULT_URI);
    instance_index = parse_instance_index_from_uri(uri);
    assert_int_equal(status, amxd_status_ok);
    assert_int_equal(instance_index, 7);

    amxc_string_new(&result_file, 0);
    amxc_string_new(&script_file, 0);
    amxc_string_appendf(result_file, "/tmp/iperf-upload-result-%d", instance_index);
    amxc_string_appendf(script_file, "/tmp/iperf-upload-%d.sh", instance_index);
    fd = get_test_fd_from_dm(instance_index, test_type, &dm);

    str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    while(strcmp(str_status, UP_DOWN_STATUS_NOT_COMPLETE) == 0) {
        ipd_upload_check(fd, &instance_index);
        handle_events();
        str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    }
    num_of_up_results = get_num_of_result_from_dm(test_type, &dm);
    size_of_up_results = size_of_results(test_type, &dm);
    number_incr_results = get_num_incr_result_from_dm(instance_index, test_type, &dm);
    number_connections_report = get_num_connection_from_dm(instance_index, test_type, &dm);
    test_period = get_test_period_from_dm(instance_index, test_type, &dm);

    common_end_test();

    result_file_exists = access(amxc_string_get(result_file, 0), F_OK);
    script_file_exists = access(amxc_string_get(script_file, 0), F_OK);
    amxc_string_delete(&result_file);
    amxc_string_delete(&script_file);
    assert_int_not_equal(result_file_exists, 0);
    assert_int_not_equal(script_file_exists, 0);

    assert_string_equal(str_status, UP_DOWN_STATUS_COMPLETE);
    assert_int_equal(num_of_up_results, 5);
    assert_int_equal(size_of_up_results, num_of_up_results);
    assert_int_equal(number_incr_results, 2);
    assert_int_equal(number_connections_report, 0);
    assert_int_equal(test_period, test_duration * 1000000);
}

void test_int_upload_time_based_mult_interval(UNUSED void** state) {
    uint16_t test_duration = 10;
    uint16_t test_interval = 2;
    int32_t fd = -1;
    cstring_t uri;
    uint32_t instance_index = 0;
    cstring_t str_status = NULL;
    uint32_t num_of_up_results = 0;
    uint32_t size_of_up_results = 0;
    uint32_t number_incr_results = 0;
    uint32_t number_connections_report = 0;
    amxc_string_t* result_file = NULL;
    amxc_string_t* script_file = NULL;
    int result_file_exists = 0;
    int script_file_exists = 0;
    amxd_status_t status;
    uint64_t test_period = 0;

    status = ipd_upload_diagnostics(&dm, &args, &ret, NULL, NULL, NULL, NULL, &test_duration, &test_interval,
                                    NULL, NULL, NULL, NULL);

    uri = (cstring_t) GET_CHAR(&ret, IPD_UP_DOWN_RESULT_URI);
    instance_index = parse_instance_index_from_uri(uri);
    assert_int_equal(status, amxd_status_ok);
    assert_int_equal(instance_index, 8);

    amxc_string_new(&result_file, 0);
    amxc_string_new(&script_file, 0);
    amxc_string_appendf(result_file, "/tmp/iperf-upload-result-%d", instance_index);
    amxc_string_appendf(script_file, "/tmp/iperf-upload-%d.sh", instance_index);
    fd = get_test_fd_from_dm(instance_index, test_type, &dm);

    str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    while(strcmp(str_status, UP_DOWN_STATUS_NOT_COMPLETE) == 0) {
        ipd_upload_check(fd, &instance_index);
        handle_events();
        str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    }
    num_of_up_results = get_num_of_result_from_dm(test_type, &dm);
    size_of_up_results = size_of_results(test_type, &dm);
    number_incr_results = get_num_incr_result_from_dm(instance_index, test_type, &dm);
    number_connections_report = get_num_connection_from_dm(instance_index, test_type, &dm);
    test_period = get_test_period_from_dm(instance_index, test_type, &dm);

    common_end_test();

    result_file_exists = access(amxc_string_get(result_file, 0), F_OK);
    script_file_exists = access(amxc_string_get(script_file, 0), F_OK);
    amxc_string_delete(&result_file);
    amxc_string_delete(&script_file);
    assert_int_not_equal(result_file_exists, 0);
    assert_int_not_equal(script_file_exists, 0);

    assert_string_equal(str_status, UP_DOWN_STATUS_COMPLETE);
    assert_int_equal(num_of_up_results, 5);
    assert_int_equal(size_of_up_results, num_of_up_results);
    assert_int_equal(number_incr_results, 5);
    assert_int_equal(number_connections_report, 0);
    assert_int_equal(test_period, test_duration * 1000000);
}

void test_int_upload_time_based_mult_conn_disable(UNUSED void** state) {
    uint16_t test_duration = 10;
    uint16_t test_interval = 2;
    uint32_t num_of_connections = 10;
    int32_t fd = -1;
    cstring_t uri;
    uint32_t instance_index = 0;
    cstring_t str_status = NULL;
    uint32_t num_of_up_results = 0;
    uint32_t size_of_up_results = 0;
    uint32_t number_incr_results = 0;
    uint32_t number_connections_report = 0;
    amxc_string_t* result_file = NULL;
    amxc_string_t* script_file = NULL;
    int result_file_exists = 0;
    int script_file_exists = 0;
    amxd_status_t status;
    uint64_t test_period = 0;

    status = ipd_upload_diagnostics(&dm, &args, &ret, NULL, NULL, NULL, NULL, &test_duration, &test_interval,
                                    NULL, NULL, &num_of_connections, NULL);

    uri = (cstring_t) GET_CHAR(&ret, IPD_UP_DOWN_RESULT_URI);
    instance_index = parse_instance_index_from_uri(uri);
    assert_int_equal(status, amxd_status_ok);
    assert_int_equal(instance_index, 9);

    amxc_string_new(&result_file, 0);
    amxc_string_new(&script_file, 0);
    amxc_string_appendf(result_file, "/tmp/iperf-upload-result-%d", instance_index);
    amxc_string_appendf(script_file, "/tmp/iperf-upload-%d.sh", instance_index);
    fd = get_test_fd_from_dm(instance_index, test_type, &dm);

    str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    while(strcmp(str_status, UP_DOWN_STATUS_NOT_COMPLETE) == 0) {
        ipd_upload_check(fd, &instance_index);
        handle_events();
        str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    }
    num_of_up_results = get_num_of_result_from_dm(test_type, &dm);
    size_of_up_results = size_of_results(test_type, &dm);
    number_incr_results = get_num_incr_result_from_dm(instance_index, test_type, &dm);
    number_connections_report = get_num_connection_from_dm(instance_index, test_type, &dm);
    test_period = get_test_period_from_dm(instance_index, test_type, &dm);

    common_end_test();

    result_file_exists = access(amxc_string_get(result_file, 0), F_OK);
    script_file_exists = access(amxc_string_get(script_file, 0), F_OK);
    amxc_string_delete(&result_file);
    amxc_string_delete(&script_file);
    assert_int_not_equal(result_file_exists, 0);
    assert_int_not_equal(script_file_exists, 0);

    assert_string_equal(str_status, UP_DOWN_STATUS_COMPLETE);
    assert_int_equal(num_of_up_results, 5);
    assert_int_equal(size_of_up_results, num_of_up_results);
    assert_int_equal(number_incr_results, 5);
    assert_int_equal(number_connections_report, 0);
    assert_int_equal(test_period, test_duration * 1000000);
}

void test_int_upload_time_based_mult_conn_enable(UNUSED void** state) {
    uint16_t test_duration = 10;
    uint16_t test_interval = 2;
    uint32_t num_of_connections = 10;
    bool en_per_connection = true;
    int32_t fd = -1;
    cstring_t uri;
    uint32_t instance_index = 0;
    cstring_t str_status = NULL;
    uint32_t num_of_up_results = 0;
    uint32_t size_of_up_results = 0;
    uint32_t number_incr_results = 0;
    uint32_t number_connections_report = 0;
    amxc_string_t* result_file = NULL;
    amxc_string_t* script_file = NULL;
    int result_file_exists = 0;
    int script_file_exists = 0;
    amxd_status_t status;
    uint64_t test_period = 0;

    status = ipd_upload_diagnostics(&dm, &args, &ret, NULL, NULL, NULL, NULL, &test_duration, &test_interval,
                                    NULL, NULL, &num_of_connections, &en_per_connection);

    uri = (cstring_t) GET_CHAR(&ret, IPD_UP_DOWN_RESULT_URI);
    instance_index = parse_instance_index_from_uri(uri);
    assert_int_equal(status, amxd_status_ok);
    assert_int_equal(instance_index, 10);

    amxc_string_new(&result_file, 0);
    amxc_string_new(&script_file, 0);
    amxc_string_appendf(result_file, "/tmp/iperf-upload-result-%d", instance_index);
    amxc_string_appendf(script_file, "/tmp/iperf-upload-%d.sh", instance_index);
    fd = get_test_fd_from_dm(instance_index, test_type, &dm);

    str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    while(strcmp(str_status, UP_DOWN_STATUS_NOT_COMPLETE) == 0) {
        ipd_upload_check(fd, &instance_index);
        handle_events();
        str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    }
    num_of_up_results = get_num_of_result_from_dm(test_type, &dm);
    size_of_up_results = size_of_results(test_type, &dm);
    number_incr_results = get_num_incr_result_from_dm(instance_index, test_type, &dm);
    number_connections_report = get_num_connection_from_dm(instance_index, test_type, &dm);
    test_period = get_test_period_from_dm(instance_index, test_type, &dm);

    common_end_test();

    result_file_exists = access(amxc_string_get(result_file, 0), F_OK);
    script_file_exists = access(amxc_string_get(script_file, 0), F_OK);
    amxc_string_delete(&result_file);
    amxc_string_delete(&script_file);
    assert_int_not_equal(result_file_exists, 0);
    assert_int_not_equal(script_file_exists, 0);

    assert_string_equal(str_status, UP_DOWN_STATUS_COMPLETE);
    assert_int_equal(num_of_up_results, 5);
    assert_int_equal(size_of_up_results, num_of_up_results);
    assert_int_equal(number_incr_results, 5);
    assert_int_equal(number_connections_report, num_of_connections);
    assert_int_equal(test_period, test_duration * 1000000);
}

void test_int_upload_time_based_error_upload_check(UNUSED void** state) {
    uint16_t test_duration = 10;
    uint16_t test_interval = 2;
    uint32_t num_of_connections = 10;
    bool en_per_connection = true;
    int32_t fd = -1;
    cstring_t uri;
    uint32_t instance_index = 0;
    cstring_t str_status = NULL;
    uint32_t num_of_up_results = 0;
    uint32_t size_of_up_results = 0;
    amxc_string_t* result_file = NULL;
    amxc_string_t* script_file = NULL;
    int result_file_exists = 0;
    int script_file_exists = 0;
    amxd_status_t status;

    status = ipd_upload_diagnostics(&dm, &args, &ret, NULL, NULL, NULL, NULL, &test_duration, &test_interval,
                                    NULL, NULL, &num_of_connections, &en_per_connection);

    uri = (cstring_t) GET_CHAR(&ret, IPD_UP_DOWN_RESULT_URI);
    instance_index = parse_instance_index_from_uri(uri);
    assert_int_equal(status, amxd_status_ok);
    assert_int_equal(instance_index, 11);

    amxc_string_new(&result_file, 0);
    amxc_string_new(&script_file, 0);
    amxc_string_appendf(result_file, "/tmp/iperf-upload-result-%d", instance_index);
    amxc_string_appendf(script_file, "/tmp/iperf-upload-%d.sh", instance_index);
    fd = get_test_fd_from_dm(instance_index, test_type, &dm);

    str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    while(strcmp(str_status, UP_DOWN_STATUS_NOT_COMPLETE) == 0) {
        sleep(5);
        ipd_upload_check(fd, NULL);
        handle_events();
        str_status = get_test_status_from_dm(instance_index, test_type, &dm);
    }
    num_of_up_results = get_num_of_result_from_dm(test_type, &dm);
    size_of_up_results = size_of_results(test_type, &dm);

    common_end_test();

    result_file_exists = access(amxc_string_get(result_file, 0), F_OK);
    script_file_exists = access(amxc_string_get(script_file, 0), F_OK);
    amxc_string_delete(&result_file);
    amxc_string_delete(&script_file);
    assert_int_not_equal(result_file_exists, 0);
    assert_int_not_equal(script_file_exists, 0);

    assert_string_equal(str_status, UP_DOWN_STATUS_ERR_OTHER);
    assert_int_equal(num_of_up_results, 5);
    assert_int_equal(size_of_up_results, num_of_up_results);
}