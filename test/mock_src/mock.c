/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <string.h>
#include <linux/netlink.h>
#include <unistd.h>
#include <stdio.h>

#include "mock.h"
#include "config.h"
#include "traceroute.h"

#define ME "ipd-mock"

static int seq[] = {-1, -1, -1, 0};
static int ind = 0;
static int once = 0;

int __wrap_amxp_subproc_astart(UNUSED amxp_subproc_t* const subproc, UNUSED amxc_array_t* cmd) {
    printf("%s ---------Matteo ------------ \r\n", __func__);
    return 23;
}

int __wrap_amxp_subproc_wait(UNUSED amxp_subproc_t* const subproc, UNUSED int timeout) {
    printf("%s ---------Matteo ------------ \r\n", __func__);
    return 1;
}

amxp_subproc_t* __wrap_amxp_subproc_find(UNUSED const int pid) {
    printf("%s ---------Matteo ------------ \r\n", __func__);
    return (amxp_subproc_t*) mock();
}

int __wrap_cfgctrlr_ipdiagnostics_execute_function(amxd_object_t* ip_diagnostics_obj,
                                                   const char* function,
                                                   amxc_var_t* data,
                                                   amxc_var_t* ret) {
    int rv = -1;
    if((strcmp(function, IPD_TRCR_TEST_CHECK) == 0)) {
        once++;
    }
    if((strcmp(function, IPD_TRCR_TEST_CHECK) == 0) && (once == 2)) {
        once = seq[ind];
        ind++;
        amxc_var_move(ret, data);
        rv = 0;
        goto exit;
    }

    when_str_empty_trace(function, exit, ERROR, "Can not execute function, empty string");
    when_null_trace(ip_diagnostics_obj, exit, ERROR, "can not execute %s, no object given", function);

    rv = cfgctrlr_execute_function(ip_diagnostics_obj, function, data, ret);
    if(rv < 0) {
        SAH_TRACEZ_ERROR(ME, "function %s failed", function);
    }

exit:
    return rv;
}