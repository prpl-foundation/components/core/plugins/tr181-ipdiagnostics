/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <yajl/yajl_gen.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxj/amxj_variant.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxb/amxb_connect.h>
#include <amxo/amxo.h>

#include "test_rpc_traceroute.h"
#include "traceroute.h"
#include "priv_traceroute.h"
#include "ip_diagnostics.h"
#include "ip_diagnostics_priv.h"
#include "rpc.h"
#include "dummy_backend.h"
#include "test_common.h"
#include "test_common_rpc.h"

static const char* odl_defs = "../../odl/tr181-ipdiagnostics_definition.odl";
static const char* mock_odl_defs = "../mock_odl/mock.odl";
static const char* mock_odl_ip = "../mock_odl/mock_ip.odl";

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxc_var_t args;
static amxc_var_t ret;

static amxc_var_t* dummy_read_json_from_file(const char* fname) {
    int fd = -1;
    variant_json_t* reader = NULL;
    amxc_var_t* data = NULL;
    // create a json reader
    if(amxj_reader_new(&reader) != 0) {
        printf("Failed to create json file reader");
        goto exit;
    }

    // open the json file
    fd = open(fname, O_RDONLY);
    if(fd == -1) {
        printf("File open file %s - error 0x%8.8X\n", fname, errno);
        goto exit;
    }

    // read the json file and parse the json text
    while(amxj_read(reader, fd) > 0) {
    }

    // get the variant
    data = amxj_reader_result(reader);

    if(data == NULL) {
        printf("Invalid JSON in file %s", fname);
    }

    close(fd);
exit:
    amxj_reader_delete(&reader);

    return data;
}

static void clean_variables() {
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static void common_end_test() {
    handle_events();
    clean_variables();
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    amxd_object_t* ipd = NULL;
    amxd_object_t* ipd_trcr = NULL;
    int ret_value = 0;
    amxd_trans_t transaction;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser,
                        amxb_get_fd(bus_ctx),
                        connection_read,
                        "dummy:/tmp/dummy.sock",
                        AMXO_BUS,
                        bus_ctx);
    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxo_resolver_ftab_add(&parser,
                                            "IPDiagnostics.TraceRoute",
                                            AMXO_FUNC(_TraceRoute)
                                            ), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser,
                                            "ipd_trcr_check_dst",
                                            AMXO_FUNC(_ipd_trcr_check_dst)
                                            ), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, mock_odl_defs, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, mock_odl_ip, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    // empty signal handler for 'amxp_slot_connect'
    amxp_sigmngr_add_signal(NULL, "connection-deleted");

    assert_int_equal(_ip_diagnostics_main(AMXO_START, &dm, &parser), 0);

    handle_events();

    ipd = amxd_object_get(root_obj, "IPDiagnostics");
    ipd_trcr = amxd_object_get(ipd, "TraceRoute");
    if(ipd_trcr == NULL) {
        ret_value = 1;
        assert_int_equal(ret_value, 0);
    }

    ret_value = amxd_trans_init(&transaction);
    assert_int_equal(ret_value, 0);

    ret_value = amxd_trans_select_object(&transaction, ipd_trcr);
    assert_int_equal(ret_value, 0);

    amxd_trans_set_value(cstring_t, &transaction, TRCR_RET_KEY_HST, "127.0.0.1");

    ret_value = amxd_trans_apply(&transaction, &dm);
    assert_int_equal(ret_value, 0);

    amxd_trans_clean(&transaction);

    return 0;
}

int test_teardown(UNUSED void** state) {
    assert_int_equal(_ip_diagnostics_main(1, &dm, &parser), 0);

    amxb_free(&bus_ctx);
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);
    clean_variables();

    test_unregister_dummy_be();
    return 0;
}

void test_trcr_invalid_arguments(UNUSED void** state) {
    uint8_t dscp_wrong = 64;
    uint8_t dscp = 10;
    cstring_t protv = "IPv4";
    cstring_t protv_wrong = "IPv2";
    cstring_t host = "127.0.0.1";
    uint32_t numberoftries_wrong = 10;
    uint32_t numberoftries = 3;
    uint32_t timeout = 1000;
    uint32_t datablocksize_wrong = 10;
    uint32_t datablocksize = 64;
    amxd_status_t status;

    status = ipd_trcr_diagnostics(&dm, &args, &ret, NULL, protv, host, &numberoftries,
                                  &timeout, &datablocksize, &dscp_wrong, NULL);
    common_end_test();
    assert_int_equal(status, amxd_status_invalid_arg);

    status = ipd_trcr_diagnostics(&dm, &args, &ret, NULL, protv_wrong, host, &numberoftries,
                                  &timeout, &datablocksize, &dscp, NULL);
    common_end_test();
    assert_int_equal(status, amxd_status_invalid_arg);

    // invalid data block size should be updated by a valid one
    status = ipd_trcr_diagnostics(&dm, &args, &ret, NULL, protv, host, &numberoftries,
                                  &timeout, &datablocksize_wrong, &dscp, NULL);
    common_end_test();
    assert_int_equal(status, amxd_status_deferred);

    status = ipd_trcr_diagnostics(&dm, &args, &ret, NULL, protv, host, &numberoftries_wrong,
                                  &timeout, &datablocksize, &dscp, NULL);
    common_end_test();
    assert_int_equal(status, amxd_status_invalid_arg);
}

void test_trcr_valid_arguments(UNUSED void** state) {
    uint8_t dscp = 10;
    cstring_t protv = "IPv4";
    cstring_t host = "google.com";
    uint32_t numberoftries = 3;
    uint32_t timeout = 1000;
    uint32_t datablocksize = 64;
    amxd_status_t status;
    cstring_t str_status = NULL;
    amxc_var_t dm_out;
    amxd_object_t* ipd_trcr_object = NULL;
    amxc_var_t* json_data = NULL;
    int32_t fd = -1;
    int res_comp = 0;

    amxc_var_init(&dm_out);
    amxc_var_set_type(&dm_out, AMXC_VAR_ID_HTABLE);

    status = ipd_trcr_diagnostics(&dm, &args, &ret, NULL, protv, host, &numberoftries,
                                  &timeout, &datablocksize, &dscp, NULL);
    assert_int_equal(status, amxd_status_deferred);
    fd = get_trcr_test_fd_from_dm(&dm);

    str_status = get_trcr_test_status_from_dm(&dm);
    trcr_copy_result_file(__func__);
    while(strcmp(str_status, TRCR_STATUS_NOT_COMPLETE) == 0) {
        ipd_trcr_check(fd, NULL);
        handle_events();
        str_status = get_trcr_test_status_from_dm(&dm);
    }

    common_end_test();

    ipd_trcr_object = amxd_dm_findf(&dm, "IPDiagnostics.TraceRoute.");
    assert_non_null(ipd_trcr_object);
    ipd_obj_data_to_var(ipd_trcr_object, NULL, &dm_out);

    assert_string_equal(str_status, TRCR_STATUS_COMPLETE);
    assert_int_equal(GET_INT32(&dm_out, TRCR_RET_KEY_MHC), 30);
    assert_int_equal(GET_INT32(&dm_out, TRCR_RET_KEY_RHNOE), 10);

    json_data = dummy_read_json_from_file("test_data/traceroute/test_valid_arguments.json");

    amxc_var_compare(GET_ARG(json_data, "RouteHops"), GET_ARG(&dm_out, "RouteHops"), &res_comp);
    assert_int_equal(res_comp, 0);

    amxc_var_delete(&json_data);
    amxc_var_clean(&dm_out);
}

void test_trcr_max_hop(UNUSED void** state) {
    uint8_t dscp = 10;
    cstring_t protv = "IPv4";
    cstring_t host = "google.com";
    uint32_t numberoftries = 3;
    uint32_t timeout = 1000;
    uint32_t datablocksize = 64;
    uint32_t maxhopcount = 9;
    amxd_status_t status;
    cstring_t str_status = NULL;
    amxc_var_t dm_out;
    amxd_object_t* ipd_trcr_object = NULL;
    int32_t fd = -1;

    amxc_var_init(&dm_out);
    amxc_var_set_type(&dm_out, AMXC_VAR_ID_HTABLE);

    status = ipd_trcr_diagnostics(&dm, &args, &ret, NULL, protv, host, &numberoftries,
                                  &timeout, &datablocksize, &dscp, &maxhopcount);
    assert_int_equal(status, amxd_status_deferred);
    fd = get_trcr_test_fd_from_dm(&dm);

    str_status = get_trcr_test_status_from_dm(&dm);
    trcr_copy_result_file(__func__);
    while(strcmp(str_status, TRCR_STATUS_NOT_COMPLETE) == 0) {
        ipd_trcr_check(fd, NULL);
        handle_events();
        str_status = get_trcr_test_status_from_dm(&dm);
    }

    common_end_test();

    ipd_trcr_object = amxd_dm_findf(&dm, "IPDiagnostics.TraceRoute.");
    assert_non_null(ipd_trcr_object);
    ipd_obj_data_to_var(ipd_trcr_object, NULL, &dm_out);

    assert_string_equal(str_status, TRCR_STATUS_ERR_MAX_HOP_COUNT);
    assert_int_equal(GET_INT32(&dm_out, TRCR_RET_KEY_MHC), 9);
    assert_int_equal(GET_INT32(&dm_out, TRCR_RET_KEY_RHNOE), 9);

    amxc_var_clean(&dm_out);
}

void test_trcr_launch_new_test(UNUSED void** state) {
    uint8_t dscp = 10;
    cstring_t protv = "IPv4";
    cstring_t host = "google.com";
    uint32_t numberoftries = 3;
    uint32_t timeout = 1000;
    uint32_t datablocksize = 64;
    uint32_t maxhopcount = 10;
    int ret_value = 0;
    amxd_status_t status;
    cstring_t str_status = NULL;
    amxc_var_t dm_out;
    amxd_object_t* ipd_trcr_object = NULL;
    int32_t fd = -1;
    amxd_trans_t transaction;

    amxc_var_init(&dm_out);
    amxc_var_set_type(&dm_out, AMXC_VAR_ID_HTABLE);

    // Launch first test
    status = ipd_trcr_diagnostics(&dm, &args, &ret, NULL, protv, host, &numberoftries,
                                  &timeout, &datablocksize, &dscp, &maxhopcount);
    assert_int_equal(status, amxd_status_deferred);
    fd = get_trcr_test_fd_from_dm(&dm);

    str_status = get_trcr_test_status_from_dm(&dm);
    trcr_copy_result_file("test_trcr_valid_arguments");
    assert_string_equal(str_status, TRCR_STATUS_NOT_COMPLETE);

    handle_events();

    ipd_trcr_object = amxd_dm_findf(&dm, "IPDiagnostics.TraceRoute.");
    assert_non_null(ipd_trcr_object);

    // Cancel the test
    ret_value = amxd_trans_init(&transaction);
    assert_int_equal(ret_value, 0);

    ret_value = amxd_trans_select_object(&transaction, ipd_trcr_object);
    assert_int_equal(ret_value, 0);

    amxd_trans_set_value(cstring_t, &transaction, TRCR_RET_KEY_DST, TRCR_STATUS_CANCELED);

    ret_value = amxd_trans_apply(&transaction, &dm);
    assert_int_equal(ret_value, 0);

    amxd_trans_clean(&transaction);

    handle_events();

    // Launch second test
    status = ipd_trcr_diagnostics(&dm, &args, &ret, NULL, protv, host, &numberoftries,
                                  &timeout, &datablocksize, &dscp, &maxhopcount);
    assert_int_equal(status, amxd_status_deferred);

    fd = get_trcr_test_fd_from_dm(&dm);
    str_status = get_trcr_test_status_from_dm(&dm);
    trcr_copy_result_file(__func__);
    while(strcmp(str_status, TRCR_STATUS_NOT_COMPLETE) == 0) {
        ipd_trcr_check(fd, NULL);
        handle_events();
        str_status = get_trcr_test_status_from_dm(&dm);
    }

    ipd_obj_data_to_var(ipd_trcr_object, NULL, &dm_out);

    common_end_test();

    assert_string_equal(str_status, TRCR_STATUS_COMPLETE);
    assert_int_equal(GET_INT32(&dm_out, TRCR_RET_KEY_MHC), 10);
    assert_int_equal(GET_INT32(&dm_out, TRCR_RET_KEY_RHNOE), 10);

    amxc_var_clean(&dm_out);
}

void test_trcr_ipv6(UNUSED void** state) {
    uint8_t dscp = 10;
    cstring_t protv = "IPv6";
    cstring_t host = "fe80::7ce1:9306:ade7:3bb3";
    cstring_t intf = "IP.Interface.3.";
    uint32_t numberoftries = 3;
    uint32_t timeout = 1000;
    uint32_t datablocksize = 72;
    uint32_t maxhopcount = 10;
    amxd_status_t status;
    cstring_t str_status = NULL;
    amxc_var_t dm_out;
    amxc_var_t* json_data = NULL;
    amxd_object_t* ipd_trcr_object = NULL;
    int32_t fd = -1;
    int res_comp = 0;

    amxc_var_init(&dm_out);
    amxc_var_set_type(&dm_out, AMXC_VAR_ID_HTABLE);

    status = ipd_trcr_diagnostics(&dm, &args, &ret, intf, protv, host, &numberoftries,
                                  &timeout, &datablocksize, &dscp, &maxhopcount);
    assert_int_equal(status, amxd_status_deferred);
    fd = get_trcr_test_fd_from_dm(&dm);

    str_status = get_trcr_test_status_from_dm(&dm);
    trcr_copy_result_file(__func__);
    while(strcmp(str_status, TRCR_STATUS_NOT_COMPLETE) == 0) {
        ipd_trcr_check(fd, NULL);
        handle_events();
        str_status = get_trcr_test_status_from_dm(&dm);
    }

    common_end_test();

    ipd_trcr_object = amxd_dm_findf(&dm, "IPDiagnostics.TraceRoute.");
    assert_non_null(ipd_trcr_object);
    ipd_obj_data_to_var(ipd_trcr_object, NULL, &dm_out);

    assert_string_equal(str_status, TRCR_STATUS_COMPLETE);
    assert_int_equal(GET_INT32(&dm_out, TRCR_RET_KEY_MHC), 10);
    assert_int_equal(GET_INT32(&dm_out, TRCR_RET_KEY_RHNOE), 1);

    json_data = dummy_read_json_from_file("test_data/traceroute/test_trcr_ipv6.json");

    amxc_var_compare(GET_ARG(json_data, "RouteHops"), GET_ARG(&dm_out, "RouteHops"), &res_comp);
    assert_int_equal(res_comp, 0);

    amxc_var_delete(&json_data);
    amxc_var_clean(&dm_out);
}