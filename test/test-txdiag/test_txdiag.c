/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_util.h>
#include <amxut/amxut_sahtrace.h>

#include "tx_diag.h"
#include "test_txdiag.h"
#include "ip_diagnostics_priv.h"

static const char* odl_defs = "../../odl/tr181-ipdiagnostics_definition.odl";

static void init_plugin(void) {
    amxut_sahtrace_setup(NULL);
    assert_int_equal(amxo_parser_parse_file(amxut_bus_parser(), odl_defs, amxd_dm_get_root(amxut_bus_dm())), 0);
    _ip_diagnostics_main(AMXO_START, amxut_bus_dm(), amxut_bus_parser());
}

static void stop_plugin(void) {
    _ip_diagnostics_main(AMXO_STOP, amxut_bus_dm(), amxut_bus_parser());
    amxut_sahtrace_teardown(NULL);
}

void test_txdiag_test(UNUSED void** state) {
    init_plugin();
    amxd_object_t* downdiag_obj = amxd_dm_findf(amxut_bus_dm(), "%s", DM_DOWNDIAG_PATH);
    amxd_object_t* updiag_obj = amxd_dm_findf(amxut_bus_dm(), "%s", DM_UPDIAG_PATH);

    assert_non_null(downdiag_obj);
    assert_non_null(updiag_obj);

    txdiag_requested(downdiag_obj, tx_download);
    txdiag_canceled(downdiag_obj, tx_download);
    txdiag_requested(updiag_obj, tx_upload);
    txdiag_canceled(updiag_obj, tx_upload);

    amxd_object_set_cstring_t(downdiag_obj, "DownloadURL", "http://127.0.0.1");
    txdiag_requested(downdiag_obj, tx_download);
    txdiag_canceled(downdiag_obj, tx_download);

    amxd_object_set_bool(downdiag_obj, "EnablePerConnectionResult", true);
    txdiag_requested(downdiag_obj, tx_download);
    txdiag_canceled(downdiag_obj, tx_download);

    amxd_object_set_cstring_t(updiag_obj, "UploadURL", "http://127.0.0.1/upload_content");
    amxd_object_set_uint32_t(updiag_obj, "TestFileLength", 40000);
    txdiag_requested(updiag_obj, tx_upload);
    txdiag_canceled(updiag_obj, tx_upload);
    stop_plugin();
}

void test_txdiag_result(UNUSED void** state) {
    init_plugin();
    amxd_object_t* downdiag_obj = amxd_dm_findf(amxut_bus_dm(), "%s", DM_DOWNDIAG_PATH);
    amxd_object_t* updiag_obj = amxd_dm_findf(amxut_bus_dm(), "%s", DM_UPDIAG_PATH);
    amxc_var_t* data = amxut_util_read_json_from_file("test-data/download_result_ok.json");
    amxc_ts_t time_now;

    assert_non_null(downdiag_obj);
    assert_non_null(data);

    amxc_ts_now(&time_now);
    amxc_var_add_key(amxc_ts_t, data, "BOMTime", &time_now);
    amxc_var_add_key(amxc_ts_t, data, "ROMTime", &time_now);
    amxc_var_add_key(amxc_ts_t, data, "EOMTime", &time_now);
    amxc_var_add_key(amxc_ts_t, data, "TCPOpenRequestTime", &time_now);
    amxc_var_add_key(amxc_ts_t, data, "TCPOpenResponseTime", &time_now);
    txdiag_requested(downdiag_obj, tx_download);
    amxut_bus_handle_events();
    downdiag_ended("", data, NULL);
    amxut_bus_handle_events();
    assert_int_equal(GET_UINT32(amxd_object_get_param_value(downdiag_obj, "TestBytesReceived"), NULL), 4000);
    txdiag_requested(downdiag_obj, tx_download);
    amxut_bus_handle_events();
    assert_int_equal(GET_UINT32(amxd_object_get_param_value(downdiag_obj, "TestBytesReceived"), NULL), 0);
    amxc_var_set_uint32_t(GET_ARG(data, "index"), 1);
    downdiag_ended("", data, NULL);
    amxut_bus_handle_events();
    downdiag_all_ended("", data, NULL);
    amxut_bus_handle_events();
    assert_int_equal(GET_UINT32(amxd_object_get_param_value(downdiag_obj, "TestBytesReceived"), NULL), 4000);
    amxc_var_delete(&data);
    data = amxut_util_read_json_from_file("test-data/upload_result_ok.json");
    amxc_var_add_key(amxc_ts_t, data, "BOMTime", &time_now);
    amxc_var_add_key(amxc_ts_t, data, "ROMTime", &time_now);
    amxc_var_add_key(amxc_ts_t, data, "EOMTime", &time_now);
    amxc_var_add_key(amxc_ts_t, data, "TCPOpenRequestTime", &time_now);
    amxc_var_add_key(amxc_ts_t, data, "TCPOpenResponseTime", &time_now);
    txdiag_requested(updiag_obj, tx_upload);
    amxut_bus_handle_events();
    updiag_ended("", data, NULL);
    amxut_bus_handle_events();
    assert_int_equal(GET_UINT32(amxd_object_get_param_value(updiag_obj, "TestBytesSent"), NULL), 4000);
    txdiag_requested(updiag_obj, tx_upload);
    amxut_bus_handle_events();
    assert_int_equal(GET_UINT32(amxd_object_get_param_value(updiag_obj, "TestBytesSent"), NULL), 0);
    amxc_var_set_uint32_t(GET_ARG(data, "index"), 1);
    updiag_ended("", data, NULL);
    amxut_bus_handle_events();
    amxc_var_set_uint32_t(GET_ARG(data, "index"), 2);
    updiag_ended("", data, NULL);
    amxut_bus_handle_events();
    updiag_all_ended("", data, NULL);
    amxut_bus_handle_events();
    assert_int_equal(GET_UINT32(amxd_object_get_param_value(updiag_obj, "TestBytesSent"), NULL), 8000);
    amxc_var_delete(&data);
    stop_plugin();
}
