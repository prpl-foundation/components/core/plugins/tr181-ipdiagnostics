/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxb/amxb_connect.h>
#include <amxo/amxo.h>

#include "test_unit_test.h"
#include "ip_diagnostics.h"
#include "ip_diagnostics_priv.h"
#include "priv_upload_download.h"
#include "rpc.h"
#include "upload_download.h"
#include "upload.h"
#include "dummy_backend.h"
#include "test_common.h"

static const char* odl_defs = "../../odl/tr181-ipdiagnostics_definition.odl";
static const char* mock_odl_defs = "../mock_odl/mock.odl";

// static ipd_up_down_test_t test_type = ipd_up_down_upload;
static amxd_dm_t dm;
static amxo_parser_t parser;
static amxc_var_t args;
static amxb_bus_ctx_t* bus_ctx = NULL;
static amxc_var_t ret;

static void clean_variables() {
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    amxd_object_t* ipd = NULL;
    amxd_object_t* ipd_config = NULL;
    int ret_value = 0;
    amxd_trans_t transaction;
    cstring_t prefix;
    amxc_string_t upload_cfg_key;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser,
                        amxb_get_fd(bus_ctx),
                        connection_read,
                        "dummy:/tmp/dummy.sock",
                        AMXO_BUS,
                        bus_ctx);
    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxo_resolver_ftab_add(&parser,
                                            "IPDiagnostics.UploadDiagnostics",
                                            AMXO_FUNC(_UploadDiagnostics)
                                            ), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, mock_odl_defs, root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    // empty signal handler for 'amxp_slot_connect'
    amxp_sigmngr_add_signal(NULL, "connection-deleted");

    assert_int_equal(_ip_diagnostics_main(AMXO_START, &dm, &parser), 0);

    handle_events();

    prefix = ipd_get_prefix();
    amxc_string_init(&upload_cfg_key, 1);
    amxc_string_appendf(&upload_cfg_key, "%sUploadConfig", prefix);
    ipd = amxd_object_get(root_obj, "IPDiagnostics");
    ipd_config = amxd_object_get(ipd, amxc_string_get(&upload_cfg_key, 0));
    amxc_string_clean(&upload_cfg_key);
    if(ipd_config == NULL) {
        ret_value = 1;
        assert_int_equal(ret_value, 0);
    }

    ret_value = amxd_trans_init(&transaction);
    assert_int_equal(ret_value, 0);

    ret_value = amxd_trans_select_object(&transaction, ipd_config);
    assert_int_equal(ret_value, 0);

    amxd_trans_set_value(cstring_t, &transaction, UP_CFG_KEY_UPLOAD_URL, "127.0.0.1");

    ret_value = amxd_trans_apply(&transaction, &dm);
    assert_int_equal(ret_value, 0);

    amxd_trans_clean(&transaction);

    return 0;
}

int test_teardown(UNUSED void** state) {
    assert_int_equal(_ip_diagnostics_main(1, &dm, &parser), 0);

    amxb_free(&bus_ctx);
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);
    clean_variables();

    test_unregister_dummy_be();
    return 0;
}

void test_ipd_search_llist(UNUSED void** state) {
    amxc_llist_t* list = NULL;
    bool str_found;
    bool str_not_found;

    amxc_llist_new(&list);

    amxc_llist_add_string(list, "Messi");
    amxc_llist_add_string(list, "Neymar");
    amxc_llist_add_string(list, "Ronaldo");

    str_found = ipd_search_llist("Messi", list);
    str_not_found = ipd_search_llist("Ribery", list);

    amxc_llist_delete(&list, amxc_string_list_it_free);

    assert_true(str_found);
    assert_false(str_not_found);
}

void test_ipd_obj_data_to_var(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    amxd_object_t* ipd = NULL;
    amxc_llist_t* filter_list = NULL;
    amxc_var_t* var;
    amxc_var_t* var_upload_cfg;
    amxc_string_t upload_cfg_key;
    uint32_t max_incr_result = 0;
    uint32_t num_of_conn = 0;
    cstring_t prefix;

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    ipd = amxd_object_get(root_obj, "IPDiagnostics");
    assert_non_null(ipd);

    amxc_var_new(&var);
    amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);

    amxc_llist_new(&filter_list);
    amxc_llist_add_string(filter_list, "UploadDiagnosticsMaxIncrementalResult");

    prefix = ipd_get_prefix();
    amxc_string_init(&upload_cfg_key, 1);
    amxc_string_appendf(&upload_cfg_key, "%sUploadConfig", prefix);

    ipd_obj_data_to_var(ipd, filter_list, var);

    var_upload_cfg = GET_ARG(var, amxc_string_get(&upload_cfg_key, 0));
    max_incr_result = GET_UINT32(var, "UploadDiagnosticsMaxIncrementalResult");
    num_of_conn = GET_UINT32(var_upload_cfg, "NumberOfConnections");

    amxc_var_delete(&var);
    amxc_llist_delete(&filter_list, amxc_string_list_it_free);
    amxc_string_clean(&upload_cfg_key);

    assert_int_equal(max_incr_result, 0);
    assert_int_equal(num_of_conn, 1);
}

void test_ipd_var_to_dm(UNUSED void** state) {
    amxd_trans_t* transaction;
    amxd_object_t* root_obj = NULL;
    amxd_object_t* ipd = NULL;
    amxd_object_t* ipd_config = NULL;
    amxc_llist_t* filter_list = NULL;
    amxc_var_t* var;
    amxc_var_t* cfg_param;
    amxc_string_t upload_cfg_key;
    cstring_t prefix;

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    ipd = amxd_object_get(root_obj, "IPDiagnostics");
    assert_non_null(ipd);

    transaction = ipd_transaction_create(ipd);
    assert_non_null(transaction);

    amxc_var_new(&var);
    amxc_var_set_type(var, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, var, UP_DOWN_CFG_KEY_RESULT_NUMBER, 5);
    amxc_var_add_key(uint8_t, var, UP_DOWN_CFG_KEY_DSCP, 49);

    amxc_llist_new(&filter_list);
    amxc_llist_add_string(filter_list, UP_DOWN_CFG_KEY_RESULT_NUMBER);

    prefix = ipd_get_prefix();
    amxc_string_init(&upload_cfg_key, 1);
    amxc_string_appendf(&upload_cfg_key, "%sUploadConfig", prefix);

    ipd_var_to_dm(transaction, amxc_string_get(&upload_cfg_key, 0), var, filter_list);

    //apply transaction
    ipd_transaction_apply(transaction);

    ipd_config = amxd_object_get(ipd, amxc_string_get(&upload_cfg_key, 0));

    amxc_var_delete(&var);
    amxc_llist_delete(&filter_list, amxc_string_list_it_free);
    amxc_string_clean(&upload_cfg_key);

    cfg_param = (amxc_var_t*) amxd_object_get_param_value(ipd_config, UP_DOWN_CFG_KEY_RESULT_NUMBER);
    assert_int_equal(GET_UINT32(cfg_param, NULL), 0);
    cfg_param = (amxc_var_t*) amxd_object_get_param_value(ipd_config, UP_DOWN_CFG_KEY_DSCP);
    assert_int_equal(GET_UINT32(cfg_param, NULL), 49);
}

void test_ipd_fetch_interface_ip(UNUSED void** state) {
    amxc_var_t* var_ip;
    cstring_t ip = NULL;
    bool is_ip_valid = false;

    var_ip = ipd_fetch_interface_ip(NULL, NULL);
    ip = (cstring_t) GET_CHAR(var_ip, NULL);
    if(strcmp(ip, "127.0.0.1") == 0) {
        is_ip_valid = true;
    }
    amxc_var_delete(&var_ip);

    assert_true(is_ip_valid);
}

void test_ipd_validate_cfg_params(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;
    amxd_object_t* ipd = NULL;
    amxd_object_t* ipd_config = NULL;
    amxc_var_t* args;
    amxc_var_t* mod_args_cfg;
    amxc_string_t upload_cfg_key;
    cstring_t prefix;
    int rv;

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    ipd = amxd_object_get(root_obj, "IPDiagnostics");
    assert_non_null(ipd);

    prefix = ipd_get_prefix();
    amxc_string_init(&upload_cfg_key, 1);
    amxc_string_appendf(&upload_cfg_key, "%sUploadConfig", prefix);

    ipd_config = amxd_object_get(ipd, amxc_string_get(&upload_cfg_key, 0));
    amxc_string_clean(&upload_cfg_key);
    assert_non_null(ipd_config);

    amxc_var_new(&args);
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_new(&mod_args_cfg);

    amxc_var_add_key(cstring_t, args, UP_DOWN_CFG_KEY_INTERFACE, "TEST");
    amxc_var_add_key(cstring_t, args, UP_CFG_KEY_UPLOAD_URL, "127.0.0.1");
    amxc_var_add_key(uint8_t, args, UP_DOWN_CFG_KEY_DSCP, 49);
    amxc_var_add_key(uint8_t, args, UP_DOWN_CFG_KEY_ETH_PRIORITY, 7);

    rv = ipd_validate_cfg_params(args, ipd_config, mod_args_cfg, "IPDiagnostics");
    amxc_var_delete(&args);
    amxc_var_delete(&mod_args_cfg);
    assert_int_equal(rv, 0);

    amxc_var_new(&args);
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_new(&mod_args_cfg);

    amxc_var_add_key(cstring_t, args, UP_DOWN_CFG_KEY_INTERFACE, "TEST");
    amxc_var_add_key(cstring_t, args, UP_CFG_KEY_UPLOAD_URL, "127.0.0.1");
    amxc_var_add_key(uint8_t, args, UP_DOWN_CFG_KEY_DSCP, 49);
    amxc_var_add_key(uint8_t, args, UP_DOWN_CFG_KEY_ETH_PRIORITY, 8);

    rv = ipd_validate_cfg_params(args, ipd_config, mod_args_cfg, "IPDiagnostics");
    amxc_var_delete(&args);
    amxc_var_delete(&mod_args_cfg);
    assert_int_not_equal(rv, 0);
}
