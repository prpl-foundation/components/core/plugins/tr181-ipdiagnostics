MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
PING_SRCDIR = $(realpath ../../src/ipping)
TRCR_SRCDIR = $(realpath ../../src/traceroute)
UPDOWN_SRCDIR = $(realpath ../../src/download_upload)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR += $(realpath ../../include_priv)
# MOCK_SRCDIR
COMMON_SRCDIR += $(realpath ../common/)
# MOCK_INCDIR = $(realpath ../common/)
INCDIR += $(realpath ../common_includes/)

HEADERS = $(wildcard $(addsuffix /*.h,$(INCDIR)))
SOURCES = $(wildcard $(SRCDIR)/*.c)
SOURCES += $(wildcard $(COMMON_SRCDIR)/*.c)
SOURCES += $(wildcard $(MOCK_SRCDIR)/*.c)
SOURCES += $(wildcard $(PING_SRCDIR)/*.c)
SOURCES += $(wildcard $(UPDOWN_SRCDIR)/*.c)
SOURCES += $(wildcard $(TRCR_SRCDIR)/*.c)

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
		  -fkeep-inline-functions -fkeep-static-functions \
		  -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread \
		  -D_GNU_SOURCE -DIP_DIAGNOSTICS_TEST

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) \
		   -lamxc -lamxp -lamxd -lamxo -lamxm -lnetmodel \
		   -lsahtrace -ldl -lpthread -lcrypt -lamxj